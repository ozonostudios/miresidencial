<?php
App::uses('AppModel', 'Model');
App::uses('Payment', 'Model');
App::uses('Charge', 'Model');

/**
 * Expense Model
 *
 * @property ExpenseType $ExpenseType
 * @property ServiceProvider $ServiceProvider
 */
class Expense extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'expense_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'payment_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'service_provider_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'amount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'expense_details' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'ExpenseType' => array(
            'className' => 'ExpenseType',
            'foreignKey' => 'expense_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'PaymentType' => array(
            'className' => 'PaymentType',
            'foreignKey' => 'payment_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'ServiceProvider' => array(
            'className' => 'ServiceProvider',
            'foreignKey' => 'service_provider_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * [auditData get data for dashboard audit box]
 * @param  [type] $neighborhood_id [description]
 * @return [type]                  [description]
 */
    public function auditData($neighborhood_id) {

        $this->recursive = 0;
        $expenses = $this->find('all', array(
            'fields' => array('COUNT(*) as total'),
            'conditions' => array(
                'Expense.neighborhood_id' => $neighborhood_id
            ),
        ));

        $expenses_with_image = $this->find('all', array(
            'fields' => array('COUNT(*) as total'),
            'conditions' => array(
                'Expense.neighborhood_id' => $neighborhood_id,
                'Expense.image IS NOT NULL',
            ),
        ));

        $audit['total'] = $expenses[0][0]['total'];
        $audit['with_image'] = $expenses_with_image[0][0]['total'];

        return $audit;
    }

    /**
     * [getChartData gets chart data from database]
     * @param  [Number] $neighborhood_id [session stored data]
     * @return [Array]                  [resultset]
     */
    public function getChartData($neighborhood_id) {

        $results = [];
        $data = [];

        $expenses= $this->find('all', array(
            'fields' => array(
                'SUM(Expense.amount) as total',
                'ExpenseType.expense_type'
            ),
            'conditions' => array(
                'and' => array(
                    'YEAR(Expense.created)' => date('Y'),
                ),
                'Expense.neighborhood_id' => $neighborhood_id
            ),
            'group' => array(
                'expense_type_id'
            ),
            'recursive' => 1
        ));

        foreach ($expenses as $value) {
            $results['labels'][] = $value['ExpenseType']['expense_type'];
            $data[] = $value[0]['total'];
        }

        $results['data'][] = $data;
        $results['series'][] = 'Gastos';

        return $results;
    }
    /**
     * [monthExpenses gets all expenses on the 
     * requested month. Current month as default
     * if month parameter is null]
     * @param  [Number] $month [Number of month]
     * @return [Number]        [Payments amount]
     */
    public function monthExpenses($neighborhood_id) {

        $expenses = $this->find('all', array(
            'fields' => array('SUM(Expense.amount) as total'),
            'conditions' => array(
                'and' => array(
                    'YEAR(Expense.created)' => date('Y'),
                    'MONTH(Expense.created) ' => date('n'),
                ),
                'Expense.neighborhood_id' => $neighborhood_id
            ),
            'recursive' => 1
        ));

        return (int) $expenses[0][0]['total'];
    }
}
