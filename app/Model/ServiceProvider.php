<?php
App::uses('AppModel', 'Model');
/**
 * ServiceProvider Model
 *
 * @property Expense $Expense
 */
class ServiceProvider extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'service_provider';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'service_provider' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'show_info' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Expense' => array(
			'className' => 'Expense',
			'foreignKey' => 'service_provider_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    /**
     * [asyncSearch executed from the quick search box]
     * @param  [String] $value [text for search]
     * @return [Array]        [resultset]
     */
    public function asyncSearch($value, $neighborhood_id) {

        $results = $this->find('list',array(
            'fields' => array('id', 'service_provider'),
            'conditions' => array(
                'or' => array(
                    'ServiceProvider.service_provider LIKE "%'.$value.'%"'
                )
            ),
            'recursive' => 1
        ));

        return $results;
    }
}
