<?php
App::uses('AppModel', 'Model');
/**
 * Inventory Model
 *
 * @property Expense $Expense
 */
class Inventory extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'inventories';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'product';
/**
 * [$virtualFields field used to concatenate
 * first and last name on async search]
 * @var array
 */
    public $virtualFields = array(
        'full_description' => "CONCAT(Inventory.product, ' ', Inventory.brand, ' - Modelo: ', Inventory.model, ' - Serie: ', Inventory.serial)"
    );
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'expense_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'product' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'brand' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'model' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'serial' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Expense' => array(
			'className' => 'Expense',
			'foreignKey' => 'expense_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Status' => array(
			'className' => 'Status',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

    /**
     * [asyncSearch executed from the quick search box]
     * @param  [String] $value [text for search]
     * @return [Array]        [resultset]
     */
    public function asyncSearch($value, $neighborhood_id) {

        $results = $this->find('list',array(
            'fields' => array('id', 'full_description'),
            'conditions' => array(
                'or' => array(
                    'Inventory.product LIKE "%'.$value.'%"',
                    'Inventory.brand LIKE "%'.$value.'%"',
                    'Inventory.model LIKE "%'.$value.'%"',
                    'Inventory.serial LIKE "%'.$value.'%"'
                )
            ),
            'recursive' => 1
        ));

        return $results;
    }	
}
