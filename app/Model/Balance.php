<?php
App::uses('AppModel', 'Model');
App::uses('Charge', 'Model');
App::uses('Payment', 'Model');
App::uses('Expense', 'Model');
/**
 * Balance Model
 *
 * @property Home $Home
 */
class Balance extends AppModel {

	public $useTable = false;

	 /**
     * [balanceChartData gets all payments, expenses and payments
     * per month
     * @param  [Number] $neighborhood_id [Neighborhood Id]
     * @return [Number]        [Payments amount]
     */
    public function balanceChartData($neighborhood_id) {

        $resultset = [];
        $paymentModel = new Payment();
        $chargeModel = new Charge();
        $expenseModel = new Expense();

        $payments = $paymentModel->find('all', array(
            'fields' => array(
                'SUM(Payment.amount) as total',
                'MONTH(Payment.created) as month'
            ),          
            'conditions' => array(
                'and' => array(
                    'YEAR(Payment.created)' => date('Y'),
                    'Home.neighborhood_id' => $neighborhood_id
                ),
                'Payment.payment_type_id <> 6',
            ),
            'group' => array('MONTH(Payment.created)'),
            'recursive' => 1
        ));

        $expenses = $expenseModel->find('all', array(
            'fields' => array(
                'SUM(Expense.amount) as total',
                'MONTH(Expense.created) as month'
            ),          
            'conditions' => array(
                'and' => array(
                    'YEAR(Expense.created)' => date('Y'),
                    'neighborhood_id' => $neighborhood_id
                )
            ),
            'group' => array('MONTH(Expense.created)'),
            'recursive' => 1
        ));

        $charges = $chargeModel->find('all', array(
            'fields' => array(
                'SUM(Charge.amount) as total',
                'MONTH(Charge.created) as month'
            ),          
            'conditions' => array(
                'and' => array(
                    'YEAR(Charge.created)' => date('Y'),
                    'Home.neighborhood_id' => $neighborhood_id
                )
            ),
            'group' => array('MONTH(Charge.created)'),
            'recursive' => 1
        ));
        /**
         * Format expense row to be readable by the
         * chart
         */
        foreach ($charges as $charge) {

            $results['labels'][] = $this->months[(int) $charge[0]['month']];
            $charges_series[] = (int) $charge[0]['total'];
        } 
        /**
         * Format expense row to be readable by the
         * chart
         */
        foreach ($expenses as $expense) {

            $results['labels'][] = $this->months[(int) $expense[0]['month']];
            $expenses_series[] = (int) $expense[0]['total'];
        } 
        /**
         * Format expense row to be readable by the
         * chart
         */
        foreach ($payments as $payment) {

            $results['labels'][] = $this->months[(int) $payment[0]['month']];
            $income_series[] = (int) $payment[0]['total'];
        }
        
        $results['data'][] = $expenses_series;
        $results['data'][] = $income_series;
        $results['data'][] = $charges_series;
        $results['series'] = array("Gastos", "Ingresos", "Cargos");

        return $results;
    }
    /**
     * [monthBalanceData returns the data charges vs payments]
     * @return [Array] [Array of charges and payments of the month]
     */
    public function monthBalanceData($neighborhood_id) {
        $paymentModel = new Payment();
        $income = $paymentModel->monthPayments($neighborhood_id);

        $expenseModel = new Expense();
        $expenses = $expenseModel->monthExpenses($neighborhood_id);

        return array(
            'income' => $income,
            'expenses' => $expenses
        );
    }
    /**
     * [monthBalanceChartData prepares data for month
     * balance chart on dashboard]
     * @return [Array] [Charges vs Payments]
     */
    public function monthBalanceChartData($neighborhood_id) {

        $chargeModel = new Charge();
        $paymentModel = new Payment();

        $totalPayments = $paymentModel->monthPayments($neighborhood_id);
        $totalCharges = $chargeModel->monthCharges($neighborhood_id);

        $monthChart['labels'] = array('Pagado', 'Cargos');
        $monthChart['data'] = array($totalPayments, $totalCharges);

        return $monthChart;
    }
    /**
     * [monthBalanceChartData prepares data for month
     * balance chart on dashboard]
     * @return [Array] [Charges vs Payments]
     */
    public function chargesPaymentsChartData($neighborhood_id) {

        $chargeModel = new Charge();
        $paymentModel = new Payment();

        $monthChart['labels'] = array('Pagado', 'Pendiente');
        $monthChart['data'] = array($chargeModel->totalCharges($neighborhood_id), $paymentModel->totalPayments($neighborhood_id));

        return $monthChart;
    }
    /**
     * [totalBalanceData prepares data for month
     * total balance table on dashboard]
     * @return [Array] [Grouped by Payment type]
     */
    public function totalBalanceData($neighborhood_id) {

        $expenseModel = new Expense();
        $expenses = $expenseModel->find('all', array(
            'fields' => array('PaymentType.is_cash', 'SUM(Expense.amount) as total'),
            'conditions' => array(
                'neighborhood_id' => $neighborhood_id
            ),
            'group' => array(
                'is_cash'
            ),
            'orderby' => array(
                'is_cash'
            ),
            'recursive'=> 1
        ));

        $paymentModel = new Payment();
        $payments = $paymentModel->find('all', array(
            'fields' => array('PaymentType.is_cash', 'SUM(Payment.amount) as total'),
            'conditions' => array(
                'Home.neighborhood_id' => $neighborhood_id
            ),
            'group' => array(
                'is_cash'
            ),
            'orderby' => array(
                'is_cash'
            ),
            'recursive'=> 1
        ));

        $total['electronic'] = $payments[0][0]['total']-$expenses[0][0]['total'];
        $total['cash'] = $payments[1][0]['total']-$expenses[1][0]['total'];

        return $total;
    }    
}
