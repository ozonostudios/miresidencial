<?php
App::uses('AppModel', 'Model');
/**
 * Log Model
 *
 * @property Home $Home
 */
class Log extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'log';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'home_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'log' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Home' => array(
			'className' => 'Home',
			'foreignKey' => 'home_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
