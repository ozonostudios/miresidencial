<?php
App::uses('AppModel', 'Model');
App::uses('Home', 'Model');
App::uses('Neighborhood', 'Model');
/**
 * Charge Model
 *
 * @property ChargeType $ChargeType
 * @property Home $Home
 */
class Charge extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'amount';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'charge_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'home_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'amount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'charge_details' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'ChargeType' => array(
            'className' => 'ChargeType',
            'foreignKey' => 'charge_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Home' => array(
            'className' => 'Home',
            'foreignKey' => 'home_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    /**
     * [getChartData gets chart data from database]
     * @param  [Number] $neighborhood_id [session stored data]
     * @return [Array]                  [resultset]
     */
    public function getChartData($neighborhood_id) {

        $results = [];
        $data = [];

        $charges = $this->find('all', array(
            'fields' => array(
                'SUM(Charge.amount) as total',
                'ChargeType.charge_type'
            ),
            'conditions' => array(
                'and' => array(
                    'YEAR(Charge.created)' => date('Y'),
                ),
                'Home.neighborhood_id' => $neighborhood_id
            ),
            'group' => array(
                'charge_type_id'
            ),
            'recursive' => 1
        ));

        foreach ($charges as $value) {
            $results['labels'][] = $value['ChargeType']['charge_type'];
            $data[] = $value[0]['total'];
        }

        $results['data'][] = $data;
        $results['series'][] = 'Cargos';

        return $results;
    }
    /**
     * [monthCharges gets all charges on the 
     * requested month. Current month as default
     * if month parameter is null]
     * @param  [Number] $month [Number of month]
     * @return [Number]        [Charges amount]
     */
    public function monthCharges($neighborhood_id) {

        $neighborhood = new Neighborhood();
        $neighborhood->recursive = 0;
        $neighborhood_data = $neighborhood->findById($neighborhood_id);

        $period_start = (int) $neighborhood_data['Neighborhood']['period_start'];

        if(date('d') < $period_start) {

            $start_month = date('m', strtotime('-1 month'));
            $start_year = date('Y', strtotime('-1 month'));
            $end_month = date('m', strtotime('now'));
            $end_year = date('Y', strtotime('now'));
        } else {

            $start_month = date('m', strtotime('now'));
            $start_year = date('Y', strtotime('now'));
            $end_month = date('m', strtotime('+1 month'));
            $end_year = date('Y', strtotime('+1 month'));
        }

        $charges = $this->find('all', array(
            'fields' => array('SUM(Charge.amount) as total'),
            'conditions' => array(
                'and' => array(
                    'Charge.created >="'.$start_year.'-'.$start_month.'-'.$period_start.'"',
                    'Charge.created <="'.$end_year.'-'.$end_month.'-'.$period_start.'"'
                ),
                'Home.neighborhood_id' => $neighborhood_id
            ),
            'recursive' => 1
        ));

        return (int) $charges[0][0]['total'];
    }
    /**
     * [monthCharges gets all charges on the 
     * requested month. Current month as default
     * if month parameter is null]
     * @param  [Number] $month [Number of month]
     * @return [Number]        [Charges amount]
     */
    public function totalCharges($neighborhood_id) {

        $charges = $this->find('all', array(
            'fields' => array('SUM(Charge.amount) as total'),
            'conditions' => array(
                'Home.neighborhood_id' => $neighborhood_id
            ),
            'recursive' => 1
        ));

        return (int) $charges[0][0]['total'];
    }
    /**
     * [applyCharges apply charges to all homes on the requested
     * neighborhood]
     * @param  [Number] $neighborhood_id [Neighborhood id]
     * @return [Boolean]                  [confirmation of the application of charges]
     */
    public function applyCharges($neighborhood_id) {
        /**
         * [$home get list of homes for the requested neighborhood]
         * @var Home
         */
        $home = new Home();
        $home->recursive = 0;
        $homes = $home->find('all', array(
            'fields' => array(
                'Home.id',
                'Home.address',
                'Resident.first_name',
                'Resident.email'
            ),
            'conditions' => array(
                'Home.neighborhood_id' => $neighborhood_id
            )
        ));
        /**
         * [$neighborhood get monthly fee from this neighborhood]
         * @var Neighborhood
         */
        $neighborhood = new Neighborhood();
        $neighborhood->recursive = 0;
        $neighborhood_data = $neighborhood->find('all', array(
            'fields' => array(
                'Neighborhood.monthly_fee',
                'Neighborhood.neighborhood'
            ),
            'conditions' => array(
                'Neighborhood.id' => $neighborhood_id
            )
        ));
        /**
         * [$amount monthly fee amount]
         * @var [type]
         */
        $amount = $neighborhood_data[0]['Neighborhood']['monthly_fee'];
        /**
         * [$amount monthly fee amount]
         * @var [type]
         */
        $neighbohood_name = $neighborhood_data[0]['Neighborhood']['neighborhood'];
        /**
         * [$charges will store the charges to be applied]
         * @var array
         */
        $charges = [];
        /**
         * [$emails stores all email addresses from the residents of the
         * requested neighborhood]
         * @var string
         */
        $emails = "";
        /**
         * Apply charge for each home
         */
        foreach ($homes as $home) {
            
            $charges[] = [
                'charge_type_id' => 1,
                'home_id' => $home['Home']['id'],
                'amount' => $amount,
                'charge_details' => 'Mensualidad de '.date('F')
            ];

            $emails .= $home['Resident']['email'].',';

        }
        
        /**
         * If charge it's applied successfully, send an email to the resident
         */
        if($this->saveMany($charges)) {

            /**
             * If there's a valid email, send an email with the amount
             * of the payment
             */
            $subject = 'MiResidencial.mx '.$neighbohood_name.' - Notifiación de Cobro';
            
            $message = 
            '<html><body>'.
                '<h2>Cargo de Mensualidad</h2>'.
                '<p>Estimado(a) residente de '.$neighbohood_name.', Hemos registrado un cargo por <b>$'.number_format($amount).'</b> a la cuenta de su residencia.</b></p>'.
                '<p>El concepto es <b>Mensualidad de '.date('F').'</b></p>'.
                '<h3>Usted ya puede revisar sus estados de cuenta y realizar sus pagos en <a href="http://www.miresidencial.mx/mi-cuenta" target="_parent">http://www.miresidencial.mx/mi-cuenta</a></h3>'.
            '</body></html>';

            if($emails != '') {

                mail(rtrim($emails, ','), $subject, $message, $this->mail_headers );
            }
        }

        return 1;
    }
}
