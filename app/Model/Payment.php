<?php
App::uses('AppModel', 'Model');
App::uses('Charge', 'Model');
App::uses('Expense', 'Model');
/**
 * Payment Model
 *
 * @property PaymentType $PaymentType
 * @property Home $Home
 */
class Payment extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'amount';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'payment_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'home_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'amount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'bonification_approved' => array(
            'Boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'payment_details' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'PaymentType' => array(
            'className' => 'PaymentType',
            'foreignKey' => 'payment_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Home' => array(
            'className' => 'Home',
            'foreignKey' => 'home_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * [getChartData gets chart data from database]
     * @param  [Number] $neighborhood_id [session stored data]
     * @return [Array]                  [resultset]
     */
    public function getChartData($neighborhood_id) {

        $results = [];
        $data = [];

        $payments = $this->find('all', array(
            'fields' => array(
                'SUM(Payment.amount) as total',
                'PaymentType.payment_type'
            ),
            'conditions' => array(
                'and' => array(
                    'YEAR(Payment.created)' => date('Y'),
                ),
                'Home.neighborhood_id' => $neighborhood_id,
                'Payment.payment_type_id <> 6',
            ),
            'group' => array(
                'payment_type_id'
            ),
            'recursive' => 1
        ));

        foreach ($payments as $value) {
            $results['labels'][] = $value['PaymentType']['payment_type'];
            $data[] = $value[0]['total'];
        }

        $results['data'][] = $data;
        $results['series'][] = 'Pagos';

        return $results;
    }

    public function getBonifications($neighborhood_id) {

        $bonifications = $this->find('all', array(
            'fields' => array(
                'Payment.id',
                'Payment.amount',
                'Payment.payment_details',
                'Payment.created',
                'Home.street',
                'Home.number'
            ),            
            'conditions' => array(
                'Payment.payment_type_id = 6',
                'Home.neighborhood_id' => $neighborhood_id,
                'Payment.bonification_approved' => false
            ),
            'recursive' => 1
        ));

        return $bonifications;
    }
    /**
     * [monthPayments gets all payments on the 
     * requested month. Current month as default
     * if month parameter is null]
     * @param  [Number] $month [Number of month]
     * @return [Number]        [Payments amount]
     */
    public function monthPayments($neighborhood_id) {

        $payments = $this->find('all', array(
            'fields' => array('SUM(Payment.amount) as total'),
            'conditions' => array(
                'and' => array(
                    'YEAR(Payment.created)' => date('Y'),
                    'MONTH(Payment.created) ' => date('n'),
                ),
                'Home.neighborhood_id' => $neighborhood_id,
                'Payment.payment_type_id <> 6',                
            ),
            'recursive' => 1
        ));

        return (int) $payments[0][0]['total'];
    }
    /**
     * [monthPayments gets all payments on the 
     * requested month. Current month as default
     * if month parameter is null]
     * @param  [Number] $month [Number of month]
     * @return [Number]        [Payments amount]
     */
    public function totalPayments($neighborhood_id) {

        $payments = $this->find('all', array(
            'fields' => array('SUM(Payment.amount) as total'),
            'conditions' => array(
                'Home.neighborhood_id' => $neighborhood_id,
                'Payment.payment_type_id <> 6',                
            ),
            'recursive' => 1
        ));

        return (int) $payments[0][0]['total'];
    }
}
