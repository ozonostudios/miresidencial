<?php
App::uses('AppModel', 'Model');
/**
 * ChargeType Model
 *
 * @property Charge $Charge
 */
class ChargeType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'charge_type';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Charge' => array(
			'className' => 'Charge',
			'foreignKey' => 'charge_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
