<?php
App::uses('AppModel', 'Model');
/**
 * Home Model
 *
 * @property Resident $Resident
 * @property HomeType $HomeType
 * @property Neighborhood $Neighborhood
 * @property Charge $Charge
 * @property FacilitiesRental $FacilitiesRental
 * @property Log $Log
 * @property Payment $Payment
 */
class Home extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'address';
/**
 * [$virtualFields field used to concatenate
 * first and last name on async search]
 * @var array
 */
    public $virtualFields = array(
        'address' => "CONCAT(Home.street, ' ', Home.number)"
    );
/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'street' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'number' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'resident_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'home_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'neighborhood_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Resident' => array(
            'className' => 'Resident',
            'foreignKey' => 'resident_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'HomeType' => array(
            'className' => 'HomeType',
            'foreignKey' => 'home_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Neighborhood' => array(
            'className' => 'Neighborhood',
            'foreignKey' => 'neighborhood_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Charge' => array(
            'className' => 'Charge',
            'foreignKey' => 'home_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'FacilitiesRental' => array(
            'className' => 'FacilitiesRental',
            'foreignKey' => 'home_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Log' => array(
            'className' => 'Log',
            'foreignKey' => 'home_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Payment' => array(
            'className' => 'Payment',
            'foreignKey' => 'home_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * [homeStatusCheck defines the status of the home
     * depending of the history of payments]
     * @param  [Array] $homes [list of homes]
     * @return [Array]        [homes with status]
     */
    public function homeStatusCheck($home) {
        /**
         * [$default_check calculate minimum payment
         * paid in the last X a months to be considered
         * under agreement]
         * @var [Number]
         */
        $default_check = $home['Neighborhood']['payments_before_default'] * $home['Neighborhood']['monthly_fee'];
        /**
         * [$amount_for_agreement amount that has to be 
         * paid to be 'under agreement']
         * @var [Number]
         */
        $amount_for_agreement = $home['Neighborhood']['payment_on_agreement'] * $home['Neighborhood']['monthly_fee'];
        /**
         * [$paid_for_agreement amount paid on the amount of
         * time needed]
         * @var [Number]
         */
        $paid_for_agreement = 0;
        /**
         * Get total Payments
         */
        $payments=0;
        
        foreach ($home['Payment'] as $payment) {

            $payments += $payment['amount'];

            /**
             * [$start_date get start date, x amount of
             * months before today, depending on what it's 
             * configured on the DB]
             * @var [Date]
             */
            $start_date = strtotime(date('Y', strtotime('-'.$home['Neighborhood']['payment_on_agreement'].' month')).'-'.date('m', strtotime('-'.$home['Neighborhood']['payment_on_agreement'].' month')).'-'.$home['Neighborhood']['period_start']);

            /**
             * If a payment was made after the minimum date
             * for agreement, sum it to check for agreement status
             */
            if(strtotime($payment['created'])>$start_date) {

                $paid_for_agreement += $payment['amount'];
            }
        }
        $home['Home']['payments'] = $payments;
        /**
         * Get total Charges
         */
        $charges=0;

        foreach ($home['Charge'] as $charge) {

            $charges += $charge['amount'];
        }
        $home['Home']['charges'] = $charges;
        /**
         * Calculate Debt
         */
        $home['Home']['debt'] = $payments-$charges;

        /**
         * Get the home status
         */
        
        /**
         * If debt it's equals or greater than
         * zero, home it's up to date
         */
        if($home['Home']['debt'] >= 0) {

            $home['Home']['status'] = 'Normal';
            $home['Home']['status_color'] = 'text-success';
        } 

        /**
         * If not, it can be behind or under agreement
         */
        else {

            if($paid_for_agreement > $amount_for_agreement){

                $home['Home']['status'] = 'En Convenio';
                $home['Home']['status_color'] = 'text-info';
            } else {
                
                $home['Home']['status'] = 'Irregular';
                $home['Home']['status_color'] = 'text-danger';
            }
        }
        /**
         * Return home data
         * with all the information
         */
        return $home;
    }
    /**
     * [homeChartData data for home page charts]
     * @return [Array] [resultset]
     */
    public function homeChartData() {
        /**
         * [$data get data from the DB]
         * @var [Array]
         */
        $data = $this->find('all', array(
            'fields' => array(
                'HomeType.home_type',
                'home_type_id',
                'COUNT(Home.home_type_id) AS total'
            ),
            'group' => 'Home.home_type_id',
            'recursive' => 0
        ));
        /**
         * [$results formatted array for front end
         * processing]
         * @var array
         */
        $results = array();

        foreach ($data as $home_type) {

            $results['labels'][] = $home_type['HomeType']['home_type'];
            $results['data'][] = $home_type[0]['total'];
        }

        return $results;
    }
    /**
     * [asyncSearch executed from the quick search box]
     * @param  [String] $value [text for search]
     * @return [Array]        [resultset]
     */
    public function asyncSearch($value, $neighborhood_id) {

        $results = $this->find('list',array(
            'fields' => array('id', 'address'),
            'conditions' => array(
                'or' => array(
                    'Home.street LIKE "%'.$value.'%"',
                )
            ),
            'recursive' => 1
        ));

        return $results;
    }
}
