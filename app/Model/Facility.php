<?php
App::uses('AppModel', 'Model');
/**
 * Facility Model
 *
 * @property Neighborhood $Neighborhood
 */
class Facility extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'facility';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'neighborhood_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'facility' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'rental_cost' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Neighborhood' => array(
            'className' => 'Neighborhood',
            'foreignKey' => 'neighborhood_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    /**
     * [getAllFacilities returns a list of all
     * Facilities for the requested
     * neighborhood]
     * @return [Array] [list of Facilities]
     */
    public function getAllFacilities() {

        $facilities = $this->find('all', array(
            'fields' => array(
                'Facility.id',
                'Facility.facility',
                'Facility.area_info',
                'Facility.latitude',
                'Facility.longitude',
            ),
            'recursive' => 0,
            'order' => array('Facility.facility DESC'),
            'conditions' => array(
                'neighborhood_id' => 1
            )
        ));

        $results = [];

        foreach ($facilities as $facility) {
            $results[] = Array(
                'id' => $facility['Facility']['id'],
                'facility' => $facility['Facility']['facility'],
                'latitude' => $facility['Facility']['latitude'],
                'longitude' => $facility['Facility']['longitude'],
            );
        }

        return $results;
    }
    /**
     * [asyncSearch executed from the quick search box]
     * @param  [String] $value [text for search]
     * @return [Array]        [resultset]
     */
    public function asyncSearch($value, $neighborhood_id) {

        $results = $this->find('list',array(
            'fields' => array('id', 'facility'),
            'conditions' => array(
                'or' => array(
                    'Facility.facility LIKE "%'.$value.'%"',
                )
            ),
            'recursive' => 1
        ));

        return $results;
    }    
}