<?php
App::uses('AppModel', 'Model');
/**
 * Permission Model
 *
 * @property GroupPermission $GroupPermission
 */
class Permission extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'permission';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'permission' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'GroupPermission' => array(
			'className' => 'GroupPermission',
			'foreignKey' => 'permission_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
