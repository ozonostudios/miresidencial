<?php
App::uses('AppModel', 'Model');
/**
 * ResidentType Model
 *
 * @property Resident $Resident
 */
class ResidentType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'resident_type';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Resident' => array(
			'className' => 'Resident',
			'foreignKey' => 'resident_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
