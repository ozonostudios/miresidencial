<?php
App::uses('AppModel', 'Model');
/**
 * FacilitiesRental Model
 *
 * @property Facility $Facility
 * @property Home $Home
 */
class FacilitiesRental extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'facilities_rental';

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'from';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'facility_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'home_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Facility' => array(
            'className' => 'Facility',
            'foreignKey' => 'facility_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Home' => array(
            'className' => 'Home',
            'foreignKey' => 'home_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * [checkAvailability checks for availability in all
     * facilities on the provided date]
     * @param  [type] $date [date to search]
     * @return [type]       [array of facilities not available]
     */
    public function checkAvailability($id) {

        $neighborhood_id = $_SESSION['Neighborhood']['id'];

        $taken_days = $this->find('all', array(
            'fields' => array(
                'FacilitiesRental.id',
                'FacilitiesRental.facility_id',
                'FacilitiesRental.from',
                'FacilitiesRental.to',
                'FacilitiesRental.paid',
                'Home.street',
                'Home.number',
            ),
            'conditions' => array(
                'Facility.neighborhood_id' => $neighborhood_id,
                'Facility.id' => $id
            )
        ));

        $days = [];

        foreach ($taken_days as $day) {
            
            $days[] = array(
                'title' => $day['Home']['street'].' '.$day['Home']['number'],
                'allDay' => false,
                'startTime' => array(
                    date('d', strtotime($day['FacilitiesRental']['from'])),
                    date('m', strtotime($day['FacilitiesRental']['from'])),
                    date('Y', strtotime($day['FacilitiesRental']['from']))
                ),
                'endTime' => array(
                    date('d', strtotime($day['FacilitiesRental']['to'])),
                    date('m', strtotime($day['FacilitiesRental']['to'])),
                    date('Y', strtotime($day['FacilitiesRental']['to']))
                ),
            );
        }

        return $days;
    }   
}
