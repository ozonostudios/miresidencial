<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    public $months = Array(
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
    );

    public $neighborhood_id;

    public $mail_headers;

    public function beforeFilter() {
        /**
         * Initialize email headers
         */
        $this->mail_headers .= "From: Finanzas - MiResidencial.mx <finanzas@miresidencial.mx>"."\r\n";
        $this->mail_headers .= "Reply-To: finanzas@miresidencial.mx"."\r\n";
        $this->mail_headers .= "MIME-Version: 1.0"."\r\n";
        $this->mail_headers .= "Content-Type: text/html; charset=ISO-8859-1"."\r\n";
    }

/**
 * Process the Upload
 * @param array $check
 * @return boolean
 */
    public function processUpload($check=array()) {

        // deal with uploaded file
        if (!empty($check['picture_path']['tmp_name'])) {

            // check file is uploaded
            if (!is_uploaded_file($check['picture_path']['tmp_name'])) {

                return FALSE;
            }

            $filenameTimestamp = time();

            // build full filename
            $filename = WWW_ROOT . $this->uploadDir . DS . $filenameTimestamp .'.'.pathinfo($check['picture_path']['name'], PATHINFO_EXTENSION);

            // try moving file
            if (!move_uploaded_file($check['picture_path']['tmp_name'], $filename)) {

                return FALSE;
            // file successfully uploaded
            } else {

                // save the file path relative from WWW_ROOT e.g. uploads/example_filename.jpg
                $this->data[$this->alias]['picture_path'] = str_replace(DS, "/", str_replace(WWW_ROOT, "", $filename) );
            }
        }

        return TRUE;
    }

/**
 * Before Validation
 * @param array $options
 * @return boolean
 */
    public function beforeValidate($options = array()) {

        if (!empty($this->data[$this->alias]['picture_path']['error']) && 
        	$this->data[$this->alias]['picture_path']['error']==4 && 
        	$this->data[$this->alias]['picture_path']['size']==0) {

            unset($this->data[$this->alias]['picture_path']);
        }

        parent::beforeValidate($options);
    }
}