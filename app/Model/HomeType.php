<?php
App::uses('AppModel', 'Model');
/**
 * HomeType Model
 *
 * @property Home $Home
 */
class HomeType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'home_type';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Home' => array(
			'className' => 'Home',
			'foreignKey' => 'home_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
