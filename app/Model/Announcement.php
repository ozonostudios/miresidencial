<?php
App::uses('AppModel', 'Model');
/**
 * Announcement Model
 *
 * @property Neighborhoods $Neighborhoods
 */
class Announcement extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'announcement';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'neighborhoods_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'announcement_types_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'announcement' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'title' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'created' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'modified' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Neighborhoods' => array(
            'className' => 'Neighborhoods',
            'foreignKey' => 'neighborhoods_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AnnouncementType' => array(
            'className' => 'AnnouncementType',
            'foreignKey' => 'announcement_types_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    /**
     * [getAllAnnouncements returns a list of all
     * announcements for the requested
     * neighborhood]
     * @return [Array] [list of announcements]
     */
    public function getAllAnnouncements($neighborhood_id) {

        $announcements = $this->find('all', array(
            'fields' => array(
                'Announcement.title',
                'Announcement.announcement',
                'AnnouncementType.class',
                'Announcement.created'
            ),
            'limit' => 6,
            'page' => 1,
            'recursive' => 0,
            'order' => array('Announcement.id DESC'),
            'conditions' => array(
                'neighborhoods_id' => $neighborhood_id
            )
        ));

        $results = [];

        foreach ($announcements as $announcement) {
            $results[] = array(
                'title' => $announcement['Announcement']['title'],
                'announcement' => $announcement['Announcement']['announcement'],
                'date' => date('d-m-Y',strtotime($announcement['Announcement']['created'])),
                'class' => $announcement['AnnouncementType']['class']
            );
        }

        return $results;
    }   
}
