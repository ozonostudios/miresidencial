<?php
App::uses('AppModel', 'Model');
/**
 * Resident Model
 *
 * @property ResidentType $ResidentType
 * @property Home $Home
 */
class Resident extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';
/**
 * [$virtualFields field used to concatenate
 * first and last name on async search]
 * @var array
 */
    public $virtualFields = array(
        'name' => "CONCAT(Resident.first_name, ' ', Resident.last_name)"
    );
/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'first_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'last_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'resident_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'show_info' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

    // The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'ResidentType' => array(
            'className' => 'ResidentType',
            'foreignKey' => 'resident_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Home' => array(
            'className' => 'Home',
            'foreignKey' => 'resident_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    /**
     * [asyncSearch executed from the quick search box]
     * @param  [String] $value [text for search]
     * @return [Array]        [resultset]
     */
    public function asyncSearch($value, $neighborhood_id) {

        $results = $this->find('list',array(
            'fields' => array('id', 'name'),
            'conditions' => array(
                'or' => array(
                    'Resident.first_name LIKE "%'.$value.'%"',
                    'Resident.last_name LIKE "%'.$value.'%"'
                )
            ),
            'recursive' => 1
        ));

        return $results;
    }

}
