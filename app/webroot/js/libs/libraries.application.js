angular
    .module('miResidencialApp', [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'chart.js',
        'ui.bootstrap',
        'ngFileUpload',
        'ngImgCrop',
        'ui.rCalendar',
        'angular-parallax',
        
        'miResidencialApp.controllers',
        'miResidencialApp.services',
        'miResidencialApp.directives',
    ]);

angular.module('miResidencialApp.controllers', []);
angular.module('miResidencialApp.services', []);
angular.module('miResidencialApp.directives', []);;angular.module('miResidencialApp.controllers')
    .controller('AnnouncementsController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {
        
        'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
        },
    ]);;angular.module('miResidencialApp.controllers')
    .controller('ChargesController', [
        '$scope',
        '$filter',
        '$uibModal',
        'DataProvider',
        'AlertsService',
        function ($scope,  $filter, $uibModal, DataProvider, AlertsService) {

            'use strict';
            
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content year chart]
        	 * @type {Boolean}
        	 */
            $scope.isCollapsed = true;

            /**
             * [modalInstance current modal window instance holder]
             * @type {Object}
             */
            $scope.modalInstance = {};
            /**
             * [hideReminder hide reminder when charges have been sent]
             * @type {Boolean}
             */
            $scope.hideReminder = false;
            /**
             * [chartMonthBalance balance between charges and payments]
             * @type {Object}
             */
            $scope.chartMonthBalance = {};
            /**
             * [chartMonthBalance balance between charges and payments]
             * @type {Object}
             */
            $scope.charChargesPayments = {};
            /**
             * [confirmCharges when click on send charges
             * makes the user confirm on this modal window]
             * @return {[type]} [description]
             */
            $scope.confirmCharges = function () {

                var modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: '/js/components/charges/apply-charges-modal.tpl.html',
                    controller: function($scope) {

                        $scope.showProgress = false;
                        /**
                         * [closeModal closes (dismiss) active
                         * modal window]
                         * @return {Boolean} [returns true after dismissing
                         * a modal window]
                         */
                        $scope.cancel = function () {
                            modalInstance.dismiss('cancel');
                            return true;
                        };
                        /**
                         * [closeModal closes (dismiss) active
                         * modal window]
                         * @return {Boolean} [returns true after dismissing
                         * a modal window]
                         */
                        $scope.apply = function () {

                            var result = false;
                            $scope.showProgress = true;

                            DataProvider.get('/charges/applyCharges').then(function(data){

                                if(parseInt(data)) {

                                    result = true;
                                    AlertsService.addAlert('success','Los cargos se han aplicado correctamente.');
                                } else {

                                    AlertsService.addAlert('danger','Hubo un error al aplicar los cargos, intente nuevamente.');
                                }

                                $scope.showProgress = false;
                                modalInstance.close(result);
                            });
                        };
                    },
                });
                /**
                 * [hides the reminder if charges were applied correctly]
                 * @param  {Boolean} result
                 */
                modalInstance.result.then(function (result) {
                    /**
                     * Refresh charts data upon charges application
                     */
                    $scope.getChartsData();
                    $scope.initChart();
                    /**
                     * [hideReminder hide the reminder box]
                     * @type {Boolean}
                     */
                    $scope.hideReminder = result;
                });              
            };

            $scope.getChartsData = function() {

                /**
                 * [get data for pie chart]
                 */
                DataProvider.get('/balance/getChartMonthBalance').then(function(data){
                    $scope.chartMonthBalance = data;
                });
                /**
                 * [get data for pie chart]
                 */
                DataProvider.get('/balance/getChargesPaymentsChartData').then(function(data){
                    $scope.charChargesPayments = data;
                });
            };
            /**
             * Initi charts data
             */
            $scope.getChartsData();
        },
    ]);
;angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('applyChargesModal', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/charges/apply-charges-modal.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('totalChargesPayments', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/charges/total-charges-payments.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('monthChargesPayments', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/charges/month-charges-payments.tpl.html'
    };
});;angular.module('miResidencialApp.controllers')
    .controller('DashboardController', [
        '$scope',
        '$uibModal',
        '$http',
        'DataProvider',
        'FrontEndHelper',
        'MapsHelper',
        function ($scope, $uibModal, $http, DataProvider, FrontEndHelper, MapsHelper) {

            'use strict';
            
            /**
             * [data for homes chart]
             * @type {Object}
             */
            $scope.chartHomes = {};
            /**
             * [monthBalance form month income chart]
             * @type {Object}
             */
            $scope.monthBalance = {};
            /**
             * [monthBalance form month income chart]
             * @type {Object}
             */
            $scope.totalBalance = {};
            /**
             * [chartExpenses data for expenses line
             * chart]
             * @type {Object}
             */
            $scope.chartBalance = {};
            /**
             * [chartMonth data for month pie chart]
             * @type {Object}
             */
            $scope.chartMonthBalance = {};
            /**
             * [month Name of current month]
             * @type {String}
             */
            $scope.month = FrontEndHelper.monthToText(new Date().getMonth());
            /**
             * [chartOptions overrides chart options
             * for all charts]
             * @type {Object}
             */
            $scope.chartOptions = {
                animateRotate : false, 
            };
            /**
             * [searchResults will contain results
             * from search box]
             * @type {Array}
             */
            $scope.searchResults = [];
            /**
             * [announcements will contain announcements
             * from the database]
             * @type {Array}
             */
            $scope.announcements = [];
            /**
             * [mapURL construct the map URL to locate
             * facilities]
             * @type {String}
             */
            $scope.mapURL = "";
            /**
             * [toggleCharts toggle collapse state on
             * charts]
             * @type {Boolean}
             */
            $scope.toggleCharts = true;
            /**
             * [initCharts init all data from dashboard
             * modules]
             */
            $scope.initData = function() {

                DataProvider.get('/balance/getMonthBalance').then(function(data){
                    $scope.monthBalance = data;
                });

                DataProvider.get('/balance/getTotalBalance').then(function(data){
                    $scope.totalBalance = data;
                });

                DataProvider.get('/balance/getChartMonthBalance').then(function(data){
                    $scope.chartMonthBalance = data;
                });

                DataProvider.get('/balance/getChartBalance').then(function(data){
                    $scope.chartBalance = data;
                });

                DataProvider.get('/homes/getChartHomes').then(function(data){
                    $scope.chartHomes = data;
                });

                DataProvider.get('/announcements/getAllAnnouncements').then(function(data){
                    $scope.announcements = data;
                });

                DataProvider.get('/payments/getBonifications').then(function(data){
                    $scope.bonifications = data;
                });

                DataProvider.get('/expenses/audit').then(function(data){
                    $scope.expensesAudit = data;
                });
            };
        },
    ]);;angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('chartsPie', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/charts-pie.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('chartMonthlyBalance', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/chart-monthly-balance.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('chartMonthBalance', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/chart-month-balance.tpl.html'
    };
})
.directive('announcements', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/announcements.tpl.html'
    };
})
.directive('adminTasks', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/admin-tasks.tpl.html'
    };
})
.directive('facilitiesAvailability', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/facilities-availability.tpl.html'
    };
})
.directive('balanceTotal', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/balance-total.tpl.html'
    };
})
.directive('bonifications', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/bonifications.tpl.html'
    };
})
.directive('balanceMonth', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/balance-month.tpl.html'
    };
})
.directive('expensesAudit', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/expenses-audit.tpl.html'
    };
});;angular.module('miResidencialApp.controllers')
    .controller('ExpensesController', [
        '$scope',
        '$filter',
        '$uibModal',
        'DataProvider',
        function ($scope,  $filter, $uibModal, DataProvider) {
            
            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;

            $scope.showImageModal = function (expenseImage) {

                $scope.expenseImage = expenseImage;

                var modalInstance = $uibModal.open({
                    animation: true,
                    resolve : {
                        expenseImage : function() {
                            return decodeURIComponent($scope.expenseImage);
                        }
                    },
                    controller: function($uibModalInstance, $scope, expenseImage){

                        $scope.expenseImage = expenseImage;

                        $scope.close = function () {
                            $uibModalInstance.dismiss('close');
                        };                        
                    },                 
                    templateUrl: '/js/components/expenses/expense_image.tpl.html',
                });
            };
        },
    ]);;angular.module('miResidencialApp.directives');;angular.module('miResidencialApp.controllers')
    .controller('FacilitiesRentalsController', [
        '$scope',
        '$filter',
        '$timeout',
        '$window',
        'DataProvider',
        function ($scope,  $filter, $timeout, $window, DataProvider) {
            
            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
            /**
             * [eventSource Contains all events from calendar
             * to mark all taken days]
             * @type {Array}
             */
            $scope.eventSource = [];
            $scope.calendarMode = 'month';
            /**
             * [showCalendar wait until eventSource array
             * has been filled]
             * @type {Boolean}
             */
            $scope.showCalendar = false;
            /**
             * [faciltyAvailable checks if it's available to enable
             * rent controls]
             * @type {Boolean}
             */
            $scope.faciltyAvailable = true;
            /**
             * [selectedDate Holds selecte date from calendar]
             * @type {Object}
             */
            $scope.selectedDate = {};

            $scope.termsAccepted = false;
            /**
             * [onTimeSelected check availability to enable
             * button when clicking a day in the calendar]
             * @param  {Date} selectedTime [date selected]
             */
            $scope.onTimeSelected = function (selectedTime) {

                selectedDateAvailable(selectedTime);
                $scope.selectedDate = selectedTime;
            };
            /**
             * [availability checks for facility
             * availability]
             */
            $scope.checkAvailability = function(id) {


                DataProvider.get('/facilitiesrentals/checkAvailability/'+id).then(function(data) {

                    angular.forEach(data, function(event){

                        var startTime = new Date(Date.UTC(parseInt(event.startTime[2]),parseInt(event.startTime[1])-1,parseInt(event.startTime[0])));
                        var endTime = new Date(Date.UTC(parseInt(event.endTime[2]),parseInt(event.endTime[1])-1,parseInt(event.endTime[0])));
                        event.startTime = startTime;
                        event.endTime = endTime;

                        $scope.eventSource.push(event);

                    });

                    $scope.showCalendar = true;
                });
            };

            $scope.enableButton = function() {

                if($scope.termsAccepted) {
                    console.log('accepted');
                } else {
                    console.log('not accepted');
                }
            };

            $scope.reservation = function(facilityId) {

                var date = new Date($scope.selectedDate);
                var param = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();

                $window.location = '/renta_areas/agregar/'+param+'-'+facilityId;
            };

            var selectedDateAvailable = function(selectedTime) {
                var continueLoop = true;

                angular.forEach($scope.eventSource, function(event){

                    var eventTime = new Date(Date.UTC(event.startTime.getYear(), event.startTime.getMonth(), event.startTime.getDate()));
                    var requestedTime = new Date(Date.UTC(selectedTime.getYear(), selectedTime.getMonth(), selectedTime.getDate()));
                    
                    if(eventTime.getTime() == requestedTime.getTime()) {
                        $scope.faciltyAvailable = false;
                        continueLoop = false;
                    } 
                });

                if(continueLoop) {

                    $scope.faciltyAvailable = true;
                }
            };
        },
    ]);;angular.module('miResidencialApp.controllers')
    .controller('FacilitiesController', [
        '$scope',
        '$uibModal',
        '$filter',
        'DataProvider',
        'MapsHelper',
        function ($scope, $uibModal, $filter, DataProvider, MapsHelper) {

            'use strict';

            /**
             * [facilities contain the list of all
             * facilities in the neighborhood]
             * @type {Array}
             */
            $scope.facilities = [];
            /**
             * Contains the instance of the active modal
             * window.
             */
            $scope.modalInstance = {};
            /**
             * [waitingResponse flag to show/hide progress
             * bar]
             * @type {Boolean}
             */
            $scope.waitingResponse = false;
            /**
             * [isCollapsed Collapses the list panel]
             * @type {Boolean}
             */
            $scope.isCollapsed = true;          
            /**
             * [facilityLocationModal opens modal window with specified
             * locations]
             * @param  {Number} latitude  [description]
             * @param  {Number} longitude [description]
             * @return {[type]}           [description]
             */
            $scope.facilityLocationModal = function (latitude, longitude) {
                /**
                 * [mapURL object to be passed to the view
                 * to render map]
                 */
                $scope.map = MapsHelper.location('100%', '500', latitude, longitude);
                /**
                 * [modalInstance opens a modal instance with the map]
                 * @type {[type]}
                 */
                $scope.modalInstance = $uibModal.open({

                    animation: true,
                    scope: $scope,
                    templateUrl: '/js/components/dashboard/facilities-location-modal.tpl.html',
                    controller: function($scope) {
                        /**
                         * [closeModal closes (dismiss) active
                         * modal window]
                         * @return {Boolean} [returns true after dismissing
                         * a modal window]
                         */
                        $scope.closeModal = function () {

                            $scope.modalInstance.dismiss('cancel');
                            return true;
                        };
                    }
                });
            };
            /**
             * [getAllFacilities gets all facilities from the DB
             * of the current neighborhood]
             */
            $scope.getAllFacilities = function() {

                DataProvider.get('/facilities/getAllFacilities').then(function(data){

                    angular.forEach(data, function(item){

                        item.checkDate = new Date();
                        $scope.facilities.push(item);
                    });
                });
            };
        },
    ]);;angular.module('miResidencialApp.directives')

/**
 * Directive that renders map location of current
 * displayed facility
 * @return void
 */
.directive('facilitiesMapLocation', function($compile, $timeout) {

    return {
    	scope: false,
        template: '<div ng-bind-html="currentFacility.MapLocation"></div>'
    };
});;angular.module('miResidencialApp.controllers')
    .controller('HomesController', [
        '$scope',
        'DataProvider',
        'MapsHelper',
        function ($scope, DataProvider, MapsHelper) {

            'use strict';
            /**
             * [currentHome contains the information of
             * the current displayed home]
             * @type {Object}
             */
            $scope.currentHome = {};
            /**
             * [initCurrentHome opens modal window with specified
             * locations]
             * @param  {Number} id  [home id]
             */
            $scope.initCurrentHome = function (id) {

                DataProvider.getHomeData(id).then(function(data){
                    $scope.currentHome = data;
                    $scope.currentHome.MapLocation = MapsHelper.location('100%','206',$scope.currentHome.Home.latitude,$scope.currentHome.Home.longitude);
                });
            };
        },
    ]);;angular.module('miResidencialApp.directives')

/**
 * Directive that renders map location of current
 * displayed home
 * @return void
 */
.directive('homeMapLocation', function($compile, $timeout) {

    return {
    	scope: false,
        template: '<div ng-bind-html="currentHome.MapLocation"></div>'
    };
});;angular.module('miResidencialApp.controllers')
    .controller('InventoriesController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
        	/**
        	 * [isExpensesCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isExpensesCollapsed = true;
            /**
             * [assignExpense toggles visibility
             * for expenses list]
             * @type {Boolean}
             */
            $scope.assignExpense = false;
            /**
             * [expenseId description]
             * @type {Number}
             */
            $scope.expenseId = 0;


            $scope.toggleExpenseId = function() {

                if(!assignExpense) {
                    
                    $scope.expenseId = 0;
                }
            };
        },
    ]);;angular.module('miResidencialApp.controllers')
    .controller('LoginController', [
        '$scope',
        '$timeout',
        'FrontEndHelper',
        function ($scope, $timeout, FrontEndHelper) {

            'use strict';
            /**
             * [Init variables when everything has been
             * loaded]
             */
            $timeout(function(){
                $scope.init();
            });

            /**
             * [init initializes variables]
             * @return {Void} [no return value]
             */
            $scope.init = function() {
            };
        },
    ]);;angular.module('miResidencialApp.directives');;angular.module('miResidencialApp.controllers')
    .controller('MainController', [
        '$scope',
        '$timeout',
        'DataProvider',
        function ($scope, $timeout, DataProvider) {

            'use strict';
            /**
             * [neighborhood general data of the current
             * neighborhood]
             * @type {Object}
             */
            $scope.neighborhood = {};
            /**
             * [getInitData does an ajax request to
             * get general data]
             */
            $scope.getInitData = function() {

                DataProvider.get('/neighborhoods/getNeighborhoodData/1').then(function(data){
                    /**
                     * [general sets variable value with
                     * resulting data from request]
                     * @type {Object}
                     */
                    $scope.neighborhood = data;
                });
            };

            $timeout(function(){
                
                $scope.getInitData();
            });
        },
    ]);;angular.module('miResidencialApp.controllers')
    .controller('NeighborhoodController', [
        '$scope',
        '$uibModal',
        '$sce',
        '$http',
        '$timeout',
        'DataProvider',
        'FrontEndHelper',
        'Upload',
        function ($scope, $uibModal, $sce, $http, $timeout, DataProvider, FrontEndHelper, Upload) {

            'use strict';
        },
    ]);;angular.module('miResidencialApp.directives')

/**
 * Directive that renders the neighborhood logo 
 * upload module
 * @return void
 */
.directive('neighborhoodLogo', function() {

    return {
    	scope: false,
    	controller: 'NeighborhoodController',
        templateUrl: '/js/components/neighborhoods/neighborhood-logo.tpl.html'
    };
});;angular.module('miResidencialApp.controllers')
    .controller('PaymentsController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
        },
    ]);;angular.module('miResidencialApp.controllers')
    .controller('ResidentsController', [
        '$scope',
        '$uibModal',
        '$sce',
        '$http',
        '$timeout',
        'DataProvider',
        'FrontEndHelper',
        'Upload',
        function ($scope, $uibModal, $sce, $http, $timeout, DataProvider, FrontEndHelper, Upload) {

            'use strict';
            /**
             * [mapURL construct the map URL to locate
             * facilities]
             * @type {String}
             */
            $scope.mapURL = "";
            /**
             * Contains the instance of the active modal
             * window.
             */
            $scope.modalInstance = {};
            /**
             * [facilityLocationModal opens modal window with specified
             * locations]
             * @param  {Number} latitude  [description]
             * @param  {Number} longitude [description]
             * @return {[type]}           [description]
             */
            $scope.homeLocationModal = function (latitude, longitude) {
                /**
                 * [mapURL object to be passed to the view
                 * to render map]
                 */
                $scope.map = {url:$sce.trustAsResourceUrl('https://maps.googleapis.com/maps/api/streetview?size=600x300&location=46.414382,10.013988&heading=151.78&pitch=-0.76&key=AIzaSyBmc6_QGOMOehybtOWTgMjBQNE7fLeVZU4')};
                $scope.modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: '/js/components/dashboard/facilityLocationModal.tpl.html',
                    controller: 'DashboardController',
                    resolve: {
                        mapURL: function () {
                            return $scope.map;
                        },
                    },
                });
            };
            /**
             * [closeModal closes (dismiss) active
             * modal window]
             * @return {Boolean} [returns true after dismissing
             * a modal window]
             */
            $scope.closeModal = function () {
                $scope.modalInstance.dismiss('cancel');
                return true;
            };
        },
    ]);;angular.module('miResidencialApp.directives')

/**
 * Directive that renders the residents picture 
 * upload module
 * @return void
 */
.directive('residentPicture', function() {

    return {
    	scope: false,
        templateUrl: '/js/components/residents/resident-picture.tpl.html'
    };
});;angular.module('miResidencialApp.controllers')
    .controller('ServiceProvidersController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
            /**
             * [assignExpense toggles visibility
             * for expenses list]
             * @type {Boolean}
             */
            $scope.assignExpense = false;
            /**
             * [expenseId description]
             * @type {Number}
             */
            $scope.expenseId = 0;


            $scope.toggleExpenseId = function() {

                if(!assignExpense) {
                    
                    $scope.expenseId = 0;
                }
            };
        },
    ]);;angular.module('miResidencialApp.controllers')
    .controller('UsersController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
        },
    ]);;angular.module('miResidencialApp.directives')

/**
 * Directive that renders the alert box
 * @return void
 */
.directive('alertsBox', function() {

    return {
    	scope: false,
        template: '<uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)" ng-cloak>{{ alert.msg }}</uib-alert>'
    };
});

;
angular.module('miResidencialApp.services')
    .factory('AlertsService', function($rootScope,$timeout) {

        'use strict';
        
        var AlertsService = {};
        /**
         * [alerts stores the alerts for the main layout]
         * @type {Array}
         */
        $rootScope.alerts = [];
        /**
         * [modalAlerts stores the alerts for the modal windows]
         * @type {Array}
         */
         $rootScope.modalAlerts = [];

        /**
         * [addAlert adds an alert to the alerts array]
         * @param {String} type [type that'll determine the color of the box]
         * @param {String} msg  [text to be displayed]
         */
        AlertsService.addAlert = function(type, msg) {

            $rootScope.alerts.push({'type': type, 'msg': msg});
            $timeout( function(){AlertsService.closeAlert(0);}, 5000);
        };
        /**
         * [addModalAlert adds an alert to the modal alerts array]
         * @param {String} type [type that'll determine the color of the box]
         * @param {String} msg  [text to be displayed]
         */
        AlertsService.addModalAlert = function(type, msg) {

            $rootScope.modalAlerts.push({'type': type, 'msg': msg});
            $timeout( function(){AlertsService.closeAlert(0);}, 5000);
        };
        /**
         * [closeAlert closes the alert and hides the box]
         * @param  {[type]} index [alert to be removed]
         */
        AlertsService.closeAlert = function(index) {

            $rootScope.alerts = [];
            $rootScope.modalAlerts = [];
        };

        return AlertsService;
    });;angular.module('miResidencialApp.controllers')
    .controller('CarouselController', [
        '$scope',
        '$window',
        'DataProvider',
        function ($scope, $window, DataProvider) {

            'use strict';
            
            /**
             * [myInterval slide transition interval]
             * @type {Number}
             */
            $scope.myInterval = 5000;
            /**
             * [noWrapSlides determines if the slide
             * should be wrapped]
             * @type {Boolean}
             */
            $scope.noWrapSlides = false;
            /**
             * [active active slide index]
             * @type {Number}
             */
            $scope.active = 0;
            /**
             * [folder folder to perform image search]
             * @type {String}
             */
            $scope.folder = "";
            /**
             * [folder folder to perform image search]
             * @type {String}
             */
            $scope.id = 0;
            /**
             * [slides array of slides]
             * @type {Array}
             */
            $scope.slides = [];

            $scope.startCarousel = function() {
                
                var path = '/'+$scope.folder+'/getImagesList/'+$scope.id;

                DataProvider.get(path).then(function(data){

                });                
            };

            /**
             * [listens to directive variable change]
             * @param  {String} [Type of search]
             */
            $scope.$watch('folder', function (value) {

                if (value) {

                    $scope.folder = value;
                }
            });
            /**
             * [listens to directive variable change]
             * @param  {String} [Type of search]
             */
            $scope.$watch('id', function (value) {

                if (value) {

                    $scope.id = value;
                }
            });
        },
    ]);;angular.module('miResidencialApp.directives')
    .directive('carousel', function() {

        return {
        	restrict: 'E',
            scope: false,
            templateUrl: '/js/shared/carousel/carousel.tpl.html',
            link: function(scope, element, attributes){

                scope.folder = attributes.folder;
                scope.id = attributes.id;
            }
        };
    });;angular.module('miResidencialApp.controllers')
    .controller('ChartsController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
            /**
             * [chart holds data from async call to
             * populate the line chart]
             * @type {Object}
             */
            $scope.chart = {};
            /**
             * [options chartjs options override]
             * @type {Object}
             */
            $scope.chart.options = {};
            /**
             * [data data values for chart]
             * @type {Array}
             */
            $scope.chart.data = [];
            /**
             * [labels labels for axis]
             * @type {Array}
             */
            $scope.chart.labels = [];
            /**
             * [series series data]
             * @type {Array}
             */
            $scope.chart.series = [];
            /**
             * [show determines what kind of data to show
             * in the charts]
             * @type {String}
             */
            $scope.show = "";
            /**
             * [type chart type
             * bar or line]
             * @type {String}
             */
            $scope.type = "";
            /**
             * [initChart gets all data from backend service
             * to display it on the chart]
             */
            $scope.initChart = function() {
                /**
                 * [Perform async call to retrieve data]
                 */
                DataProvider.get('/'+$scope.show+'/getChartData').then(function(results){
                    
                    $scope.chart.data = results.data;
                    $scope.chart.labels = results.labels;
                    $scope.chart.series = results.series;
                });
            };
            /**
             * [watches for show variable change]
             * @param  {String}  [type of data in the chart]
             */
            $scope.$watchGroup(['show','type'], function (newValue, oldValues, scope) {

                if (newValue) {

                    $scope.show = newValue[0];
                    $scope.type = newValue[1];
                    $scope.initChart();
                }
            });
        },
    ]);;angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('lineChart', function($compile) {

    return {
        restrict: 'E',
        scope: false,
        controller: 'ChartsController',
        templateUrl: '/js/shared/charts/chart.tpl.html',
        link: function(scope, element, attributes){

            scope.show = attributes.show;
            scope.type = attributes.type;
        }
    };
});;angular.module('miResidencialApp.services')
    .factory('DataProvider', [
        '$rootScope',
        '$http',
        'FrontEndHelper',
        function($rootScope, $http, FrontEndHelper) {

            'use strict';
            
            var DataProvider = {};


            /**
             * [chartBalance returns data for total balance
             * chart]
             * @return {Object} [data to generate total
             * balance chart]
             */
            DataProvider.get = function(url) {

                return $http.get(url).then(function(results){

                    return results.data;
                });
            };
            /**
             * [residentSearch ajax search for residents]
             * @param  {String} value [Text entered by the user]
             * @return {Promise}       [contains the data to be sent to
             * the front end]
             */
            DataProvider.quickSearch = function(value, type) {

                return $http.get('/'+type+'/search/'+value).then(function(items){

                    return _.map(items.data,function(value, index){
                        return {'id':index,'value':value};
                    });
                });            
            };
            /**
             * [getHomeData gets data from specified home]
             * @param  {Number} id [id of the home]
             * @return {Object}    [Json data]
             */
            DataProvider.getHomeData = function(id) {

                return $http.get('/homes/getHomeData/'+id).then(function(homeData){

                    return homeData.data;
                });               
            };

            return DataProvider;
        },
    ]);
;angular.module('miResidencialApp.controllers')
    .controller('FileUploadController', [
        '$scope',
        '$timeout',
        'DataProvider',
        'FrontEndHelper',
        'Upload',
        function ($scope, $timeout, DataProvider, FrontEndHelper, Upload) {

            'use strict';
            /**
             * [currentLogo stores Current ]
             * @type {String}
             */
            $scope.currentImage = '';
            /**
             * [folder destination of the uploaded image]
             * @type {Number}
             */
            $scope.folder = '';
            /**
             * [id of the item]
             * @type {Number}
             */
            $scope.id = '';
            /**
             * [isCollapsed show/hide the image upload form
             * so as the uploaded image]
             * @type {Boolean}
             */
            $scope.isCollapsed = true;
            /**
             * [init initializes the variables]
             */
            $scope.init = function() {

                DataProvider.get('/image-exists/'+$scope.folder+'/'+$scope.id).then(function(data){

                    if(data !== '\\') {
                        
                        $scope.currentImage = data;
                        $scope.isCollapsed = false;
                    }
                });
            };
            /**
             * [toggleCollapse toggles form view]
             */
            $scope.toggleCollapse = function() {

                $scope.isCollapsed = !$scope.isCollapsed;
            };
            /**
             * [upload upload resident picture]
             * @param  {String} dataUrl [path]
             * @param  {String} name    [image name]
             */
            $scope.upload = function (dataUrl, name) {

                Upload.upload({

                    url: '/file-upload/'+$scope.folder+'/'+$scope.id,
                    method: 'POST',
                    data: {
                        file: Upload.dataUrltoBlob(dataUrl, name),
                    },
                }).then(function (response) {

                    $timeout(function () {

                        $scope.currentImage = '\\'+response.data;
                        $scope.isCollapsed = false;
                    });
                }, function (response) {

                    if (response.status > 0) {

                        $scope.errorMsg = response.status + ': ' + response.data;
                    }
                }, function (evt) {

                    $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                });
            };
            /**
             * [watches for folder variable to change (from the directive)
             * to initialize variables] 
             */
            $scope.$watch('id', function(){

                $scope.init();
            });            
        },
    ]);;angular.module('miResidencialApp.directives')
    .directive('fileUploadForm', function() {

        return {
        	restrict: 'E',
            scope: false,
            controller: 'FileUploadController',
            templateUrl: '/js/shared/file-upload/file-upload.tpl.html',
            link: function(scope, element, attributes){

                scope.folder = attributes.folder;
                scope.id = attributes.id;
            }
        };
    });;angular.module('miResidencialApp.services')
    .factory('FrontEndHelper', [
        '$rootScope',
        function($rootScope){

            'use strict';
            
            var FrontEndHelper = {};
            /**
             * [randomHexCode generates a random hex code
             * for charts]
             * @return {String} [random hex code]
             */
            FrontEndHelper.randomHexCode = function() {
                /**
                 * Randomly generated hex code
                 */
                return '#'+Math.floor(Math.random()*16777215).toString(16);
            };

            /**
             * [getDate gets datetime formatted variable
             * from PHP and returns only the date]
             * @return {[type]} [description]
             */
            FrontEndHelper.getDate = function (requestedDate) {

                requestedDate = String(requestedDate);
                
                var date = requestedDate.slice(0, requestedDate.indexOf(" "));
                var t = date.split(/[-]/);
                
                return t[2]+"-"+t[1]+"-"+t[0];
            };
            /**
             * [monthToText returns the month in
             * text format according to the id
             * sent]
             * @param  {Number} id [month id]
             * @return {Sting}    [month in text]
             */
            FrontEndHelper.monthToText = function(id) {
                /**
                 * [months list of months, eventually switch it
                 * to a language service]
                 * @type {Array}
                 */
                var months = [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre',
                ];
                /**
                 * Month in text format
                 */
                return months[id];
            };

            return FrontEndHelper;
        },
    ]);
;angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('platformFooter', function() {

    return {
    	scope: false,
        templateUrl: '/js/shared/layout/platform-footer.tpl.html'
    };
})
.directive('platformSidebar', function() {

    return {
    	scope: false,
        templateUrl: '/js/shared/layout/platform-sidebar.tpl.html'
    };
})
.directive('platformNavbar', function() {

    return {
        scope: false,
        templateUrl: '/js/shared/layout/platform-navbar.tpl.html'
    };
})
.directive('loginNavbar', function() {

    return {
    	scope: false,
        templateUrl: '/js/shared/layout/login-navbar.tpl.html'
    };
});;angular.module('miResidencialApp.services')
    .factory('MapsHelper', [
        '$rootScope',
        '$sce',
        function($rootScope, $sce){

            'use strict';
            
            var MapsHelper = {};
            /**
             * [apiKey provided by google]
             * @type {String}
             */
            var apiKey = "AIzaSyBmc6_QGOMOehybtOWTgMjBQNE7fLeVZU4";
            /**
             * [location returns a location map]
             * @param  {String} width     [width value with unit]
             * @param  {String} height    [height value with unit]
             * @param  {Number} latitude  [latitude value]
             * @param  {Number} longitude [longitude value]
             * @return {String}           [iframed map]
             */
            MapsHelper.location = function(width, height, latitude, longitude) {

                var mapURL = 'https://www.google.com/maps/embed/v1/view?maptype=satellite&center='+latitude+','+longitude+'&zoom=20&key='+apiKey;
                return $sce.trustAsHtml('<iframe width="'+width+'" height="'+height+'" src="'+mapURL+'"></iframe>');
            };

            return MapsHelper;
        },
    ]);
;angular.module('miResidencialApp.controllers')
    .controller('QuickSearchController', [
        '$scope',
        '$window',
        'DataProvider',
        function ($scope, $window, DataProvider) {

            'use strict';
            /**
             * [search stores search string]
             * @type {String}
             */
            $scope.search = "";
            /**
             * [searchFor indicates the modal to be used
             * for searching. This value comes from a directive
             * attribute]
             * @type {String}
             */
            $scope.searchFor = "";
            /**
             * [default default model for redirect]
             * @type {String}
             */
            $scope.default = "residentes";
            /**
             * [getSearchResults get result list from respective 
             * model]
             * @param  {String} val [text to be searched by the backend controller]
             * @return {String}     [items for quick search box]
             */
            $scope.getSearchResults = function(val) {

                return DataProvider.quickSearch(val, $scope.searchFor).then(function(response){

                    return response.map(function(item){
                        return item;
                    });
                });
            };
            /**
             * [listens to directive variable change]
             * @param  {String} [Type of search]
             */
            $scope.$watch('searchfor', function (value) {
                if (value) {
                    $scope.searchFor = value;
                }
            });
            /**
             * [viewRecord go to resident summary page]
             * @param  {Object} $item  [default typehead params]
             */
            $scope.viewRecord = function($item) {

                var path = $window.location.pathname == "/" ? 
                    $scope.default : $window.location.pathname;
                    
                $window.location.href = path+'/ver/'+$item.id;
            };
        },
    ]);;angular.module('miResidencialApp.directives')
    .directive('quickSearchBox', function() {

        return {
        	restrict: 'E',
            scope: false,
            templateUrl: '/js/shared/quick-search/quick-search.tpl.html',
            link: function(scope, element, attributes){

                scope.searchfor = attributes.searchfor;
            }
        };
    });