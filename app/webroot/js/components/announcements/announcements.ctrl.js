/**
 * @ngdoc function
 * @name miresidencialApp.controller:AnnouncementsController
 * @description
 * # AnnouncementsController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('AnnouncementsController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {
        
        'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
        },
    ]);