/**
 * @ngdoc function
 * @name miresidencialApp.controller:ResidentsController
 * @description
 * # ResidentsController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('FacilitiesController', [
        '$scope',
        '$uibModal',
        '$filter',
        'DataProvider',
        'MapsHelper',
        function ($scope, $uibModal, $filter, DataProvider, MapsHelper) {

            'use strict';

            /**
             * [facilities contain the list of all
             * facilities in the neighborhood]
             * @type {Array}
             */
            $scope.facilities = [];
            /**
             * Contains the instance of the active modal
             * window.
             */
            $scope.modalInstance = {};
            /**
             * [waitingResponse flag to show/hide progress
             * bar]
             * @type {Boolean}
             */
            $scope.waitingResponse = false;
            /**
             * [isCollapsed Collapses the list panel]
             * @type {Boolean}
             */
            $scope.isCollapsed = true;          
            /**
             * [facilityLocationModal opens modal window with specified
             * locations]
             * @param  {Number} latitude  [description]
             * @param  {Number} longitude [description]
             * @return {[type]}           [description]
             */
            $scope.facilityLocationModal = function (latitude, longitude) {
                /**
                 * [mapURL object to be passed to the view
                 * to render map]
                 */
                $scope.map = MapsHelper.location('100%', '500', latitude, longitude);
                /**
                 * [modalInstance opens a modal instance with the map]
                 * @type {[type]}
                 */
                $scope.modalInstance = $uibModal.open({

                    animation: true,
                    scope: $scope,
                    templateUrl: '/js/components/dashboard/facilities-location-modal.tpl.html',
                    controller: function($scope) {
                        /**
                         * [closeModal closes (dismiss) active
                         * modal window]
                         * @return {Boolean} [returns true after dismissing
                         * a modal window]
                         */
                        $scope.closeModal = function () {

                            $scope.modalInstance.dismiss('cancel');
                            return true;
                        };
                    }
                });
            };
            /**
             * [getAllFacilities gets all facilities from the DB
             * of the current neighborhood]
             */
            $scope.getAllFacilities = function() {

                DataProvider.get('/facilities/getAllFacilities').then(function(data){

                    angular.forEach(data, function(item){

                        item.checkDate = new Date();
                        $scope.facilities.push(item);
                    });
                });
            };
        },
    ]);