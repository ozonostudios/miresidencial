angular.module('miResidencialApp.directives')

/**
 * Directive that renders map location of current
 * displayed facility
 * @return void
 */
.directive('facilitiesMapLocation', function($compile, $timeout) {

    return {
    	scope: false,
        template: '<div ng-bind-html="currentFacility.MapLocation"></div>'
    };
});