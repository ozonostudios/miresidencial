/**
 * @ngdoc function
 * @name miresidencialApp.controller:PaymentsController
 * @description
 * # PaymentsController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('PaymentsController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
        },
    ]);