/**
 * @ngdoc function
 * @name miresidencialApp.controller:DashboardController
 * @description
 * # DashboardController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('DashboardController', [
        '$scope',
        '$uibModal',
        '$http',
        'DataProvider',
        'FrontEndHelper',
        'MapsHelper',
        function ($scope, $uibModal, $http, DataProvider, FrontEndHelper, MapsHelper) {

            'use strict';
            
            /**
             * [data for homes chart]
             * @type {Object}
             */
            $scope.chartHomes = {};
            /**
             * [monthBalance form month income chart]
             * @type {Object}
             */
            $scope.monthBalance = {};
            /**
             * [monthBalance form month income chart]
             * @type {Object}
             */
            $scope.totalBalance = {};
            /**
             * [chartExpenses data for expenses line
             * chart]
             * @type {Object}
             */
            $scope.chartBalance = {};
            /**
             * [chartMonth data for month pie chart]
             * @type {Object}
             */
            $scope.chartMonthBalance = {};
            /**
             * [month Name of current month]
             * @type {String}
             */
            $scope.month = FrontEndHelper.monthToText(new Date().getMonth());
            /**
             * [chartOptions overrides chart options
             * for all charts]
             * @type {Object}
             */
            $scope.chartOptions = {
                animateRotate : false, 
            };
            /**
             * [searchResults will contain results
             * from search box]
             * @type {Array}
             */
            $scope.searchResults = [];
            /**
             * [announcements will contain announcements
             * from the database]
             * @type {Array}
             */
            $scope.announcements = [];
            /**
             * [mapURL construct the map URL to locate
             * facilities]
             * @type {String}
             */
            $scope.mapURL = "";
            /**
             * [toggleCharts toggle collapse state on
             * charts]
             * @type {Boolean}
             */
            $scope.toggleCharts = true;
            /**
             * [initCharts init all data from dashboard
             * modules]
             */
            $scope.initData = function() {

                DataProvider.get('/balance/getMonthBalance').then(function(data){
                    $scope.monthBalance = data;
                });

                DataProvider.get('/balance/getTotalBalance').then(function(data){
                    $scope.totalBalance = data;
                });

                DataProvider.get('/balance/getChartMonthBalance').then(function(data){
                    $scope.chartMonthBalance = data;
                });

                DataProvider.get('/balance/getChartBalance').then(function(data){
                    $scope.chartBalance = data;
                });

                DataProvider.get('/homes/getChartHomes').then(function(data){
                    $scope.chartHomes = data;
                });

                DataProvider.get('/announcements/getAllAnnouncements').then(function(data){
                    $scope.announcements = data;
                });

                DataProvider.get('/payments/getBonifications').then(function(data){
                    $scope.bonifications = data;
                });

                DataProvider.get('/expenses/audit').then(function(data){
                    $scope.expensesAudit = data;
                });
            };
        },
    ]);