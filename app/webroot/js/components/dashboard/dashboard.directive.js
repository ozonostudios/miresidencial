angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('chartsPie', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/charts-pie.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('chartMonthlyBalance', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/chart-monthly-balance.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('chartMonthBalance', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/chart-month-balance.tpl.html'
    };
})
.directive('announcements', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/announcements.tpl.html'
    };
})
.directive('adminTasks', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/admin-tasks.tpl.html'
    };
})
.directive('facilitiesAvailability', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/facilities-availability.tpl.html'
    };
})
.directive('balanceTotal', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/balance-total.tpl.html'
    };
})
.directive('bonifications', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/bonifications.tpl.html'
    };
})
.directive('balanceMonth', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/dashboard/balance-month.tpl.html'
    };
})
.directive('expensesAudit', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/dashboard/expenses-audit.tpl.html'
    };
});