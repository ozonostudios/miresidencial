/**
 * @ngdoc function
 * @name miresidencialApp.controller:InventoriesController
 * @description
 * # InventoriesController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('InventoriesController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
        	/**
        	 * [isExpensesCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isExpensesCollapsed = true;
            /**
             * [assignExpense toggles visibility
             * for expenses list]
             * @type {Boolean}
             */
            $scope.assignExpense = false;
            /**
             * [expenseId description]
             * @type {Number}
             */
            $scope.expenseId = 0;


            $scope.toggleExpenseId = function() {

                if(!assignExpense) {
                    
                    $scope.expenseId = 0;
                }
            };
        },
    ]);