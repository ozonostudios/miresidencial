angular.module('miResidencialApp.directives')

/**
 * Directive that renders map location of current
 * displayed home
 * @return void
 */
.directive('homeMapLocation', function($compile, $timeout) {

    return {
    	scope: false,
        template: '<div ng-bind-html="currentHome.MapLocation"></div>'
    };
});