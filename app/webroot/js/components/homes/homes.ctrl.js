/**
 * @ngdoc function
 * @name miresidencialApp.controller:HomesController
 * @description
 * # HomesController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('HomesController', [
        '$scope',
        'DataProvider',
        'MapsHelper',
        function ($scope, DataProvider, MapsHelper) {

            'use strict';
            /**
             * [currentHome contains the information of
             * the current displayed home]
             * @type {Object}
             */
            $scope.currentHome = {};
            /**
             * [initCurrentHome opens modal window with specified
             * locations]
             * @param  {Number} id  [home id]
             */
            $scope.initCurrentHome = function (id) {

                DataProvider.getHomeData(id).then(function(data){
                    $scope.currentHome = data;
                    $scope.currentHome.MapLocation = MapsHelper.location('100%','206',$scope.currentHome.Home.latitude,$scope.currentHome.Home.longitude);
                });
            };
        },
    ]);