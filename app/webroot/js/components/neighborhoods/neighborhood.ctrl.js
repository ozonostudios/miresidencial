/**
 * @ngdoc function
 * @name miresidencialApp.controller:NeighborhoodController
 * @description
 * # NeighborhoodController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('NeighborhoodController', [
        '$scope',
        '$uibModal',
        '$sce',
        '$http',
        '$timeout',
        'DataProvider',
        'FrontEndHelper',
        'Upload',
        function ($scope, $uibModal, $sce, $http, $timeout, DataProvider, FrontEndHelper, Upload) {

            'use strict';
        },
    ]);