angular.module('miResidencialApp.directives')

/**
 * Directive that renders the neighborhood logo 
 * upload module
 * @return void
 */
.directive('neighborhoodLogo', function() {

    return {
    	scope: false,
    	controller: 'NeighborhoodController',
        templateUrl: '/js/components/neighborhoods/neighborhood-logo.tpl.html'
    };
});