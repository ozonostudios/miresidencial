/**
 * @ngdoc function
 * @name miresidencialApp.controller:ExpensesController
 * @description
 * # ExpensesController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('ExpensesController', [
        '$scope',
        '$filter',
        '$uibModal',
        'DataProvider',
        function ($scope,  $filter, $uibModal, DataProvider) {
            
            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;

            $scope.showImageModal = function (expenseImage) {

                $scope.expenseImage = expenseImage;

                var modalInstance = $uibModal.open({
                    animation: true,
                    resolve : {
                        expenseImage : function() {
                            return decodeURIComponent($scope.expenseImage);
                        }
                    },
                    controller: function($uibModalInstance, $scope, expenseImage){

                        $scope.expenseImage = expenseImage;

                        $scope.close = function () {
                            $uibModalInstance.dismiss('close');
                        };                        
                    },                 
                    templateUrl: '/js/components/expenses/expense_image.tpl.html',
                });
            };
        },
    ]);