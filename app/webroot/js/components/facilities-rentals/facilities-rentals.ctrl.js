/**
 * @ngdoc function
 * @name miresidencialApp.controller:FacilitiesRentalsController
 * @description
 * # FacilitiesRentalsController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('FacilitiesRentalsController', [
        '$scope',
        '$filter',
        '$timeout',
        '$window',
        'DataProvider',
        function ($scope,  $filter, $timeout, $window, DataProvider) {
            
            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
            /**
             * [eventSource Contains all events from calendar
             * to mark all taken days]
             * @type {Array}
             */
            $scope.eventSource = [];
            $scope.calendarMode = 'month';
            /**
             * [showCalendar wait until eventSource array
             * has been filled]
             * @type {Boolean}
             */
            $scope.showCalendar = false;
            /**
             * [faciltyAvailable checks if it's available to enable
             * rent controls]
             * @type {Boolean}
             */
            $scope.faciltyAvailable = true;
            /**
             * [selectedDate Holds selecte date from calendar]
             * @type {Object}
             */
            $scope.selectedDate = {};

            $scope.termsAccepted = false;
            /**
             * [onTimeSelected check availability to enable
             * button when clicking a day in the calendar]
             * @param  {Date} selectedTime [date selected]
             */
            $scope.onTimeSelected = function (selectedTime) {

                selectedDateAvailable(selectedTime);
                $scope.selectedDate = selectedTime;
            };
            /**
             * [availability checks for facility
             * availability]
             */
            $scope.checkAvailability = function(id) {


                DataProvider.get('/facilitiesrentals/checkAvailability/'+id).then(function(data) {

                    angular.forEach(data, function(event){

                        var startTime = new Date(Date.UTC(parseInt(event.startTime[2]),parseInt(event.startTime[1])-1,parseInt(event.startTime[0])));
                        var endTime = new Date(Date.UTC(parseInt(event.endTime[2]),parseInt(event.endTime[1])-1,parseInt(event.endTime[0])));
                        event.startTime = startTime;
                        event.endTime = endTime;

                        $scope.eventSource.push(event);

                    });

                    $scope.showCalendar = true;
                });
            };

            $scope.enableButton = function() {

                if($scope.termsAccepted) {
                    console.log('accepted');
                } else {
                    console.log('not accepted');
                }
            };

            $scope.reservation = function(facilityId) {

                var date = new Date($scope.selectedDate);
                var param = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();

                $window.location = '/renta_areas/agregar/'+param+'-'+facilityId;
            };

            var selectedDateAvailable = function(selectedTime) {
                var continueLoop = true;

                angular.forEach($scope.eventSource, function(event){

                    var eventTime = new Date(Date.UTC(event.startTime.getYear(), event.startTime.getMonth(), event.startTime.getDate()));
                    var requestedTime = new Date(Date.UTC(selectedTime.getYear(), selectedTime.getMonth(), selectedTime.getDate()));
                    
                    if(eventTime.getTime() == requestedTime.getTime()) {
                        $scope.faciltyAvailable = false;
                        continueLoop = false;
                    } 
                });

                if(continueLoop) {

                    $scope.faciltyAvailable = true;
                }
            };
        },
    ]);