angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('applyChargesModal', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/charges/apply-charges-modal.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('totalChargesPayments', function() {

    return {
        restrict: 'E',
        scope: false,
        templateUrl: '/js/components/charges/total-charges-payments.tpl.html'
    };
})
/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('monthChargesPayments', function() {

    return {
        restrict: 'E',
    	scope: false,
        templateUrl: '/js/components/charges/month-charges-payments.tpl.html'
    };
});