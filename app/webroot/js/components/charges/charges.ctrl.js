/**
 * @ngdoc function
 * @name miresidencialApp.controller:ChargesController
 * @description
 * # ChargesController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('ChargesController', [
        '$scope',
        '$filter',
        '$uibModal',
        'DataProvider',
        'AlertsService',
        function ($scope,  $filter, $uibModal, DataProvider, AlertsService) {

            'use strict';
            
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content year chart]
        	 * @type {Boolean}
        	 */
            $scope.isCollapsed = true;

            /**
             * [modalInstance current modal window instance holder]
             * @type {Object}
             */
            $scope.modalInstance = {};
            /**
             * [hideReminder hide reminder when charges have been sent]
             * @type {Boolean}
             */
            $scope.hideReminder = false;
            /**
             * [chartMonthBalance balance between charges and payments]
             * @type {Object}
             */
            $scope.chartMonthBalance = {};
            /**
             * [chartMonthBalance balance between charges and payments]
             * @type {Object}
             */
            $scope.charChargesPayments = {};
            /**
             * [confirmCharges when click on send charges
             * makes the user confirm on this modal window]
             * @return {[type]} [description]
             */
            $scope.confirmCharges = function () {

                var modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: '/js/components/charges/apply-charges-modal.tpl.html',
                    controller: function($scope) {

                        $scope.showProgress = false;
                        /**
                         * [closeModal closes (dismiss) active
                         * modal window]
                         * @return {Boolean} [returns true after dismissing
                         * a modal window]
                         */
                        $scope.cancel = function () {
                            modalInstance.dismiss('cancel');
                            return true;
                        };
                        /**
                         * [closeModal closes (dismiss) active
                         * modal window]
                         * @return {Boolean} [returns true after dismissing
                         * a modal window]
                         */
                        $scope.apply = function () {

                            var result = false;
                            $scope.showProgress = true;

                            DataProvider.get('/charges/applyCharges').then(function(data){

                                if(parseInt(data)) {

                                    result = true;
                                    AlertsService.addAlert('success','Los cargos se han aplicado correctamente.');
                                } else {

                                    AlertsService.addAlert('danger','Hubo un error al aplicar los cargos, intente nuevamente.');
                                }

                                $scope.showProgress = false;
                                modalInstance.close(result);
                            });
                        };
                    },
                });
                /**
                 * [hides the reminder if charges were applied correctly]
                 * @param  {Boolean} result
                 */
                modalInstance.result.then(function (result) {
                    /**
                     * Refresh charts data upon charges application
                     */
                    $scope.getChartsData();
                    $scope.initChart();
                    /**
                     * [hideReminder hide the reminder box]
                     * @type {Boolean}
                     */
                    $scope.hideReminder = result;
                });              
            };

            $scope.getChartsData = function() {

                /**
                 * [get data for pie chart]
                 */
                DataProvider.get('/balance/getChartMonthBalance').then(function(data){
                    $scope.chartMonthBalance = data;
                });
                /**
                 * [get data for pie chart]
                 */
                DataProvider.get('/balance/getChargesPaymentsChartData').then(function(data){
                    $scope.charChargesPayments = data;
                });
            };
            /**
             * Initi charts data
             */
            $scope.getChartsData();
        },
    ]);
