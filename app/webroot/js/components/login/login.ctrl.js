/**
 * @ngdoc function
 * @name miresidencialApp.controller:LoginController
 * @description
 * # LoginController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('LoginController', [
        '$scope',
        '$timeout',
        'FrontEndHelper',
        function ($scope, $timeout, FrontEndHelper) {

            'use strict';
            /**
             * [Init variables when everything has been
             * loaded]
             */
            $timeout(function(){
                $scope.init();
            });

            /**
             * [init initializes variables]
             * @return {Void} [no return value]
             */
            $scope.init = function() {
            };
        },
    ]);