/**
 * @ngdoc function
 * @name miresidencialApp.controller:HomesController
 * @description
 * # HomesController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('MainController', [
        '$scope',
        '$timeout',
        'DataProvider',
        function ($scope, $timeout, DataProvider) {

            'use strict';
            /**
             * [neighborhood general data of the current
             * neighborhood]
             * @type {Object}
             */
            $scope.neighborhood = {};
            /**
             * [getInitData does an ajax request to
             * get general data]
             */
            $scope.getInitData = function() {

                DataProvider.get('/neighborhoods/getNeighborhoodData/1').then(function(data){
                    /**
                     * [general sets variable value with
                     * resulting data from request]
                     * @type {Object}
                     */
                    $scope.neighborhood = data;
                });
            };

            $timeout(function(){
                
                $scope.getInitData();
            });
        },
    ]);