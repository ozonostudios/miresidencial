/**
 * @ngdoc function
 * @name miresidencialApp.controller:ResidentsController
 * @description
 * # ResidentsController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('ResidentsController', [
        '$scope',
        '$uibModal',
        '$sce',
        '$http',
        '$timeout',
        'DataProvider',
        'FrontEndHelper',
        'Upload',
        function ($scope, $uibModal, $sce, $http, $timeout, DataProvider, FrontEndHelper, Upload) {

            'use strict';
            /**
             * [mapURL construct the map URL to locate
             * facilities]
             * @type {String}
             */
            $scope.mapURL = "";
            /**
             * Contains the instance of the active modal
             * window.
             */
            $scope.modalInstance = {};
            /**
             * [facilityLocationModal opens modal window with specified
             * locations]
             * @param  {Number} latitude  [description]
             * @param  {Number} longitude [description]
             * @return {[type]}           [description]
             */
            $scope.homeLocationModal = function (latitude, longitude) {
                /**
                 * [mapURL object to be passed to the view
                 * to render map]
                 */
                $scope.map = {url:$sce.trustAsResourceUrl('https://maps.googleapis.com/maps/api/streetview?size=600x300&location=46.414382,10.013988&heading=151.78&pitch=-0.76&key=AIzaSyBmc6_QGOMOehybtOWTgMjBQNE7fLeVZU4')};
                $scope.modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: '/js/components/dashboard/facilityLocationModal.tpl.html',
                    controller: 'DashboardController',
                    resolve: {
                        mapURL: function () {
                            return $scope.map;
                        },
                    },
                });
            };
            /**
             * [closeModal closes (dismiss) active
             * modal window]
             * @return {Boolean} [returns true after dismissing
             * a modal window]
             */
            $scope.closeModal = function () {
                $scope.modalInstance.dismiss('cancel');
                return true;
            };
        },
    ]);