angular.module('miResidencialApp.directives')

/**
 * Directive that renders the residents picture 
 * upload module
 * @return void
 */
.directive('residentPicture', function() {

    return {
    	scope: false,
        templateUrl: '/js/components/residents/resident-picture.tpl.html'
    };
});