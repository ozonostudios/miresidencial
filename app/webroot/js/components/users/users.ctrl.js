/**
 * @ngdoc function
 * @name miresidencialApp.controller:UsersController
 * @description
 * # UsersController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('UsersController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
        	/**
        	 * [isCollapsed default state for collapsed
        	 * content]
        	 * @type {Boolean}
        	 */
        	$scope.isCollapsed = true;
        },
    ]);