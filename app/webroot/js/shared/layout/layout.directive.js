angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('platformFooter', function() {

    return {
    	scope: false,
        templateUrl: '/js/shared/layout/platform-footer.tpl.html'
    };
})
.directive('platformSidebar', function() {

    return {
    	scope: false,
        templateUrl: '/js/shared/layout/platform-sidebar.tpl.html'
    };
})
.directive('platformNavbar', function() {

    return {
        scope: false,
        templateUrl: '/js/shared/layout/platform-navbar.tpl.html'
    };
})
.directive('loginNavbar', function() {

    return {
    	scope: false,
        templateUrl: '/js/shared/layout/login-navbar.tpl.html'
    };
});