angular.module('miResidencialApp.directives')
    .directive('carousel', function() {

        return {
        	restrict: 'E',
            scope: false,
            templateUrl: '/js/shared/carousel/carousel.tpl.html',
            link: function(scope, element, attributes){

                scope.folder = attributes.folder;
                scope.id = attributes.id;
            }
        };
    });