
/**
 * @ngdoc function
 * @name miresidencialApp.controller:CarouselController
 * @description
 * # CarouselController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('CarouselController', [
        '$scope',
        '$window',
        'DataProvider',
        function ($scope, $window, DataProvider) {

            'use strict';
            
            /**
             * [myInterval slide transition interval]
             * @type {Number}
             */
            $scope.myInterval = 5000;
            /**
             * [noWrapSlides determines if the slide
             * should be wrapped]
             * @type {Boolean}
             */
            $scope.noWrapSlides = false;
            /**
             * [active active slide index]
             * @type {Number}
             */
            $scope.active = 0;
            /**
             * [folder folder to perform image search]
             * @type {String}
             */
            $scope.folder = "";
            /**
             * [folder folder to perform image search]
             * @type {String}
             */
            $scope.id = 0;
            /**
             * [slides array of slides]
             * @type {Array}
             */
            $scope.slides = [];

            $scope.startCarousel = function() {
                
                var path = '/'+$scope.folder+'/getImagesList/'+$scope.id;

                DataProvider.get(path).then(function(data){

                });                
            };

            /**
             * [listens to directive variable change]
             * @param  {String} [Type of search]
             */
            $scope.$watch('folder', function (value) {

                if (value) {

                    $scope.folder = value;
                }
            });
            /**
             * [listens to directive variable change]
             * @param  {String} [Type of search]
             */
            $scope.$watch('id', function (value) {

                if (value) {

                    $scope.id = value;
                }
            });
        },
    ]);