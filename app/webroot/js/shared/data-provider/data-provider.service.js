angular.module('miResidencialApp.services')
    .factory('DataProvider', [
        '$rootScope',
        '$http',
        'FrontEndHelper',
        function($rootScope, $http, FrontEndHelper) {

            'use strict';
            
            var DataProvider = {};


            /**
             * [chartBalance returns data for total balance
             * chart]
             * @return {Object} [data to generate total
             * balance chart]
             */
            DataProvider.get = function(url) {

                return $http.get(url).then(function(results){

                    return results.data;
                });
            };
            /**
             * [residentSearch ajax search for residents]
             * @param  {String} value [Text entered by the user]
             * @return {Promise}       [contains the data to be sent to
             * the front end]
             */
            DataProvider.quickSearch = function(value, type) {

                return $http.get('/'+type+'/search/'+value).then(function(items){

                    return _.map(items.data,function(value, index){
                        return {'id':index,'value':value};
                    });
                });            
            };
            /**
             * [getHomeData gets data from specified home]
             * @param  {Number} id [id of the home]
             * @return {Object}    [Json data]
             */
            DataProvider.getHomeData = function(id) {

                return $http.get('/homes/getHomeData/'+id).then(function(homeData){

                    return homeData.data;
                });               
            };

            return DataProvider;
        },
    ]);
