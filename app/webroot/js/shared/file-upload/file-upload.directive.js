angular.module('miResidencialApp.directives')
    .directive('fileUploadForm', function() {

        return {
        	restrict: 'E',
            scope: false,
            controller: 'FileUploadController',
            templateUrl: '/js/shared/file-upload/file-upload.tpl.html',
            link: function(scope, element, attributes){

                scope.folder = attributes.folder;
                scope.id = attributes.id;
            }
        };
    });