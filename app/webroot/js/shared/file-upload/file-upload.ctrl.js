/**
 * @ngdoc function
 * @name miresidencialApp.controller:FileUploadController
 * @description
 * # FileUploadController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('FileUploadController', [
        '$scope',
        '$timeout',
        'DataProvider',
        'FrontEndHelper',
        'Upload',
        function ($scope, $timeout, DataProvider, FrontEndHelper, Upload) {

            'use strict';
            /**
             * [currentLogo stores Current ]
             * @type {String}
             */
            $scope.currentImage = '';
            /**
             * [folder destination of the uploaded image]
             * @type {Number}
             */
            $scope.folder = '';
            /**
             * [id of the item]
             * @type {Number}
             */
            $scope.id = '';
            /**
             * [isCollapsed show/hide the image upload form
             * so as the uploaded image]
             * @type {Boolean}
             */
            $scope.isCollapsed = true;
            /**
             * [init initializes the variables]
             */
            $scope.init = function() {

                DataProvider.get('/image-exists/'+$scope.folder+'/'+$scope.id).then(function(data){

                    if(data !== '\\') {
                        
                        $scope.currentImage = data;
                        $scope.isCollapsed = false;
                    }
                });
            };
            /**
             * [toggleCollapse toggles form view]
             */
            $scope.toggleCollapse = function() {

                $scope.isCollapsed = !$scope.isCollapsed;
            };
            /**
             * [upload upload resident picture]
             * @param  {String} dataUrl [path]
             * @param  {String} name    [image name]
             */
            $scope.upload = function (dataUrl, name) {

                Upload.upload({

                    url: '/file-upload/'+$scope.folder+'/'+$scope.id,
                    method: 'POST',
                    data: {
                        file: Upload.dataUrltoBlob(dataUrl, name),
                    },
                }).then(function (response) {

                    $timeout(function () {

                        $scope.currentImage = '\\'+response.data;
                        $scope.isCollapsed = false;
                    });
                }, function (response) {

                    if (response.status > 0) {

                        $scope.errorMsg = response.status + ': ' + response.data;
                    }
                }, function (evt) {

                    $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                });
            };
            /**
             * [watches for folder variable to change (from the directive)
             * to initialize variables] 
             */
            $scope.$watch('id', function(){

                $scope.init();
            });            
        },
    ]);