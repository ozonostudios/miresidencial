angular.module('miResidencialApp.directives')

/**
 * Directive that renders the alert box
 * @return void
 */
.directive('alertsBox', function() {

    return {
    	scope: false,
        template: '<uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)" ng-cloak>{{ alert.msg }}</uib-alert>'
    };
});

