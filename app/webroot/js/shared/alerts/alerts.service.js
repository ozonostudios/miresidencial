
angular.module('miResidencialApp.services')
    .factory('AlertsService', function($rootScope,$timeout) {

        'use strict';
        
        var AlertsService = {};
        /**
         * [alerts stores the alerts for the main layout]
         * @type {Array}
         */
        $rootScope.alerts = [];
        /**
         * [modalAlerts stores the alerts for the modal windows]
         * @type {Array}
         */
         $rootScope.modalAlerts = [];

        /**
         * [addAlert adds an alert to the alerts array]
         * @param {String} type [type that'll determine the color of the box]
         * @param {String} msg  [text to be displayed]
         */
        AlertsService.addAlert = function(type, msg) {

            $rootScope.alerts.push({'type': type, 'msg': msg});
            $timeout( function(){AlertsService.closeAlert(0);}, 5000);
        };
        /**
         * [addModalAlert adds an alert to the modal alerts array]
         * @param {String} type [type that'll determine the color of the box]
         * @param {String} msg  [text to be displayed]
         */
        AlertsService.addModalAlert = function(type, msg) {

            $rootScope.modalAlerts.push({'type': type, 'msg': msg});
            $timeout( function(){AlertsService.closeAlert(0);}, 5000);
        };
        /**
         * [closeAlert closes the alert and hides the box]
         * @param  {[type]} index [alert to be removed]
         */
        AlertsService.closeAlert = function(index) {

            $rootScope.alerts = [];
            $rootScope.modalAlerts = [];
        };

        return AlertsService;
    });