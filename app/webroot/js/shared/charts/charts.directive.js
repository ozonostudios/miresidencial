angular.module('miResidencialApp.directives')

/**
 * Directive that renders the quick resident search box
 * @return void
 */
.directive('lineChart', function($compile) {

    return {
        restrict: 'E',
        scope: false,
        controller: 'ChartsController',
        templateUrl: '/js/shared/charts/chart.tpl.html',
        link: function(scope, element, attributes){

            scope.show = attributes.show;
            scope.type = attributes.type;
        }
    };
});