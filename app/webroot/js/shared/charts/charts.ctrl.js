/**
 * @ngdoc function
 * @name miresidencialApp.controller:ExpensesController
 * @description
 * # ExpensesController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('ChartsController', [
        '$scope',
        '$filter',
        'DataProvider',
        function ($scope,  $filter, DataProvider) {

            'use strict';
            /**
             * [chart holds data from async call to
             * populate the line chart]
             * @type {Object}
             */
            $scope.chart = {};
            /**
             * [options chartjs options override]
             * @type {Object}
             */
            $scope.chart.options = {};
            /**
             * [data data values for chart]
             * @type {Array}
             */
            $scope.chart.data = [];
            /**
             * [labels labels for axis]
             * @type {Array}
             */
            $scope.chart.labels = [];
            /**
             * [series series data]
             * @type {Array}
             */
            $scope.chart.series = [];
            /**
             * [show determines what kind of data to show
             * in the charts]
             * @type {String}
             */
            $scope.show = "";
            /**
             * [type chart type
             * bar or line]
             * @type {String}
             */
            $scope.type = "";
            /**
             * [initChart gets all data from backend service
             * to display it on the chart]
             */
            $scope.initChart = function() {
                /**
                 * [Perform async call to retrieve data]
                 */
                DataProvider.get('/'+$scope.show+'/getChartData').then(function(results){
                    
                    $scope.chart.data = results.data;
                    $scope.chart.labels = results.labels;
                    $scope.chart.series = results.series;
                });
            };
            /**
             * [watches for show variable change]
             * @param  {String}  [type of data in the chart]
             */
            $scope.$watchGroup(['show','type'], function (newValue, oldValues, scope) {

                if (newValue) {

                    $scope.show = newValue[0];
                    $scope.type = newValue[1];
                    $scope.initChart();
                }
            });
        },
    ]);