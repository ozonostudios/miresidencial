angular.module('miResidencialApp.services')
    .factory('MapsHelper', [
        '$rootScope',
        '$sce',
        function($rootScope, $sce){

            'use strict';
            
            var MapsHelper = {};
            /**
             * [apiKey provided by google]
             * @type {String}
             */
            var apiKey = "AIzaSyBmc6_QGOMOehybtOWTgMjBQNE7fLeVZU4";
            /**
             * [location returns a location map]
             * @param  {String} width     [width value with unit]
             * @param  {String} height    [height value with unit]
             * @param  {Number} latitude  [latitude value]
             * @param  {Number} longitude [longitude value]
             * @return {String}           [iframed map]
             */
            MapsHelper.location = function(width, height, latitude, longitude) {

                var mapURL = 'https://www.google.com/maps/embed/v1/view?maptype=satellite&center='+latitude+','+longitude+'&zoom=20&key='+apiKey;
                return $sce.trustAsHtml('<iframe width="'+width+'" height="'+height+'" src="'+mapURL+'"></iframe>');
            };

            return MapsHelper;
        },
    ]);
