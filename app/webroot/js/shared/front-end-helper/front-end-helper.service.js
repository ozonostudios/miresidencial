angular.module('miResidencialApp.services')
    .factory('FrontEndHelper', [
        '$rootScope',
        function($rootScope){

            'use strict';
            
            var FrontEndHelper = {};
            /**
             * [randomHexCode generates a random hex code
             * for charts]
             * @return {String} [random hex code]
             */
            FrontEndHelper.randomHexCode = function() {
                /**
                 * Randomly generated hex code
                 */
                return '#'+Math.floor(Math.random()*16777215).toString(16);
            };

            /**
             * [getDate gets datetime formatted variable
             * from PHP and returns only the date]
             * @return {[type]} [description]
             */
            FrontEndHelper.getDate = function (requestedDate) {

                requestedDate = String(requestedDate);
                
                var date = requestedDate.slice(0, requestedDate.indexOf(" "));
                var t = date.split(/[-]/);
                
                return t[2]+"-"+t[1]+"-"+t[0];
            };
            /**
             * [monthToText returns the month in
             * text format according to the id
             * sent]
             * @param  {Number} id [month id]
             * @return {Sting}    [month in text]
             */
            FrontEndHelper.monthToText = function(id) {
                /**
                 * [months list of months, eventually switch it
                 * to a language service]
                 * @type {Array}
                 */
                var months = [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre',
                ];
                /**
                 * Month in text format
                 */
                return months[id];
            };

            return FrontEndHelper;
        },
    ]);
