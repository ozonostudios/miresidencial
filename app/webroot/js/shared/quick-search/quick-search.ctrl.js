/**
 * @ngdoc function
 * @name miresidencialApp.controller:QuickSearchController
 * @description
 * # QuickSearchController
 * Controller of the miresidencialApp
 */
angular.module('miResidencialApp.controllers')
    .controller('QuickSearchController', [
        '$scope',
        '$window',
        'DataProvider',
        function ($scope, $window, DataProvider) {

            'use strict';
            /**
             * [search stores search string]
             * @type {String}
             */
            $scope.search = "";
            /**
             * [searchFor indicates the modal to be used
             * for searching. This value comes from a directive
             * attribute]
             * @type {String}
             */
            $scope.searchFor = "";
            /**
             * [default default model for redirect]
             * @type {String}
             */
            $scope.default = "residentes";
            /**
             * [getSearchResults get result list from respective 
             * model]
             * @param  {String} val [text to be searched by the backend controller]
             * @return {String}     [items for quick search box]
             */
            $scope.getSearchResults = function(val) {

                return DataProvider.quickSearch(val, $scope.searchFor).then(function(response){

                    return response.map(function(item){
                        return item;
                    });
                });
            };
            /**
             * [listens to directive variable change]
             * @param  {String} [Type of search]
             */
            $scope.$watch('searchfor', function (value) {
                if (value) {
                    $scope.searchFor = value;
                }
            });
            /**
             * [viewRecord go to resident summary page]
             * @param  {Object} $item  [default typehead params]
             */
            $scope.viewRecord = function($item) {

                var path = $window.location.pathname == "/" ? 
                    $scope.default : $window.location.pathname;
                    
                $window.location.href = path+'/ver/'+$item.id;
            };
        },
    ]);