angular.module('miResidencialApp.directives')
    .directive('quickSearchBox', function() {

        return {
        	restrict: 'E',
            scope: false,
            templateUrl: '/js/shared/quick-search/quick-search.tpl.html',
            link: function(scope, element, attributes){

                scope.searchfor = attributes.searchfor;
            }
        };
    });