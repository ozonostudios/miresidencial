/**
 * @ngdoc overview
 * @name miresidencialApp
 * @description
 * # miresidencialApp
 *
 * Main module of the application.
 */
angular
    .module('miResidencialApp', [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'chart.js',
        'ui.bootstrap',
        'ngFileUpload',
        'ngImgCrop',
        'ui.rCalendar',
        'angular-parallax',
        
        'miResidencialApp.controllers',
        'miResidencialApp.services',
        'miResidencialApp.directives',
    ]);

angular.module('miResidencialApp.controllers', []);
angular.module('miResidencialApp.services', []);
angular.module('miResidencialApp.directives', []);