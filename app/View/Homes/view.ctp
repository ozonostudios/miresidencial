<h2 class="well">
    <span class="fa fa-home"></span>
    <?php echo h($home['Home']['street'])." #".h($home['Home']['number']); ?>
</h2>

<div ng-controller="HomesController" class="homes view row" ng-init="initCurrentHome(<?php echo h($home['Home']['id']); ?>)">

    <div class="view-sidebar col-md-3">
        <x-file-upload-form folder="homes" id="<?php echo h($home['Home']['id']); ?>"></x-file-upload-form>
    </div>

    <ul class="list-group col-md-9 ">

        <li class="list-group-item">
            <small class="text-muted">Id</small>
            <h4><?php echo h($home['Home']['id']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Residente</small>
            <h4><?php echo $this->Html->link($home['Resident']['first_name']." ".$home['Resident']['last_name'], array('controller' => 'residents', 'action' => 'view', $home['Resident']['id'])); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Tipo de Casa</small>
            <h4><?php echo $this->Html->link($home['HomeType']['home_type'], array('controller' => 'home_types', 'action' => 'view', $home['HomeType']['id'])); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Información Adicional</small>
            <h4><?php echo h($home['Home']['home_info']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Status</small>
            <h4><?php echo h($home['Home']['status']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Saldo</small>
            <h4>$<?php echo h(number_format($home['Home']['debt'],2)); ?></h4>
        </li>
        <li class="list-group-item btn-group btn-group-justified">
            <?php echo $this->Html->link('Editar Casa', array('action' => 'edit', $home['Home']['id']), array('class' => 'btn btn-default')); ?>
            <?php echo $this->Html->link('Nuevo Pago', array('controller' => 'payments', 'action' => 'add', $home['Home']['id']), array('class' => 'btn btn-default')); ?>
            <?php echo $this->Html->link(__('Renta de Área Común'), array('controller' => 'facilities_rentals', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
        </li>
    </ul>

</div>


<div class="related row col-md-12">

    <?php if (!empty($home['Payment'])): ?>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePayments" aria-expanded="true" aria-controls="collapsePayments">
                            <span class="fa fa-usd"></span>                            
                            Pagos
                        </a>
                    </h4>
                </div>
                <div id="collapsePayments" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <table class="table table-bordered table-hovered table-striped">
                        <tr>
                            <th>Recibo</th>
                            <th>Cantidad</th>
                            <th>Detalles</th>
                            <th>Agregado</th>
                            <th>Acciones</th>
                        </tr>
                        <?php foreach ($home['Payment'] as $payment): ?>
                            <tr>
                                <td class="center"><?php echo $payment['id']; ?></td>
                                <td class="right">$<?php echo number_format($payment['amount'],2); ?></td>
                                <td><?php echo $payment['payment_details']; ?></td>
                                <td><?php echo date('d-M-Y', strtotime($payment['created'])); ?></td>
                                <td class="actions center">
                                    <a class="btn btn-info btn-sm" href="/pagos/ver/<?php echo $payment['id'];?>"><span class="fa fa-eye"></span></a>
                                    <a class="btn btn-default btn-sm" href="/pagos/editar/<?php echo $payment['id'];?>"><span class="fa fa-edit"></span></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>


    <?php if (!empty($home['Charge'])): ?>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCharges" aria-expanded="true" aria-controls="collapseCharges">
                            <span class="glyphicon glyphicon-list-alt"></span>                            
                            Cargos
                        </a>
                    </h4>
                </div>
                <div id="collapseCharges" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <table class="table table-bordered table-hovered table-striped">
                            <tr>
                                <th>Folio</th>
                                <th>Cantidad</th>
                                <th>Detalles</th>
                                <th>Agregado</th>
                                <th>Acciones</th>
                            </tr>
                        <?php foreach ($home['Charge'] as $charge): ?>
                            <tr>
                                <td class="center"><?php echo $charge['id']; ?></td>
                                <td class="right">$<?php echo number_format($charge['amount'],2); ?></td>
                                <td><?php echo $charge['charge_details']; ?></td>
                                <td><?php echo date('d-M-Y', strtotime($charge['created'])); ?></td>
                                <td class="actions center">
                                    <a class="btn btn-info btn-sm" href="/cargos/ver/<?php echo $charge['id'];?>"><span class="fa fa-eye"></span></a>
                                    <a class="btn btn-default btn-sm" href="/cargos/editar/<?php echo $charge['id'];?>"><span class="fa fa-edit"></span></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>


    <?php if (!empty($home['FacilitiesRental'])): ?>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFacilities" aria-expanded="true" aria-controls="collapseFacilities">
                            <span class="fa fa-tree"></span>
                            Rentas de Áreas Comunes
                        </a>
                    </h4>
                </div>
                <div id="collapseFacilities" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <table class="table table-bordered table-hovered table-striped">
                        <tr>
                            <th>Área Común</th>
                            <th>Desde</th>
                            <th>Hasta</th>
                            <th class="actions"><?php echo __('Acciones'); ?></th>
                        </tr>
                        <?php foreach ($home['FacilitiesRental'] as $facilitiesRental): ?>
                            <tr>
                                <td><?php echo $facilitiesRental['Facility']['facility']; ?></td>
                                <td><?php echo date('d-M-Y H:s', strtotime($facilitiesRental['from'])); ?></td>
                                <td><?php echo date('d-M-Y H:s', strtotime($facilitiesRental['to'])); ?></td>
                                <td class="actions center">
                                    <a class="btn btn-info btn-sm" href="/rentaareas/ver/<?php echo $facilitiesRental['id'];?>"><span class="fa fa-eye"></span></a>
                                    <a class="btn btn-default btn-sm" href="/rentaareas/editar/<?php echo $facilitiesRental['id'];?>"><span class="fa fa-edit"></span></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>