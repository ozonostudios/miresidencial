<h2 class="well">
    <span class="fa fa-home"></span> Lista de Domicilios
    <a class="btn btn-default btn-lg pull-right" href="/casas/agregar"><span class="fa fa-plus"></span>Agregar nuevo domicilio</a>
</h2>

<div class="homes index" ng-controller="HomesController">

    <x-quick-search-box searchfor="homes" label="Casas"></x-quick-search-box>

    <table class="table table-hovered table-striped table-bordered">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('street', 'Domicilio'); ?></th>
            <th><?php echo $this->Paginator->sort('resident_id', 'Residentes'); ?></th>
            <th><?php echo $this->Paginator->sort('home_type_id', 'Tipo'); ?></th>
            <th><?php echo $this->Paginator->sort('home_info', 'Información Adicional'); ?></th>
            <th>Saldo</th>
            <th>Estado</th>
            <th class="actions">Acciones</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($homes as $home): ?>
                <tr>
                    <td class="center"><?php echo h($home['Home']['id']); ?></td>
                    <td><?php echo h($home['Home']['street'])." #".h($home['Home']['number']); ?></td>
                    <td>
                        <?php echo $this->Html->link($home['Resident']['first_name']." ".$home['Resident']['last_name'], array('controller' => 'residents', 'action' => 'view', $home['Resident']['id'])); ?>
                    </td>
                    <td><?php echo $home['HomeType']['home_type']; ?></td>
                    <td><?php echo h($home['Home']['home_info']); ?></td>
                    <td class="right <?php echo $home['Home']['status_color']; ?>">$<?php echo h(number_format($home['Home']['debt'], 2)); ?></td>
                    <td class="<?php echo $home['Home']['status_color']; ?>"><?php echo h($home['Home']['status']);?></td>
                    <td class="actions center">
                        <a class="btn btn-info btn-sm" href="/casas/ver/<?php echo $home['Home']['id'];?>"><span class="fa fa-eye"></span></a>
                        <a class="btn btn-default btn-sm" href="/casas/editar/<?php echo $home['Home']['id'];?>"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?>
    </blockquote>
    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'casas', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
