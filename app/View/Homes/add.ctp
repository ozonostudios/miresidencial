<h2 class="well">
    <span class="fa fa-home"></span>
    Agregar Casa
</h2>

<div ng-controller="HomesController" class="homes form col-md-122">
<?php echo $this->Form->create('Home', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
    <fieldset>
        <?php
            echo $this->Form->input('street', array('class' => 'form-control', 'label' => 'Calle'));
            echo $this->Form->input('number', array('class' => 'form-control', 'label' => 'Número'));
            echo $this->Form->input('resident_id', array('class' => 'form-control', 'label' => 'Residente'));
            echo $this->Form->input('home_type_id', array('class' => 'form-control', 'label' => 'Tipo de Casa'));
            echo $this->Form->input('neighborhood_id', array('class' => 'form-control', 'label' => 'Fraccionamiento'));
            echo $this->Form->input('latitude', array('class' => 'form-control', 'label' => 'Latitud'));
            echo $this->Form->input('longitude', array('class' => 'form-control', 'label' => 'Longitud'));
            echo $this->Form->input('home_info', array('class' => 'form-control', 'label' => 'Información Adicional'));
        ?>
    </fieldset>
    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar')
        );
        echo $this->Form->end(); 
    ?>
</div>
