<div class="residentTypes form col-md-122">
<?php echo $this->Form->create('ResidentType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Resident Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('resident_type');
		echo $this->Form->input('created');
		echo $this->Form->input('modified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ResidentType.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ResidentType.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Resident Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Residents'), array('controller' => 'residents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resident'), array('controller' => 'residents', 'action' => 'add')); ?> </li>
	</ul>
</div>
