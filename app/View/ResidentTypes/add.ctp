<div class="residentTypes form col-md-122">
<?php echo $this->Form->create('ResidentType'); ?>
	<fieldset>
		<legend><?php echo __('Add Resident Type'); ?></legend>
	<?php
		echo $this->Form->input('resident_type');
		echo $this->Form->input('created');
		echo $this->Form->input('modified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Resident Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Residents'), array('controller' => 'residents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resident'), array('controller' => 'residents', 'action' => 'add')); ?> </li>
	</ul>
</div>
