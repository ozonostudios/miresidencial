<div class="residentTypes view panel panel-default">
<div class="panel-heading"><?php echo __('Resident Type'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($residentType['ResidentType']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Resident Type'); ?></label>
		<dd>
			<?php echo h($residentType['ResidentType']['resident_type']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($residentType['ResidentType']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Updated At'); ?></label>
		<dd>
			<?php echo h($residentType['ResidentType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resident Type'), array('action' => 'edit', $residentType['ResidentType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resident Type'), array('action' => 'delete', $residentType['ResidentType']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $residentType['ResidentType']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Resident Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resident Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Residents'), array('controller' => 'residents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resident'), array('controller' => 'residents', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Residents'); ?></h3>
	<?php if (!empty($residentType['Resident'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('First Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Resident Type Id'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Mobile'); ?></th>
		<th><?php echo __('Mobile 2'); ?></th>
		<th><?php echo __('Work Phone'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Occupation'); ?></th>
		<th><?php echo __('Website'); ?></th>
		<th><?php echo __('Show Info'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Additional Info'); ?></th>
		<th><?php echo __('Created At'); ?></th>
		<th><?php echo __('Updated At'); ?></th>
		<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($residentType['Resident'] as $resident): ?>
		<tr>
			<td><?php echo $resident['id']; ?></td>
			<td><?php echo $resident['first_name']; ?></td>
			<td><?php echo $resident['last_name']; ?></td>
			<td><?php echo $resident['resident_type_id']; ?></td>
			<td><?php echo $resident['phone']; ?></td>
			<td><?php echo $resident['mobile']; ?></td>
			<td><?php echo $resident['mobile_2']; ?></td>
			<td><?php echo $resident['work_phone']; ?></td>
			<td><?php echo $resident['email']; ?></td>
			<td><?php echo $resident['occupation']; ?></td>
			<td><?php echo $resident['website']; ?></td>
			<td><?php echo $resident['show_info']; ?></td>
			<td><?php echo $resident['status']; ?></td>
			<td><?php echo $resident['additional_info']; ?></td>
			<td><?php echo $resident['created']; ?></td>
			<td><?php echo $resident['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'residents', 'action' => 'view', $resident['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'residents', 'action' => 'edit', $resident['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'residents', 'action' => 'delete', $resident['id']), array('confirm' => __('Are you sure you want to delete # %s?', $resident['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Resident'), array('controller' => 'residents', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
