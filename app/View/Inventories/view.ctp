<div class="inventories view" ng-controller="InventoriesController">

    <h2 class="well">
        <span class="fa fa-square"></span>
        Inventario
    </h2>

    <div class="row">
        <div class="view-sidebar col-md-3">

            <x-file-upload-form folder='inventories' id='<?php echo $inventory['Inventory']['id'];?>'></x-file-upload-form>
            
            <?php if($inventory['Inventory']['brochure'] != '') { ?>
                <a href="<?php echo $inventory['Inventory']['brochure'];?>" class="btn btn-info btn-sm btn-block"><i class="fa fa-file-text"></i> <span> Manual de Usuario</span></a>
            <?php } ?>
            <a href="/inventario/editar/<?php echo $inventory['Inventory']['id'];?>" class="btn btn-info btn-block"><i class="fa fa-edit"></i> <span> Editar Inventario</span></a>
        </div>

        <div class="col-md-9">
            <ul class="list-group">
                <li class="list-group-item">
                    <small>Folio Inventario</small>
                    <h4><?php echo h($inventory['Inventory']['id']); ?></h4>
                </li>
                <li class="list-group-item">
                    <small>Producto</small>
                    <h4><?php echo h($inventory['Inventory']['product']); ?></h4>
                </li>
                <li class="list-group-item">
                    <small>Descripción</small>
                    <h4><?php echo h($inventory['Inventory']['description']); ?></h4>
                </li>
                <li class="list-group-item">
                    <small>Marca</small>
                    <h4><?php echo h($inventory['Inventory']['brand']); ?></h4>
                </li>
                <li class="list-group-item">
                    <small>Modelo</small>
                    <h4><?php echo h($inventory['Inventory']['model']); ?></h4>
                </li>
                <li class="list-group-item">
                    <small>Serial</small>
                    <h4><?php echo h($inventory['Inventory']['serial']); ?></h4>
                </li>
                <li class="list-group-item">
                    <small>Ubicación</small>
                    <h4><?php echo h($inventory['Inventory']['location']); ?></h4>
                </li>
                <li class="list-group-item">
                    <small>Agregado</small>
                    <h4><?php echo date('d-m-Y', strtotime($inventory['Inventory']['created'])); ?></h4>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="row">
        <?php if($show_expense_info) {?>
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="#" ng-click="isExpensesCollapsed = !isExpensesCollapsed">
                            <span class="fa" ng-class="{'fa-chevron-up' : isExpensesCollapsed, 'fa-chevron-down' : isExpensesCollapsed == false}"></span>
                            Ver detalles del gasto
                        </a>
                    </div>
                    <div class="panel-body" uib-collapse="isExpensesCollapsed">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <p>Folio Gasto</p>
                                <h5><?php echo $inventory['Expense']['id']; ?></h5>
                                <input type="hidden" name="data[Inventory][expense_id]" value="<?php echo $inventory['Expense']['id']; ?>" />
                            </li>                
                            <li class="list-group-item">
                                <p>Cantidad</p>
                                <h5>$<?php echo number_format($inventory['Expense']['amount'],2); ?></h5>
                            </li>
                            <li class="list-group-item">
                                <p>Detalles</p>
                                <h5><?php echo $inventory['Expense']['expense_details']; ?></h5>
                            </li>
                        </ul>
                    </div>
                </div>
            
            </div>
        <?php } ?>
    </div>

</div>
