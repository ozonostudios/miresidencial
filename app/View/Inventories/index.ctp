<h2 class="well">
    <span class="fa fa-square"></span> Inventario
    <a class="btn btn-default btn-lg pull-right" href="/inventario/agregar"><span class="fa fa-plus"></span>Agregar Nuevo</a>
</h2>

<div class="inventories index" ng-controller="InventoriesController">

    <x-quick-search-box searchfor="inventories" label="Inventario"></x-quick-search-box>

    <table class="table table-hovered table-striped table-bordered">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'Folio'); ?></th>
                <th><?php echo $this->Paginator->sort('expense_id', 'Folio Gasto'); ?></th>
                <th><?php echo $this->Paginator->sort('product', 'Producto'); ?></th>
                <th><?php echo $this->Paginator->sort('description', 'Proveedor'); ?></th>
                <th><?php echo $this->Paginator->sort('brand', 'Marca'); ?></th>
                <th><?php echo $this->Paginator->sort('model', 'Modelo'); ?></th>
                <th><?php echo $this->Paginator->sort('serial', 'Serie'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($inventories as $inventory): ?>
                <tr>
                    <td class="center"><?php echo h($inventory['Inventory']['id']); ?></td>
                    <td class="center">
                        <?php 
                            if($inventory['Inventory']['expense_id']>0) {
                                echo $this->Html->link($inventory['Inventory']['expense_id'], array('controller' => 'expenses', 'action' => 'view', $inventory['Inventory']['expense_id']));
                            } else {
                                echo 'Sin Asignar';
                            }
                        ?>
                    </td>
                    <td><?php echo $inventory['Inventory']['product']; ?></td>
                    <td><?php echo $inventory['Inventory']['description']; ?></td>
                    <td><?php echo $inventory['Inventory']['brand']; ?></td>
                    <td><?php echo $inventory['Inventory']['model']; ?></td>
                    <td><?php echo $inventory['Inventory']['serial']; ?></td>

                    <td class="actions center">
                        <a class="btn btn-info btn-sm" href="inventario/ver/<?php echo $inventory['Inventory']['id'];?>"><span class="fa fa-eye"></span></a>
                        <a class="btn btn-default btn-sm" href="inventario/editar/<?php echo $inventory['Inventory']['id'];?>"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?> 
    </blockquote>

    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'inventario', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
