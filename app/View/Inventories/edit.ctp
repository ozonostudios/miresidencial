<h2 class="well">
    <span class="fa fa-square"></span>
    Editar Inventario
</h2>

<div class="col-lg-3 col-md-5">
    <x-file-upload-form folder="inventories" id="<?php echo $id;?>"></x-file-upload-form>
</div>

<div ng-controller="InventoriesController" class="inventories form  col-lg-9 col-md-7">

    <?php echo $this->Form->create('Inventory', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
      <fieldset>
    <?php

    if($show_expense_info) {?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="#" ng-click="isCollapsed = !isCollapsed">
                    <span class="fa" ng-class="{'fa-chevron-up' : isCollapsed, 'fa-chevron-down' : isCollapsed == false}"></span>
                    Ver detalles del gasto
                </a>
            </div>
            <div class="panel-body" uib-collapse="isCollapsed">
                <ul class="list-group">
                    <li class="list-group-item">
                        <p>Folio Gasto</p>
                        <h5><?php echo $expense['Expense']['id']; ?></h5>
                        <input type="hidden" name="data[Inventory][expense_id]" value="<?php echo $expense['Expense']['id']; ?>" />
                    </li>                
                    <li class="list-group-item">
                        <p>Cantidad</p>
                        <h5>$<?php echo number_format($expense['Expense']['amount'],2); ?></h5>
                    </li>
                    <li class="list-group-item">
                        <p>Detalles</p>
                        <h5><?php echo $expense['Expense']['expense_details']; ?></h5>
                    </li>
                    <li class="list-group-item">
                        <p>Proveedor</p>
                        <h5><?php echo $expense['ServiceProvider']['service_provider']; ?></h5>
                    </li>
                </ul>
            </div>
        </div>
    
    <?php } 
        echo $this->Form->input('id');
        echo $this->Form->input('product', array('class' => 'form-control', 'label' => 'Producto'));
        echo $this->Form->input('status_id', array('class' => 'form-control', 'label' => 'Estado'));
        echo $this->Form->input('description', array('class' => 'form-control', 'label' => 'Descripción'));
        echo $this->Form->input('brand', array('class' => 'form-control', 'label' => 'Marca'));
        echo $this->Form->input('model', array('class' => 'form-control', 'label' => 'Modelo'));
        echo $this->Form->input('serial', array('class' => 'form-control', 'label' => 'Serial'));
        echo $this->Form->input('location', array('class' => 'form-control', 'label' => 'Ubicación'));
    ?>
    </fieldset>
    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Editar')
        );
        echo $this->Form->end(); 
    ?>

</div>
