<h2 class=" well">
    <span class="fa fa-list"></span> Comprobante de Reservacíón
</h2>

<ul class="list-group" ng-controller="PaymentsController">
    <li class="list-group-item">
        <label class="text-muted">Folio de Reservación</label>
        <h4>#<?php echo h($facilitiesRental['FacilitiesRental']['id']); ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Área Común Solicitada</label>
        <h4><?php echo $facilitiesRental['Facility']['facility']; ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Domicilio Solicitante</label>
        <h4><?php echo $facilitiesRental['Home']['address']; ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Fecha</label>
        <h4><?php echo date('d-m-Y', strtotime($facilitiesRental['FacilitiesRental']['from'])); ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Pagado</label>
        <h4><?php echo ($facilitiesRental['FacilitiesRental']['paid']) ? 'Sí': 'No'; ?></h4>
    </li>
</ul>

<a href="/pagos/agregar" class="btn btn-info btn-lg no-print"> <span class="fa fa-usd"></span>Pagar</a>
<a href="#" onClick="print()" class="btn btn-info btn-lg no-print"> <span class="fa fa-print"></span>Imprimir Recibo</a>
