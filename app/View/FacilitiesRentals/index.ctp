<h2 class="well">
    <span class="fa fa-list"></span> Reservación de Áreas Comunes
    <a class="btn btn-default btn-lg pull-right" href="/renta_areas/agregar"><span class="fa fa-plus"></span>Agregar Nueva</a>
</h2>

<div class="facilities index" ng-controller="FacilitiesRentalsController">

    <table class="table table-hovered table-striped table-bordered">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('facility_id', 'Área Común'); ?></th>
            <th><?php echo $this->Paginator->sort('home_id', 'Solicitante'); ?></th>
            <th><?php echo $this->Paginator->sort('from', 'Fecha'); ?></th>
            <th class="actions">Acciones</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($facilitiesRentals as $facilitiesRental): ?>
                <tr>
                    <td class="center"><?php echo h($facilitiesRental['FacilitiesRental']['id']); ?></td>
                    <td><?php echo $this->Html->link($facilitiesRental['Facility']['facility'], array('controller' => 'facilities', 'action' => 'view', $facilitiesRental['Facility']['id'])); ?></td>
                    <td><?php echo $this->Html->link($facilitiesRental['Home']['address'], array('controller' => 'homes', 'action' => 'view', $facilitiesRental['Home']['id'])); ?></td>
                    <td class="right"><?php echo date('d-m-Y', strtotime($facilitiesRental['FacilitiesRental']['from'])); ?></td>
                    <td class="actions center">
                        <a class="btn btn-info btn-sm" href="/renta_areas/ver/<?php echo $facilitiesRental['FacilitiesRental']['id'];?>"><span class="fa fa-eye"></span></a>
                        <a class="btn btn-default btn-sm" href="/renta_areas/editar/<?php echo $facilitiesRental['FacilitiesRental']['id'];?>"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <blockquote>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
        ));
        ?>
    </blockquote>
    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'renta_areas', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
