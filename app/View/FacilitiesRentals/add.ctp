<h2 class="well">
    <span class="fa fa-list"></span>
    Reservar Área Común
</h2>

<div ng-controller="FacilitiesRentalsController" class="facilities-rentals form col-md-12">

    <?php echo $this->Form->create('FacilitiesRental', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
    <fieldset>
        <?php
            echo $this->Form->input('home_id', array('class' => 'form-control', 'label' => 'Solicitante'));
            
            if($params == null) {

                echo $this->Form->input('facility_id', array('class' => 'form-control', 'label' => 'Área Común'));
                echo $this->Form->input('from', array('label' => 'Fecha', 'class' => 'form-control date-input', 'type' => 'date',));
            } else {

                echo '<input name="data[FacilitiesRental][from][day]" type="hidden" value="'.$params[2].'"/>';
                echo '<input name="data[FacilitiesRental][from][month]" type="hidden" value="'.$params[1].'"/>';
                echo '<input name="data[FacilitiesRental][from][year]" type="hidden" value="'.$params[0].'"/>';
                echo '<input name="data[FacilitiesRental][facility_id]" type="hidden" value="'.$params[3].'"/>';
                echo '<div class="form-group"><label for="FacilitiesRentalFromMonth">Área</label><span>'.$facilities[$params[3]].'</span></div>';
                echo '<div class="form-group"><label for="FacilitiesRentalFromMonth">Fecha</label><span>'.$params[2].'-'.$params[1].'-'.$params[0].'</span></div>';
            }
        ?>

        <div class="well">
            <h5>
                
                <p>Al verificar esta casilla el solicitante acepta haber leído el contrato de arrendamiento del área común. Este formato deberá ser llenado y entregado en la administración previo al día del evento:</p>
                <a href="#"><span class="fa fa-download"></span> Descarga el Contrato de Arrendamiento</a>

            </h5>
            <?php
                echo $this->Form->input('terms_conditions', array( 'type' => 'checkbox', 'ng-click' => 'termsAccepted = !termsAccepted;', 'label' => 'Acepto el contrato'));
            ?>
        </div>

    </fieldset>
    <?php
        echo $this->Form->submit('Agregar',
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar', 'ng-if' => 'termsAccepted')
        );
        echo $this->Form->end(); 
    ?>
</div>
