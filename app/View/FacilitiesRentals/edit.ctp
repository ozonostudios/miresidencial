<h2 class="well">
    <span class="fa fa-list"></span>
    Editar Reservación de Área Común
</h2>

<div ng-controller="FacilitiesRentalsController" class="facilities-rentals form col-md-12">

    <?php echo $this->Form->create('FacilitiesRental', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
    <fieldset>
        <?php
        	echo $this->Form->input('id');
            echo $this->Form->input('facility_id', array('class' => 'form-control', 'label' => 'Área Común'));
            echo $this->Form->input('home_id', array('class' => 'form-control', 'label' => 'Solicitante'));
            echo $this->Form->input('from', array('label' => 'Fecha', 'class' => 'form-control date-input', 'type' => 'date'));
        ?>
        </fieldset>
    <?php
        echo $this->Form->submit('Editar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Editar')
        );
        echo $this->Form->end(); 
    ?>
</div>
