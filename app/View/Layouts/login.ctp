<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<!DOCTYPE html>
<html>

    <head>
        <?php echo $this->element('head'); ?>
    </head>
    <body ng-app="miResidencialApp" ng-controller="LoginController" class='login-layout' data-spy="scroll" data-target=".navbar" data-offset="100">

        <x-login-navbar></x-login-navbar>

        <div class='login-background' parallax-background parallax-ratio="1.6"></div>

        <?php echo $this->Flash->render(); ?>

        <div id="container row" class="col-md-10 col-md-offset-1">

            <div id="content">
                <?php echo $this->Flash->render(); ?>
                <?php echo $this->fetch('content'); ?>
            </div>

        </div>

        <x-platform-footer class='footer center'></x-platform-footer>

        <?php echo $this->element('analytics'); ?>

    </body>

</html>
