<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<!DOCTYPE html>
<html>
    
    <head>
        <?php echo $this->element('head'); ?>
    </head>
    <body ng-app="miResidencialApp" ng-controller="MainController" class="default-layout">
        
        <x-platform-navbar></x-platform-navbar>

        <div id="container-fluid">

            <div class="col-lg-2 col-md-2 col-sm-4 hidden-xs sidebar">
                <x-platform-sidebar></x-platform-sidebar>
            </div>

            <div id="content" class="col-lg-10 col-md-9 col-sm-8 col-xs-12 col-lg-offset-2 col-md-offset-3 col-sm-offset-4 main">
                <?php echo $this->Flash->render(); ?>
                <x-alerts-box></x-alerts-box>
                <?php echo $this->fetch('content'); ?>
            </div>             

        </div>
        
        <?php echo $this->element('analytics'); ?>

    </body>
</html>
