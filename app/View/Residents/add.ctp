<h2 class="well">
    <span class="fa fa-user"></span>
    Agregar Residente
</h2>

<div ng-controller="ResidentsController" class="residents form col-md-122">
    <?php echo $this->Form->create('Resident', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
    <fieldset>
    <?php
        echo $this->Form->input('id');
        echo $this->Form->input('first_name', array('class' => 'form-control', 'label' => 'Nombre'));
        echo $this->Form->input('last_name', array('class' => 'form-control', 'label' => 'Apellido'));
        echo $this->Form->input('resident_type_id', array('class' => 'form-control', 'label' => 'Tipo de Residente'));
        echo $this->Form->input('phone', array('class' => 'form-control', 'label' => 'Teléfono'));
        echo $this->Form->input('mobile', array('class' => 'form-control', 'label' => 'Celular'));
        echo $this->Form->input('mobile_2', array('class' => 'form-control', 'label' => 'Celular 2'));
        echo $this->Form->input('work_phone', array('class' => 'form-control', 'label' => 'Trabajo'));
        echo $this->Form->input('email', array('class' => 'form-control', 'label' => 'Email'));
        echo $this->Form->input('occupation', array('class' => 'form-control', 'label' => 'Opcupación'));
        echo $this->Form->input('website', array('class' => 'form-control', 'label' => 'Sitio Web'));
        echo $this->Form->input('additional_info', array('class' => 'form-control', 'label' => 'Información Adicional'));
    ?>
    </fieldset>
    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar')
        );
        echo $this->Form->end(); 
    ?>
</div>
