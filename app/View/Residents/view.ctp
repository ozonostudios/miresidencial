<h2 class="well">
    <span class="fa fa-user"></span>
    <?php echo h($resident['Resident']['first_name'])." ".h($resident['Resident']['last_name']); ?>
</h2>

<div ng-controller="ResidentsController" class="residents view">
    
    <div class="view-sidebar col-md-3">

        <x-file-upload-form folder='residents' id='<?php echo $resident['Resident']['id'];?>'></x-file-upload-form>
    
    </div>

    <ul class="list-group col-md-9 ">
        <li class="list-group-item">
            <small class="text-muted">Domicilios</small>
            
            <div>
                <?php if (!empty($resident['Home'])): ?>

                <table class="table table-striped table-hovered table-bordered">
                <tr>
                    <th class="col-md-3">Domicilio</th>
                    <th>Información</th>
                    <th class="col-md-2 actions">Acciones</th>
                </tr>
                <?php foreach ($resident['Home'] as $home): ?>
                    <tr>
                        <td>
                            <a href="#" ng-click="homeLocationModal(<?php echo $home['latitude']; ?>,<?php echo $home['longitude']; ?>)">
                                <span class="fa fa-map-o"></span>
                            </a>
                            <?php echo $home['street']." #".$home['number']; ?>
                        </td>

                        <td><?php echo $home['home_info']; ?></td>
                        <td class="actions center">
                            <a class="btn btn-info btn-sm" href="/casas/ver/<?php echo $home['id'];?>"><span class="fa fa-eye"></span></a>
                            <a class="btn btn-default btn-sm" href="/casas/editar/<?php echo $home['id'];?>"><span class="fa fa-edit"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </table>

                <?php else: ?>
                    <h3><?php echo h($resident['Resident']['first_name']);?> aun no tiene casa.</h3>
                <?php endif; ?>

            </div>


        </li>
        <li class="list-group-item">
            <small class="text-muted">Tipo de Residente</small class="text-muted">
            <h4><?php echo $resident['ResidentType']['resident_type']; ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Teléfono de Casa</small class="text-muted">
            <h4><?php echo h($resident['Resident']['phone']); ?></h4>
            <small class="text-muted">Celular</small class="text-muted">
            <h4><?php echo h($resident['Resident']['mobile']); ?></h4>
            <h4><?php echo h($resident['Resident']['mobile_2']); ?></h4>
            <small class="text-muted">Trabajo</small class="text-muted">
            <h4><?php echo h($resident['Resident']['work_phone']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Email</small class="text-muted">
            <h4><?php echo h($resident['Resident']['email']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Ocupación</small class="text-muted">
            <h4><?php echo h($resident['Resident']['occupation']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Sitio Web</small class="text-muted">
            <h4><?php echo h($resident['Resident']['website']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Información Adicional</small class="text-muted">
            <h4><?php echo h($resident['Resident']['additional_info']); ?></h4>
        </li>
    </ul>

</div>