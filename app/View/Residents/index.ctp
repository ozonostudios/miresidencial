<h2 class="well">
    <span class="fa fa-user"></span> Lista de Residentes
    <a class="btn btn-default btn-lg pull-right" href="/residentes/agregar"><span class="fa fa-plus"></span>Agregar Nuevo</a>
</h2>

<div class="residents index" ng-controller="ResidentsController">

    <x-quick-search-box searchfor="residents" label="Residentes"></x-quick-search-box>

    <table class="table table-hovered table-striped table-bordered">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('first_name', 'Nombre'); ?></th>
            <th>Domicilios</th>
            <th><?php echo $this->Paginator->sort('phone', 'Teléfonos'); ?></th>
            <th><?php echo $this->Paginator->sort('email'); ?></th>
            <th class="actions"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($residents as $resident): ?>
                <tr>
                    <td class="center"><?php echo h($resident['Resident']['id']); ?>&nbsp;</td>
                    <td><?php echo h($resident['Resident']['first_name'])." ".h($resident['Resident']['last_name']); ?>&nbsp;</td>
                    <td>
                        <?php 
                            foreach ($resident['Home'] as $home) {
                                echo '<a href="/casas/ver/'.$home["id"].'">'.$home["address"].'</a><br>';
                            } 
                        ?>
                    </td>
                    <td>
                        <?php if($resident['Resident']['phone']!=""){?><p><span class="fa fa-phone"></span><?php echo h($resident['Resident']['phone']); ?></p><?php } ?>
                        <?php if($resident['Resident']['mobile']!=""){?><p><span class="fa fa-mobile-phone"></span><?php echo h($resident['Resident']['mobile']); ?></p><?php } ?>
                    </td>
                    <td><?php echo h($resident['Resident']['email']); ?>&nbsp;</td>
                    <td class="actions center">
                        <a class="btn btn-info btn-sm" href="residentes/ver/<?php echo $resident['Resident']['id'];?>"><span class="fa fa-eye"></span></a>
                        <a class="btn btn-default btn-sm" href="residentes/editar/<?php echo $resident['Resident']['id'];?>"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?> 
    </blockquote>

    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'residentes', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>

</div>
