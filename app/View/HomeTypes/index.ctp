<div class="homeTypes index">
	<div class="panel-heading"><?php echo __('Home Types'); ?></div>
	<table class="table table-hovered table-striped table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('home_type'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($homeTypes as $homeType): ?>
	<tr>
		<td><?php echo h($homeType['HomeType']['id']); ?>&nbsp;</td>
		<td><?php echo h($homeType['HomeType']['home_type']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $homeType['HomeType']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $homeType['HomeType']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $homeType['HomeType']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $homeType['HomeType']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Home Type'), array('action' => 'add')); ?></li>
	</ul>
</div>
