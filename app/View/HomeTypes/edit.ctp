<div class="homeTypes form col-md-122">
<?php echo $this->Form->create('HomeType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Home Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('home_type');
		echo $this->Form->input('created');
		echo $this->Form->input('modified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('HomeType.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('HomeType.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Home Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Homes'), array('controller' => 'homes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Home'), array('controller' => 'homes', 'action' => 'add')); ?> </li>
	</ul>
</div>
