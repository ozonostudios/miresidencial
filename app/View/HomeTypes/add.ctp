<div class="homeTypes form col-md-122">
<?php echo $this->Form->create('HomeType'); ?>
	<fieldset>
		<legend><?php echo __('Add Home Type'); ?></legend>
	<?php
		echo $this->Form->input('home_type');
		echo $this->Form->input('created');
		echo $this->Form->input('modified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Home Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Homes'), array('controller' => 'homes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Home'), array('controller' => 'homes', 'action' => 'add')); ?> </li>
	</ul>
</div>
