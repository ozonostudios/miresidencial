<div class="homeTypes view panel panel-default">
<div class="panel-heading"><?php echo __('Home Type'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($homeType['HomeType']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Home Type'); ?></label>
		<dd>
			<?php echo h($homeType['HomeType']['home_type']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($homeType['HomeType']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Updated At'); ?></label>
		<dd>
			<?php echo h($homeType['HomeType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Home Type'), array('action' => 'edit', $homeType['HomeType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Home Type'), array('action' => 'delete', $homeType['HomeType']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $homeType['HomeType']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Home Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Home Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Homes'), array('controller' => 'homes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Home'), array('controller' => 'homes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Homes'); ?></h3>
	<?php if (!empty($homeType['Home'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Street'); ?></th>
		<th><?php echo __('Number'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('Resident Id'); ?></th>
		<th><?php echo __('Home Type Id'); ?></th>
		<th><?php echo __('Neighborhood Id'); ?></th>
		<th><?php echo __('Latitude'); ?></th>
		<th><?php echo __('Longitude'); ?></th>
		<th><?php echo __('Additional Info'); ?></th>
		<th><?php echo __('Home Info'); ?></th>
		<th><?php echo __('Created At'); ?></th>
		<th><?php echo __('Updated At'); ?></th>
		<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($homeType['Home'] as $home): ?>
		<tr>
			<td><?php echo $home['id']; ?></td>
			<td><?php echo $home['street']; ?></td>
			<td><?php echo $home['number']; ?></td>
			<td><?php echo $home['city']; ?></td>
			<td><?php echo $home['state']; ?></td>
			<td><?php echo $home['resident_id']; ?></td>
			<td><?php echo $home['home_type_id']; ?></td>
			<td><?php echo $home['neighborhood_id']; ?></td>
			<td><?php echo $home['latitude']; ?></td>
			<td><?php echo $home['longitude']; ?></td>
			<td><?php echo $home['additional_info']; ?></td>
			<td><?php echo $home['home_info']; ?></td>
			<td><?php echo $home['created']; ?></td>
			<td><?php echo $home['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'homes', 'action' => 'view', $home['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'homes', 'action' => 'edit', $home['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'homes', 'action' => 'delete', $home['id']), array('confirm' => __('Are you sure you want to delete # %s?', $home['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Home'), array('controller' => 'homes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
