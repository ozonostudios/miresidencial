<div class="expenseTypes form col-md-122">
<?php echo $this->Form->create('ExpenseType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Expense Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('expense_type');
		echo $this->Form->input('created');
		echo $this->Form->input('modified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ExpenseType.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ExpenseType.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Expense Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Expenses'), array('controller' => 'expenses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Expense'), array('controller' => 'expenses', 'action' => 'add')); ?> </li>
	</ul>
</div>
