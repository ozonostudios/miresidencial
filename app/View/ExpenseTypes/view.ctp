<div class="expenseTypes view panel panel-default">
<div class="panel-heading"><?php echo __('Expense Type'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($expenseType['ExpenseType']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Expense Type'); ?></label>
		<dd>
			<?php echo h($expenseType['ExpenseType']['expense_type']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($expenseType['ExpenseType']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Updated At'); ?></label>
		<dd>
			<?php echo h($expenseType['ExpenseType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Expense Type'), array('action' => 'edit', $expenseType['ExpenseType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Expense Type'), array('action' => 'delete', $expenseType['ExpenseType']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $expenseType['ExpenseType']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Expense Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Expense Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Expenses'), array('controller' => 'expenses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Expense'), array('controller' => 'expenses', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Expenses'); ?></h3>
	<?php if (!empty($expenseType['Expense'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Expense Type Id'); ?></th>
		<th><?php echo __('Service Provider Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Expense Details'); ?></th>
		<th><?php echo __('Created At'); ?></th>
		<th><?php echo __('Updated At'); ?></th>
		<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($expenseType['Expense'] as $expense): ?>
		<tr>
			<td><?php echo $expense['id']; ?></td>
			<td><?php echo $expense['expense_type_id']; ?></td>
			<td><?php echo $expense['service_provider_id']; ?></td>
			<td><?php echo $expense['amount']; ?></td>
			<td><?php echo $expense['expense_details']; ?></td>
			<td><?php echo $expense['created']; ?></td>
			<td><?php echo $expense['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'expenses', 'action' => 'view', $expense['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'expenses', 'action' => 'edit', $expense['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'expenses', 'action' => 'delete', $expense['id']), array('confirm' => __('Are you sure you want to delete # %s?', $expense['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Expense'), array('controller' => 'expenses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
