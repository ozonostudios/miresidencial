<div class="chargeTypes form col-md-122">
<?php echo $this->Form->create('ChargeType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Charge Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('charge_type');
		echo $this->Form->input('created');
		echo $this->Form->input('modified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ChargeType.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('ChargeType.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Charge Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Charges'), array('controller' => 'charges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Charge'), array('controller' => 'charges', 'action' => 'add')); ?> </li>
	</ul>
</div>
