<div class="chargeTypes view panel panel-default">
<div class="panel-heading"><?php echo __('Charge Type'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($chargeType['ChargeType']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Charge Type'); ?></label>
		<dd>
			<?php echo h($chargeType['ChargeType']['charge_type']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($chargeType['ChargeType']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Updated At'); ?></label>
		<dd>
			<?php echo h($chargeType['ChargeType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Charge Type'), array('action' => 'edit', $chargeType['ChargeType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Charge Type'), array('action' => 'delete', $chargeType['ChargeType']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $chargeType['ChargeType']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Charge Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Charge Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Charges'), array('controller' => 'charges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Charge'), array('controller' => 'charges', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Charges'); ?></h3>
	<?php if (!empty($chargeType['Charge'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Charge Type Id'); ?></th>
		<th><?php echo __('Home Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Charge Details'); ?></th>
		<th><?php echo __('Created At'); ?></th>
		<th><?php echo __('Updated At'); ?></th>
		<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($chargeType['Charge'] as $charge): ?>
		<tr>
			<td><?php echo $charge['id']; ?></td>
			<td><?php echo $charge['charge_type_id']; ?></td>
			<td><?php echo $charge['home_id']; ?></td>
			<td><?php echo $charge['amount']; ?></td>
			<td><?php echo $charge['charge_details']; ?></td>
			<td><?php echo $charge['created']; ?></td>
			<td><?php echo $charge['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'charges', 'action' => 'view', $charge['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'charges', 'action' => 'edit', $charge['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'charges', 'action' => 'delete', $charge['id']), array('confirm' => __('Are you sure you want to delete # %s?', $charge['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Charge'), array('controller' => 'charges', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
