<div class="announcements form col-md-122">
<?php echo $this->Form->create('Announcement'); ?>
	<fieldset>
		<legend><?php echo __('Edit Announcement'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('neighborhoods_id');
		echo $this->Form->input('announcement');
		echo $this->Form->input('created');
		echo $this->Form->input('modified_at');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Announcement.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Announcement.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Announcements'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Neighborhoods'), array('controller' => 'neighborhoods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Neighborhoods'), array('controller' => 'neighborhoods', 'action' => 'add')); ?> </li>
	</ul>
</div>
