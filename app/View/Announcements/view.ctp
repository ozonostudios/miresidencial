<div class="announcements view panel panel-default">
<div class="panel-heading"><?php echo __('Announcement'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($announcement['Announcement']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Neighborhoods'); ?></label>
		<dd>
			<?php echo $this->Html->link($announcement['Neighborhoods']['id'], array('controller' => 'neighborhoods', 'action' => 'view', $announcement['Neighborhoods']['id'])); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Announcement'); ?></label>
		<dd>
			<?php echo h($announcement['Announcement']['announcement']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($announcement['Announcement']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Modified At'); ?></label>
		<dd>
			<?php echo h($announcement['Announcement']['modified_at']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Announcement'), array('action' => 'edit', $announcement['Announcement']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Announcement'), array('action' => 'delete', $announcement['Announcement']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $announcement['Announcement']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Announcements'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Announcement'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Neighborhoods'), array('controller' => 'neighborhoods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Neighborhoods'), array('controller' => 'neighborhoods', 'action' => 'add')); ?> </li>
	</ul>
</div>
