<h2 class="well">
    <span class="fa fa-comment"></span> Avisos
</h2>

<div ng-controller="AnnouncementsController" class="announcements index">

    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="#" ng-click="isCollapsed = !isCollapsed">Enviar Nuevo Aviso</a>
        </div>
        <div class="panel-body" uib-collapse="isCollapsed">

            <div class="form col-md-122">
                <?php echo $this->Form->create('Announcement', array(
                    'class' => 'form-horizontal', 
                    'url' => 'add',
                    'inputDefaults' => array(
                        'div' => array(
                            'class' => 'form-group',
                        ),
                        'label' => array('class' => 'control-label col-lg-2')
                    )
                )); ?>

                <fieldset>
                <?php
                    echo $this->Form->input('announcement_types_id', array('class' => 'form-control', 'label' => 'Tipo de Anuncio'));
                    echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Titulo'));
                    echo $this->Form->input('announcement', array('class' => 'form-control', 'label' => 'Aviso'));
                    echo $this->Form->input('neighborhoods_id', array('type' => 'hidden', 'value' => $neighborhoods_id));
                ?>
                </fieldset>
                <?php
                    echo $this->Form->submit('Agregar', 
                        array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar')
                    );
                    echo $this->Form->end(); 
                ?>
            </div>
        </div>
    </div>

    <table class="table table-bordered table-hovered table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('title', 'Título'); ?></th>
            <th><?php echo $this->Paginator->sort('announcement', 'Anuncio'); ?></th>
            <th><?php echo $this->Paginator->sort('announcement_types_id', 'Tipo de Anuncio'); ?></th>
            <th><?php echo $this->Paginator->sort('created', 'Publicado'); ?></th>
            <th class="actions">Acciones</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($announcements as $announcement): ?>
                <tr>
                    <td><?php echo h($announcement['Announcement']['title']); ?></td>
                    <td><?php echo h($announcement['Announcement']['announcement']); ?></td>
                    <td><?php echo h($announcement['AnnouncementType']['announcement_type']); ?></td>
                    <td><?php echo h(date('d-m-Y', strtotime($announcement['Announcement']['created']))); ?></td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $announcement['Announcement']['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $announcement['Announcement']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $announcement['Announcement']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $announcement['Announcement']['id']))); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?> 
    </blockquote>

    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'avisos', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>