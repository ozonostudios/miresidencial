<div class="announcementTypes form col-md-122">
<?php echo $this->Form->create('AnnouncementType'); ?>
	<fieldset>
		<legend><?php echo __('Edit Announcement Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('announcement_type');
		echo $this->Form->input('created');
		echo $this->Form->input('modified_at');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AnnouncementType.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('AnnouncementType.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Announcement Types'), array('action' => 'index')); ?></li>
	</ul>
</div>
