<div class="announcementTypes view panel panel-default">
<div class="panel-heading"><?php echo __('Announcement Type'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($announcementType['AnnouncementType']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Announcement Type'); ?></label>
		<dd>
			<?php echo h($announcementType['AnnouncementType']['announcement_type']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($announcementType['AnnouncementType']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Modified At'); ?></label>
		<dd>
			<?php echo h($announcementType['AnnouncementType']['modified_at']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Announcement Type'), array('action' => 'edit', $announcementType['AnnouncementType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Announcement Type'), array('action' => 'delete', $announcementType['AnnouncementType']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $announcementType['AnnouncementType']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Announcement Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Announcement Type'), array('action' => 'add')); ?> </li>
	</ul>
</div>
