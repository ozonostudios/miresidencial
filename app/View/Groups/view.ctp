<div class="groups view panel panel-default">
<div class="panel-heading"><?php echo __('Group'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($group['Group']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Name'); ?></label>
		<dd>
			<?php echo h($group['Group']['name']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($group['Group']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Updated At'); ?></label>
		<dd>
			<?php echo h($group['Group']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Group'), array('action' => 'edit', $group['Group']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Group'), array('action' => 'delete', $group['Group']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $group['Group']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Group Permissions'), array('controller' => 'group_permissions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group Permission'), array('controller' => 'group_permissions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Group Permissions'); ?></h3>
	<?php if (!empty($group['GroupPermission'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('Permission Id'); ?></th>
		<th><?php echo __('Created At'); ?></th>
		<th><?php echo __('Updated At'); ?></th>
		<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($group['GroupPermission'] as $groupPermission): ?>
		<tr>
			<td><?php echo $groupPermission['group_id']; ?></td>
			<td><?php echo $groupPermission['permission_id']; ?></td>
			<td><?php echo $groupPermission['created']; ?></td>
			<td><?php echo $groupPermission['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'group_permissions', 'action' => 'view', $groupPermission['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'group_permissions', 'action' => 'edit', $groupPermission['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'group_permissions', 'action' => 'delete', $groupPermission['id']), array('confirm' => __('Are you sure you want to delete # %s?', $groupPermission['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Group Permission'), array('controller' => 'group_permissions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
