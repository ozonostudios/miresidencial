<div class="groups form col-md-122">
<?php echo $this->Form->create('Group'); ?>
	<fieldset>
		<legend><?php echo __('Edit Group'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('created');
		echo $this->Form->input('modified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Group.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Group.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Group Permissions'), array('controller' => 'group_permissions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group Permission'), array('controller' => 'group_permissions', 'action' => 'add')); ?> </li>
	</ul>
</div>
