<h2 class="well">
    <span class="fa fa-tree"></span>
    Área Común: <?php echo h($facility['Facility']['facility']); ?>
</h2>

<div ng-controller="FacilitiesController" class="facilities view">


    <div class="view-sidebar col-md-3">

        <x-file-upload-form folder="facilities" id="<?php echo h($facility['Facility']['id']); ?>"></x-file-upload-form>

        <?php echo $this->Html->link('Editar Área Común', array('action' => 'edit', $facility['Facility']['id']), array('class' => 'btn btn-primary btn-block')); ?>
        <?php echo $this->Html->link('Renta de Área Común', array('controller' => 'facilities_rentals', 'action' => 'add'), array('class' => 'btn btn-primary btn-block')); ?>
        
    </div>    

    <ul class="list-group col-md-9 ">

        <x-facility-map-location></x-facility-map-location>

        <li class="list-group-item">
            <small class="text-muted">Id</small>
            <h4><?php echo h($facility['Facility']['id']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Costo de Renta</small>
            <h4>$<?php echo h(number_format($facility['Facility']['rental_cost'],2)); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Información Adicional</small>
            <h4> <?php echo h($facility['Facility']['area_info']); ?></h4>
        </li>
    </ul>

</div>
