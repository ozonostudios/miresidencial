<h2 class="well">
    <span class="fa fa-tree"></span>
    Agregar Área Común
</h2>

<div ng-controller="FacilitiesController" class="facilities form col-md-122">

    <?php echo $this->Form->create('Facility', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
    <fieldset>
        <?php
            echo $this->Form->input('neighborhood_id', array('class' => 'form-control', 'label' => 'Fraccionamiento'));
            echo $this->Form->input('facility', array('class' => 'form-control', 'label' => 'Nombre del Área'));
            echo $this->Form->input('area_info', array('class' => 'form-control', 'label' => 'Información Adicional'));
            echo $this->Form->input('latitude', array('class' => 'form-control', 'label' => 'Latitud'));
            echo $this->Form->input('longitude', array('class' => 'form-control', 'label' => 'Longitud'));
            echo $this->Form->input('rental_cost', array('class' => 'form-control', 'label' => 'Costo de Renta'));
        ?>
        </fieldset>
    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar')
        );
        echo $this->Form->end(); 
    ?>
</div>
