<h2 class="well">
    <span class="fa fa-tree"></span> Lista de Áreas Comunes
    <a class="btn btn-default btn-lg pull-right" href="/areas/agregar"><span class="fa fa-plus"></span>Agregar Nueva</a>
    <a class="btn btn-default btn-lg pull-right" href="/renta_areas/agregar"><span class="fa fa-plus"></span>Reservar Área</a>
</h2>

<div class="facilities index" ng-controller="FacilitiesController">

    <x-quick-search-box searchfor="facilities" label="Áreas"></x-quick-search-box>

    <div class="well">
        <a href="/renta_areas"><span class="fa fa-list"></span> Ver Reservaciones</a>
    </div>    

    <table class="table table-hovered table-striped table-bordered">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('facility', 'Área Común'); ?></th>
            <th><?php echo $this->Paginator->sort('area_info', 'Información Adicional'); ?></th>
            <th><?php echo $this->Paginator->sort('rental_cost', 'Costo de Renta'); ?></th>
            <th class="actions"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($facilities as $facility): ?>
                <tr>
                    <td class="center"><?php echo h($facility['Facility']['id']); ?>&nbsp;</td>
                    <td><?php echo h($facility['Facility']['facility']); ?>&nbsp;</td>
                    <td><?php echo h($facility['Facility']['area_info']); ?>&nbsp;</td>
                    <td class="right">$<?php echo h(number_format($facility['Facility']['rental_cost'],2)); ?>&nbsp;</td>
                    <td class="actions center">
                        <a class="btn btn-info btn-sm" href="areas/ver/<?php echo $facility['Facility']['id'];?>"><span class="fa fa-eye"></span></a>
                        <a class="btn btn-default btn-sm" href="areas/editar/<?php echo $facility['Facility']['id'];?>"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <blockquote>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
        ));
        ?>
    </blockquote>
    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'areas', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
