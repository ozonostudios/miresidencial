<div class="neighborhoods index">
	<div class="panel-heading"><?php echo __('Neighborhoods'); ?></div>
	<table class="table table-hovered table-striped table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('neighborhood'); ?></th>
			<th><?php echo $this->Paginator->sort('website'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($neighborhoods as $neighborhood): ?>
	<tr>
		<td><?php echo h($neighborhood['Neighborhood']['id']); ?>&nbsp;</td>
		<td><?php echo h($neighborhood['Neighborhood']['neighborhood']); ?>&nbsp;</td>
		<td><?php echo h($neighborhood['Neighborhood']['website']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $neighborhood['Neighborhood']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $neighborhood['Neighborhood']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $neighborhood['Neighborhood']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $neighborhood['Neighborhood']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Neighborhood'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Facilities'), array('controller' => 'facilities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facility'), array('controller' => 'facilities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Homes'), array('controller' => 'homes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Home'), array('controller' => 'homes', 'action' => 'add')); ?> </li>
	</ul>
</div>
