<div class="neighborhoods form col-md-122">
<?php echo $this->Form->create('Neighborhood'); ?>
    <fieldset>
        <legend><?php echo __('Add Neighborhood'); ?></legend>
    <?php
        echo $this->Form->input('neighborhood');
        echo $this->Form->input('latitude');
        echo $this->Form->input('longitude');
        echo $this->Form->input('website');
        echo $this->Form->input('additional_info');
        echo $this->Form->input('monthly_fee');
        echo $this->Form->input('created');
        echo $this->Form->input('modified');
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>