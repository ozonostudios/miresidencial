<h2 class="well">
    <span class="fa fa-users"></span>
    Editar Fraccionamiento
</h2>

<div class="neighborhoods form col-md-122" ng-controller="NeighborhoodController">

    <div class="col-lg-3 col-md-5">
        <x-file-upload-form folder="neighborhoods" id="<?php echo $neighborhood_id; ?>"></x-file-upload-form>
    </div>
    <div class="col-lg-9 col-md-7">
        <?php echo $this->Form->create('Neighborhood', array(
                'class' => 'form-horizontal', 
                'enctype' => 'multipart/form-data',
                'inputDefaults' => array(
                    'div' => array(
                        'class' => 'form-group',
                    ),
                    'label' => array('class' => 'control-label col-lg-2')
                )
            )); ?>

        <fieldset>
            <?php
                echo $this->Form->input('id');
                echo $this->Form->input('neighborhood', array('readonly' => 'readonly', 'class' => 'form-control', 'label' => 'Nombre'));
                echo $this->Form->input('latitude', array('class' => 'form-control', 'label' => 'Latitud'));
                echo $this->Form->input('longitude', array('class' => 'form-control', 'label' => 'Longitud'));
                echo $this->Form->input('website', array('class' => 'form-control', 'label' => 'Sitio Web'));
                echo $this->Form->input('additional_info', array('class' => 'form-control', 'label' => 'Información'));
                echo $this->Form->input('monthly_fee', array('class' => 'form-control', 'label' => 'Cuota Mensual'));
                echo $this->Form->input('period_start', array('class' => 'form-control', 'label' => 'Día Inicio del Ciclo'));
                echo $this->Form->input('payments_before_default', array('class' => 'form-control', 'label' => 'Máximo Pagos Atrasados'));
                echo $this->Form->input('payment_on_agreement', array('class' => 'form-control', 'label' => 'Mínimo de pagos para Convenio'));
            ?>
        </fieldset>
    </div>

    <?php
        echo $this->Form->submit('Editar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Editar')
        );
        echo $this->Form->end(); 
    ?>

</div>

<div class="connect-account jumbotron col-md-12">
    <p><b>MiResidencial.mx</b> puede ayudar a facilitar muchas de las actividades de su cuenta vinculándola con los siguientes servicios. Conecte los necesarios en caso que en su residencial los utilicen.</p>
    <a href="#" class="btn btn-lg btn-info disabled"><span class="fa fa-paypal"></span> Conectar PayPal</a>
    <a href="#" class="btn btn-lg btn-info disabled"><span class="fa fa-dropbox"></span> Conectar Dropbox</a>
</div>
