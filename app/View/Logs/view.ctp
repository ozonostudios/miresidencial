<div class="logs view panel panel-default">
<div class="panel-heading"><?php echo __('Log'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($log['Log']['id']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Home'); ?></label>
		<dd>
			<?php echo $this->Html->link($log['Home']['street'], array('controller' => 'homes', 'action' => 'view', $log['Home']['id'])); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Log'); ?></label>
		<dd>
			<?php echo h($log['Log']['log']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Created At'); ?></label>
		<dd>
			<?php echo h($log['Log']['created']); ?>
			&nbsp;
		</dd>
		<label class="text-muted"><?php echo __('Updated At'); ?></label>
		<dd>
			<?php echo h($log['Log']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Log'), array('action' => 'edit', $log['Log']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Log'), array('action' => 'delete', $log['Log']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $log['Log']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Logs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Log'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Homes'), array('controller' => 'homes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Home'), array('controller' => 'homes', 'action' => 'add')); ?> </li>
	</ul>
</div>
