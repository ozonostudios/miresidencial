<?php echo $this->Html->charset(); ?>

<?php $siteDescription = 'Mi Residencial'; ?>

<title>
    <?php echo $siteDescription ?>:
    <?php echo $this->fetch('title'); ?>
</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
    /**
     * CSS vendors minified Files
     */
    echo $this->Html->css('libs/libraries.vendors');
    /**
     * Vendors Files, refer to Grunfile.js -> concat task
     */
    echo $this->Html->script('libs/libraries.vendors');        

    /**
     * If local environment
     * load readable libraries
     */
    if(strpos($_SERVER['SERVER_NAME'],'local') === 0 ) {
        /**
         * Simple CSS files separated
         */
        $dir_iterator = new RecursiveDirectoryIterator($_SERVER['DOCUMENT_ROOT']."/css/custom");
        $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

        foreach ($iterator as $file) {

            if( is_file($file) ) {

                $css_file = str_replace("\\","/",strstr($file, '/css'));
                echo $this->Html->css($css_file);
            }
        }
        /**
         * Simple JS files separated
         */
        $dir_iterator = new RecursiveDirectoryIterator($_SERVER['DOCUMENT_ROOT']."/js");
        $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

        foreach ($iterator as $file) {

            if( is_file($file) && strpos($file, '.js') > 0 ) {

                $js_file = str_replace("\\","/",strstr($file, '/js'));

                if(preg_match('/js\/components|js\/shared|js\/app/', $js_file)){

                    echo $this->Html->script($js_file);
                }
            }
        }
    } 
    /**
     * If local enironment, load all files from
     * shared and component folders separatedly
     */
    else {
        /**
         * JS concatenated file
         */
        echo $this->Html->script('libs/libraries.application.min');
        /**
         * CSS minified Files
         */
        echo $this->Html->css('libs/libraries.compiled.min');
    }
    /**
     * Include all Javascript Files
     */
    echo $this->fetch('script');
    /**
     * Include all CSS Files
     */
    echo $this->fetch('css');