<div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert"><i class="glyphicon glyphicon-remove"></i></button>
    <strong><?php echo h($message) ?></a>
</div>