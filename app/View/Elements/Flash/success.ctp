<div class="alert alert-dismissible alert-success">
    <button type="button" class="close" data-dismiss="alert"><i class="glyphicon glyphicon-remove"></i></button>
    <strong><?php echo h($message) ?></strong>
</div>