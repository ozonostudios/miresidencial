<div class="report">

    <div class="well row no-print">
        
        <div class="pull-left">
            <h2>
                <span class="fa fa-file"></span>
                Reportes - <?php echo $timeframe ?>
            </h2>
        </div>
        
        <div class="pull-right">
            <a href="#" onClick="print()" class="btn btn-info btn-lg"> <span class="fa fa-print"></span>Imprimir Reporte</a>
        </div>
    </div>

    <h3>Gastos</h3>

    <?php if(count($expenses)>0) {?>
        <?php $total =0; ?>
        <table class="table table-hovered table-striped table-bordered">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Cantidad</th>
                    <th>Detalles</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($expenses as $expense) { ?>
                    <tr>
                        <td class="center"><?php echo $expense['Expenses']['id'];?></td>
                        <td class="right">$<?php echo number_format($expense['Expenses']['amount'], 2);?></td>
                        <td><?php echo $expense['Expenses']['expense_details'];?></td>
                        <td><?php echo date('d-m-Y', strtotime($expense['Expenses']['created']));?></td>
                    </tr>
                    <?php $total += $expense['Expenses']['amount']; ?>
                <?php } ?>
            </tbody>
        </table>

        <blockquote>
            <small>Total Gastos</small>
            <h3>$<?php echo  number_format($total,2);?></h3>
        </blockquote>

    <?php } else { ?>
        <blockquote>No hay Gastos <?php echo $when; ?></blockquote>
    <?php } ?>

    <h3>Ingresos</h3>

    <?php if(count($payments)>0) {?>

        <?php $total =0; ?>
        <table class="table table-hovered table-striped table-bordered">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Cantidad</th>
                    <th>Detalles</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($payments as $payment) { ?>
                    <tr>
                        <td class="center"><?php echo $payment['Payments']['id'];?></td>
                        <td class="right">$<?php echo number_format($payment['Payments']['amount'], 2);?></td>
                        <td><?php echo $payment['Payments']['payment_details'];?></td>
                        <td><?php echo date('d-m-Y', strtotime($payment['Payments']['created']));?></td>
                    </tr>
                    <?php $total += $payment['Payments']['amount']; ?>                    

                <?php } ?>
            </tbody>
        </table>

        <blockquote>
            <small>Total Ingresos</small>
            <h3>$<?php echo  number_format($total,2);?></h3>
        </blockquote>

    <?php } else { ?>
        <blockquote>No hay Ingresos <?php echo $when; ?></blockquote>
    <?php } ?>


    <h3>Cargos</h3>

    <?php if(count($charges)>0) {?>

        <?php $total =0; ?>
        <table class="table table-hovered table-striped table-bordered">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Cantidad</th>
                    <th>Detalles</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($charges as $charge) { ?>
                    <tr>
                        <td class="center"><?php echo $charge['Charges']['id'];?></td>
                        <td class="right">$<?php echo number_format($charge['Charges']['amount'], 2);?></td>
                        <td><?php echo $charge['Charges']['charge_details'];?></td>
                        <td><?php echo date('d-m-Y', strtotime($charge['Charges']['created']));?></td>
                    </tr>
                    <?php $total += $charge['Charges']['amount']; ?>                    

                <?php } ?>
            </tbody>
        </table>

        <blockquote>
            <small>Total Cargos</small>
            <h3>$<?php echo  number_format($total,2);?></h3>
        </blockquote>

    <?php } else { ?>
        <blockquote>No hay cargos <?php echo $when; ?></blockquote>
    <?php } ?>

</div>
