<h2 class="well">
    <span class="fa fa-dashboard"></span>
    <span>Panel de Control</span>
    <span class="pull-right hidden-md">
        <x-admin-tasks></x-admin-tasks>
    </span>
</h2>

<div class="dashboard" ng-controller="DashboardController" ng-init="initData()">    
    
    <div class="row">
        <div class="col-lg-12">
            <x-quick-search-box searchfor="residents" label="Residentes"></x-quick-search-box>
        </div>
    </div>
        
    <div class="row">

        <div class="col-md-6 col-sm-12">
            <x-balance-total></x-balance-total>
        </div>

        <div class="col-md-6 col-sm-12">
            <x-balance-month></x-balance-month>
        </div>

    </div>

    <div class="row well">

        <div>
            <h4>
                <a href="#" ng-click="toggleCharts = !toggleCharts">
                    <i class="fa fa-line-chart"></i> 
                    <span ng-show="toggleCharts">Ver Gráficos</span>
                    <span ng-show="!toggleCharts">Ocultar Gráficos</span>
                </a>
            </h4>
        </div>
        <div uib-collapse="toggleCharts">
            <br>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <x-charts-pie></x-charts-pie>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
                <x-month-charges-payments></x-month-charges-payments>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12">
                <x-chart-monthly-balance></x-chart-monthly-balance>
            </div>        
        </div>

    </div>

    <div class="row">

        <div class="col-md-6 col-sm-12">
            <x-expenses-audit></x-expenses-audit>
        </div>

        <div class="col-md-6 col-sm-12">
            <x-bonifications></x-bonifications>
        </div>

    </div>
    
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <x-announcements></x-announcements>
        </div>

        
        <div class="col-md-6 col-sm-12">
            <x-facilities-availability></x-facilities-availability>
        </div>
    </div>

</div>