<h2 class="well">
    <span class="fa fa-users"></span>
    Agregar Usuario
</h2>

<div class="users form col-md-122" ng-controller="UsersController">

    <?php echo $this->Form->create('User', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>

    <fieldset>
        <?php
            echo $this->Form->input('id', array('class' => 'form-control'));
            echo $this->Form->input('group_id', array('class' => 'form-control', 'label' => 'Tipo de Usuario'));
            echo $this->Form->input('neighborhood_id', array('class' => 'form-control', 'label' => 'Residencial'));
            echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nombre'));
            echo $this->Form->input('email', array('class' => 'form-control', 'label' => 'Correo Electrónico'));
            echo $this->Form->input('username', array('class' => 'form-control', 'label' => 'Usuario'));
            echo $this->Form->input('password', array('class' => 'form-control', 'label' => 'Contraseña'));
            echo $this->Form->input('active', array('checked' => 'checked', 'label' => ' Usuario Activo'));
        ?>
    </fieldset>

    <div class="well">
        <h5>Asignar Residente</h5>
        <?php
            echo $this->Form->input('residents_id', array('class' => 'form-control', 'label' => 'Residente'));
        ?>
    </div>
    
    <div class="well">
        <h5>Notificaciones</h5>
        <?php
            echo $this->Form->input('suscribe_announcements', array('checked' => 'checked', 'label' => 'Publicación de Avisos'));
            echo $this->Form->input('suscribe_finance', array('checked' => 'checked', 'label' => 'Balance Financiero'));
        ?>
    </div>

    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar')
        );
        echo $this->Form->end(); 
    ?>
</div>
