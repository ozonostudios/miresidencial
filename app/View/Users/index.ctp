<h2 class="well">
    <span class="fa fa-users"></span> Lista de Usuarios
    <a class="btn btn-default btn-lg pull-right" href="/usuarios/agregar"><span class="fa fa-plus"></span>Agregar Nuevo</a>
</h2>


<div class="users index" ng-controller="UsersController">

    <table class="table table-hovered table-striped table-bordered">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
                <th><?php echo $this->Paginator->sort('email', 'Correo Electrónico'); ?></th>
                <th><?php echo $this->Paginator->sort('group_id', 'Tipo de Usuario'); ?></th>
                <th><?php echo $this->Paginator->sort('username', 'Usuario'); ?></th>
                <th><?php echo $this->Paginator->sort('active', 'Estado'); ?></th>
                <th class="actions">Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo h($user['User']['name']); ?></td>
                    <td><?php echo h($user['User']['email']); ?></td>
                    <td><?php echo $user['Group']['name']; ?></td>
                    <td><?php echo $user['User']['username']; ?></td>
                    <td><?php echo $user['User']['active'] ? 'Activo' : 'Suspendido'; ?></td>
                    <td class="actions center">
                        <a class="btn btn-default btn-sm" href="usuarios/editar/<?php echo $user['User']['id'];?>"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?> 
    </blockquote>

    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'usuarios', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
