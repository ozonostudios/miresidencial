<div class="users col-md-12">

    <div class='jumbotron heading row col-md-8 col-md-offset-2'>

        <h2>
            MiResidencial.mx
            <p>
                <small>
                    <a href="#login-form">
                        <span class='fa fa-sign-in'></span>
                        Iniciar Sesión
                    </a>
                </small>
            </p>
        </h2>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <img src="/img/platform/login-family.jpg"/>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <blockquote class="blockquote">
                Transparencia y Manejo Eficiente de los recursos del Fraccionamiento.
                <small>Administra y gestiona todo lo relacionado con tu fraccionamiento desde una sola aplicación: Seguridad, Finanzas, Áreas Comunes y Residentes.</small>
                <small>Conoce qué hacen tus cuotas y cómo ayudan a beneficiarte.</small>
            </blockquote>
        </div>

    </div>

    <a href='#' name='login-form' class='hidden-link col-md-12' id="login-form">Iniciar Sesión</a>

    <div class="jumbotron form col-md-8 row col-md-offset-2">
        <h2>Favor de Ingresar al Sistema</h2>
        <?php echo $this->Form->create('User', array(
                'class' => 'form-horizontal', 
                'inputDefaults' => array(
                    'div' => array(
                        'class' => 'form-group',
                    ),
                    'label' => array('class' => 'control-label col-md-0')
                )
            )); 
        ?>

        <fieldset>
            <?php 
                echo $this->Form->input('username', array('class' => 'form-control', 'label' => '', 'placeholder' => 'Usuario'));
                echo $this->Form->input('password', array('class' => 'form-control', 'label' => '', 'placeholder' => 'Contraseña'));
            ?>
        </fieldset>

        <?php
            echo $this->Form->submit('Ingresar', 
                array('class' => 'btn btn-default btn-lg', 'title' => 'Ingresar')
            );
            echo $this->Form->end(); 
        ?>

        <a class="btn btn-sm btn-block btn-primary" href='#'>¿Olvidaste tu Contraseña?</a>
    </div>

    <div class='well client-logos col-md-8 col-md-offset-2'>
        <img src="/img/platform/logos/corceles.png" />
    </div>
    
</div>

<div class="benefits jumbotron col-md-12">

    <a href='#' name='que_ofrece' id="que_ofrece">Manejo de Finanzas</a>
    <h2>¿Qué ofrece?</h2>

    <a href='#' name='finanzas' id="finanzas">Manejo de Finanzas</a>
    <h3>Manejo de Finanzas</h3>
    <div class='media'>
        <div class="media-left">
            <img class="media-object" src="/img/platform/login-finance.jpg"/>
        </div>
        <div class="media-body">
            <blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</blockquote>
        </div>
    </div>

    <a href='#' name='areas_comunes' id='areas_comunes'>Uso de Áreas Comunes</a>
    <h3>Uso de Áreas Comunes</h3>
    <div class='media'>
        <div class="media-body">
            <blockquote class='blockquote-reverse'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</blockquote>
        </div>
        <div class="media-right">
            <img class="media-object" src="/img/platform/login-facilities.jpg"/>
        </div>
    </div>

    <a href='#' name='residencias' id='residencias'>Inventario de Residencias</a>
    <h3>Inventario de Residencias</h3>
    <div class='media'>
        <div class="media-left">
            <img class="media-object" src="/img/platform/login-homes.jpg"/>
        </div>
        <div class="media-body">
            <blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</blockquote>
        </div>
    </div>

    <a href='#' name='residentes' id='residentes'>Panel para Residentes</a>
    <h3>Panel para Residentes</h3>
    <div class='media'>
        <div class="media-body">
            <blockquote class='blockquote-reverse'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</blockquote>
        </div>
        <div class="media-right">
            <img class="media-object" src="/img/platform/login-residents.jpg"/>
        </div>
    </div>

    <a href='#' name='seguridad' id='seguridad'>Panel para Caseta de Vigilancia</a>
    <h3>Panel para Caseta de Vigilancia</h3>
    <div class='media'>
        <div class="media-left">
            <img class="media-object" src="/img/platform/login-security.jpg"/>
        </div>
        <div class="media-body">
            <blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</blockquote>
        </div>
    </div>

</div>
