<h2 class="well">
    <span class="fa fa-users"></span>
    <?php echo $title?>
</h2>

<div class="col-lg-3 col-md-5">
    <x-file-upload-form folder="users" id="<?php echo $id;?>"></x-file-upload-form>
</div>

<div class="users form col-lg-9 col-md-7" ng-controller="UsersController">

    <?php echo $this->Form->create('User', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>

    <fieldset>
        <?php
            echo $this->Form->input('id', array('class' => 'form-control'));
            echo $this->Form->input('group_id', array('class' => 'form-control', 'label' => 'Tipo de Usuario'));
            echo $this->Form->input('neighborhood_id', array('class' => 'form-control', 'label' => 'Residencial'));
            echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nombre'));
            echo $this->Form->input('email', array('class' => 'form-control', 'label' => 'Correo Electrónico'));
            echo $this->Form->input('password', array('class' => 'form-control', 'label' => 'Contraseña'));
            echo $this->Form->input('active', array('label' => ' Usuario Activo'));
        ?>
    </fieldset>

    <div class="well">
        <h5>Asignar Residente</h5>
        <?php
            echo $this->Form->input('residents_id', array('class' => 'form-control', 'label' => 'Residente'));
        ?>
    </div>
    
    <div class="well">
        <h5>Notificaciones</h5>
        <?php
            echo $this->Form->input('suscribe_announcements', array('type' =>'checkbox', 'label' => 'Publicación de Avisos'));
            echo $this->Form->input('suscribe_finance', array('type' =>'checkbox', 'label' => 'Balance Financiero'));
            echo $this->Form->input('suscribe_payments', array('type' =>'checkbox', 'label' => 'Mis Pagos'));
        ?>
    </div>

    <?php
        echo $this->Form->submit('Editar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Editar')
        );
        echo $this->Form->end(); 
    ?>
</div>
