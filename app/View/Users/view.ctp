<div class="users view panel panel-default" ng-controller="UsersController">
<div class="panel-heading"><?php echo __('User'); ?></div>
	<dl class="panel-body">
		<label class="text-muted"><?php echo __('Id'); ?></label>
		<dd>
			<?php echo h($user['User']['id']); ?>
		</dd>
		<label class="text-muted"><?php echo __('User Type'); ?></label>
		<dd>
			<?php echo $user['Group']['name']; ?>
		</dd>
		<label class="text-muted"><?php echo __('Name'); ?></label>
		<dd>
			<?php echo h($user['User']['name']); ?>
		</dd>
		<label class="text-muted"><?php echo __('Email'); ?></label>
		<dd>
			<?php echo h($user['User']['email']); ?>
		</dd>
	</dl>
</div>