<div class="view">
    <h2 class= well">
        <span class="fa fa-suitcase"></span> Comprobante de Gasto
    </h2>

    <div class="view-sidebar col-md-3 no-print">
        <x-file-upload-form folder="expenses" id="<?php echo $expense['Expense']['id']; ?>"></x-file-upload-form>
    </div>

    <ul class="list-group col-md-9" ng-controller="PaymentsController">
        <li class="list-group-item">
            <label class="text-muted">Folio</label>
            <h4>#<?php echo h($expense['Expense']['id']); ?></h4>
        </li>
        <li class="list-group-item">
            <label class="text-muted">Tipo de Gasto</label>
            <h4><?php echo $expense['ExpenseType']['expense_type']; ?></h4>
        </li>
        <li class="list-group-item">
            <label class="text-muted">Tipo de Pago</label>
            <h4><?php echo $expense['PaymentType']['payment_type']; ?></h4>
        </li>
        <li class="list-group-item">
            <label class="text-muted">Detalles</label>
            <h4><?php echo h($expense['Expense']['expense_details']); ?></h4>
        </li>
        <li class="list-group-item">
            <label class="text-muted">Proveedor</label>
            <h4><?php echo $expense['ServiceProvider']['service_provider']; ?></h4>
        </li>
        <li class="list-group-item">
            <label class="text-muted">Cantidad</label>
            <h4>$<?php echo h(number_format($expense['Expense']['amount'],2)); ?></h4>
        </li>
        <li class="list-group-item">
            <label class="text-muted">Gasto hecho el</label>
            <h4><?php echo h(date('d-m-Y h:s',strtotime($expense['Expense']['created']))); ?></h4>
        </li>
    </ul>

    <a href="/inventario/agregar/<?php echo h($expense['Expense']['id']); ?>" class="btn btn-info btn-lg no-print"> <span class="fa fa-plus"></span>Agregar Inventario</a>
    <a href="#" onClick="print()" class="btn btn-info btn-lg no-print"> <span class="fa fa-print"></span>Imprimir Comprobante</a>
    <a href="#" ng-click="sendEmail()" class="btn btn-info btn-lg no-print"> <span class="fa fa-send"></span>Enviar por Correo</a>
</div>