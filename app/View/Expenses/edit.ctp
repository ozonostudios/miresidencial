<h2 class="well">
    <span class="fa fa-suitcase"></span>
    Editar Gasto
</h2>

<div class="view-sidebar col-md-3">
    <x-file-upload-form folder="expenses" id="<?php echo $id; ?>"></x-file-upload-form>
</div>

<div ng-controller="ExpensesController" class="expenses form col-md-9">
    <?php echo $this->Form->create('Expense', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
      <fieldset>
	<?php
        echo $this->Form->input('id');
		echo $this->Form->input('expense_type_id', array('class' => 'form-control', 'label' => 'Tipo de Gasto'));
        echo $this->Form->input('payment_type_id', array('class' => 'form-control', 'label' => 'Tipo de Pago'));
		echo $this->Form->input('service_provider_id', array('class' => 'form-control', 'label' => 'Proveedor'));
		echo $this->Form->input('amount', array('class' => 'form-control', 'label' => 'Cantidad'));
		echo $this->Form->input('expense_details', array('class' => 'form-control', 'label' => 'Detalles'));
		echo $this->Form->input('created', array('type' => 'hidden'));
		echo $this->Form->input('modified', array('type' => 'hidden'));
	?>
	</fieldset>
    <?php
        echo $this->Form->submit('Editar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Editar')
        );
        echo $this->Form->end(); 
    ?>
</div>

<div class="view-sidebar col-md-12">
    <a href="/inventario/agregar/<?php echo $id; ?>" class="btn btn-info btn-lg no-print"> <span class="fa fa-plus"></span>Agregar Inventario</a>
</div>
