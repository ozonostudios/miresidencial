<h2 class="well">
    <span class="fa fa-suitcase"></span>
    Agregar Gasto
</h2>

<div ng-controller="ExpensesController" class="expenses form col-md-122">
    <?php echo $this->Form->create('Expense', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
      <fieldset>
    <?php
        echo $this->Form->input('expense_type_id', array('class' => 'form-control', 'label' => 'Tipo de Gasto'));
        echo $this->Form->input('payment_type_id', array('class' => 'form-control', 'label' => 'Tipo de Pago'));
        echo $this->Form->input('service_provider_id', array('class' => 'form-control', 'label' => 'Proveedor'));
        echo $this->Form->input('amount', array('class' => 'form-control', 'label' => 'Cantidad'));
        echo $this->Form->input('expense_details', array('class' => 'form-control', 'label' => 'Detalles'));
    ?>
    </fieldset>

    <div class="well">
        <h5>¿Es un gasto inventariable?</h5>
        <?php
            echo $this->Form->input('add_inventory', array('type' =>'checkbox', 'label' => 'Agregar a Inventario'));
        ?>
    </div>

    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar')
        );
        echo $this->Form->end(); 
    ?>
</div>
