<h2 class="well">
    <span class="fa fa-suitcase"></span> Lista de Gastos
    <a class="btn btn-default btn-lg pull-right" href="/gastos/agregar"><span class="fa fa-plus"></span>Agregar Nuevo</a>
</h2>

<div class="expenses index" ng-controller="ExpensesController">

    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="#" ng-click="isCollapsed = !isCollapsed">
                <span class="fa" ng-class="{'fa-chevron-up' : isCollapsed, 'fa-chevron-down' : isCollapsed == false}"></span>
                Ver gráfico de gastos del <?php echo date('Y')?>
            </a>
        </div>
        <div class="panel-body" uib-collapse="isCollapsed">
            <x-line-chart show="expenses" type="bar" class="col-md-12"/>/>
        </div>
    </div>

    <table class="table table-hovered table-striped table-bordered">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'Folio'); ?></th>
                <th><?php echo $this->Paginator->sort('expense_type_id', 'Tipo de Gasto'); ?></th>
                <th><?php echo $this->Paginator->sort('payment_type_id', 'Tipo de Pago'); ?></th>
                <th><?php echo $this->Paginator->sort('service_provider_id', 'Proveedor'); ?></th>
                <th><?php echo $this->Paginator->sort('amount', 'Cantidad'); ?></th>
                <th><?php echo $this->Paginator->sort('expense_details', 'Detalles'); ?></th>
                <th><?php echo $this->Paginator->sort('created', 'Fecha'); ?></th>
                <th class="actions"><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($expenses as $expense): ?>
                <tr>
                    <td class="center"><?php echo h($expense['Expense']['id']); ?></td>
                    <td>
                        <?php echo $this->Html->link($expense['ExpenseType']['expense_type'], array('controller' => 'expense_types', 'action' => 'view', $expense['ExpenseType']['id'])); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($expense['PaymentType']['payment_type'], array('controller' => 'payment_types', 'action' => 'view', $expense['PaymentType']['id'])); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($expense['ServiceProvider']['service_provider'], array('controller' => 'service_providers', 'action' => 'view', $expense['ServiceProvider']['id'])); ?>
                    </td>
                    <td class="right">$<?php echo h(number_format($expense['Expense']['amount'],2)); ?></td>
                    <td><?php echo h($expense['Expense']['expense_details']); ?></td>
                    <td><?php echo h(date('d-m-Y', strtotime($expense['Expense']['created']))); ?></td>
                    <td class="actions center">
                        <?php if($expense['Expense']['image']!=""){?><a class="btn btn-info btn-sm" href="#" ng-click="showImageModal('<?php echo urlencode($expense['Expense']['image']);?>')"><span class="fa fa-photo"></span></a><?php } ?>
                        <a class="btn btn-info btn-sm" href="gastos/ver/<?php echo $expense['Expense']['id'];?>"><span class="fa fa-eye"></span></a>
                        <a class="btn btn-default btn-sm" href="gastos/editar/<?php echo $expense['Expense']['id'];?>"><span class="fa fa-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?> 
    </blockquote>

    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'gastos', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
