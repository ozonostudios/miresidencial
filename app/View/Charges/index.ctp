<h2 class="well">
    <span class="fa fa-suitcase"></span> Lista de Cargos
</h2>

<div class="charges index" ng-controller="ChargesController">

    <?php if($showChargesReminder) {?>
        <div class="panel panel-warning" ng-hide="hideReminder">
            <div class="panel-heading">
                <span class="fa fa-warning"></span> Recordatorio
            </div>
            <div class="panel-body">
                <p>La fecha de corte del mes de <?php echo $month ?> acaba de pasar, y aun no se envía el cargo por la mensualidad actual.</p>
                <p>¿desea enviar el cargo por mensualidad a los residentes?</p>
                <p><small>Esto les hará llegar la notificación a sus correos de que hay que pagar la mensualidad actual, además de recordarles el saldo.</small></p>
                <button ng-click="confirmCharges()"class="btn btn-success"><span class="fa fa-send"></span> Enviar Cargo</button>
            </div>
        </div>
    <?php } ?>

    <div class="panel panel-default xs-hidden">
        <div class="panel-heading">
            <a href="#" ng-click="isCollapsed = !isCollapsed">
                <span class="fa" ng-class="{'fa-chevron-up' : isCollapsed, 'fa-chevron-down' : isCollapsed == false}"></span>
                Ver gráfico de cargos del <?php echo date('Y')?>
            </a>
        </div>
        <div class="panel-body" uib-collapse="isCollapsed">
            <x-line-chart show="charges" type="bar" class="col-md-12"></x-line-chart>
        </div>
    </div>

    <x-month-charges-payments class="col-md-6"></x-month-charges-payments>
    <x-total-charges-payments class="col-md-6"></x-total-charges-payments>

    <table class="table table-hovered table-striped table-bordered">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id', 'Folio'); ?></th>
            <th><?php echo $this->Paginator->sort('charge_type_id', 'Tipo de Cargo'); ?></th>
            <th><?php echo $this->Paginator->sort('home_id', 'Domicilio'); ?></th>
            <th><?php echo $this->Paginator->sort('amount', 'Cantidad'); ?></th>
            <th><?php echo $this->Paginator->sort('charge_details', 'Detalles'); ?></th>
            <th><?php echo $this->Paginator->sort('created',' Creado'); ?></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($charges as $charge): ?>
                <tr>
                    <td><?php echo h($charge['Charge']['id']); ?></td>
                    <td>
                        <?php echo $charge['ChargeType']['charge_type']; ?>
                    </td>
                    <td>
                        <?php echo $charge['Home']['address']; ?>
                    </td>
                    <td class="right">$<?php echo h(number_format($charge['Charge']['amount'],2)); ?></td>
                    <td><?php echo h($charge['Charge']['charge_details']); ?></td>
                    <td><?php echo h(date('d-m-Y', strtotime($charge['Charge']['created']))); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?> 
    </blockquote>

    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'cargos', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
