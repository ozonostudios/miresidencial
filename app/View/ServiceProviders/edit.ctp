<h2 class="well">
    <span class="fa fa-truck"></span>
    Editar Proveedor
</h2>

<div ng-controller="ServiceProvidersController" class="serviceProviders form col-md-12">

    <?php echo $this->Form->create('ServiceProvider', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>


	<fieldset>

	<?php
		echo $this->Form->input('service_provider', array('class' => 'form-control', 'label' => 'Proveedor'));
		echo $this->Form->input('phone', array('class' => 'form-control', 'label' => 'Teléfono'));
		echo $this->Form->input('mobile', array('class' => 'form-control', 'label' => 'Celular'));
		echo $this->Form->input('mobile_2', array('class' => 'form-control', 'label' => 'Celular 2'));
		echo $this->Form->input('work_phone', array('class' => 'form-control', 'label' => 'Oficina'));
		echo $this->Form->input('email', array('class' => 'form-control', 'label' => 'Correo Electrónico'));
		echo $this->Form->input('website', array('class' => 'form-control', 'label' => 'Sitio Web'));
		echo $this->Form->input('status', array('class' => 'form-control', 'label' => 'Estado'));
		echo $this->Form->input('additional_info', array('class' => 'form-control', 'label' => 'Información Adicional'));
	?>
	</fieldset>
    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Editar')
        );
        echo $this->Form->end(); 
    ?>
</div>