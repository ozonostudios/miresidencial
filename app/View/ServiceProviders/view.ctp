<h2 class="well">
    <span class="fa fa-truck"></span>
    <?php echo h($serviceProvider['ServiceProvider']['service_provider']); ?>
</h2>

<div ng-controller="ServiceProvidersController" class="serviceProviders view">

    <ul class="list-group col-md-12 ">

        <li class="list-group-item">
            <small class="text-muted">Tipo de Residente</small class="text-muted">
            <h4><?php echo $serviceProvider['ServiceProvider']['service_provider']; ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Teléfono de Casa</small class="text-muted">
            <h4><?php echo h($serviceProvider['ServiceProvider']['phone']); ?></h4>
            <small class="text-muted">Celular</small class="text-muted">
            <h4><?php echo h($serviceProvider['ServiceProvider']['mobile']); ?></h4>
            <h4><?php echo h($serviceProvider['ServiceProvider']['mobile_2']); ?></h4>
            <small class="text-muted">Trabajo</small class="text-muted">
            <h4><?php echo h($serviceProvider['ServiceProvider']['work_phone']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Email</small class="text-muted">
            <h4><?php echo h($serviceProvider['ServiceProvider']['email']); ?></h4>
            <h4><?php echo h($serviceProvider['ServiceProvider']['website']); ?></h4>
        </li>
        <li class="list-group-item">
            <small class="text-muted">Información Adicional</small class="text-muted">
            <h4><?php echo h($serviceProvider['ServiceProvider']['additional_info']); ?></h4>
        </li>
    </ul>

    <div class="related row col-md-12">
    <?php if (!empty($serviceProvider['Expense'])): ?>


        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseExpenses" aria-expanded="true" aria-controls="collapseExpenses">
                            <span class="glyphicon glyphicon-list-alt"></span> Compras a <?php echo $serviceProvider['ServiceProvider']['service_provider']; ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseExpenses" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">

                        <table class="table table-bodered table-striped table-hovered">
                            <tr>
                                <th>Folio</th>
                                <th>Monto</th>
                                <th>Detalles</th>
                                <th>Fecha</th>
                                <th class="actions">Acciones</th>
                            </tr>
                            <?php foreach ($serviceProvider['Expense'] as $expense): ?>
                                <tr>
                                    <td class="center"><?php echo $expense['id']; ?></td>
                                    <td class="right">$<?php echo number_format($expense['amount'],2); ?></td>
                                    <td><?php echo $expense['expense_details']; ?></td>
                                    <td><?php echo date('d-m-Y',strtotime($expense['created'])); ?></td>
                                    <td class="actions">
                                        <a class="btn btn-info btn-sm" href="/gastos/ver/<?php echo $expense['id'];?>"><span class="fa fa-eye"></span></a>
                                        <a class="btn btn-default btn-sm" href="/gastos/editar/<?php echo $expense['id'];?>"><span class="fa fa-edit"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    <?php endif; ?>
    </div>
</div>

