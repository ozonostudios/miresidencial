<h2 class="well">
    <span class="fa fa-truck"></span> Lista de Proveedores
    <a class="btn btn-default btn-lg pull-right" href="/proveedores/agregar"><span class="fa fa-plus"></span>Agregar Nuevo</a>
</h2>

<div class="serviceProviders index" ng-controller="ServiceProvidersController">

    <x-quick-search-box searchfor="serviceProviders" label="Proveedores"></x-quick-search-box>

    <table class="table table-hovered table-striped table-bordered">
        <thead>
			<tr>
					<th><?php echo $this->Paginator->sort('service_provider', 'Proveedor'); ?></th>
					<th><?php echo $this->Paginator->sort('phone', 'Teléfono'); ?></th>
					<th><?php echo $this->Paginator->sort('mobile', 'Celular'); ?></th>
					<th><?php echo $this->Paginator->sort('work_phone','Oficina'); ?></th>
                    <th><?php echo $this->Paginator->sort('email', 'Correo Electrónico'); ?></th>
					<th><?php echo $this->Paginator->sort('website', 'Sitio'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
			</tr>        
        </thead>
	<tbody>
		<?php foreach ($serviceProviders as $serviceProvider): ?>
		<tr>
			<td><?php echo h($serviceProvider['ServiceProvider']['service_provider']); ?>&nbsp;</td>
			<td><?php echo h($serviceProvider['ServiceProvider']['phone']); ?>&nbsp;</td>
			<td><?php echo h($serviceProvider['ServiceProvider']['mobile']); ?>&nbsp;</td>
			<td><?php echo h($serviceProvider['ServiceProvider']['work_phone']); ?>&nbsp;</td>
			<td><?php echo h($serviceProvider['ServiceProvider']['email']); ?>&nbsp;</td>
			<td><?php echo h($serviceProvider['ServiceProvider']['website']); ?>&nbsp;</td>
            <td class="actions center">
                <a class="btn btn-info btn-sm" href="proveedores/ver/<?php echo $serviceProvider['ServiceProvider']['id'];?>"><span class="fa fa-eye"></span></a>
                <a class="btn btn-default btn-sm" href="proveedores/editar/<?php echo $serviceProvider['ServiceProvider']['id'];?>"><span class="fa fa-edit"></span></a>
            </td>
		</tr>
		<?php endforeach; ?>
	</tbody>
    </table>
    <blockquote>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
        ));
        ?>
    </blockquote>
    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'proveedores', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
