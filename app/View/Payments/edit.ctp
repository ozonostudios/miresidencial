<h2 class="well">
    <span class="fa fa-suitcase"></span>
    Editar Pago
</h2>

<div ng-controller="PaymentsController" class="residents form col-md-122">
    <?php echo $this->Form->create('Payment', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
        
    <fieldset>
        <?php
            echo $this->Form->input('payment_type_id', array('class' => 'form-control', 'label' => 'Tipo de Pago'));
            echo $this->Form->input('home_id', array('class' => 'form-control', 'label' => 'Domicilio'));
            echo $this->Form->input('amount', array('class' => 'form-control', 'label' => 'Cantidad'));
            echo $this->Form->input('payment_details', array('class' => 'form-control', 'label' => 'Detalles'));
            echo $this->Form->input('bonification_approved', array( 'type' => 'checkbox', 'label' => 'Aprobar Bonificación'));
            echo $this->Form->input('id', array('type' => 'hidden'));
            echo $this->Form->input('created', array('type' => 'hidden'));
            echo $this->Form->input('modified', array('type' => 'hidden'));
        ?>
    </fieldset>

    <?php
        echo $this->Form->submit('Editar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Editar')
        );
        echo $this->Form->end(); 
    ?>
</div>
