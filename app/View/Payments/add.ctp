<h2 class="well">
    <span class="fa fa-suitcase"></span>
    Agregar Pago
</h2>

<div ng-controller="PaymentsController" class="residents form col-md-122">
    <?php echo $this->Form->create('Payment', array(
            'class' => 'form-horizontal', 'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group',
                ),
                'label' => array('class' => 'control-label col-lg-2')
            )
        )); ?>
    <fieldset>
	<?php
		echo $this->Form->input('payment_type_id', array('class' => 'form-control', 'label' => 'Tipo de Pago'));
		echo $this->Form->input('home_id', array('class' => 'form-control', 'label' => 'Domicilio'));
		echo $this->Form->input('amount', array('class' => 'form-control', 'label' => 'Cantidad'));
        echo $this->Form->input('payment_details', array('class' => 'form-control', 'label' => 'Detalles'));
		echo $this->Form->input('created', array('class' => 'form-control', 'label' => 'Fecha de Pago'));
    ?>
    </fieldset>

    <div class="well">
        <h5><span class="fa fa-send"></span> Notificar al residente por correo electrónico del pago:</h5>
        <?php
            echo $this->Form->input('send_email', array( 'type' => 'checkbox', 'checked' => 'checked', 'label' => 'Enviar Recibo'));
        ?>
    </div>

    <?php
        echo $this->Form->submit('Agregar', 
            array('class' => 'btn btn-default btn-lg pull-left', 'title' => 'Agregar')
        );
        echo $this->Form->end(); 
    ?>
</div>
