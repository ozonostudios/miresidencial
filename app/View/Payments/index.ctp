<h2 class="well">
    <span class="fa fa-suitcase"></span> Lista de Pagos
    <a class="btn btn-default btn-lg pull-right" href="/pagos/agregar"><span class="fa fa-plus"></span>Agregar Nuevo</a>
</h2>

<div class="payments index" ng-controller="PaymentsController">

    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="#" ng-click="isCollapsed = !isCollapsed">
                <span class="fa" ng-class="{'fa-chevron-up' : isCollapsed, 'fa-chevron-down' : isCollapsed == false}"></span>
                Ver gráfico de pagos del <?php echo date('Y')?>
            </a>
        </div>
        <div class="panel-body" uib-collapse="isCollapsed">
            <x-line-chart show="payments" type="bar" class="col-md-12"/>/>
        </div>
    </div>

    <table class="table table-hovered table-striped table-bordered">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'Recibo'); ?></th>
                <th><?php echo $this->Paginator->sort('payment_type_id', 'Tipo de Pago'); ?></th>
                <th><?php echo $this->Paginator->sort('home_id', 'Domicilio'); ?></th>
                <th><?php echo $this->Paginator->sort('amount', 'Cantidad'); ?></th>
                <th><?php echo $this->Paginator->sort('payment_details', 'Detalles'); ?></th>
                <th class="actions">Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($payments as $payment): ?>
            <tr>
                <td class="center"><?php echo h($payment['Payment']['id']); ?></td>
                <td>
                    <?php echo $this->Html->link($payment['PaymentType']['payment_type'], array('controller' => 'payment_types', 'action' => 'view', $payment['PaymentType']['id'])); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($payment['Home']['address'], array('controller' => 'homes', 'action' => 'view', $payment['Home']['id'])); ?>
                </td>
                <td class="right">$<?php echo h(number_format($payment['Payment']['amount'],2)); ?></td>
                <td><?php echo h($payment['Payment']['payment_details']); ?></td>
                <td class="actions center">
                    <a class="btn btn-default btn-sm" href="pagos/editar/<?php echo $payment['Payment']['id'];?>"><span class="fa fa-edit"></span></a>
                    <a class="btn btn-info btn-sm" href="pagos/ver/<?php echo $payment['Payment']['id'];?>"><span class="fa fa-print"></span></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <blockquote>
        <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}')
            ));
        ?> 
    </blockquote>

    <ul class="pagination">
        <?php
            $this->Paginator->options['url'] = array('controller' => 'pagos', 'action' => '/index/');
            echo '<li>'.$this->Paginator->prev('<' , array(), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
            echo '<li>'.$this->Paginator->next('>', array(), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a')).'</li>';
        ?>
    </ul>
</div>
