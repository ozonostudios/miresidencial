<h2 class=" well">
    <span class="fa fa-suitcase"></span> Recibo de Pago
</h2>

<ul class="list-group" ng-controller="PaymentsController">
    <li class="list-group-item">
        <label class="text-muted">Folio</label>
        <h4>#<?php echo h($payment['Payment']['id']); ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Tipo de Pago</label>
        <h4><?php echo $payment['PaymentType']['payment_type']; ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Domicilio</label>
        <h4><?php echo $payment['Home']['street']; ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Cantidad</label>
        <h4>$<?php echo h(number_format($payment['Payment']['amount'],2)); ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Detalles</label>
        <h4><?php echo h($payment['Payment']['payment_details']); ?></h4>
    </li>
    <li class="list-group-item">
        <label class="text-muted">Pagado el</label>
        <h4><?php echo h(date('d-m-Y h:s',strtotime($payment['Payment']['created']))); ?></h4>
    </li>
</ul>

<a href="#" onClick="print()" class="btn btn-info btn-lg no-print"> <span class="fa fa-print"></span>Imprimir Recibo</a>
