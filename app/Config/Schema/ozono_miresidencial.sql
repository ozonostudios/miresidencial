-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-09-2016 a las 08:24:08
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ozono_miresidencial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhoods_id` int(10) UNSIGNED NOT NULL,
  `announcement_types_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `announcement` text CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `announcements`
--

INSERT INTO `announcements` (`id`, `neighborhoods_id`, `announcement_types_id`, `title`, `announcement`, `created`, `modified`) VALUES
(3, 1, 1, 'Bienvenida', 'Primer anuncio para fraccionamiento', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(4, 1, 2, 'Renta Áreas Verdes', 'Otro anuncio de prueba', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(5, 1, 3, 'Recarpeteo de Calles', 'Prueba de Logro en el fraccionamiento.', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(6, 1, 1, 'Pago de Cuotas', 'A partir de hoy el pago de cuotas se hará solo de manera electrónica.', '2016-03-23 00:00:00', '2016-03-23 00:00:00'),
(7, 1, 2, 'Cierre de caseta', 'Temporalmente estará cerrada la caseta el día de hoy.', '2016-03-23 00:00:00', '2016-03-23 00:00:00'),
(8, 1, 2, 'Prueba de Mensaje', 'Primer mensaje desde el formulario', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `announcement_types`
--

CREATE TABLE `announcement_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `announcement_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `class` varchar(50) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `announcement_types`
--

INSERT INTO `announcement_types` (`id`, `announcement_type`, `class`, `created`, `modified`) VALUES
(1, 'General', 'default', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(2, 'Urgente', 'danger', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(3, 'Logro', 'success', '2016-03-10 00:00:00', '2016-03-10 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `charges`
--

CREATE TABLE `charges` (
  `id` int(10) UNSIGNED NOT NULL,
  `charge_type_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `charge_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `charges`
--

INSERT INTO `charges` (`id`, `charge_type_id`, `home_id`, `amount`, `charge_details`, `created`, `modified`) VALUES
(1, 4, 20, 362.00, 'Nihil cum.', '2016-01-13 13:10:09', '2016-01-13 13:10:09'),
(2, 4, 14, 355.00, 'Voluptatem.', '2016-01-13 13:10:09', '2016-01-13 13:10:09'),
(3, 4, 6, 372.00, 'Et sit ullam.', '2016-01-13 13:10:09', '2016-01-13 13:10:09'),
(4, 3, 18, 367.00, 'Ullam ut ut veniam.', '2016-01-13 13:10:09', '2016-01-13 13:10:09'),
(5, 4, 4, 370.00, 'Et modi doloremque.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(6, 3, 7, 350.00, 'Suscipit blanditiis.', '2016-03-13 13:10:10', '2016-01-13 13:10:10'),
(7, 2, 15, 375.00, 'Aliquam distinctio.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(8, 4, 2, 367.00, 'Quisquam.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(9, 5, 18, 375.00, 'Blanditiis ab.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(10, 4, 17, 393.00, 'Sunt magnam.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(11, 3, 12, 364.00, 'Adipisci natus.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(12, 5, 20, 396.00, 'Eius aut aut nam.', '2016-03-13 13:10:10', '2016-01-13 13:10:10'),
(13, 2, 11, 359.00, 'Maiores praesentium.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(14, 5, 4, 368.00, 'Quisquam delectus.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(15, 3, 9, 360.00, 'Omnis nulla eveniet.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(16, 4, 1, 354.00, 'Laboriosam sed unde.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(17, 3, 9, 389.00, 'Quam accusamus.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(18, 4, 17, 358.00, 'Rerum.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(19, 3, 17, 363.00, 'Et adipisci aliquam.', '2016-03-09 13:10:10', '2016-01-13 13:10:10'),
(20, 3, 15, 387.00, 'Officia non eveniet.', '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(42, 1, 1, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(43, 1, 2, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(44, 1, 3, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(45, 1, 4, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(46, 1, 5, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(47, 1, 6, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(48, 1, 7, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(49, 1, 8, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(50, 1, 9, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(51, 1, 10, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(52, 1, 11, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(53, 1, 12, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(54, 1, 13, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(55, 1, 14, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(56, 1, 15, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(57, 1, 16, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(58, 1, 17, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(59, 1, 18, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(60, 1, 19, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(61, 1, 20, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(62, 1, 21, 350.00, 'Mensualidad de April', '2016-04-16 15:41:41', '2016-04-16 15:41:41'),
(63, 1, 1, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(64, 1, 2, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(65, 1, 3, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(66, 1, 4, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(67, 1, 5, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(68, 1, 6, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(69, 1, 7, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(70, 1, 8, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(71, 1, 9, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(72, 1, 10, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(73, 1, 11, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(74, 1, 12, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(75, 1, 13, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(76, 1, 14, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(77, 1, 15, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(78, 1, 16, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(79, 1, 17, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(80, 1, 18, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(81, 1, 19, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(82, 1, 20, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(83, 1, 21, 350.00, 'Mensualidad de May', '2016-05-14 16:18:22', '2016-05-14 16:18:22'),
(84, 1, 1, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(85, 1, 2, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(86, 1, 3, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(87, 1, 4, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(88, 1, 5, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(89, 1, 6, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(90, 1, 7, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(91, 1, 8, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(92, 1, 9, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(93, 1, 10, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(94, 1, 11, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(95, 1, 12, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(96, 1, 13, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(97, 1, 14, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(98, 1, 15, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(99, 1, 16, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(100, 1, 17, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(101, 1, 18, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(102, 1, 19, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(103, 1, 20, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(104, 1, 21, 350.00, 'Mensualidad de June', '2016-06-09 21:54:50', '2016-06-09 21:54:50'),
(105, 1, 1, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(106, 1, 2, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(107, 1, 3, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(108, 1, 4, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(109, 1, 5, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(110, 1, 6, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(111, 1, 7, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(112, 1, 8, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(113, 1, 9, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(114, 1, 10, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(115, 1, 11, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(116, 1, 12, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(117, 1, 13, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(118, 1, 14, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(119, 1, 15, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(120, 1, 16, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(121, 1, 17, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(122, 1, 18, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(123, 1, 19, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(124, 1, 20, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26'),
(125, 1, 21, 350.00, 'Mensualidad de September', '2016-09-22 23:23:26', '2016-09-22 23:23:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `charge_types`
--

CREATE TABLE `charge_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `charge_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `charge_types`
--

INSERT INTO `charge_types` (`id`, `charge_type`, `created`, `modified`) VALUES
(1, 'Mensualidad', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(2, 'Recargo', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(3, 'Renta de Área Común', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(4, 'Pago Daños', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(5, 'Aportación a Mejoras', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(6, 'Multa', '2016-04-16 16:02:00', '2016-04-16 16:02:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(11) UNSIGNED NOT NULL,
  `expense_type_id` int(10) UNSIGNED NOT NULL,
  `payment_type_id` int(10) UNSIGNED NOT NULL,
  `service_provider_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `expense_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `expenses`
--

INSERT INTO `expenses` (`id`, `neighborhood_id`, `expense_type_id`, `payment_type_id`, `service_provider_id`, `amount`, `expense_details`, `image`, `created`, `modified`) VALUES
(1, 1, 3, 1, 11, 972.00, 'Reprehenderit nihil et alias amet sequi et.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(2, 1, 9, 1, 7, 943.00, 'Quis aut laboriosam odit amet adipisci.', NULL, '2016-04-13 13:10:10', '2016-01-13 13:10:10'),
(3, 1, 10, 1, 10, 914.00, 'Sit sunt placeat vel odio.', NULL, '2016-04-13 13:10:10', '2016-01-13 13:10:10'),
(4, 1, 2, 1, 20, 947.00, 'Natus at eos odit provident odit quis.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(5, 1, 4, 1, 17, 957.00, 'Itaque qui corrupti veritatis repudiandae.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(6, 1, 10, 1, 11, 916.00, 'Omnis dolore odio nobis.', NULL, '2016-03-13 13:10:10', '2016-01-13 13:10:10'),
(7, 1, 2, 1, 4, 915.00, 'Consequatur sed odio aut dolorem.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(8, 1, 5, 1, 6, 997.00, 'Molestiae saepe quis et vero ullam rem.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(9, 1, 7, 1, 10, 808.00, 'Qui omnis sapiente quibusdam ut totam quo.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(10, 1, 7, 1, 18, 942.00, 'Sed et sed unde dicta architecto et dolores.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(11, 1, 9, 2, 10, 909.00, 'Alias officiis eos sequi.', NULL, '2016-02-13 13:10:10', '2016-01-13 13:10:10'),
(12, 1, 9, 2, 12, 822.00, 'Sint blanditiis nesciunt maiores unde amet.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(13, 1, 4, 2, 3, 815.00, 'Suscipit aut qui ut dolor incidunt non iure.', NULL, '2016-01-13 13:10:10', '2016-01-13 13:10:10'),
(14, 1, 10, 2, 7, 947.00, 'Et qui reprehenderit delectus ut voluptatem.', NULL, '2016-05-13 13:10:10', '2016-01-13 13:10:10'),
(15, 1, 2, 2, 5, 999.00, 'Aut nesciunt sunt eos provident.', NULL, '2016-01-13 13:10:11', '2016-01-13 13:10:11'),
(16, 1, 10, 2, 15, 955.00, 'Nihil qui sed esse qui minima numquam nostrum.', NULL, '2016-01-13 13:10:11', '2016-01-13 13:10:11'),
(17, 1, 8, 2, 16, 817.00, 'Consequatur quibusdam quia aut.', NULL, '2016-02-13 13:10:11', '2016-01-13 13:10:11'),
(18, 1, 8, 2, 20, 862.00, 'Aut repellat et optio qui enim aspernatur.', NULL, '2016-05-13 13:10:11', '2016-01-13 13:10:11'),
(19, 1, 9, 2, 11, 923.00, 'Quia culpa sit officiis minima nulla est magnam.', NULL, '2016-03-23 13:10:11', '2016-01-13 13:10:11'),
(20, 1, 2, 2, 14, 958.00, 'Aut qui consequuntur molestiae ipsa.', NULL, '2016-03-23 13:10:11', '2016-01-13 13:10:11'),
(21, 0, 1, 1, 1, 520.00, 'Prueba', NULL, '2016-04-16 00:15:47', '2016-04-16 00:15:47'),
(22, 0, 1, 1, 1, 345.00, 'aa', NULL, '2016-04-16 00:16:19', '2016-04-16 00:16:19'),
(23, 1, 9, 1, 7, 350.00, 'Cámaras de vigilancia', 'img\\expense\\23\\DSC02593.JPG', '2016-04-16 15:37:50', '2016-05-14 19:46:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expense_types`
--

CREATE TABLE `expense_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `expense_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `expense_types`
--

INSERT INTO `expense_types` (`id`, `expense_type`, `created`, `modified`) VALUES
(1, 'Administrativo', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(2, 'Jardinería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(3, 'Seguridad', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(4, 'Equimamiento', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(5, 'Pavimentación', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(6, 'Alberca', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(7, 'Plomería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(8, 'Albañilería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(9, 'Computación', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(10, 'Transporte', '2016-01-13 13:10:01', '2016-01-13 13:10:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `facility` varchar(255) CHARACTER SET utf8 NOT NULL,
  `area_info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rental_cost` double(19,4) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `facilities`
--

INSERT INTO `facilities` (`id`, `neighborhood_id`, `facility`, `area_info`, `latitude`, `longitude`, `rental_cost`, `image`, `created`, `modified`) VALUES
(1, 1, 'Entrada', 'Área ubicada al costado derecho de la caseta de vigilancia. Cuenta con baño y kiosko.', '29.076097', '-111.026481', 2000.0000, 'img\\facilities\\1\\DSC03008.JPG', '2016-01-13 13:10:04', '2016-06-09 23:30:43'),
(2, 1, 'Alberca', 'Área ubicada una cuadra después de la caseta. Cuenta con alberca, chapoteadero, palapa y baños.', '29.075933', '-111.027681', 3500.0000, '', '2016-01-13 13:10:04', '2016-01-13 13:10:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facilities_rental`
--

CREATE TABLE `facilities_rental` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `facilities_rental`
--

INSERT INTO `facilities_rental` (`id`, `facility_id`, `home_id`, `from`, `to`, `paid`, `created`, `modified`) VALUES
(1, 2, 12, '2016-01-12 04:35:28', '2016-01-14 04:26:14', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(2, 1, 18, '2016-01-12 02:44:51', '2016-01-14 08:08:33', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(3, 2, 20, '2016-01-12 02:20:41', '2016-01-13 18:39:00', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(4, 2, 1, '2016-01-11 14:45:59', '2016-01-14 00:11:09', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(5, 1, 14, '2016-01-12 02:59:44', '2016-01-13 21:01:05', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(6, 2, 9, '2016-01-12 04:47:17', '2016-01-13 21:43:48', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(7, 1, 15, '2016-01-12 13:03:48', '2016-01-14 03:43:40', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(8, 1, 17, '2016-01-11 13:27:22', '2016-01-14 03:41:30', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(9, 2, 10, '2016-01-11 18:36:44', '2016-01-13 22:28:18', 0, '2016-01-13 13:10:07', '2016-01-13 13:10:07'),
(10, 1, 9, '2016-01-12 05:50:47', '2016-01-13 23:21:56', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(11, 2, 7, '2016-01-11 20:58:06', '2016-01-13 21:06:39', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(12, 1, 19, '2016-01-12 03:11:56', '2016-01-14 08:08:02', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(13, 1, 11, '2016-01-11 14:04:19', '2016-01-14 08:24:22', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(14, 1, 6, '2016-01-12 00:29:25', '2016-01-14 01:07:08', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(15, 2, 7, '2016-01-11 14:26:07', '2016-01-13 13:49:19', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(16, 2, 4, '2016-01-12 08:47:29', '2016-01-13 19:39:29', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(17, 2, 1, '2016-01-12 01:27:42', '2016-01-13 23:11:44', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(18, 1, 2, '2016-01-11 17:52:56', '2016-01-14 05:21:06', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(19, 2, 19, '2016-01-11 22:12:32', '2016-01-14 07:31:58', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(20, 2, 2, '2016-01-12 07:03:21', '2016-01-13 13:21:27', 0, '2016-01-13 13:10:08', '2016-01-13 13:10:08'),
(21, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2016-05-22 22:57:04', '2016-05-22 22:57:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Residente', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(2, 'Seguridad', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(3, 'Finanzas', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(4, 'Administrador', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(5, 'Super Administrador', '2016-04-08 00:09:00', '2016-04-08 00:09:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `group_permissions`
--

CREATE TABLE `group_permissions` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `homes`
--

CREATE TABLE `homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resident_id` int(10) UNSIGNED NOT NULL,
  `home_type_id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `home_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `homes`
--

INSERT INTO `homes` (`id`, `street`, `number`, `city`, `state`, `resident_id`, `home_type_id`, `neighborhood_id`, `latitude`, `longitude`, `additional_info`, `home_info`, `image`, `created`, `modified`) VALUES
(1, 'Cassin Stream', '97', 'Heatherville', 'Alabama', 16, 1, 1, '74.180338', '179.245123', 'Molestiae illum et quis. Dignissimos dolor reprehenderit et quaerat omnis repellat excepturi ut.', 'Aliquid ea illum veritatis quia. Accusantium repellendus nostrum sit.', 'img\\home\\1\\DSC02466.JPG', '2016-01-13 06:10:06', '2016-04-13 22:36:04'),
(2, 'Hackett Lakes', '36', 'Leannonmouth', 'Georgia', 12, 1, 1, '-67.780952', '-178.917578', 'Aut impedit vero sequi quasi corporis. Aliquam fuga voluptates ut qui accusantium sit.', 'Amet dolor quibusdam animi non. Molestias nemo magni aliquam nobis similique. Aut sequi illum sit.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(3, 'Georgiana Prairie', '40', 'East Rosario', 'Montana', 4, 3, 1, '47.590716', '-44.601062', 'Aspernatur pariatur harum ratione. Amet nihil aut mollitia qui repellat.', 'Ut et velit molestiae dolores eum et libero. Recusandae consequatur et dolor quis totam est.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(4, 'Edward Ports', '2', 'West Fanny', 'California', 5, 3, 1, '16.374865', '-52.105006', 'Laborum ipsa ipsum dolores illo voluptatem officia. Assumenda illo qui qui.', 'Inventore ea labore quo quaerat. Ut dolores velit quia et dicta velit.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(5, 'Ambrose Mission', '84', 'West Geoside', 'Georgia', 11, 2, 1, '8.498394', '-101.736265', 'Magnam error nihil inventore quam iusto. Eaque nulla voluptatem voluptas voluptatem non.', 'Nulla id veritatis qui eius et. Voluptatem enim perferendis et voluptatum.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(6, 'Daugherty Fork', '86', 'Dibberttown', 'Missouri', 10, 2, 1, '-64.890745', '-81.317489', 'Molestiae est nesciunt temporibus omnis vitae rerum alias. Omnis voluptatibus ab quae quos quis ut.', 'Et illo iste ex ipsa. Fuga officiis optio qui tenetur.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(7, 'Dino Motorway', '27', 'Kerluketon', 'Alabama', 7, 2, 1, '61.407798', '-7.544821', 'Laboriosam et at recusandae quisquam debitis est ratione. Quaerat dolorem aut quis quia et.', 'Est consequuntur iure vero maxime sint assumenda. Molestias ad ut modi quia et autem et.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(8, 'Delphine Pines', '61', 'East Ivymouth', 'Nevada', 13, 3, 1, '-47.637767', '27.442495', 'Id rerum quaerat fuga hic sit ut in. Odit veniam similique saepe.', 'Qui est corrupti tempore. Sunt ut vero eius ut. Omnis voluptatem odit aut itaque consequatur.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(9, 'Bailey Fords', '63', 'Haydenborough', 'Louisiana', 10, 2, 1, '-27.531713', '-7.583823', 'Pariatur dicta et asperiores. Natus nihil id laboriosam quis. Minima quibusdam facere neque.', 'Ipsum rem aut et. Aut eligendi quia est id similique quae aut earum.', '', '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(10, 'Juliana Union', '96', 'North Willowburgh', 'Kansas', 17, 1, 1, '-3.696972', '149.265358', 'Consequuntur voluptas in maxime iusto. Sit ab nihil impedit quo explicabo nemo est.', 'Corrupti ea et non ut. Tempore sint qui neque modi facilis est neque. Quia iusto nesciunt qui.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(11, 'Toy Radial', '28', 'New Demarcoton', 'South Carolina', 15, 2, 1, '56.153514', '-153.416497', 'Beatae iste officia provident quam voluptatum quos non. Eum in omnis fugiat odit dolorem aut eum.', 'Et sit cupiditate debitis et magni minima et aliquam. Eius a quam at voluptas doloremque nisi.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(12, 'Bridgette Forges', '80', 'Hintzton', 'South Dakota', 18, 2, 1, '-64.934492', '119.149733', 'Ut fugiat a aut magni. Illo nulla aut inventore corporis. Quo non accusamus voluptatum neque.', 'Ullam ea quisquam facilis nemo. Maiores est voluptatem minus libero. Eos dolore iste error.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(13, 'Leffler Inlet', '54', 'Collinside', 'Tennessee', 16, 3, 1, '-10.522005', '-62.35987', 'Blanditiis maxime voluptatem iure provident. Qui dignissimos commodi quos dolorum et.', 'Facere laudantium aut ut dicta nemo ducimus. Ex suscipit et et. Eum quis ut quas quis est non quas.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(14, 'Feeney Square', '95', 'Lake Eusebio', 'Michigan', 20, 2, 1, '-65.441126', '98.751798', 'Quibusdam qui pariatur occaecati. Dolores nam voluptatibus impedit in vitae veniam.', 'Et aut quo adipisci et. Omnis ut vitae consectetur saepe. Itaque autem impedit modi.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(15, 'Lexi Fields', '88', 'North Daniella', 'Vermont', 19, 3, 1, '-45.103717', '0.35855699999999', 'Facilis voluptatibus quisquam qui quae est labore. Voluptate animi quia dolores nihil rem.', 'Aut quo voluptate non. Quo harum ipsa numquam. Voluptatibus voluptas aut et delectus vel non.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(16, 'Beer Underpass', '95', 'Lake Jacintoland', 'Rhode Island', 12, 1, 1, '36.356868', '25.591678', 'Numquam aperiam nemo ut corporis sit. Sit quisquam deserunt ut est.', 'Quis tempore vel commodi. Excepturi est voluptatum necessitatibus animi sunt nihil facere.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(17, 'Blanda Vista', '16', 'East Kaciville', 'Delaware', 19, 3, 1, '54.897451', '87.474203', 'Officia est deserunt magni quia inventore. Inventore unde culpa autem dolores.', 'Non et quis nihil commodi et maxime iusto. Ipsum voluptate earum repudiandae officiis accusamus.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(18, 'Marcelino Villages', '86', 'Port Leliamouth', 'Virginia', 8, 2, 1, '-34.617902', '65.369079', 'Cumque laudantium omnis nihil. Cumque laborum vitae voluptatem eum non velit eius.', 'Assumenda sit illum est excepturi sed omnis. Totam exercitationem doloribus eius.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(19, 'Thiel Burgs', '12', 'West Trudie', 'Texas', 5, 2, 1, '34.826301', '-139.004357', 'Aut est dolorem maiores amet corporis non. Id est occaecati pariatur dolorum atque sed ea.', 'Autem vel impedit nulla dolorum cum in et. Omnis et pariatur accusamus voluptatem aut commodi enim.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(20, 'Hubert Ford', '9', 'South Aidanburgh', 'Illinois', 13, 3, 1, '36.023681', '-23.9588', 'Accusantium rem quia ab sunt. Excepturi vero voluptate quas. Doloribus et magnam amet qui.', 'Ullam possimus nisi omnis est. Ut soluta nostrum facere exercitationem. Nihil eligendi ut et quo.', '', '2016-01-13 06:10:07', '2016-01-13 06:10:07'),
(21, 'El Cid', '6', '', '', 1, 1, 1, '27.03', '-111.05', NULL, 'Prueba', 'img\\homes\\21\\DSC02512.JPG', '0000-00-00 00:00:00', '2016-06-26 23:11:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_types`
--

CREATE TABLE `home_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_types`
--

INSERT INTO `home_types` (`id`, `home_type`, `created`, `modified`) VALUES
(1, 'Propia', '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(2, 'Renta', '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(3, 'Desocupada', '2016-01-13 06:09:59', '2016-01-13 06:09:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventories`
--

CREATE TABLE `inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `expense_id` int(10) UNSIGNED NOT NULL,
  `status_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `product` varchar(150) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8 NOT NULL,
  `model` varchar(150) CHARACTER SET utf8 NOT NULL,
  `serial` varchar(150) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `brochure` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `inventories`
--

INSERT INTO `inventories` (`id`, `expense_id`, `status_id`, `product`, `description`, `brand`, `model`, `serial`, `image`, `brochure`, `created`, `modified`) VALUES
(1, 22, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', '', '', '2016-04-16 14:06:15', '2016-04-16 14:06:15'),
(2, 23, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', '', '', '2016-04-16 15:38:07', '2016-04-16 15:38:07'),
(3, 21, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', '', '', '2016-09-15 01:07:03', '2016-09-15 01:07:03'),
(8, 21, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', '', 'captured_by_snapseed_1_1.jpg', '2016-09-15 02:01:22', '2016-09-15 02:01:22'),
(9, 21, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', '', 'captured_by_snapseed_1_1.jpg', '2016-09-15 02:27:51', '2016-09-15 02:27:51'),
(10, 0, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', '', 'IMG-20160808-WA0004-01.jpeg', '2016-09-19 23:44:58', '2016-09-19 23:44:58'),
(11, 0, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', 'IMG_20160906_143049-02.jpeg', 'Ley+de+Amparo+en+Lenguaje+Llano+150609.pdf', '2016-09-22 20:30:27', '2016-09-22 20:30:27'),
(12, 0, 1, 'Cámara', 'DSLR', 'Sony', 'A57', 'SDFG65F1DG', 'IMG_20160906_143049-02.jpeg', 'Ley+de+Amparo+en+Lenguaje+Llano+150609.pdf', '2016-09-22 20:32:46', '2016-09-22 20:32:46'),
(13, 0, 1, 'Cámara', 'DSLR SLT A57', 'Sony', 'A57', 'SDFG65F1DG', NULL, NULL, '2016-09-22 21:42:01', '2016-09-22 21:42:01'),
(14, 21, 1, 'Cámara', 'Cámara DSLR', 'Sony', 'A57', 'SDFG65F1DG', NULL, NULL, '2016-09-22 21:44:22', '2016-09-22 21:44:22'),
(15, 0, 1, 'Cámara', 'prueba', 'Sony', 'A57', 'SDFG65F1DG', NULL, NULL, '2016-09-22 21:45:54', '2016-09-22 21:45:54'),
(16, 15, 1, 'Cámara', 'Prueba', 'Sony', 'A57', 'SDFG65F1DG', NULL, NULL, '2016-09-22 21:51:27', '2016-09-22 21:51:27'),
(17, 15, 1, 'Cámara', 'Prueba', 'Sony', 'A57', 'SDFG65F1DG', NULL, NULL, '2016-09-22 21:53:25', '2016-09-22 21:53:25'),
(18, 15, 1, 'Cámara', 'Prueba', 'Sony', 'A57', 'SDFG65F1DG', 'img\\inventories\\18\\IMG_20160906_143049-02.jpeg', NULL, '2016-09-22 21:53:35', '2016-09-22 21:53:55'),
(19, 0, 2, 'Cámara', 'No funciona', 'Sony', 'A57', 'SDFG65F1DG', 'img\\inventories\\19\\IMG_7727.JPG', NULL, '2016-09-22 23:05:59', '2016-09-22 23:06:14'),
(20, 8, 1, 'Videocámara', 'Prueba', 'Panasonic', 'SD0980', '6165SD1FSDF56', 'img\\inventories\\20\\IMG_8092 (1).JPG', NULL, '2016-09-22 23:14:45', '2016-09-22 23:14:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `log` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `neighborhoods`
--

CREATE TABLE `neighborhoods` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `monthly_fee` float NOT NULL,
  `period_start` tinyint(2) NOT NULL DEFAULT '1',
  `payments_before_default` tinyint(2) NOT NULL DEFAULT '3',
  `payment_on_agreement` float UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `neighborhoods`
--

INSERT INTO `neighborhoods` (`id`, `neighborhood`, `latitude`, `longitude`, `website`, `additional_info`, `image`, `monthly_fee`, `period_start`, `payments_before_default`, `payment_on_agreement`, `created`, `modified`) VALUES
(1, 'Alter Real', '29.076030', '-111.026247', 'https://www.facebook.com/groups/alterreal/', 'Cerrada Alter Real del Fraccionamiento Corceles Residencial.', 'img\\neighborhood\\1\\DSC02466.JPG', 350, 15, 3, 2, '2016-03-14 00:00:00', '2016-05-15 19:33:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bonification_approved` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payments`
--

INSERT INTO `payments` (`id`, `payment_type_id`, `home_id`, `amount`, `payment_details`, `bonification_approved`, `image`, `created`, `modified`) VALUES
(1, 3, 1, 458.00, 'Consequatur autem sapiente esse est id. Maxime dolor eligendi sed error officiis vitae.', 0, '', '2016-03-13 06:10:09', '2016-01-13 06:10:09'),
(2, 2, 20, 223.00, 'Aliquam atque illo omnis illo. Earum eos ea assumenda sed. Sint quasi tenetur voluptatem.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(3, 3, 9, 337.00, 'Veniam iste sint adipisci sit velit. Rerum libero cum ipsa eos dolorem iste ipsa.', 0, '', '2016-03-13 06:10:09', '2016-01-13 06:10:09'),
(4, 2, 12, 411.00, 'Eaque sapiente consequatur voluptatem architecto. Veniam culpa qui ut rerum.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(5, 4, 5, 230.00, 'Ab quo eos illum et eum eos voluptatem voluptatem. Accusantium voluptatem aut dolorem mollitia.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(6, 3, 10, 332.00, 'At perspiciatis quia et aut nemo qui. Dolorem libero provident voluptatem accusamus.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(7, 3, 1, 265.00, 'Et sed omnis ut. Aliquam rerum qui ut optio. Sunt ab enim est voluptatibus soluta.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(8, 4, 15, 480.00, 'Voluptas qui labore voluptas debitis. Illum architecto qui ut nobis.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(9, 2, 12, 251.00, 'Facilis ipsam modi molestias non minima. Eligendi ut et est cum.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(10, 5, 7, 200.00, 'Officia est iusto et qui soluta rem. Itaque eum voluptatibus porro optio voluptate rerum.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(11, 2, 9, 424.00, 'Est atque et non consequuntur omnis ipsa sint. Beatae cumque et quia perspiciatis nisi labore.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(12, 5, 19, 344.00, 'Vitae saepe sit aliquid sit voluptates est et. Autem dolores maxime minus earum consequatur ea.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(13, 3, 8, 302.00, 'Placeat autem tempora similique laudantium. Eveniet aperiam labore architecto iure.', 0, '', '2016-03-13 06:10:09', '2016-01-13 06:10:09'),
(14, 5, 1, 419.00, 'Assumenda omnis placeat suscipit est nihil non illo. Error vitae modi vel.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(15, 2, 13, 406.00, 'Blanditiis enim et quas nam aliquam tenetur. Est et qui a aut ea odit.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(16, 3, 7, 478.00, 'Sint eum voluptatibus esse atque eaque velit et. Et aut minus debitis saepe et eveniet et.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(17, 2, 7, 282.00, 'Iure id consequuntur deserunt qui voluptas. Amet unde suscipit quaerat corporis.', 0, '', '2016-03-13 06:10:09', '2016-01-13 06:10:09'),
(18, 4, 8, 487.00, 'Atque qui et magni. Excepturi cupiditate totam animi ut mollitia alias enim consectetur.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(19, 5, 5, 211.00, 'Ratione aut deleniti amet praesentium vel eius qui. Aut autem harum in dignissimos.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(20, 5, 16, 473.00, 'Vel est dolores impedit. Dolorum possimus pariatur doloremque hic ut dolore rem.', 0, '', '2016-01-13 06:10:09', '2016-01-13 06:10:09'),
(21, 1, 7, 500.00, 'Prueba', 0, '', '2016-01-13 07:18:20', '2016-01-13 07:18:20'),
(22, 1, 1, 2000.00, 'Ninguno', 0, '', '2016-04-13 21:54:43', '2016-04-13 21:54:43'),
(23, 1, 1, 6000.00, '10', 0, '', '2016-04-14 15:53:50', '2016-04-14 15:53:50'),
(24, 1, 1, 6500.00, 'Ninguno', 0, '', '2016-04-14 15:55:14', '2016-04-14 15:55:14'),
(25, 1, 1, 3520.00, 'Otro pago', 0, '', '2016-04-14 15:56:36', '2016-04-14 15:56:36'),
(26, 1, 1, 1650.00, 'Dos meses', 0, '', '2016-04-14 16:06:30', '2016-04-14 16:06:30'),
(27, 1, 1, 1650.00, 'Dos meses', 0, '', '2016-04-14 16:06:46', '2016-04-14 16:06:46'),
(28, 3, 3, 5000.00, 'Pago atrasado', 0, '', '2016-04-16 15:36:01', '2016-04-16 15:36:01'),
(29, 6, 1, 1500.00, 'Bonificación por pronto pago', 0, '', '2016-06-09 21:53:59', '2016-06-09 21:53:59'),
(30, 1, 1, 750.00, 'Ninguno', 0, '', '2016-06-09 21:55:12', '2016-06-09 21:55:12'),
(31, 1, 1, 1400.00, 'Pago atrasado', 0, '', '2016-06-26 22:41:00', '2016-06-26 22:43:06'),
(32, 1, 1, 100.00, 'Ninguno', 0, '', '2016-06-26 22:56:00', '2016-06-26 22:56:28'),
(33, 1, 1, 1300.00, 'test', 0, '', '2016-07-19 20:04:00', '2016-07-19 20:05:02'),
(34, 1, 1, 2000.00, 'Ninguno', 0, '', '2016-07-19 20:06:00', '2016-07-19 20:07:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_types`
--

CREATE TABLE `payment_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_cash` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payment_types`
--

INSERT INTO `payment_types` (`id`, `payment_type`, `is_cash`, `created`, `modified`) VALUES
(1, 'Efectivo', 1, '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(2, 'Transferencia', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(3, 'PayPal', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(4, 'Cheque', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(5, 'Trajeta', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(6, 'Bonificación por Convenio', 0, '2016-05-14 18:29:00', '2016-05-14 18:29:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residents`
--

CREATE TABLE `residents` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resident_type_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_info` tinyint(1) NOT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `residents`
--

INSERT INTO `residents` (`id`, `first_name`, `last_name`, `resident_type_id`, `phone`, `mobile`, `mobile_2`, `work_phone`, `email`, `occupation`, `website`, `show_info`, `additional_info`, `image`, `created`, `modified`) VALUES
(1, 'Idella', 'White', 2, '+19646763740', '1-328-843-6683', '1-578-817-3260', '+1.710.443.9818', 'lbernier@hotmail.com', 'Unde.', 'kutch.com', 0, 'Tempora dignissimos omnis autem id. Dolorem eos laudantium necessitatibus suscipit enim.', 'img\\resident\\1\\DSC02607.JPG', '2016-01-13 06:10:05', '2016-05-30 22:33:42'),
(2, 'Howell', 'Yost', 5, '887-963-5591 x09062', '+1-395-683-6594', '219-268-7949 x4439', '734.901.4117 x1979', 'citlalli.dubuque@yahoo.com', 'Totam.', 'schamberger.com', 1, 'Atque eligendi enim voluptas. Sint aliquid iste consectetur rerum placeat odit sed maxime.', 'img\\resident\\2\\DSC02591.JPG', '2016-01-13 06:10:05', '2016-05-30 22:34:10'),
(3, 'Jennifer', 'Mann', 2, '+1.573.389.6322', '771-658-0496 x6397', '+1-976-596-2362', '658.583.9157 x03914', 'ybreitenberg@shields.org', 'Quibusdam.', 'friesen.com', 1, 'Natus eveniet fugiat perferendis est. Totam ab et laudantium eum at esse. Qui aut qui nulla qui.', NULL, '2016-01-13 06:10:05', '2016-01-13 06:10:05'),
(4, 'Vivian', 'Kozey', 4, '309.892.8946', '561-223-2505', '783-800-3840 x9210', '696.361.2678 x8806', 'karson.bergnaum@wisozk.com', 'Odio rem.', 'hintz.biz', 0, 'Voluptate ut sed occaecati quos praesentium. Nihil ut iste voluptatem veritatis eveniet officia.', NULL, '2016-01-13 06:10:05', '2016-01-13 06:10:05'),
(5, 'Lauriane', 'Dare', 3, '1-740-728-3594', '552.430.6059 x242', '683-308-0221 x64054', '+1.416.212.7570', 'ubailey@sporer.info', 'Nostrum.', 'schinner.com', 0, 'Neque incidunt optio nihil totam ipsam quo eum blanditiis. Cum nesciunt accusamus quia omnis modi.', 'img\\resident\\5\\DSC03013.JPG', '2016-01-13 06:10:05', '2016-06-09 22:58:20'),
(6, 'Tyler', 'Kihn', 5, '1-872-933-5566 x4088', '(837) 565-3341 x6551', '383.559.6816', '(376) 386-5157', 'bergnaum.jerrold@hoppe.com', 'Omnis.', 'kutch.info', 0, 'Qui et non quis. Quo et non atque quo minus. Nulla ab minima commodi ratione.', NULL, '2016-01-13 06:10:05', '2016-01-13 06:10:05'),
(7, 'Lorenzo', 'Hane', 4, '(332) 982-8634 x0052', '464-221-1559', '+1-281-783-7653', '1-458-519-1054 x05658', 'pmurray@gmail.com', 'Minima.', 'senger.com', 1, 'Iure sed doloribus maxime quia fugiat cum. Accusamus nihil iusto sed sit hic.', NULL, '2016-01-13 06:10:05', '2016-01-13 06:10:05'),
(8, 'Douglas', 'Cummerata', 4, '(557) 704-1179 x660', '1-832-917-2103', '314-707-3944', '(837) 556-9415 x6561', 'isom23@beahan.com', 'Sapiente.', 'goodwin.net', 1, 'Sit dolorem dolorum modi consequatur. Ab harum iure quis sint et non. Doloremque ut sed ut beatae.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(9, 'Theodora', 'Kessler', 3, '204.580.4332', '1-832-788-8979 x02276', '760.327.1557 x58580', '1-609-852-7766 x15547', 'natalie.howell@hotmail.com', 'Et.', 'rau.com', 0, 'Magnam asperiores esse nihil quaerat est. Aut voluptas inventore eius.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(10, 'Parker', 'Larson', 2, '537-642-5368 x659', '876.651.7923', '962.954.8656 x711', '982-576-5796 x16440', 'rolson@ondricka.org', 'Aut.', 'emmerich.info', 0, 'Fugiat consequatur commodi tempora reiciendis eius. Aspernatur sapiente doloremque aliquam nam sed.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(11, 'Roderick', 'Leffler', 5, '1-269-474-8674 x255', '1-649-286-5989', '(969) 913-6519 x528', '(752) 354-0262 x728', 'piper74@gerlach.com', 'Aut odit.', 'kemmer.com', 0, 'Est dolores quo laborum. Hic excepturi reprehenderit ut quod voluptatem. Ab quae est omnis ad.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(12, 'Dovie', 'Schmidt', 4, '381.684.5495 x2156', '547-909-3722 x3620', '(764) 632-0409 x734', '1-813-764-5632 x461', 'ncorwin@feil.com', 'Deleniti.', 'herzog.com', 1, 'Provident voluptates aut sit quas odit dolorem. Et non quidem impedit.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(13, 'Bill', 'Bartell', 3, '454-819-2371', '(991) 490-8504', '561.485.5766 x5137', '620.685.4380', 'iyost@metz.com', 'Tempora.', 'von.biz', 0, 'Voluptate rerum rerum ea. Possimus iure qui eius dolores. Et qui ab recusandae eum rerum.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(14, 'Kariane', 'Muller', 5, '763.775.1970 x65264', '680.509.3052', '(758) 824-3495 x564', '(991) 394-4070 x93091', 'iliana47@hirthe.org', 'Eos.', 'schneider.info', 0, 'Vitae ut nam harum quam sit quos corrupti. Id qui autem unde possimus atque ipsam deserunt.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(15, 'Maximillian', 'McDermott', 5, '320.212.1089', '703-705-9093 x2586', '(329) 534-9321', '(984) 951-6809 x8670', 'kutch.freeda@gmail.com', 'Et enim.', 'hickle.com', 0, 'Dolorum in rerum maiores molestiae. Labore facilis ea eligendi possimus.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(16, 'Oran', 'Sanford', 4, '+1.449.407.2360', '1-576-234-0522', '1-941-717-2745', '(786) 244-0846 x4399', 'robtejeda@gmail.com', 'Placeat.', 'harber.com', 0, 'Quisquam accusamus tempore id. Tenetur sint quis vel optio quaerat.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(17, 'Rosalee', 'Bechtelar', 3, '336-348-8157', '383.839.8986 x848', '(784) 364-7440', '+18854935070', 'kassulke.alford@hotmail.com', 'Impedit.', 'breitenberg.net', 1, 'Saepe dolor recusandae quia. Omnis quia est non officia. Sed ut qui nesciunt non ratione cumque ut.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(18, 'Abbigail', 'Barrows', 4, '(571) 820-8149 x63988', '(685) 221-7206', '1-402-214-4405 x7873', '861.518.1734', 'johns.lottie@lebsack.org', 'Placeat.', 'langworth.com', 0, 'Velit voluptatem quibusdam omnis. Id temporibus voluptatem sit velit exercitationem animi quo.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(19, 'Victor', 'Jacobi', 2, '562-648-9390 x613', '+1 (306) 690-8047', '(284) 409-5332', '+1-360-200-5371', 'grimes.abigail@hotmail.com', 'Harum ut.', 'spinka.com', 0, 'Qui deserunt quis quibusdam porro. Illo nemo voluptatibus illo sit. Et officia commodi ipsa earum.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06'),
(20, 'Kennith', 'Bradtke', 5, '+1-527-457-7851', '+1-668-450-7850', '1-323-894-5599', '(959) 453-9040 x136', 'cschuppe@glover.com', 'Maxime.', 'rodriguez.biz', 1, 'Magni in ducimus reiciendis quia. Et et illum repellendus voluptatem ut.', NULL, '2016-01-13 06:10:06', '2016-01-13 06:10:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resident_types`
--

CREATE TABLE `resident_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `resident_type` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `resident_types`
--

INSERT INTO `resident_types` (`id`, `resident_type`, `created`, `modified`) VALUES
(1, 'Russell Schimmel III', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(2, 'Dock Watsica', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(3, 'Rico Parisian', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(4, 'Ron Johnston', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(5, 'Antonina Cormier', '2016-01-13 06:10:00', '2016-01-13 06:10:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_providers`
--

CREATE TABLE `service_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_info` tinyint(1) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `service_providers`
--

INSERT INTO `service_providers` (`id`, `service_provider`, `phone`, `mobile`, `mobile_2`, `work_phone`, `email`, `website`, `show_info`, `status`, `additional_info`, `created`, `modified`) VALUES
(1, 'Columbus', '779.344.4713 x40952', '+1 (924) 816-9451', '(242) 750-5489', '380.637.0889 x659', 'lorenz.rempel@hotmail.com', 'jakubowski.net', 1, 'Qui dolore numquam.', 'Aut quasi ullam nisi quo recusandae.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(2, 'Theodore', '+1-742-210-5938', '1-235-987-5807 x022', '+1 (716) 458-0987', '257-495-0014', 'zhowell@kovacek.info', 'hilll.com', 1, 'Dignissimos nihil.', 'Culpa eos quasi in quisquam.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(3, 'River', '464.320.8912 x982', '1-808-794-7037 x44571', '602-310-9833 x1893', '457.371.3171', 'jaron66@herman.com', 'barrows.com', 1, 'Eos et sit animi.', 'Aspernatur tempore non soluta et possimus quam.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(4, 'Julianne', '+1-394-744-3813', '+1 (871) 274-7063', '1-306-954-8805 x5677', '+1 (854) 731-7697', 'omari.cartwright@hotmail.com', 'corwin.com', 1, 'Non eos quidem.', 'Sit nisi a natus eum eos unde.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(5, 'Yadira', '1-706-989-0268', '853-481-3742', '(698) 721-0336', '(252) 376-1784', 'qlittel@gmail.com', 'witting.org', 0, 'Qui porro ea vitae.', 'Repellat odio sapiente debitis dolorum ut soluta.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(6, 'Eunice', '654.753.2796', '(768) 907-0091', '(701) 785-3401 x5836', '1-281-894-0454 x94129', 'arnoldo.rosenbaum@yahoo.com', 'kris.info', 0, 'Et delectus debitis.', 'Ea provident dolorem nisi magnam dicta ut et.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(7, 'Matilda', '1-813-993-0329', '873.245.9194 x02454', '891-467-4548', '604.925.6429 x41869', 'gregorio53@schaefer.com', 'lynch.biz', 0, 'Qui fugit dolorem.', 'Explicabo non nesciunt reiciendis et voluptatem.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(8, 'Bernardo', '+1-846-895-4277', '467.997.0622', '(549) 976-3422 x51487', '+1-703-339-1377', 'ocremin@king.info', 'swaniawski.com', 0, 'Nisi accusamus non.', 'Dolorum odit maiores aperiam non.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(9, 'Ezra', '1-350-453-1165', '+1.594.654.4053', '921.547.5921 x45875', '+1-531-665-2139', 'angeline43@paucek.com', 'homenick.com', 0, 'Corporis autem.', 'Quis et iure qui officiis.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(10, 'Ewald', '330.938.2477 x4959', '+1 (720) 647-3606', '382-464-6869 x294', '319.729.0827', 'mozelle32@hotmail.com', 'kautzer.info', 0, 'Adipisci qui sint.', 'Quo velit sit inventore magnam dolorem.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(11, 'Rosario', '992-743-7035', '781.691.6838 x65267', '398-934-5167', '+1.648.245.2668', 'isabella73@cremin.com', 'jast.com', 0, 'In ex aut neque.', 'Placeat sit delectus optio eos.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(12, 'Kaylie', '+1.706.412.9995', '+1-526-959-9084', '1-216-753-8801 x712', '+1-234-965-5085', 'mccullough.maryse@nolan.com', 'medhurst.info', 1, 'Nobis tempore.', 'Nihil fugit explicabo animi.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(13, 'Lempi', '320.400.0562 x40794', '345.281.4279', '1-294-778-1592', '(373) 262-3237 x60368', 'huel.alysson@von.com', 'bogisich.com', 1, 'Et facilis quae ab.', 'Quia nisi non totam ea.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(14, 'Althea', '371.367.2045 x669', '1-709-674-7313', '+1-335-515-1375', '(791) 554-0312 x71421', 'misty.ledner@hotmail.com', 'oberbrunner.com', 0, 'Corporis ullam.', 'Aut quis officia quia.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(15, 'Asia', '389.629.2430', '+1.354.564.0333', '+1-372-505-3041', '+1.336.339.3907', 'noemie.tillman@gmail.com', 'hoeger.com', 0, 'Maiores amet.', 'Quis provident totam blanditiis ea in tempore.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(16, 'Sedrick', '1-464-693-6347 x795', '+1-225-786-2826', '+1-585-912-2948', '312-457-4561 x5673', 'else38@gmail.com', 'kautzer.biz', 0, 'Molestiae eum.', 'Accusamus eum distinctio qui.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(17, 'Lenna', '940.903.1570', '531.958.4562 x328', '516.616.1973 x4064', '(612) 249-7790', 'brycen49@kautzer.org', 'orn.biz', 0, 'Suscipit doloribus.', 'Ut quis laboriosam quaerat totam aliquam beatae.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(18, 'Doris', '(329) 563-1279 x01176', '839.659.9515 x52943', '(725) 400-0359 x09086', '365.583.5116 x147', 'bpfannerstill@wisoky.com', 'carroll.org', 0, 'Possimus dolores.', 'Quod quidem aliquam voluptas minus tempora.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(19, 'Seth', '+1.237.216.2706', '797.598.1746 x22654', '298-937-2712', '(307) 930-4719 x24522', 'ndoyle@gmail.com', 'wunsch.com', 1, 'Non nam beatae ea.', 'Odio qui officia molestias ex qui.', '2016-01-13 06:10:04', '2016-01-13 06:10:04'),
(20, 'Aiden', '(287) 799-2635 x23154', '+1-787-510-9742', '375.297.9123', '585.558.6511 x85286', 'boyer.nico@hotmail.com', 'muller.com', 0, 'Corrupti sit magni.', 'Iusto nobis id ipsam ab.', '2016-01-13 06:10:04', '2016-01-13 06:10:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `statuses`
--

INSERT INTO `statuses` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Activo', '2016-09-22 22:05:52', '2016-09-22 22:05:52'),
(2, 'Con Fallas', '2016-09-22 22:05:52', '2016-09-22 22:05:52'),
(3, 'Dañado', '2016-09-22 22:07:01', '2016-09-22 22:07:01'),
(4, 'Obsoleto', '2016-09-22 22:07:01', '2016-09-22 22:07:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `residents_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(55) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `group_id`, `residents_id`, `username`, `name`, `email`, `password`, `remember_token`, `active`, `image`, `neighborhood_id`, `created`, `modified`) VALUES
(2, 5, 19, 'rob', 'Roberto', 'robtejeda@gmail.com', '$2a$10$gWRRe8DJCTorV3AiomtFx.tiQ1tEM.rfW7zXftsBLJjN9/XCSekn.', NULL, 1, '', 1, '2016-04-07 17:18:35', '2016-04-14 16:18:30'),
(4, 5, 18, 'admin', 'Roberto Tejeda', 'rob@ozonostudios.com', '$2a$10$4iX3s6IBsHm07kLYVQngTO1erYGJD3ghkwO.5llqUaKm32Gn8tBqe', NULL, 1, 'img\\users\\4\\DSC03013.JPG', 1, '2016-04-07 17:36:05', '2016-06-09 23:34:13');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announcements_neighborhoods_id_foreign` (`neighborhoods_id`) USING BTREE,
  ADD KEY `announcements_announcement_types_id_foreign` (`announcement_types_id`);

--
-- Indices de la tabla `announcement_types`
--
ALTER TABLE `announcement_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `charges`
--
ALTER TABLE `charges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charges_charge_type_id_foreign` (`charge_type_id`),
  ADD KEY `charges_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `charge_types`
--
ALTER TABLE `charge_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_expense_type_id_foreign` (`expense_type_id`),
  ADD KEY `expenses_service_provider_id_foreign` (`service_provider_id`),
  ADD KEY `expenses_payment_type_id_foreign` (`payment_type_id`);

--
-- Indices de la tabla `expense_types`
--
ALTER TABLE `expense_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilities_neighborhood_id_foreign` (`neighborhood_id`);

--
-- Indices de la tabla `facilities_rental`
--
ALTER TABLE `facilities_rental`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilities_rental_facility_id_foreign` (`facility_id`),
  ADD KEY `facilities_rental_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD KEY `group_permission_group_id_foreign` (`group_id`),
  ADD KEY `group_permission_permission_id_foreign` (`permission_id`);

--
-- Indices de la tabla `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homes_resident_id_foreign` (`resident_id`),
  ADD KEY `homes_home_type_id_foreign` (`home_type_id`),
  ADD KEY `homes_neighborhood_id_foreign` (`neighborhood_id`);

--
-- Indices de la tabla `home_types`
--
ALTER TABLE `home_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory_expense_id_foreign` (`expense_id`),
  ADD KEY `inventories_status_id_foreign` (`status_id`);

--
-- Indices de la tabla `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_payment_type_id_foreign` (`payment_type_id`),
  ADD KEY `payments_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `residents`
--
ALTER TABLE `residents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `residents_resident_type_id_foreign` (`resident_type_id`);

--
-- Indices de la tabla `resident_types`
--
ALTER TABLE `resident_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `service_providers`
--
ALTER TABLE `service_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_user_unique` (`username`) USING BTREE,
  ADD KEY `users_residents_id_foreign` (`residents_id`),
  ADD KEY `users_group_id_foreign` (`group_id`) USING BTREE,
  ADD KEY `users_neighborhood_id_foreign` (`neighborhood_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `announcement_types`
--
ALTER TABLE `announcement_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `charges`
--
ALTER TABLE `charges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT de la tabla `charge_types`
--
ALTER TABLE `charge_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `expense_types`
--
ALTER TABLE `expense_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `facilities_rental`
--
ALTER TABLE `facilities_rental`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `homes`
--
ALTER TABLE `homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `home_types`
--
ALTER TABLE `home_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `neighborhoods`
--
ALTER TABLE `neighborhoods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `residents`
--
ALTER TABLE `residents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `resident_types`
--
ALTER TABLE `resident_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `service_providers`
--
ALTER TABLE `service_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `charges`
--
ALTER TABLE `charges`
  ADD CONSTRAINT `charges_charge_type_id_foreign` FOREIGN KEY (`charge_type_id`) REFERENCES `charge_types` (`id`),
  ADD CONSTRAINT `charges_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`);

--
-- Filtros para la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_expense_type_id_foreign` FOREIGN KEY (`expense_type_id`) REFERENCES `expense_types` (`id`),
  ADD CONSTRAINT `expenses_service_provider_id_foreign` FOREIGN KEY (`service_provider_id`) REFERENCES `service_providers` (`id`);

--
-- Filtros para la tabla `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_neighborhood_id_foreign` FOREIGN KEY (`neighborhood_id`) REFERENCES `neighborhoods` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
