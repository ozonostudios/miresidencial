-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-07-2016 a las 20:17:54
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ozono_miresidencial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhoods_id` int(10) UNSIGNED NOT NULL,
  `announcement_types_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `announcement` text CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `announcement_types`
--

CREATE TABLE `announcement_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `announcement_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `class` varchar(50) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `announcement_types`
--

INSERT INTO `announcement_types` (`id`, `announcement_type`, `class`, `created`, `modified`) VALUES
(1, 'General', 'default', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(2, 'Urgente', 'danger', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(3, 'Logro', 'success', '2016-03-10 00:00:00', '2016-03-10 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `charges`
--

CREATE TABLE `charges` (
  `id` int(10) UNSIGNED NOT NULL,
  `charge_type_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `charge_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `charge_types`
--

CREATE TABLE `charge_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `charge_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `charge_types`
--

INSERT INTO `charge_types` (`id`, `charge_type`, `created`, `modified`) VALUES
(1, 'Mensualidad', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(2, 'Recargo', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(3, 'Renta de Área Común', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(4, 'Pago Daños', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(5, 'Aportación a Mejoras', '2016-01-13 13:10:00', '2016-01-13 13:10:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(11) UNSIGNED NOT NULL,
  `expense_type_id` int(10) UNSIGNED NOT NULL,
  `payment_type_id` int(10) UNSIGNED NOT NULL,
  `service_provider_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `expense_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expense_types`
--

CREATE TABLE `expense_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `expense_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `expense_types`
--

INSERT INTO `expense_types` (`id`, `expense_type`, `created`, `modified`) VALUES
(1, 'Administrativo', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(2, 'Jardinería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(3, 'Seguridad', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(4, 'Equimamiento', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(5, 'Pavimentación', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(6, 'Alberca', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(7, 'Plomería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(8, 'Albañilería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(9, 'Computación', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(10, 'Transporte', '2016-01-13 13:10:01', '2016-01-13 13:10:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `facility` varchar(255) CHARACTER SET utf8 NOT NULL,
  `area_info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rental_cost` double(19,4) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `facilities`
--

INSERT INTO `facilities` (`id`, `neighborhood_id`, `facility`, `area_info`, `latitude`, `longitude`, `rental_cost`, `image`, `created`, `modified`) VALUES
(1, 1, 'Entrada', 'Área ubicada al costado derecho de la caseta de vigilancia. Cuenta con baño y kiosko.', '29.076097', '-111.026481', 2000.0000, '', '2016-01-13 13:10:04', '2016-01-13 13:10:04'),
(2, 1, 'Alberca', 'Área ubicada una cuadra después de la caseta. Cuenta con alberca, chapoteadero, palapa y baños.', '29.075933', '-111.027681', 3500.0000, '', '2016-01-13 13:10:04', '2016-01-13 13:10:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facilities_rental`
--

CREATE TABLE `facilities_rental` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Residente', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(2, 'Seguridad', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(3, 'Finanzas', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(4, 'Administrador', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(5, 'Super Administrador', '2016-04-08 00:09:00', '2016-04-08 00:09:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `group_permissions`
--

CREATE TABLE `group_permissions` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `homes`
--

CREATE TABLE `homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resident_id` int(10) UNSIGNED NOT NULL,
  `home_type_id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `home_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `homes`
--

INSERT INTO `homes` (`id`, `street`, `number`, `city`, `state`, `resident_id`, `home_type_id`, `neighborhood_id`, `latitude`, `longitude`, `additional_info`, `home_info`, `image`, `created`, `modified`) VALUES
(1, 'Alazan', '2', 'Hermosillo', 'Sonora', 1, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(2, 'Alazan', '4', 'Hermosillo', 'Sonora', 2, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(3, 'Alazan', '5', 'Hermosillo', 'Sonora', 3, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(4, 'Alazan', '6', 'Hermosillo', 'Sonora', 4, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(5, 'Alazan', '7', 'Hermosillo', 'Sonora', 5, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(6, 'Alazan', '8', 'Hermosillo', 'Sonora', 6, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(7, 'Alazan', '9', 'Hermosillo', 'Sonora', 7, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(8, 'Alazan', '10', 'Hermosillo', 'Sonora', 8, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(9, 'Alazan', '11', 'Hermosillo', 'Sonora', 9, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(10, 'Alazan', '12', 'Hermosillo', 'Sonora', 10, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(11, 'Alazan', '13', 'Hermosillo', 'Sonora', 11, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(12, 'Alazan', '14', 'Hermosillo', 'Sonora', 12, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(13, 'Alazan', '15', 'Hermosillo', 'Sonora', 13, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(14, 'Alazan', '16', 'Hermosillo', 'Sonora', 14, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(15, 'Alazan', '17', 'Hermosillo', 'Sonora', 15, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(16, 'Alazan', '18', 'Hermosillo', 'Sonora', 16, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(17, 'Alazan', '20', 'Hermosillo', 'Sonora', 17, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(18, 'Argento', '17', 'Hermosillo', 'Sonora', 18, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(19, 'Argento', '19', 'Hermosillo', 'Sonora', 19, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(20, 'Argento', '21', 'Hermosillo', 'Sonora', 20, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(21, 'Argento', '23', 'Hermosillo', 'Sonora', 21, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(22, 'Argento', '25', 'Hermosillo', 'Sonora', 22, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(23, 'Argento', '27', 'Hermosillo', 'Sonora', 23, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(24, 'Argento', '29', 'Hermosillo', 'Sonora', 24, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(25, 'Argento', '31', 'Hermosillo', 'Sonora', 25, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(26, 'Argento', '43', 'Hermosillo', 'Sonora', 26, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(27, 'Argento', '45', 'Hermosillo', 'Sonora', 27, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(28, 'Argento', '47', 'Hermosillo', 'Sonora', 28, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(29, 'Argento', '49', 'Hermosillo', 'Sonora', 29, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(30, 'Argento', '51', 'Hermosillo', 'Sonora', 30, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(31, 'Asturcon', '1', 'Hermosillo', 'Sonora', 31, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(32, 'Asturcon', '2', 'Hermosillo', 'Sonora', 32, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(33, 'Asturcon', '3', 'Hermosillo', 'Sonora', 33, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(34, 'Asturcon', '4', 'Hermosillo', 'Sonora', 34, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(35, 'Asturcon', '5', 'Hermosillo', 'Sonora', 35, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(36, 'Asturcon', '6', 'Hermosillo', 'Sonora', 36, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(37, 'Asturcon', '7', 'Hermosillo', 'Sonora', 37, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(38, 'Asturcon', '8', 'Hermosillo', 'Sonora', 38, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(39, 'Asturcon', '9', 'Hermosillo', 'Sonora', 39, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(40, 'Asturcon', '10', 'Hermosillo', 'Sonora', 40, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(41, 'Asturcon', '12', 'Hermosillo', 'Sonora', 41, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(42, 'Azabache', '1', 'Hermosillo', 'Sonora', 42, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(43, 'Azabache', '2', 'Hermosillo', 'Sonora', 43, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(44, 'Azabache', '3', 'Hermosillo', 'Sonora', 44, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(45, 'Azabache', '4', 'Hermosillo', 'Sonora', 45, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(46, 'Azabache', '5', 'Hermosillo', 'Sonora', 46, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(47, 'Azabache', '6', 'Hermosillo', 'Sonora', 47, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(48, 'Azabache', '7', 'Hermosillo', 'Sonora', 48, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(49, 'Azabache', '8', 'Hermosillo', 'Sonora', 49, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(50, 'Azabache', '9', 'Hermosillo', 'Sonora', 50, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(51, 'Azabache', '10', 'Hermosillo', 'Sonora', 51, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(52, 'Azabache', '11', 'Hermosillo', 'Sonora', 52, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(53, 'Azabache', '12', 'Hermosillo', 'Sonora', 53, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(54, 'Azabache', '14', 'Hermosillo', 'Sonora', 54, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(55, 'Babieca', '1', 'Hermosillo', 'Sonora', 55, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(56, 'Babieca', '2', 'Hermosillo', 'Sonora', 56, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(57, 'Babieca', '3', 'Hermosillo', 'Sonora', 57, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(58, 'Babieca', '4', 'Hermosillo', 'Sonora', 58, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(59, 'Babieca', '5', 'Hermosillo', 'Sonora', 59, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(60, 'Babieca', '6', 'Hermosillo', 'Sonora', 60, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(61, 'Babieca', '7', 'Hermosillo', 'Sonora', 61, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(62, 'Babieca', '8', 'Hermosillo', 'Sonora', 62, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(63, 'Babieca', '9', 'Hermosillo', 'Sonora', 63, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(64, 'Babieca', '10', 'Hermosillo', 'Sonora', 64, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(65, 'Babieca', '11', 'Hermosillo', 'Sonora', 65, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(66, 'Babieca', '12', 'Hermosillo', 'Sonora', 66, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(67, 'Babieca', '13', 'Hermosillo', 'Sonora', 67, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(68, 'Babieca', '14', 'Hermosillo', 'Sonora', 68, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(69, 'Babieca', '15', 'Hermosillo', 'Sonora', 69, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(70, 'Babieca', '16', 'Hermosillo', 'Sonora', 70, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(71, 'Babieca', '17', 'Hermosillo', 'Sonora', 71, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(72, 'Babieca', '18', 'Hermosillo', 'Sonora', 72, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(73, 'Babieca', '19', 'Hermosillo', 'Sonora', 73, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(74, 'Babieca', '20', 'Hermosillo', 'Sonora', 74, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(75, 'Babieca', '21', 'Hermosillo', 'Sonora', 75, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(76, 'Balio', '1', 'Hermosillo', 'Sonora', 76, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(77, 'Balio', '3', 'Hermosillo', 'Sonora', 77, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(78, 'Balio', '4', 'Hermosillo', 'Sonora', 78, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(79, 'Balio', '5', 'Hermosillo', 'Sonora', 79, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(80, 'Balio', '6', 'Hermosillo', 'Sonora', 80, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(81, 'Balio', '7', 'Hermosillo', 'Sonora', 81, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(82, 'Balio', '9', 'Hermosillo', 'Sonora', 82, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(83, 'Balio', '11', 'Hermosillo', 'Sonora', 83, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(84, 'Campirano', '1', 'Hermosillo', 'Sonora', 84, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(85, 'Campirano', '2', 'Hermosillo', 'Sonora', 85, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(86, 'Campirano', '4', 'Hermosillo', 'Sonora', 86, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(87, 'Campirano', '5', 'Hermosillo', 'Sonora', 87, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(88, 'Campirano', '6', 'Hermosillo', 'Sonora', 88, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(89, 'Campirano', '7', 'Hermosillo', 'Sonora', 89, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(90, 'Campirano', '8', 'Hermosillo', 'Sonora', 90, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(91, 'Campirano', '9', 'Hermosillo', 'Sonora', 91, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(92, 'Campirano', '10', 'Hermosillo', 'Sonora', 92, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(93, 'Campirano', '11', 'Hermosillo', 'Sonora', 93, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(94, 'Campirano', '12', 'Hermosillo', 'Sonora', 94, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(95, 'Campirano', '13', 'Hermosillo', 'Sonora', 95, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(96, 'Campirano', '14', 'Hermosillo', 'Sonora', 96, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(97, 'Canelo', '2', 'Hermosillo', 'Sonora', 97, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(98, 'Canelo', '3', 'Hermosillo', 'Sonora', 98, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(99, 'Canelo', '4', 'Hermosillo', 'Sonora', 99, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(100, 'Canelo', '5', 'Hermosillo', 'Sonora', 100, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(101, 'Canelo', '6', 'Hermosillo', 'Sonora', 101, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(102, 'Canelo', '7', 'Hermosillo', 'Sonora', 102, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(103, 'Canelo', '8', 'Hermosillo', 'Sonora', 103, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(104, 'Canelo', '9', 'Hermosillo', 'Sonora', 104, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(105, 'Canelo', '10', 'Hermosillo', 'Sonora', 105, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(106, 'Canelo', '11', 'Hermosillo', 'Sonora', 106, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(107, 'Canelo', '12', 'Hermosillo', 'Sonora', 107, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(108, 'Canelo', '13', 'Hermosillo', 'Sonora', 108, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(109, 'Canelo', '14', 'Hermosillo', 'Sonora', 109, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(110, 'Casejero', '1', 'Hermosillo', 'Sonora', 110, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(111, 'Casejero', '2', 'Hermosillo', 'Sonora', 111, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(112, 'Casejero', '3', 'Hermosillo', 'Sonora', 112, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(113, 'Casejero', '4', 'Hermosillo', 'Sonora', 113, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(114, 'Casejero', '5', 'Hermosillo', 'Sonora', 114, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(115, 'Casejero', '6', 'Hermosillo', 'Sonora', 115, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(116, 'Casejero', '7', 'Hermosillo', 'Sonora', 116, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(117, 'Casejero', '8', 'Hermosillo', 'Sonora', 117, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(118, 'Casejero', '9', 'Hermosillo', 'Sonora', 118, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(119, 'Casejero', '10', 'Hermosillo', 'Sonora', 119, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(120, 'Casejero', '11', 'Hermosillo', 'Sonora', 120, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(121, 'Casejero', '12', 'Hermosillo', 'Sonora', 121, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(122, 'Casejero', '14', 'Hermosillo', 'Sonora', 122, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(123, 'Citlali', '2', 'Hermosillo', 'Sonora', 123, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(124, 'Citlali', '4', 'Hermosillo', 'Sonora', 124, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(125, 'Citlali', '6', 'Hermosillo', 'Sonora', 125, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(126, 'Citlali', '8', 'Hermosillo', 'Sonora', 126, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(127, 'Citlali', '10', 'Hermosillo', 'Sonora', 127, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(128, 'Citlali', '11', 'Hermosillo', 'Sonora', 128, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(129, 'Citlali', '12', 'Hermosillo', 'Sonora', 129, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(130, 'Citlali', '13', 'Hermosillo', 'Sonora', 130, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(131, 'Citlali', '14', 'Hermosillo', 'Sonora', 131, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(132, 'Citlali', '15', 'Hermosillo', 'Sonora', 132, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(133, 'Citlali', '17', 'Hermosillo', 'Sonora', 133, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(134, 'Citlali', '19', 'Hermosillo', 'Sonora', 134, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(135, 'Citlali', '21', 'Hermosillo', 'Sonora', 135, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(136, 'Citlali', '23', 'Hermosillo', 'Sonora', 136, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(137, 'Crepello', '1', 'Hermosillo', 'Sonora', 137, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(138, 'Crepello', '2', 'Hermosillo', 'Sonora', 138, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(139, 'Crepello', '3', 'Hermosillo', 'Sonora', 139, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(140, 'Crepello', '4', 'Hermosillo', 'Sonora', 140, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(141, 'Crepello', '5', 'Hermosillo', 'Sonora', 141, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(142, 'Crepello', '6', 'Hermosillo', 'Sonora', 142, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(143, 'Crepello', '7', 'Hermosillo', 'Sonora', 143, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(144, 'Crepello', '8', 'Hermosillo', 'Sonora', 144, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(145, 'Crepello', '9', 'Hermosillo', 'Sonora', 145, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(146, 'Crepello', '10', 'Hermosillo', 'Sonora', 146, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(147, 'Crepello', '11', 'Hermosillo', 'Sonora', 147, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(148, 'Crepello', '12', 'Hermosillo', 'Sonora', 148, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(149, 'El Cid', '1', 'Hermosillo', 'Sonora', 149, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(150, 'El Cid', '2', 'Hermosillo', 'Sonora', 150, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(151, 'El Cid', '3', 'Hermosillo', 'Sonora', 151, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(152, 'El Cid', '4', 'Hermosillo', 'Sonora', 152, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(153, 'El Cid', '5', 'Hermosillo', 'Sonora', 153, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(154, 'El Cid', '6', 'Hermosillo', 'Sonora', 154, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(155, 'Falabella', '1', 'Hermosillo', 'Sonora', 155, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(156, 'Falabella', '2', 'Hermosillo', 'Sonora', 156, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(157, 'Falabella', '3', 'Hermosillo', 'Sonora', 157, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(158, 'Falabella', '4', 'Hermosillo', 'Sonora', 158, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(159, 'Falabella', '5', 'Hermosillo', 'Sonora', 159, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(160, 'Falabella', '6', 'Hermosillo', 'Sonora', 160, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(161, 'Falabella', '7', 'Hermosillo', 'Sonora', 161, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(162, 'Falabella', '8', 'Hermosillo', 'Sonora', 162, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(163, 'Falabella', '9', 'Hermosillo', 'Sonora', 163, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(164, 'Falabella', '10', 'Hermosillo', 'Sonora', 164, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(165, 'Falabella', '11', 'Hermosillo', 'Sonora', 165, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(166, 'Falabella', '12', 'Hermosillo', 'Sonora', 166, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(167, 'Falabella', '13', 'Hermosillo', 'Sonora', 167, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(168, 'Falabella ', '14', 'Hermosillo', 'Sonora', 168, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(169, 'falabella', '15', 'Hermosillo', 'Sonora', 169, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(170, 'Falabella', '16', 'Hermosillo', 'Sonora', 170, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(171, 'Grisone', '1', 'Hermosillo', 'Sonora', 171, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(172, 'Grisone', '2', 'Hermosillo', 'Sonora', 172, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(173, 'Grisone', '3', 'Hermosillo', 'Sonora', 173, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(174, 'Grisone', '4', 'Hermosillo', 'Sonora', 174, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(175, 'Grisone', '5', 'Hermosillo', 'Sonora', 175, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(176, 'Grisone', '6', 'Hermosillo', 'Sonora', 176, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(177, 'Jenofonte', '1', 'Hermosillo', 'Sonora', 177, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(178, 'Jenofonte', '2', 'Hermosillo', 'Sonora', 178, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(179, 'Jenofonte', '3', 'Hermosillo', 'Sonora', 179, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(180, 'Jenofonte', '4', 'Hermosillo', 'Sonora', 180, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(181, 'Jenofonte', '5', 'Hermosillo', 'Sonora', 181, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(182, 'Jenofonte', '6', 'Hermosillo', 'Sonora', 182, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(183, 'Palomo', '2', 'Hermosillo', 'Sonora', 183, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(184, 'Palomo', '3', 'Hermosillo', 'Sonora', 184, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(185, 'Palomo', '4', 'Hermosillo', 'Sonora', 185, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(186, 'Palomo', '5', 'Hermosillo', 'Sonora', 186, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(187, 'Palomo', '6', 'Hermosillo', 'Sonora', 187, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(188, 'Palomo', '7', 'Hermosillo', 'Sonora', 188, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(189, 'Palomo', '8', 'Hermosillo', 'Sonora', 189, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(190, 'Palomo', '9', 'Hermosillo', 'Sonora', 190, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(191, 'Palomo', '10', 'Hermosillo', 'Sonora', 191, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(192, 'Palomo', '11', 'Hermosillo', 'Sonora', 192, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(193, 'Palomo', '12', 'Hermosillo', 'Sonora', 193, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(194, 'Palomo', '13', 'Hermosillo', 'Sonora', 194, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(195, 'Palomo', '14', 'Hermosillo', 'Sonora', 195, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(196, 'Palomo', '16', 'Hermosillo', 'Sonora', 196, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(197, 'Roano', '1', 'Hermosillo', 'Sonora', 197, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(198, 'Roano', '2', 'Hermosillo', 'Sonora', 198, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(199, 'Roano', '3', 'Hermosillo', 'Sonora', 199, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(200, 'Roano', '5', 'Hermosillo', 'Sonora', 200, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(201, 'Roano', '6', 'Hermosillo', 'Sonora', 201, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(202, 'Roano', '7', 'Hermosillo', 'Sonora', 202, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(203, 'Roano', '8', 'Hermosillo', 'Sonora', 203, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(204, 'Roano', '9', 'Hermosillo', 'Sonora', 204, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(205, 'Roano', '10', 'Hermosillo', 'Sonora', 205, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(206, 'Roano', '11', 'Hermosillo', 'Sonora', 206, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(207, 'Roano', '12', 'Hermosillo', 'Sonora', 207, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(208, 'Roano', '13', 'Hermosillo', 'Sonora', 208, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(209, 'Roano', '15', 'Hermosillo', 'Sonora', 209, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(210, 'Rocinante', '2', 'Hermosillo', 'Sonora', 210, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(211, 'Rocinante', '3', 'Hermosillo', 'Sonora', 211, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(212, 'Rocinante', '4', 'Hermosillo', 'Sonora', 212, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(213, 'Rocinante', '5', 'Hermosillo', 'Sonora', 213, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(214, 'Rocinante', '6', 'Hermosillo', 'Sonora', 214, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(215, 'Rocinante', '7', 'Hermosillo', 'Sonora', 215, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(216, 'Rocinante', '8', 'Hermosillo', 'Sonora', 216, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(217, 'Rocinante', '9', 'Hermosillo', 'Sonora', 217, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(218, 'Rocinante', '10', 'Hermosillo', 'Sonora', 218, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(219, 'Rocinante', '11', 'Hermosillo', 'Sonora', 219, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(220, 'Rocinante', '12', 'Hermosillo', 'Sonora', 220, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(221, 'Rocinante', '13', 'Hermosillo', 'Sonora', 221, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(222, 'Rocinante', '14', 'Hermosillo', 'Sonora', 222, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(223, 'Rocinante', '15', 'Hermosillo', 'Sonora', 223, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(224, 'Rocinante', '16', 'Hermosillo', 'Sonora', 224, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(225, 'Rocinante', '17', 'Hermosillo', 'Sonora', 225, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(226, 'Rocinante', '18', 'Hermosillo', 'Sonora', 226, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(227, 'Rocinante', '19', 'Hermosillo', 'Sonora', 227, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(228, 'Rocinante', '20', 'Hermosillo', 'Sonora', 228, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(229, 'Rocinante', '21', 'Hermosillo', 'Sonora', 229, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(230, 'Rocinante', '22', 'Hermosillo', 'Sonora', 230, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(231, 'Rocinante', '23', 'Hermosillo', 'Sonora', 231, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(232, 'Rocinante', '24', 'Hermosillo', 'Sonora', 232, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(233, 'Rocinante', '25', 'Hermosillo', 'Sonora', 233, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(234, 'Rocinante', '26', 'Hermosillo', 'Sonora', 234, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(235, 'Tobiano', '1', 'Hermosillo', 'Sonora', 235, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(236, 'Tobiano', '3', 'Hermosillo', 'Sonora', 236, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(237, 'Tobiano', '5', 'Hermosillo', 'Sonora', 237, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(238, 'Tobiano', '7', 'Hermosillo', 'Sonora', 238, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(239, 'Tobiano', '8', 'Hermosillo', 'Sonora', 239, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(240, 'Tobiano', '9', 'Hermosillo', 'Sonora', 240, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(241, 'Tobiano', '10', 'Hermosillo', 'Sonora', 241, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(242, 'Tobiano', '11', 'Hermosillo', 'Sonora', 242, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(243, 'Tobiano', '12', 'Hermosillo', 'Sonora', 243, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(244, 'Tobiano', '13', 'Hermosillo', 'Sonora', 244, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(245, 'Tobiano', '15', 'Hermosillo', 'Sonora', 245, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(246, 'Vicir', '2', 'Hermosillo', 'Sonora', 246, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(247, 'Vicir', '3', 'Hermosillo', 'Sonora', 247, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(248, 'Vicir', '4', 'Hermosillo', 'Sonora', 248, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(249, 'Vicir', '5', 'Hermosillo', 'Sonora', 249, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(250, 'Vicir', '6', 'Hermosillo', 'Sonora', 250, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(251, 'Vicir', '7', 'Hermosillo', 'Sonora', 251, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(252, 'Vicir', '8', 'Hermosillo', 'Sonora', 252, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(253, 'Vicir', '9', 'Hermosillo', 'Sonora', 253, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(254, 'Vicir', '10', 'Hermosillo', 'Sonora', 254, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(255, 'Vicir', '11', 'Hermosillo', 'Sonora', 255, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(256, 'Vicir', '12', 'Hermosillo', 'Sonora', 256, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(257, 'Vicir', '13', 'Hermosillo', 'Sonora', 257, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(258, 'Zaino', '2', 'Hermosillo', 'Sonora', 258, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(259, 'Zaino', '3', 'Hermosillo', 'Sonora', 259, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(260, 'Zaino', '4', 'Hermosillo', 'Sonora', 260, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(261, 'Zaino', '5', 'Hermosillo', 'Sonora', 261, 2, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(262, 'Zaino', '6', 'Hermosillo', 'Sonora', 262, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(263, 'Zaino', '7', 'Hermosillo', 'Sonora', 263, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(264, 'Zaino', '8', 'Hermosillo', 'Sonora', 264, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(265, 'Zaino', '9', 'Hermosillo', 'Sonora', 265, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(266, 'Zaino', '10', 'Hermosillo', 'Sonora', 266, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(267, 'Zaino', '11', 'Hermosillo', 'Sonora', 267, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(268, 'Zaino', '12', 'Hermosillo', 'Sonora', 268, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(269, 'Zaino', '13', 'Hermosillo', 'Sonora', 269, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL),
(270, 'Zaino', '15', 'Hermosillo', 'Sonora', 270, 1, 1, NULL, NULL, '', NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_types`
--

CREATE TABLE `home_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_types`
--

INSERT INTO `home_types` (`id`, `home_type`, `created`, `modified`) VALUES
(1, 'Propia', '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(2, 'Renta', '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(3, 'Desocupada', '2016-01-13 06:09:59', '2016-01-13 06:09:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventory`
--

CREATE TABLE `inventory` (
  `id` int(10) UNSIGNED NOT NULL,
  `expense_id` int(10) UNSIGNED NOT NULL,
  `product` varchar(150) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8 NOT NULL,
  `model` varchar(150) CHARACTER SET utf8 NOT NULL,
  `serial` varchar(150) CHARACTER SET utf8 NOT NULL,
  `location` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `brochure` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `log` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `neighborhoods`
--

CREATE TABLE `neighborhoods` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `monthly_fee` float NOT NULL,
  `period_start` tinyint(3) UNSIGNED NOT NULL,
  `payments_before_default` tinyint(3) UNSIGNED NOT NULL DEFAULT '2',
  `payment_on_agreement` tinyint(3) UNSIGNED NOT NULL DEFAULT '2',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `neighborhoods`
--

INSERT INTO `neighborhoods` (`id`, `neighborhood`, `latitude`, `longitude`, `website`, `additional_info`, `image`, `monthly_fee`, `period_start`, `payments_before_default`, `payment_on_agreement`, `created`, `modified`) VALUES
(1, 'Alter Real', '29.076030', '-111.026247', 'https://www.facebook.com/groups/alterreal/', 'Cerrada Alter Real del Fraccionamiento Corceles Residencial.', 'img\\neighborhood\\1\\DSC02466.JPG', 350, 15, 2, 2, '2016-03-14 00:00:00', '2016-05-17 10:06:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bonification_approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_types`
--

CREATE TABLE `payment_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_cash` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payment_types`
--

INSERT INTO `payment_types` (`id`, `payment_type`, `is_cash`, `created`, `modified`) VALUES
(1, 'Efectivo', 1, '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(2, 'Transferencia', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(3, 'PayPal', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(4, 'Cheque', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(5, 'Trajeta', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(6, 'Bonificación', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residents`
--

CREATE TABLE `residents` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resident_type_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_info` tinyint(1) NOT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `residents`
--

INSERT INTO `residents` (`id`, `first_name`, `last_name`, `resident_type_id`, `phone`, `mobile`, `mobile_2`, `work_phone`, `email`, `occupation`, `website`, `show_info`, `additional_info`, `image`, `created`, `modified`) VALUES
(1, 'Alejandro ', 'Castro ', 1, '', '6622797265', '6623589066', NULL, 'alejandro.castrov@gmail.com', NULL, NULL, 0, 'Alejandro Castro', NULL, NULL, NULL),
(2, 'Francisco Javier', 'Espinoza Tapia', 1, '', '6621878791', '6623452454', NULL, 'tadeofraire@gmail.com', NULL, NULL, 0, 'Francisco Javier Espinoza Tapia', NULL, NULL, NULL),
(3, 'Laura Alicia', 'Munoz Moreno', 1, '', '6621718601', '6622990017', NULL, 'buraliciamm@yahoo.com', NULL, NULL, 0, 'Laura Alicia Munoz Moreno', NULL, NULL, NULL),
(4, 'Ivan ', 'Villalobos ', 1, '', '6621554331', '6628476151', NULL, 'ilobo77@hotmail.com', NULL, NULL, 0, 'Ivan Villalobos', NULL, NULL, NULL),
(5, 'Hector Salazar', 'Gonzalez ', 1, '6623468696', '6622003281', '', NULL, 'djcra@hotmail.com, hectorm78@yahoo.com', NULL, NULL, 0, 'Hector Salazar Gonzalez', NULL, NULL, NULL),
(6, 'Iliana Tapia', 'Gaspar ', 1, '', '6621236069', '', NULL, 'ilianatapia@hotmail.', NULL, NULL, 0, 'Iliana Tapia GASPAR', NULL, NULL, NULL),
(7, 'Jesus Servando', 'Rodriguez Fierro', 1, '', '6621241184 ', '6621240709', NULL, 'servando_rguez@yahoo.com', NULL, NULL, 0, 'Jesus Servando Rodriguez Fierro y Diana Alduelda ', NULL, NULL, NULL),
(8, 'Gerardo Carreon', 'Arroyo ', 1, '', '6621904586', '', NULL, 'gercarreon@gmail.com', NULL, NULL, 0, 'Gerardo Carreon Arroyo', NULL, NULL, NULL),
(9, 'Francisco Javier', 'Lopez Alvarez', 1, '', '6622990070', '', NULL, 'lupita.arellano@hotmail.com', NULL, NULL, 0, 'Francisco Javier Lopez Alvarez, Maria Guadalupe Arellano Trujillo', NULL, NULL, NULL),
(10, 'Jorge ', 'Gonzalez ', 1, '', '6621909189', '6621573590', NULL, '', NULL, NULL, 0, 'Jorge Gonzalez', NULL, NULL, NULL),
(11, 'Enrique Lugo', 'Araiza ', 1, '', '6622563284', '6623004500', NULL, 'elugo4@gmail.com', NULL, NULL, 0, 'Enrique Lugo Araiza, Mireya Martinez', NULL, NULL, NULL),
(12, 'Alfonso ', 'Paz ', 1, '', '6621272834', '6621381641', NULL, 'a.pazypuente@gmail.com', NULL, NULL, 0, 'Alfonso Paz y Puente Antunez', NULL, NULL, NULL),
(13, 'Alejandra Meza,', 'Augusto Hernandez', 1, '', '6621422112', '6621971396', NULL, 'alejandrita725@hotmail.com, augustohr@prodigy.net.mx', NULL, NULL, 0, 'Alejandra Meza, Augusto Hernandez', NULL, NULL, NULL),
(14, 'Laura Elena', 'Cuevas Gomez', 1, '', '6331029532', '6333385276', NULL, 'dani2299@live.com', NULL, NULL, 0, 'Laura Elena Cuevas Gomez', NULL, NULL, NULL),
(15, 'Alberto Escobedo', 'Zamorano ', 1, '', '6623599038', '6621270968', NULL, 'albez84@hotmail.com', NULL, NULL, 0, 'Alberto Escobedo Zamorano', NULL, NULL, NULL),
(16, 'Claudia Lizeth', 'Ortiz Villegas', 1, '', '6622448201', '6621050611', NULL, 'apolo.ment@hotmail.com', NULL, NULL, 0, 'Claudia Lizeth Ortiz Villegas', NULL, NULL, NULL),
(17, 'Teresita  De Jesus', 'Organiz Terrazas', 2, '', '6621696595', '6622023219', NULL, 'valenciajunior69@hotmail.com', NULL, NULL, 0, 'Teresita de Jesus Organiz Terrazas FAM VALENCIA ORGANIZ', NULL, NULL, NULL),
(18, 'Ninivet Viridiana', 'Hernandez Muñoz', 2, '', '5543818200', '5511032257', NULL, '', NULL, NULL, 0, 'paulina durazo// renta  Ninivet Viridiana Hernandez Muñoz', NULL, NULL, NULL),
(19, 'Omar Eduardo', 'Hoyos Viscarra', 1, '', '6621726555', '6621410595', NULL, 'omarhoyos80@hotmail.com', NULL, NULL, 0, 'Omar Eduardo Hoyos Viscarra', NULL, NULL, NULL),
(20, 'Jorge Alberto', 'Arce Salas', 1, '', '6623267476', '6621393161', NULL, 'toro.arce@live.com.mx', NULL, NULL, 0, 'Jorge Alberto Arce Salas', NULL, NULL, NULL),
(21, 'Luis Fernando', 'Guerrero Mendoza', 1, '', '6622447634', '6622762404', NULL, 'lgmendoz@sct.gob.mx', NULL, NULL, 0, 'Luis Fernando Guerrero Mendoza/ Ma Teresa Mejia', NULL, NULL, NULL),
(22, 'Juan Domingo', 'Barraza ', 1, '', '6621130985', '6623183296', NULL, 'norma_bustamante@hotmail', NULL, NULL, 0, 'JUAN DOMINGO BARRAZA Y NORMA BUSTAMANTE', NULL, NULL, NULL),
(23, 'Duane ', 'Grasman ', 1, '', '21800050', '6621671076', NULL, 'dbgrasman@gmail.com', NULL, NULL, 0, 'Duane Grasman/ margarita grasman', NULL, NULL, NULL),
(24, 'Ivan Duenas', 'Cabrera ', 1, '', '6629346303', '6621198384', NULL, 'gduenas@telmex.com', NULL, NULL, 0, 'Ivan Duenas Cabrera ', NULL, NULL, NULL),
(25, 'Jesus Alberto', 'Borbon Apodaca', 1, '', '6621480220', '6623161766', NULL, 'jaaborbon@hotmail.com, ailedluna@hotmail.com', NULL, NULL, 0, 'Jesus Alberto Borbon Apodaca, ailed luna', NULL, NULL, NULL),
(26, 'Rosa Maria', 'Cabrera Pacheco', 1, '', '6622337426', '6622220247', NULL, 'nlep_cabrera@hotmail.com', NULL, NULL, 0, 'Rosa Maria Cabrera Pacheco', NULL, NULL, NULL),
(27, 'Hector Manuel', 'Franco Cabrera', 1, '', '6621830939', '6621812971', NULL, 'hfranco@ford.com', NULL, NULL, 0, 'Hector Manuel Franco Cabrera', NULL, NULL, NULL),
(28, 'Francisco Javier', 'Corbala Ramirez', 1, '', '6622290936', '6621575332', NULL, '', NULL, NULL, 0, 'Francisco Javier Corbala Ramirez', NULL, NULL, NULL),
(29, 'Yolanda Rodriguez', 'Lizcano ', 2, '', '6622796754', '6621487051', NULL, '', NULL, NULL, 0, 'Yolanda Rodriguez Lizcano FAM CASTILLO', NULL, NULL, NULL),
(30, 'David ', 'Valenzuela ', 1, '', '6622226595', '6621398585', NULL, '', NULL, NULL, 0, 'David Valenzuela, Jorge Armando Urrea Prado', NULL, NULL, NULL),
(31, 'Araceli Abud', 'Tapia ', 1, '', '6621020370', '', NULL, 'arroyocorp@hotmail.com', NULL, NULL, 0, 'Araceli Abud Tapia', NULL, NULL, NULL),
(32, 'Alberto Guereque', 'Tarango ', 1, '', '6623005124', '6623602124', NULL, 'albertoguereque@hootmail.com', NULL, NULL, 0, 'Alberto Guereque Tarango', NULL, NULL, NULL),
(33, 'Carlos Lopez', 'Martinez ', 1, '', '6621150323', '6621820284', NULL, 'roka582@msn.com', NULL, NULL, 0, 'Carlos Lopez Martinez', NULL, NULL, NULL),
(34, 'Juan Carlos', 'Reyes Vega', 1, '', '6621706734', '6623186192', NULL, 'juancarlosreyes@yahoo.com', NULL, NULL, 0, 'Juan Carlos Reyes Vega', NULL, NULL, NULL),
(35, 'Jorge Munguia', 'Zatarain ', 1, '', '6621077506', '6621865427', NULL, '', NULL, NULL, 0, 'Jorge Munguia Zatarain y Carmen Gutierrez', NULL, NULL, NULL),
(36, 'Claudia Esparza', 'Duarte Dueña', 1, '', '6623002682', '6622290308', NULL, 'claudia.esparzaduarte@banamez.com', NULL, NULL, 0, 'CLAUDIA ESPARZA DUARTE DUEÑA', NULL, NULL, NULL),
(37, 'Diego Ricardo', 'Buendia Alcantara', 1, '', '6621387852', '6622047270', NULL, 'buendiad@prodigy.net.mx', NULL, NULL, 0, 'Diego Ricardo Buendia Alcantara', NULL, NULL, NULL),
(38, 'Oscar Domingo', 'Dessens Jusaino', 1, '', '6622448437', '6621420893', NULL, 'oscardessens@hotmail.com, gladyskrimpe@hotmail.com', NULL, NULL, 0, 'Oscar Domingo Dessens Jusaino, Gladys Krimpe', NULL, NULL, NULL),
(39, 'Erika Rodriguez', 'Ruiz ', 1, '', '6622448804', '6622448638', NULL, '', NULL, NULL, 0, 'Erika Rodriguez Ruiz', NULL, NULL, NULL),
(40, 'Pablo Facundo', 'Velazquez Gonzalez', 1, '', '6621910169', '6622969800', NULL, 'fack_v@hotmail.com', NULL, NULL, 0, 'Pablo Facundo Velazquez Gonzalez', NULL, NULL, NULL),
(41, 'Servio Tulio', 'Garcia Hidalgo', 1, '', '6622883944', '6621883944', NULL, 'sgmoreno_d@hotmail.com', NULL, NULL, 0, 'Servio Tulio Garcia Hidalgo', NULL, NULL, NULL),
(42, 'Felipe Roberto', 'Lugo Nunez', 1, '', '6621694302', '6621560593', NULL, 'jossieq@hotmail.com', NULL, NULL, 0, 'Felipe Roberto Lugo Nunez, Ana Alicia Herrera Moreno', NULL, NULL, NULL),
(43, 'Alejandro Garcia', 'Flores ', 1, '', '6621117062', '6621383371', NULL, 'margaritamend@hotmail.com', NULL, NULL, 0, 'Alejandro Garcia Flores, Margarita mendivil', NULL, NULL, NULL),
(44, 'Humberto ', 'Bringas Tadei', 2, '', '6622564488', '4448564223', NULL, 'humbertobringast@hotmail.com', NULL, NULL, 0, 'Humberto bringas tadei (renta)', NULL, NULL, NULL),
(45, 'Hiram Meza', 'Enriquez ', 1, '', '6621710191', '6622568791', NULL, 'hmeza@te.com', NULL, NULL, 0, 'Hiram Meza Enriquez, Gloria Valdez', NULL, NULL, NULL),
(46, 'Rosa Isela', 'Ayon Reyes', 1, '', '6621830783', '6623186775', NULL, '', NULL, NULL, 0, 'Rosa Isela Ayon Reyes y Angela Ayon ', NULL, NULL, NULL),
(47, 'Ana Maria', 'Rodriguez Nunez', 1, '', '6621716151', '6622005463', NULL, '', NULL, NULL, 0, 'Ana Maria Rodriguez Nunez', NULL, NULL, NULL),
(48, 'Jose Alfredo', 'Valverde Payan', 1, '', '6621689143', '6621052649', NULL, '', NULL, NULL, 0, 'Jose Alfredo Valverde Payan', NULL, NULL, NULL),
(49, 'Victor Eduardo', 'Perez Orozco', 1, '', '6628470266', '2627040', NULL, '', NULL, NULL, 0, 'Victor Eduardo Perez Orozco, Zulema Celaya Gortari', NULL, NULL, NULL),
(50, 'Luis Guillermo', 'Luna Rivera', 1, '', '6622335155', '6621890048', NULL, 'luislunarivera@tufesa.com.mx', NULL, NULL, 0, 'Luis Guillermo Luna Rivera, Jesus Oscar Luna Medina', NULL, NULL, NULL),
(51, 'Alam Judth', 'Beltran Murillo', 1, '', '6621800441', '', NULL, 'viajesconchitabeltran@hotmail.com', NULL, NULL, 0, 'Alam judth Beltran Murillo', NULL, NULL, NULL),
(52, 'Fabiola ', 'Ramos Merino', 1, '', '6622791447', '6621876163', NULL, '', NULL, NULL, 0, 'Fabiola Ramos Merino', NULL, NULL, NULL),
(53, 'Lourdes Gutierrez', 'Alquicira ', 1, '', '6622259969', '6622232418', NULL, 'rokatoledo@gmail.com', NULL, NULL, 0, 'Lourdes Gutierrez Alquicira', NULL, NULL, NULL),
(54, 'Guadalupe ', 'Moreno Gil', 1, '', '6621490989', '', NULL, 'arq.mariogalaz@gmail.com', NULL, NULL, 0, 'Guadalupe Moreno Gil', NULL, NULL, NULL),
(55, 'Rosa Arminda', 'Quijada Vasquez', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Rosa Arminda Quijada Vasquez', NULL, NULL, NULL),
(56, 'Cristina ', 'Olvera Barrios', 1, '', '6621020040', '', NULL, '', NULL, NULL, 0, 'Cristina Olvera Barrios', NULL, NULL, NULL),
(57, 'Oscar ', 'Soria Noriega', 1, '', '6621121674', '6621962487', NULL, 'oscsoria@aol.com', NULL, NULL, 0, 'Oscar Soria Noriega', NULL, NULL, NULL),
(58, 'Liliana Ivet', 'Leyva Figueroa', 1, '', '6621670840', '', NULL, 'lilflilf@yahoo.com.mx', NULL, NULL, 0, 'Liliana Ivet Leyva Figueroa', NULL, NULL, NULL),
(59, 'Karla Gpa', 'Arredondo Ruiz', 1, '', '6622338679', '6622260009', NULL, '', NULL, NULL, 0, 'karla gpa arredondo ruiz', NULL, NULL, NULL),
(60, 'Rosario ', 'Ortiz Bravo', 1, '', '6622292039', '6621574370', NULL, 'chaly_ortiz@hotmail.com', NULL, NULL, 0, 'Rosario Ortiz Bravo claudia Ortiz', NULL, NULL, NULL),
(61, 'Eduardo ', 'Quintero Leyva', 1, '', '6621202550', '', NULL, '', NULL, NULL, 0, 'eduardo quintero leyva', NULL, NULL, NULL),
(62, 'Maria Griselda', 'Duron Borchardt', 1, '', '6621670649', '', NULL, 'marvinsalcedo@hotmail.com', NULL, NULL, 0, 'Maria Griselda Duron Borchardt, Marvin Salcedo', NULL, NULL, NULL),
(63, 'Krupshupsky ', 'Ibarra Sainz', 1, '', '6622001970', '6623004145', NULL, 'krushus@hotmail.com', NULL, NULL, 0, 'Krupshupsky Ibarra Sainz', NULL, NULL, NULL),
(64, 'Maria ', 'Dolores Rubio', 1, '', '6621410730', '6622789841', NULL, '', NULL, NULL, 0, 'Maria Dolores Rubio, Samuel Jesus Salazar Rascon', NULL, NULL, NULL),
(65, 'German ', 'Verdugo Estrada', 1, '', '6622446688', '6621911112', NULL, '', NULL, NULL, 0, 'German Verdugo Estrada y Aama Delia Arambulo', NULL, NULL, NULL),
(66, 'Luz Maria', 'Gomez Rojas', 1, '', '6621226356', '6621558178', NULL, '', NULL, NULL, 0, 'Luz Maria Gomez Rojas', NULL, NULL, NULL),
(67, 'Fidel Ayala', 'Ruiz ', 1, '', '6622969872', '6621120013', NULL, 'fidel_1000@hotmail.com', NULL, NULL, 0, 'Fidel Ayala Ruiz, Diana Gonzalez', NULL, NULL, NULL),
(68, 'Felipe Chavez', 'Lechuga ', 2, '', '6621436871', '', NULL, 'felipe@metrolink.com.mx', NULL, NULL, 0, 'Felipe Chavez Lechuga / Sergio Martinez G (renta)', NULL, NULL, NULL),
(69, 'Jesus Angel', 'Espinoza Duarte', 1, '', '6622829334', '6622827153', NULL, 'changel19@hotmail.com', NULL, NULL, 0, 'Jesus Angel Espinoza Duarte', NULL, NULL, NULL),
(70, 'Alma Yadira', 'Parada Briseno', 1, '', '6622957381', '6623409421', NULL, 'alyaces@hotmail.com', NULL, NULL, 0, 'Alma Yadira Parada Briseno', NULL, NULL, NULL),
(71, 'Marina Argelia', 'Encinas Ibarra', 1, '', '6622564713', '', NULL, 'marimba8@yahoo.com', NULL, NULL, 0, 'Marina Argelia Encinas Ibarra', NULL, NULL, NULL),
(72, 'Axel ', 'Sosa Chavez', 1, '', '6621285339', '6621800284', NULL, 'minero73mx@hotmail.com', NULL, NULL, 0, 'Axel Sosa Chavez', NULL, NULL, NULL),
(73, 'Jose Arturo', 'Torres Lopez', 1, '', '', '', NULL, 'pepetorres104@hotmail.com', NULL, NULL, 0, 'Jose Arturo Torres Lopez', NULL, NULL, NULL),
(74, 'Jose Pedro', 'Cardenas Marquez', 1, '', '6622026736', '', NULL, 'mazul_01@hotmail.com', NULL, NULL, 0, 'Jose Pedro Cardenas Marquez', NULL, NULL, NULL),
(75, 'Ana Laura', 'Riley Sanchez', 1, '', '6622571098', '6621490156', NULL, 'annys_riley@hotmail.com, everardo.inda@cfe.gob.mx', NULL, NULL, 0, 'Ana Laura Riley Sanchez, Jose Everardo Inda Gallardo', NULL, NULL, NULL),
(76, 'Juan Carlos', 'Perez Gomez', 1, '', '6621690793', '6622571926', NULL, 'jcpg13@hotmail.com', NULL, NULL, 0, 'Juan Carlos Perez Gomez', NULL, NULL, NULL),
(77, 'Maria Cecilia', 'Figueroa Borboa', 1, '', '6621119611', '66222562795', NULL, '', NULL, NULL, 0, 'Maria Cecilia Figueroa Borboa', NULL, NULL, NULL),
(78, 'Joaquin ', 'Rabago Rodriguez', 1, '', '6621240942', '6621918970', NULL, 'chakmast@hotmail.com', NULL, NULL, 0, 'Joaquin Rabago Rodriguez', NULL, NULL, NULL),
(79, 'Cristian ', 'Duran Leal', 1, '', '6621251384', '', NULL, 'cduranl@hotmail.com', NULL, NULL, 0, 'Cristian Duran Leal', NULL, NULL, NULL),
(80, 'Miguel Antonio', 'Salazar Perez', 1, '', '6621900472', '6621900471', NULL, 'miguelantonio@gmail.com', NULL, NULL, 0, 'Miguel Antonio Salazar Perez', NULL, NULL, NULL),
(81, 'Gerardo ', 'Garcia Salcedo', 1, '', '6621201276', '6621862524', NULL, 'esgub@hotmail.com', NULL, NULL, 0, 'Gerardo Garcia Salcedo/Marcela Ruiz', NULL, NULL, NULL),
(82, 'Emilio ', 'Carbajal Garcia', 1, '', '', '', NULL, 'ivette_801021@live.com.mx', NULL, NULL, 0, 'Emilio Carbajal Garcia', NULL, NULL, NULL),
(83, 'Jesus Ricardo', 'Valenzuela Ceniceros', 1, '', '6621040474', '', NULL, '', NULL, NULL, 0, 'Jesus Ricardo Valenzuela Ceniceros', NULL, NULL, NULL),
(84, 'Jesus Enrique', 'Flores Ruiz', 1, '', '6621130539', '6621050389', NULL, 'arq.fernandaflores@gmail.com', NULL, NULL, 0, 'Jesus Enrique Flores Ruiz y Fernanda Flores y Sergio Estrella', NULL, NULL, NULL),
(85, 'Bernardino ', 'Garcia Arcos', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Bernardino Garcia Arcos, Esperanza Tovar', NULL, NULL, NULL),
(86, 'Victor Alfonso', 'Morfin Woolfolk', 1, '', '6622240987', '', NULL, 'amorfin@yahoo.com', NULL, NULL, 0, 'Victor Alfonso Morfin Woolfolk y yolanda Hdz', NULL, NULL, NULL),
(87, ' ', ' ', 1, '', '6621240025', '6621047527', NULL, 'naguirro@gmail.com', NULL, NULL, 0, 'A', NULL, NULL, NULL),
(88, 'Maria Guadalupe', 'Ocano Icedo', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Maria Guadalupe Ocano Icedo', NULL, NULL, NULL),
(89, 'Manuel ', 'Cruz Encinas', 1, '', '6629488764', '6621861413', NULL, 'manuel_cruz66@hotmail.com', NULL, NULL, 0, 'Manuel Cruz Encinas y loly hernandez', NULL, NULL, NULL),
(90, 'Martha Berenice', 'Estrada Gomez', 1, '', '6621400381', '6622780857', NULL, 'david@pgpsmexico.com', NULL, NULL, 0, 'Martha Berenice Estrada Gomez, David Ramos Felix', NULL, NULL, NULL),
(91, 'Paul ', 'Pacheco Avalos', 1, '6622449803', '6621390454', '22205037', NULL, 'pumtemoc77@gmail.com', NULL, NULL, 0, 'paul c. pacheco Avalos', NULL, NULL, NULL),
(92, 'Elizardo ', 'Ramos Aguilera', 1, '', '6622258656', '6621742146', NULL, 'elizardo.ramos@hsbc.com.mx', NULL, NULL, 0, 'Elizardo Ramos Aguilera', NULL, NULL, NULL),
(93, 'Patricia ', 'Fontes Cota', 2, '', '6623393968', '6621824655', NULL, 'Javier.Alvarez@zobele.com, pfcota@gmail.com', NULL, NULL, 0, 'Patricia E. Fontes Cota, Javier Alvarez (renta)', NULL, NULL, NULL),
(94, 'Juan Alberto  ', 'Esquer Amavizca ', 1, '', '6621384127', '', NULL, 'Juan Alberto Esquer Amavizca', NULL, NULL, 0, '', NULL, NULL, NULL),
(95, 'Julio Cesar', 'Rodriguez Machado', 1, '', '6621374759', '6621384127', NULL, '', NULL, NULL, 0, 'Julio Cesar Rodriguez Machado', NULL, NULL, NULL),
(96, 'Francisca Marcela', 'Lizarraga Ruiz', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Francisca Marcela Lizarraga Ruiz', NULL, NULL, NULL),
(97, 'Alejandro ', 'Ernesto Vega', 1, '', '6621396480', '6621439919', NULL, '', NULL, NULL, 0, 'Alejandro Ernesto Vega ', NULL, NULL, NULL),
(98, 'Mario ', 'Abril Coronado', 1, '', '6622570210', '', NULL, '', NULL, NULL, 0, 'Mario Abril Coronado', NULL, NULL, NULL),
(99, 'Pablo ', 'Vazquez Garcia', 1, '', '6622699080', '6622449085', NULL, 'pvg_vazquez@hotmail.com', NULL, NULL, 0, 'Pablo Vazquez Garcia', NULL, NULL, NULL),
(100, 'Leonel Alfonso', 'Sanchez Pacheco', 1, '', '6621973112', '6623286973', NULL, 'lsanchez@ganfer.com', NULL, NULL, 0, 'Leonel Alfonso Sanchez Pacheco', NULL, NULL, NULL),
(101, 'Ramon Hector', 'Barraza Guardado', 1, '', '6621571866', '6621878577', NULL, '', NULL, NULL, 0, 'Ramon Hector Barraza Guardado', NULL, NULL, NULL),
(102, 'Socorro Santiago', 'Chavez Zamora', 2, '', '6622291353', '6622004473', NULL, 'chavezcoyosan@hotmail.com, liliarivera@hotmail.com', NULL, NULL, 0, 'Socorro Santiago Chavez Zamora, FAM CHAVEZ', NULL, NULL, NULL),
(103, 'Alicia ', 'Ocampo Martinez', 1, '', '6621680737', '6621480509', NULL, 'jmesolano@hotmail.com, licha1@hotmail.com', NULL, NULL, 0, 'Alicia Ocampo Martinez, Jorge Mario Echeverria', NULL, NULL, NULL),
(104, 'Enrique ', 'Villegas Lino', 1, '', '6621907820', '6621950516', NULL, 'zulemagarcia3@hotmail.com', NULL, NULL, 0, 'Enrique Villegas Lino', NULL, NULL, NULL),
(105, 'Mario Ivan', 'Martinez Gonzalez', 1, '', '6641676725', '6623003715', NULL, 'marioivanm@hotmail.com', NULL, NULL, 0, 'Mario Ivan Martinez Gonzalez', NULL, NULL, NULL),
(106, 'Daniel ', 'Leon Torres', 1, '', '6621720494', '6621960112', NULL, 'daniel.leon.t@gmail.com', NULL, NULL, 0, 'Daniel Leon Torres/ Diana Johnson', NULL, NULL, NULL),
(107, 'Guadalupe Elena', 'Salazar Mayboca', 1, '', '', '', NULL, 'lupelena79@hotmail.com', NULL, NULL, 0, 'Guadalupe Elena Salazar Mayboca', NULL, NULL, NULL),
(108, 'Ruben ', 'Barajas Gallardo', 1, '', '6621120836', '6622446718', NULL, 'rbarajas83@hotmail.com', NULL, NULL, 0, 'Ruben Barajas Gallardo y sol Angel garcia', NULL, NULL, NULL),
(109, 'Sandra Elivia', 'Becerril Lopez', 1, '', '6621960108', '', NULL, '', NULL, NULL, 0, 'Sandra Elivia Becerril Lopez', NULL, NULL, NULL),
(110, 'Francisco Jonathan', 'Garcia Salcedo', 1, '', '', '', NULL, 'fjgarcias@hotmail.com, danslabella10@hotmail.com', NULL, NULL, 0, 'Francisco Jonathan Garcia Salcedo, Daniela Gonzalez', NULL, NULL, NULL),
(111, 'Yomara ', 'Padilla Mendez', 1, '', '6621970653', '6621970652', NULL, '', NULL, NULL, 0, 'Yomara Padilla Mendez', NULL, NULL, NULL),
(112, 'Milton ', 'Colli Serrano', 1, '', '6621710541', '6622449311', NULL, 'miltoncolli@prodigy.net.mx', NULL, NULL, 0, 'Milton Colli Serrano O IRMA MACAZAN', NULL, NULL, NULL),
(113, 'Juan Carlos', 'Sandoval Padilla', 1, '', '6621460492', '6621053661', NULL, '', NULL, NULL, 0, 'Juan Carlos Sandoval Padilla', NULL, NULL, NULL),
(114, 'Guillermo ', 'Espinosa Garcia', 2, '', '6621880871', '6622059294', NULL, 'gespinosa@itesm.mx', NULL, NULL, 0, 'Guillermo Espinosa Garcia ( renta: Mike Kiefer)', NULL, NULL, NULL),
(115, 'Armando ', 'Acosta ', 1, '', '6621712620', '6623585002', NULL, 'norma_riivero@hotmail', NULL, NULL, 0, 'Norma Alcia Bustamante, fam Rivero//Nuevo dueño Armando Acosta', NULL, NULL, NULL),
(116, 'María Teresa', 'Aguirre Bordallo', 1, '', '6621705090', '6628470277', NULL, 'mvinny23@hotmail.com', NULL, NULL, 0, 'ma TERESA AGUIRRE BORDALLO', NULL, NULL, NULL),
(117, 'Jorge ', 'Soto Rodriguez', 2, '', '6622240313', '6622980999', NULL, '', NULL, NULL, 0, 'Jorge Soto Rodriguez y renta :briseida peña', NULL, NULL, NULL),
(118, 'Diana Rocio', 'Aguirre Leyva', 1, '', '6621480823', '6621046829', NULL, 'diana_4062@hotmail.com', NULL, NULL, 0, 'Diana Rocio Aguirre Leyva, Eduardo Aguirre Lugo', NULL, NULL, NULL),
(119, 'Eloisa ', 'Gonzalez ', 1, '', '6622768655', '', NULL, 'egonzalez@hersonpack.com', NULL, NULL, 0, 'Eloisa Gonzalez, Domingo Vazquez Lugo', NULL, NULL, NULL),
(120, 'Gilberto ', 'Mazon Acosta', 1, '', '6622787147', '6621950905', NULL, 'gilmazon@hotmail.com', NULL, NULL, 0, 'Gilberto Mazon Acosta', NULL, NULL, NULL),
(121, 'Fernando ', 'Pavlovich Braun', 1, '', '6621690312', '', NULL, 'fernando_pavlovich@hotmail.com', NULL, NULL, 0, 'DUEÑO:  FERNANDO PAVLOVICH  BRAUN', NULL, NULL, NULL),
(122, 'Marcos ', 'Encinas Cornejo', 1, '', '6622766320', '6441072619', NULL, 'rendongina3@gmail.com', NULL, NULL, 0, 'Marcos Encinas Cornejo', NULL, NULL, NULL),
(123, 'Ana Maria', 'Salido Vazquez', 2, '', '6622917077', '6621020907', NULL, 'layala@connexum.mx', NULL, NULL, 0, 'Ana Maria Salido Vazquez, Leonardo Quintana A renta: davd lopez', NULL, NULL, NULL),
(124, 'Clarissa ', 'Angulo ', 1, '', '6621900079', '6622560427', NULL, 'rene@loquequieras.mx, clarisaag@hotmail.com', NULL, NULL, 0, 'Clarissa Angulo', NULL, NULL, NULL),
(125, 'Maria ', 'Irais Navarro', 1, '', '6621120463', '6621300842', NULL, 'iraisnavarro@hotmail.com', NULL, NULL, 0, 'maria irais Navarro y alejandro cervantes', NULL, NULL, NULL),
(126, 'Guillermo Guadalupe', 'Aguirre Caraveo', 1, '6621680887', '6621720115', '6621878132', NULL, 'aguirre_guillermo@hotmail.com', NULL, NULL, 0, 'Guillermo Guadalupe Aguirre Caraveo y Lorenia Figueroa', NULL, NULL, NULL),
(127, 'Aaron ', 'Leon Lopez', 1, '', '6621865561', '6621865567', NULL, '', NULL, NULL, 0, 'Aaron Leon Lopez', NULL, NULL, NULL),
(128, 'Zoila Guadalupe', 'Soto Rodriguez', 1, '', '6474820672', '', NULL, 'renta: prospero ibarra ', NULL, NULL, 0, 'Zoila Guadalupe Soto Rodriguez', NULL, NULL, NULL),
(129, 'Ana Lourdes', 'Vidal Robles', 1, '', '2166623', 'Ana Lourdes', NULL, '', NULL, NULL, 0, 'Ana Lourdes Vidal Robles', NULL, NULL, NULL),
(130, 'Sandra Elena', 'Montero Aguayo', 1, '', '6622040717', '6622060743', NULL, 'bmoralesmc@hotmail.com, alusion@hotmail.com', NULL, NULL, 0, 'Sandra Elena Montero Aguayo y Benjamin Morales', NULL, NULL, NULL),
(131, 'Rebeca Maria', 'Zepeda Arriola', 1, '', '6621730099', '6621990927', NULL, '', NULL, NULL, 0, 'Rebeca Maria Zepeda Arriola, Juan Fernando Zepeda ', NULL, NULL, NULL),
(132, 'Maria Dolores', 'Rodriguez Rocha', 1, '', '', '', NULL, 'lolardz@hotmail.com', NULL, NULL, 0, 'Maria Dolores Rodriguez Rocha, Hector Rodriguez Espinoza', NULL, NULL, NULL),
(133, 'Ferrer ', 'Gracia Ozuna', 1, '', '6311572373', '', NULL, '', NULL, NULL, 0, 'Ferrer Gracia Ozuna, Prospero Ibarra y Gildegar Quiroz', NULL, NULL, NULL),
(134, 'Nadia Marisol', 'Burgos Ruiz', 2, '', '6621720535', '6629489243', NULL, '', NULL, NULL, 0, 'Nadia Marisol Burgos Ruiz, David Guerra (Renta)', NULL, NULL, NULL),
(135, 'Gerardo ', 'Novoa Rodriguez', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Gerardo Novoa Rodriguez', NULL, NULL, NULL),
(136, 'Sandra Luz', 'Tapia Cota', 1, '', '6621557918', '2166538', NULL, 'sandraluz710@hotmail.com', NULL, NULL, 0, 'Sandra Luz Tapia Cota/ olga romo', NULL, NULL, NULL),
(137, 'Ruben ', 'Tirado Alvarez', 2, '', '6449988509', '', NULL, 'csillas@yoreme.com', NULL, NULL, 0, 'Ruben Tirado Alvarez ( renta Claudia Enriquez o fam sillas Enriquez', NULL, NULL, NULL),
(138, 'Marcos Javier', 'Lopez Peralta', 1, '', '6622050503', '6621120707', NULL, 'abigailfe_2@hotmail.com', NULL, NULL, 0, 'Marcos Javier Lopez Peralta, Abigail Felix', NULL, NULL, NULL),
(139, 'Maria Rosario', 'Huerta Calles', 1, '', '6621270616 ', '6629487934', NULL, 'wwe_ijm@hotmail.com', NULL, NULL, 0, 'Maria Rosario Huerta Calles, Ildefonso Jimenez Martinez', NULL, NULL, NULL),
(140, 'Maria Sandra', 'Castaneda Felix', 2, '', '', '', NULL, 'stacetrejo_17@hotmail.com', NULL, NULL, 0, 'dueña:Maria Sandra Castaneda Felix renta ', NULL, NULL, NULL),
(141, 'Rigoberto ', 'Castillo Garcia', 1, '', '6623255636', '6629488581', NULL, 'rigoberto_castillo@yahoo.com.mx, sotomayor.anam@gmail.com', NULL, NULL, 0, 'Rigoberto Castillo Garcia, Ana Sotomayor', NULL, NULL, NULL),
(142, 'Cesar Alberto', 'Garcia Leyva', 1, '', '6622567349', '6622561025', NULL, 'cesar_leyva@hotmail.com', NULL, NULL, 0, 'Cesar Alberto Garcia Leyva', NULL, NULL, NULL),
(143, 'Adolfo ', 'Ruiz Amaya', 1, '', '6621506870', '6622969450', NULL, '', NULL, NULL, 0, 'Adolfo Ruiz Amaya y Marina Rosas', NULL, NULL, NULL),
(144, 'Juan Carlos', 'Real Redondo', 1, '', '6622566652', '6622339348', NULL, 'nube_751@hotmail.com', NULL, NULL, 0, 'Juan Carlos Real Redondo', NULL, NULL, NULL),
(145, 'Yovany Benigno', 'Camacho Inzunza', 1, '', '6621376407', '6621960458', NULL, 'yovany_camacho@hotmail.com', NULL, NULL, 0, 'Yovany Benigno Camacho Inzunza', NULL, NULL, NULL),
(146, 'Ruth ', 'Murrieta Gamez', 1, '', '6621192799', '', NULL, 'rmurrietag@hotmail.com,javiermurrieta02@hotmail', NULL, NULL, 0, 'Ruth Murrieta Gamez', NULL, NULL, NULL),
(147, 'Melchor ', 'Colsa ', 1, '', '6622052787', '6622001787', NULL, '', NULL, NULL, 0, 'melchor colsa ', NULL, NULL, NULL),
(148, 'Jose German', 'Cervantes Ramos', 1, '', '6621400300', '6622567932', NULL, 'cervantesrj@hotmail.com', NULL, NULL, 0, 'Jose German Cervantes Ramos, Rosi Carmina Aispuro Picos', NULL, NULL, NULL),
(149, 'Juan Carlos', 'Marufo Flores.', 2, '', '6621200984', '6621050305', NULL, 'jc_marrufo_f@yahoo.com.mx,militzatolano@hotmail.com', NULL, NULL, 0, 'Juan Carlos Marufo Flores... renta: Carlos Servin de la Mora y Millie Tolano', NULL, NULL, NULL),
(150, 'Manuel ', 'Sandoval Delgado', 1, '', '6623262820', '', NULL, 'msandovaldelgado@yahoo.com.mx', NULL, NULL, 0, 'dueño :Manuel Sandoval Delgado ', NULL, NULL, NULL),
(151, 'Lizette Alejandra', 'Garcia Sobarzo', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Lizette Alejandra Garcia Sobarzo', NULL, NULL, NULL),
(152, 'Michell ', 'Salazar Velazquez', 1, '', '6629481384', '6621120697', NULL, '', NULL, NULL, 0, 'Michell Salazar Velazquez, Luis Fierros', NULL, NULL, NULL),
(153, 'Maria ', 'Encinas ', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Maria Encinas ', NULL, NULL, NULL),
(154, 'Julio Cesar', 'Becerra Favela', 1, '', '6623186978', '6621124944', NULL, 'juliobecerra@gmail.com, monnicatorres@gmail.com', NULL, NULL, 0, 'Julio Cesar Becerra Favela, Monica Nayely Torres Tellez', NULL, NULL, NULL),
(155, 'Mario ', 'Navarro Santos', 1, '2626745', '6622334733', '6621910113', NULL, 'mmatape.navarro@gmail.com', NULL, NULL, 0, 'Mario Navarro Santos, Danithza Navarro Sugich', NULL, NULL, NULL),
(156, 'Juan Francisco', 'Lopez ', 1, '', '6622992565', '6622991181', NULL, 'inglopezm@hotmail.com', NULL, NULL, 0, 'Juan Francisco Lopez M, Martina Irene Miranda Munguia', NULL, NULL, NULL),
(157, 'Lizeth ', 'Karina Enriquez', 1, '', '66213806601', '6621280921', NULL, 'supernancyta@hotmail.com', NULL, NULL, 0, 'Lizeth Karina Enriquez', NULL, NULL, NULL),
(158, 'Lorenzo ', 'Lopez ', 1, '', '6621619536', '', NULL, 'lantonio5@hotmail.com', NULL, NULL, 0, 'Lorenzo lopez O FAM LOPEZ', NULL, NULL, NULL),
(159, 'Jorge Francisco', 'Ruiz Sanchez', 1, '', '6622230672', '6621120606', NULL, 'denissecano@hotmail.com', NULL, NULL, 0, 'Jorge Francisco Ruiz Sanchez', NULL, NULL, NULL),
(160, 'Antonio ', 'De Los Reyes Silva', 1, '', '6621864269', '6622766401', NULL, 'tonny_600613@hotmail.com', NULL, NULL, 0, 'Antonio de los Reyes Silva', NULL, NULL, NULL),
(161, 'Hugo Abundio', 'Macias Esparza', 1, '', '6622567777', '', NULL, '', NULL, NULL, 0, 'Hugo Abundio Macias Esparza', NULL, NULL, NULL),
(162, 'Mariano Jose', 'Carreno Puebla', 1, '', '6621669261', '6621669257', NULL, 'mjosecarreno@gmail.com', NULL, NULL, 0, 'Mariano Jose Carreno Puebla y Adriana Castañeda', NULL, NULL, NULL),
(163, 'Jose Jesus', 'Ramirez Navarro', 1, '', '6621240917', '6621430716', NULL, 'pepe_ramirez@hotmail.com', NULL, NULL, 0, 'Jose Jesus Ramirez Navarro', NULL, NULL, NULL),
(164, 'Abraham Rene', 'Liera Martinez', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Fam Parra / sra Carmen Maria /Abraham Rene Liera Martinez', NULL, NULL, NULL),
(165, 'Luis ', 'Aldo Figueroa', 1, '', '6621117626', '6621567661', NULL, '', NULL, NULL, 0, 'Luis Aldo Figueroa y Myrna Islas', NULL, NULL, NULL),
(166, 'Monica ', 'Preux Preciado', 1, '', '6621902113', '6621696841', NULL, 'monicapreux@hotmail.com', NULL, NULL, 0, 'Monica Preux Preciado', NULL, NULL, NULL),
(167, 'Ramiro ', 'Valenzuela Lopez', 1, '', '6621730440', '6621190462', NULL, 'edraval@hotmail.com', NULL, NULL, 0, 'Ramiro Valenzuela  Lopez', NULL, NULL, NULL),
(168, 'Arturo ', 'Loustaunau Morales', 1, '', '6622562869', '6621418557', NULL, '', NULL, NULL, 0, 'Arturo Loustaunau Morales', NULL, NULL, NULL),
(169, 'Daniel ', 'Sainz ', 1, '', '6621429252', '6621840939', NULL, 'mabs_77@hotmail.com', NULL, NULL, 0, 'DANIEL  SAINZ', NULL, NULL, NULL),
(170, 'Belcesar ', 'Jimenez Ramirez', 1, '', '6621270440', '6621839271', NULL, 'relyr@live.com.mx', NULL, NULL, 0, 'Belcesar Jimenez Ramirez, Reyna Ruiz', NULL, NULL, NULL),
(171, 'Graciela ', 'Rodriguez Gutierrez', 1, '', '', '', NULL, 'gracielasegypian@hotmail.com, ortizvaldez_asesores@hotmail.com', NULL, NULL, 0, 'Graciela Rodriguez Gutierrez, Juan Carlos Ortiz', NULL, NULL, NULL),
(172, 'Carlos Julian', 'Sanchez Moreno', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Carlos Julian Sanchez Moreno', NULL, NULL, NULL),
(173, 'Ana Jossie', 'Salido Avila', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Ana Jossie Salido Avila y Roberto Rivera', NULL, NULL, NULL),
(174, 'Francisco ', 'Guerrero Rodriguez', 1, '', '', '', NULL, 'marciosca@hotmail.com', NULL, NULL, 0, 'Francisco Guerrero Rodriguez', NULL, NULL, NULL),
(175, 'Maria De Lourdes', 'Dorado Huitron', 1, '', '6621135571', '6621400921', NULL, 'lourdesdoradohuitron@hotmail.com', NULL, NULL, 0, 'Maria de Lourdes Dorado Huitron', NULL, NULL, NULL),
(176, 'Pavel Amaya', 'Amaya ', 1, '', '6621840320', '6621880837', NULL, 'clauruiz@hotmail.com', NULL, NULL, 0, 'Pavel Amaya', NULL, NULL, NULL),
(177, '  ', ' ', 1, '', '6621570699', '', NULL, '', NULL, NULL, 0, ' ', NULL, NULL, NULL),
(178, 'Erika Vanessa', 'Icedo Durazo', 1, '', '6622763007', '6629487877', NULL, 'erika_icedo@hotmail.com, vlad_76@hotmail.com', NULL, NULL, 0, 'Erika Vanessa Icedo Durazo, Vladimir Gutierrez Urquidez', NULL, NULL, NULL),
(179, 'Gabriela ', 'Escalante ', 1, '', '6621380975', '6622760779', NULL, '', NULL, NULL, 0, 'Gabriela Escalante', NULL, NULL, NULL),
(180, 'Maria Angelica', 'Vazquez Ramirez', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Maria Angelica Vazquez Ramirez', NULL, NULL, NULL),
(181, 'Luis ', 'Alfonso Ruiz', 1, '', '', '22260714', NULL, 'gmiranda@admvos.uson.mx', NULL, NULL, 0, 'LUIS ALFONSO RUIZ Y GPE LIZETH MIRANDA', NULL, NULL, NULL),
(182, 'Jose Omer', 'Guillot Verdugo', 1, '', '6622569288', '6621460145', NULL, '', NULL, NULL, 0, 'Jose Omer Guillot Verdugo', NULL, NULL, NULL),
(183, 'Francys Margarita', 'Arrieta Brambila', 1, '', '6621424090', '6621505401', NULL, 'uniformesarrieta@yahoo.com.mx', NULL, NULL, 0, 'Francys Margarita Arrieta Brambila', NULL, NULL, NULL),
(184, 'Denisse Sarai', 'Vazquez Tanori', 1, '', '6621788372', '', NULL, 'ernestopallanesd@hotmail.com', NULL, NULL, 0, 'denisse sarai vazquez tanori y fco ernesto pallanes', NULL, NULL, NULL),
(185, 'Marcia ', 'Lilian Oviedo', 1, '', '6622566439', '', NULL, 'oscardiaz_12@hotmail.com', NULL, NULL, 0, 'MARCIA LILIAN OVIEDO Y OSCAR E DIAZ CARRERAS', NULL, NULL, NULL),
(186, 'Luis Alberto', 'Leon Durazo', 1, '', '6622449280', '', NULL, 'paquin28@hotmail.com', NULL, NULL, 0, 'Luis Alberto Leon Durazo', NULL, NULL, NULL),
(187, 'Jose Enrique', 'Mendivil Mendoza', 1, '', '6623002253', '6622059908', NULL, 'emendivilm@hotmail.com', NULL, NULL, 0, 'Jose Enrique Mendivil Mendoza', NULL, NULL, NULL),
(188, 'Omar ', 'Estrada Martinez', 1, '', '6622791190', '', NULL, 'paraisovivero@hotmail.com', NULL, NULL, 0, 'Omar Estrada Martinez y ReYna neri', NULL, NULL, NULL),
(189, 'Leonardo ', 'Navarro Romero', 1, '2501146', '6621719050', '', NULL, '', NULL, NULL, 0, 'Leonardo Navarro Romero Y Isabel Gautrin ,  navarro ', NULL, NULL, NULL),
(190, 'Silvia Arvizu', 'Arismendiz ', 2, '', '6629189836', '6621726318', NULL, 'silhouse66@hotmail.com (Dueña), richard.barba@latecoere.fr', NULL, NULL, 0, 'Silvia Arvizu Arismendiz, Richard Barba (Inquilino, Francés)', NULL, NULL, NULL),
(191, 'Jesus Eduardo', 'Figueroa Symonds', 1, '', '2222384450', '', NULL, 'lalofigueroa@hotmail.com', NULL, NULL, 0, 'Jesus Eduardo Figueroa Symonds', NULL, NULL, NULL),
(192, 'Adriana ', 'Lagarda Tinajero', 1, '2626277', '6623001323', '6621124536', NULL, 'adlati@gmail.com, olivertp@gmail.com', NULL, NULL, 0, 'Adriana Lagarda Tinajero, Luis Oliver Thornton Pacheco', NULL, NULL, NULL),
(193, 'Gonzalo ', 'Leyva Fimbres', 1, '', '6621940374', '6621433849', NULL, 'gonzalo_leyva@hotmail.com', NULL, NULL, 0, 'Gonzalo Leyva Fimbres', NULL, NULL, NULL),
(194, 'Ramon Gabriel', 'Burrola Hernandez', 1, '', '6622563470', '6621147110', NULL, 'rgburrola@cox.net', NULL, NULL, 0, 'Ramon Gabriel Burrola Hernandez', NULL, NULL, NULL),
(195, 'Pablo Cesar', 'Morales Garcia', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Pablo Cesar Morales Garcia', NULL, NULL, NULL),
(196, 'Hector ', 'Navarro Sanchez', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Hector Navarro Sanchez', NULL, NULL, NULL),
(197, 'Carla Alejandra', 'Martinez Carrillo', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Carla Alejandra Martinez Carrillo', NULL, NULL, NULL),
(198, 'Dora Edith', 'Rios Campa', 1, '', '6622979078', '6621897648', NULL, 'dedithrios@hotmail.com', NULL, NULL, 0, 'Dora Edith Rios Campa', NULL, NULL, NULL),
(199, 'Sergio ', 'Sosa Contreras', 1, '', '6453405018', '6453312216', NULL, 'elmack15@hotmail.com', NULL, NULL, 0, 'Sergio Sosa Contreras', NULL, NULL, NULL),
(200, 'Maria Concepcion', 'Delgado Arias', 1, '', '6323248646', '6621705512', NULL, '', NULL, NULL, 0, 'Maria Concepcion  Delgado Arias', NULL, NULL, NULL),
(201, 'Jose Alberto', 'Lopez Valenzuela', 1, '', '6622396892', '', NULL, '', NULL, NULL, 0, 'Jose Alberto Lopez Valenzuela', NULL, NULL, NULL),
(202, 'Luis Enrique', 'Guardado Lugo', 1, '', '6622793134', '6623290753', NULL, '', NULL, NULL, 0, 'Luis Enrique Guardado Lugo', NULL, NULL, NULL),
(203, 'Manuel ', 'Trujillo Escobedo', 1, '', '6622823740', '', NULL, 'yadira1129@gmail.com', NULL, NULL, 0, 'Manuel Trujillo Escobedo', NULL, NULL, NULL),
(204, 'Oscar ', 'Vega Amaya', 1, '', '6621950380', '6623009568', NULL, 'victor.ramirez.2012@hotmail.com', NULL, NULL, 0, 'Oscar Vega Amaya, Maria del carmen vazquez Valente Antinio Valdez fam valdez ', NULL, NULL, NULL),
(205, 'Silvia Rosario', 'Felix Encinas', 1, '', '6622060188', '6622999930', NULL, 'maquipo39@hotmail.com', NULL, NULL, 0, 'SILVIA ROSARIO FELIX ENCINAS', NULL, NULL, NULL),
(206, 'Maria ', 'Teresa Robles', 1, '', '6621044439', '', NULL, 'trobles@gauss.mat.uson.mx', NULL, NULL, 0, 'Maria Teresa Robles,silvia calvimontes y hector garavito', NULL, NULL, NULL),
(207, 'Rene Alejandro', 'Rodriguez Pedraza', 1, '', '6621050492', '', NULL, '', NULL, NULL, 0, 'Rene Alejandro Rodriguez Pedraza', NULL, NULL, NULL),
(208, 'Guillermo ', 'Williams Bautista', 1, '', '', '', NULL, 'gwilliamsb@live.com.mx', NULL, NULL, 0, 'Guillermo Williams Bautista, Oscar Vega Amaya', NULL, NULL, NULL),
(209, 'Diego ', 'Silva ', 1, '', '', '', NULL, 'sacosmty@hotmail.com', NULL, NULL, 0, 'DIEGO  SILVA', NULL, NULL, NULL),
(210, 'Karina Elena', 'Del Cid Robles', 1, '', '6622784123', '6621272001', NULL, 'karina_delcid@hotmail.com', NULL, NULL, 0, 'Karina Elena del Cid Robles, fam del cid', NULL, NULL, NULL),
(211, 'Bertha Ema', 'Valdez Ruedaflores', 1, '', '6623443049', '6621804629', NULL, 'makaveli7@hotmail.com', NULL, NULL, 0, 'Bertha Ema Valdez Ruedaflores Fam guerrero', NULL, NULL, NULL),
(212, 'Miguel ', 'Woo Cejudo', 1, '', '', '', NULL, 'miguel.woo@gmail.com', NULL, NULL, 0, 'Miguel Woo Cejudo', NULL, NULL, NULL),
(213, 'Angel ', 'Acuna ', 1, '', '6621840705', '6621560982', NULL, 'angelacuna@aquafim.com', NULL, NULL, 0, 'Angel Acuna, Carolina Espadas Rodriguez', NULL, NULL, NULL),
(214, 'Blanca Ofelia', 'Rios ', 1, '', '6621730620', '6621202328', NULL, 'boro21@hotmail.com, wilfredo.perez@cfe.gob.mx', NULL, NULL, 0, 'Blanca Ofelia Rios, Wilfredo Perez, Jorge Soto Rodriguez', NULL, NULL, NULL),
(215, 'Cristina Carlos', 'Bugarin ', 1, '', '6621690616', '6621690617', NULL, 'cristinitacb@hotmail.com', NULL, NULL, 0, 'Cristina Carlos Bugarin y jesus Durazo', NULL, NULL, NULL),
(216, 'Domingo ', 'Romero Morales', 1, '', '6621465663', '6621464416', NULL, 'chochiros@hotmail.com', NULL, NULL, 0, 'Domingo Romero Morales, Xochitl Sanchez', NULL, NULL, NULL),
(217, 'Gabriela Elena', 'Villegas Venegas', 1, '', '6621404166', '6621460416', NULL, 'gaby_ev@hotmail.com', NULL, NULL, 0, 'Gabriela Elena Villegas Venegas, Jesus Julian Bracamonte', NULL, NULL, NULL),
(218, 'Zoila Guadalupe', 'Soto Rodriguez', 1, '', '6621420350', '6621430714', NULL, 'dansg79@gmail.com', NULL, NULL, 0, 'Zoila Guadalupe Soto Rodriguez', NULL, NULL, NULL),
(219, 'Agustina ', 'Soria Valdez', 1, '', '6621120664', '6621832604', NULL, 'valeriasoria1@hotmail.com', NULL, NULL, 0, 'Agustina Soria Valdez', NULL, NULL, NULL),
(220, 'Paulo David', 'Villa Holguin', 1, '', '6621940439', '6622293980', NULL, 'carol_calderonv@yahoo.com.mx', NULL, NULL, 0, 'Paulo David Villa Holguin, Carolina Calderon', NULL, NULL, NULL),
(221, 'Salvador ', 'Ibanez Vasquez', 1, '6861222089', '6861222089', '6621030768', NULL, '', NULL, NULL, 0, 'Salvador Ibanez Vasquez', NULL, NULL, NULL),
(222, 'Erendira ', 'Bustamante Garcia', 1, '', '6621490892', '6622232927', NULL, '', NULL, NULL, 0, 'Erendira Bustamante Garcia', NULL, NULL, NULL),
(223, 'Manuel Alberto', 'Navarro Herrera', 1, '', '6621240418', '6621401902', NULL, 'alberto_navarro_herrera@hotmail.com', NULL, NULL, 0, 'Manuel Alberto Navarro Herrera', NULL, NULL, NULL),
(224, 'Rene Alejandro', 'Arreola Servin', 1, '', '6621020756', '6629488449', NULL, 'rene.arreola@gmail.com', NULL, NULL, 0, 'Rene Alejandro Arreola Servin', NULL, NULL, NULL),
(225, 'Emma Luz', 'Ruiz Sanchez', 1, '', '6622811849', '6621395595', NULL, 'emmaluz26@hotmail.com', NULL, NULL, 0, 'EMMA LUZ RUIZ SANCHEZ Y GILDEGARD QUROZ M.', NULL, NULL, NULL),
(226, 'Juan Carlos', 'Almada Gutierrez', 1, '', '6622685853', '', NULL, 'jc_almada_cacho@hotmail.com', NULL, NULL, 0, 'Juan Carlos Almada Gutierrez', NULL, NULL, NULL),
(227, 'Ramon Heberto', 'Gomez Velarde', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Ramon Heberto Gomez Velarde', NULL, NULL, NULL),
(228, 'Lizeth Amine', 'Gaxiola Lem', 1, '', '6621020305', '6622339705', NULL, 'liz_amine@hotmail.com', NULL, NULL, 0, 'Lizeth Amine Gaxiola Lem', NULL, NULL, NULL),
(229, 'Jose Cruz', 'Valenzuela Leon', 1, '', '6621054277', '6621055005', NULL, '', NULL, NULL, 0, 'Jose Cruz Valenzuela Leon ', NULL, NULL, NULL),
(230, 'Alan ', 'Durazo B', 1, '', '6621270458', '6621130401', NULL, 'adurazo@gmail.com, jelyguzman@hotmail.com', NULL, NULL, 0, 'Alan Durazo B, Jesus Alicia Guzman Hinojosa', NULL, NULL, NULL),
(231, 'Joel Avel', 'Ayala Rodriguez', 1, '', '6621020072', '2626263', NULL, 'joel.ayala@hotmail.com', NULL, NULL, 0, 'Joel Avel Ayala Rodriguez y Yolanda edith Castillo', NULL, NULL, NULL),
(232, 'Sergio Humberto', 'Valenzuela Mendez', 1, '', '6621131038', '2 62 7878', NULL, '', NULL, NULL, 0, 'Sergio Humberto Valenzuela Mendez', NULL, NULL, NULL),
(233, 'Melina ', 'Briseño ', 1, '', '6621816957', '', NULL, 'melinabrise-o1@hotmail.com', NULL, NULL, 0, 'MELINA BRISEÑO, ', NULL, NULL, NULL),
(234, 'Joel ', 'Gonzalez Estrella', 1, '', '6621077185', '6621937492', NULL, '', NULL, NULL, 0, 'Joel Gonzalez Estrella', NULL, NULL, NULL),
(235, 'Jesus Antonio', 'Valdez Vega', 1, '', '6621871367', '', NULL, '', NULL, NULL, 0, 'Jesus Antonio Valdez Vega y Veronica Romero y fco j valdez', NULL, NULL, NULL),
(236, 'Juan Pablo', 'Acosta Gutierrez', 1, '', '6621030375', '6621492811', NULL, 'whatanna_2000@hotmail.com', NULL, NULL, 0, 'Juan Pablo Acosta Gutierrez', NULL, NULL, NULL),
(237, 'Cesar Eduardo', 'Patino Patino', 1, '', '', '', NULL, '', NULL, NULL, 0, 'Cesar Eduardo Patino Patino, Martha Ruiz Patino', NULL, NULL, NULL),
(238, 'Carlos Santiago', 'Quiros Beltrones', 1, '', '6621240612', '29482088', NULL, 'csqb@hotmail.com', NULL, NULL, 0, 'Carlos Santiago Quiros Beltrones', NULL, NULL, NULL),
(239, 'Carlos Alberto', 'Valdez Ruedaflores', 1, '', '6621240576', '', NULL, 'varc65@prodigy.net.mx', NULL, NULL, 0, 'Carlos Alberto Valdez Ruedaflores', NULL, NULL, NULL),
(240, 'Ringo ', 'Ramos Duran', 1, '', '6622334939', '6622386640', NULL, 'lorenia_ab2@hotmail.com', NULL, NULL, 0, 'Ringo Ramos Duran', NULL, NULL, NULL),
(241, 'Jeronimo ', 'Martinez Garcia', 1, '', '6621490860', '6621490473', NULL, 'jero14@hotmail.com, lorenava75@hotmail.com', NULL, NULL, 0, 'Jeronimo Martinez Garcia, Lorena Navarro Coronado', NULL, NULL, NULL),
(242, 'Juan Jaime', 'Sanchez Meza', 1, '', '', '', NULL, '', NULL, NULL, 0, 'JUAN JAIME SANCHEZ MEZA', NULL, NULL, NULL),
(243, 'Fabian Aaron', 'Rivera Gonzalez', 1, '', '6622918350', '6621410183', NULL, 'lupitariveranovias@hotmail.com', NULL, NULL, 0, 'Fabian Aaron Rivera Gonzalez', NULL, NULL, NULL),
(244, 'Pedro ', 'Griego Durazo', 1, '', '6621576304', '6621801970', NULL, 'cecy.peralta@hotmail.com', NULL, NULL, 0, ' Pedro Griego Durazoy Cecilia Peralta', NULL, NULL, NULL),
(245, 'Adolfo ', 'Espinoza Larios', 1, '', '6621550728', '6621489397', NULL, 'monica_lm14@hotmail.com', NULL, NULL, 0, 'Adolfo Espinoza Larios', NULL, NULL, NULL),
(246, 'Saul Ernesto', 'Sanabria Gibert', 1, '', '6621230602', '6621670082', NULL, 'shady_ysm@hotmail.com', NULL, NULL, 0, 'Saul Ernesto Sanabria Gibert', NULL, NULL, NULL),
(247, 'Patricia ', 'Salazar ', 1, '', '', '6622567063', NULL, 'psalazarcampillo@gmail.com', NULL, NULL, 0, 'Patricia Salazar', NULL, NULL, NULL),
(248, 'Maria Eugenia', 'Robles Leyva', 1, '', '6621141263', '6622213132', NULL, 'brunosoto78@hotmail.com', NULL, NULL, 0, 'Maria Eugenia Robles Leyva, Ricardo Soto Acuna', NULL, NULL, NULL),
(249, 'Edgardo Solis', 'Bobadilla ', 1, '', '6622990966', '', NULL, 'esolis1973@hotmail.com', NULL, NULL, 0, 'Edgardo Solis Bobadilla', NULL, NULL, NULL),
(250, 'Jorge Luis', 'Gomez Amado', 1, '', '6621370880', '6621050403', NULL, 'pattyvalenciaq@live.com.mx', NULL, NULL, 0, 'Jorge Luis Gomez Amado', NULL, NULL, NULL),
(251, 'Leopoldo Salcido', 'Grijalva ', 1, '', '6621140923', '6623592350', NULL, 'polo_salcido@hotmail.com', NULL, NULL, 0, 'Leopoldo Salcido Grijalva', NULL, NULL, NULL),
(252, 'Jose Antonio', 'Martinez Lopez', 1, '', '5543811636', '', NULL, '', NULL, NULL, 0, 'Jose Antonio Martinez Lopez', NULL, NULL, NULL),
(253, 'Vicente ', 'Sagrestano Alcaraz', 1, '', '6622231722', '6629487116', NULL, '', NULL, NULL, 0, 'Vicente Sagrestano Alcaraz', NULL, NULL, NULL),
(254, 'Erick ', 'Mora ', 1, '', '6621490309', '6621420501', NULL, 'ing_erickmora@hotmail', NULL, NULL, 0, ' dueño erick mora', NULL, NULL, NULL),
(255, 'Ada Lucia', 'Tellez Alvarez', 1, '', '6622023772', '6622780745', NULL, 'abelmonrealt@hotmail.com', NULL, NULL, 0, 'Ada Lucia Tellez Alvarez y Abel Monrreal', NULL, NULL, NULL),
(256, 'Beatriz Adriana', 'Molina Vazquez', 1, '', '6622229411', '', NULL, '', NULL, NULL, 0, 'Beatriz Adriana Molina Vazquez', NULL, NULL, NULL),
(257, 'Maria Guadalupe', 'Andrade Estrada', 1, '', '6629489896', '6622762123', NULL, 'luphyta_andrade@hotmail.com', NULL, NULL, 0, 'Maria Guadalupe Andrade Estrada', NULL, NULL, NULL),
(258, 'Ramon Eduardo', 'Arvizu Luna', 1, '', '6621430855', '6621866092', NULL, 'sonia@valencianavarrete.com', NULL, NULL, 0, 'Roberto Valencia Perez/Nuevo dueño Ramon Eduardo Arvizu Luna', NULL, NULL, NULL),
(259, 'Edna ', 'Diaz Diaz', 1, '', '6621671577', '6622260179', NULL, 'mdiaz@energiaelectrica.com.mx', NULL, NULL, 0, 'Edna Diaz', NULL, NULL, NULL),
(260, 'Angela Beatriz', 'Gonzalez Vargas', 1, '', '6629489723', '6622990071', NULL, '', NULL, NULL, 0, 'Angela Beatriz Gonzalez Vargas', NULL, NULL, NULL),
(261, 'Hipolito ', 'Lopez Gordillo', 2, '', '6623535867', '', NULL, 'fernandolopezdurazo@gmail.com', NULL, NULL, 0, 'Hipolito Lopez Gordillo, renta Rosario', NULL, NULL, NULL),
(262, 'Felipe ', 'Velazquez Luis', 1, '', '6621832364', '', NULL, 'velazquezlf@gmail.com', NULL, NULL, 0, 'Felipe Velazquez Luis', NULL, NULL, NULL),
(263, 'Marco Antonio', 'Davila Flores', 1, '', '6622566200', '6622909197', NULL, 'giraexitos@hotmail.com, rodriguezmartha@live.com', NULL, NULL, 0, 'Marco Antonio Davila Flores y Martha l. Rodriguez', NULL, NULL, NULL),
(264, 'Javier Humberto', 'Vivian Jimenez', 1, '', '6621380631', '6621159383', NULL, 'javiervivian1@hotmail.com', NULL, NULL, 0, 'Javier Humberto Vivian Jimenez', NULL, NULL, NULL),
(265, 'Erik Ivan', 'Camou Vucovich', 1, '', '6621035555', '6621274516', NULL, 'guerocamou@hotmail.com', NULL, NULL, 0, 'Erik Ivan Camou Vucovich, Ana Alicia Preciado', NULL, NULL, NULL),
(266, 'Luis Alberto', 'Chavez Perez', 1, '', '6622566587', '6622762214', NULL, 'luisalberto.chavez@hotmail.com', NULL, NULL, 0, 'Luis Alberto Chavez Perez', NULL, NULL, NULL),
(267, 'Argelia ', 'Villelas ', 1, '', '6621960696', '6621960425', NULL, '', NULL, NULL, 0, ' Argelia Villelas', NULL, NULL, NULL),
(268, 'Estanislao Andres', 'Salas Ramirez', 1, '', '6622960082', '', NULL, 'asalas1920@hotmail.com', NULL, NULL, 0, 'Estanislao Andres Salas Ramirez/ Georgina Gamez', NULL, NULL, NULL),
(269, 'Abdel ', 'Valles ', 1, '', '6621695320', '6622473295', NULL, 'karluz_paty@hotmail.com', NULL, NULL, 0, 'Abdel Valles, Patricia Jaime Garcia', NULL, NULL, NULL),
(270, 'Margarita Rodriguez', 'Salcido ', 1, '', '6621960340', '6621732777', NULL, 'margarita.rodriguez@dicanosa.com.mx', NULL, NULL, 0, 'Margarita Rodriguez Salcido, Mauricio Muñoz Romo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resident_types`
--

CREATE TABLE `resident_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `resident_type` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `resident_types`
--

INSERT INTO `resident_types` (`id`, `resident_type`, `created`, `modified`) VALUES
(1, 'Permanente', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(2, 'Temporal', '2016-01-13 06:10:00', '2016-01-13 06:10:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_providers`
--

CREATE TABLE `service_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_info` tinyint(1) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `residents_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(55) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `group_id`, `residents_id`, `username`, `name`, `email`, `password`, `remember_token`, `active`, `image`, `neighborhood_id`, `created`, `modified`) VALUES
(2, 5, 19, 'rob', 'Roberto', 'robtejeda@gmail.com', '$2a$10$gWRRe8DJCTorV3AiomtFx.tiQ1tEM.rfW7zXftsBLJjN9/XCSekn.', NULL, 1, '', 1, '2016-04-07 17:18:35', '2016-04-14 16:18:30'),
(4, 5, 18, 'admin', 'Roberto Tejeda', 'rob@ozonostudios.com', '$2a$10$KC/cNgpM9tBP7iouZrr22.AbXzKDU9ElpX0bqDu9KfnSysHlpI.9C', NULL, 1, 'img\\user\\4\\DSC02591.JPG', 1, '2016-04-07 17:36:05', '2016-04-13 22:32:54'),
(5, 5, 154, 'juliobecerra', 'Julio Cesar Becerra Favela', 'juliobecerra@gmail.com', '$2a$10$NCQUIFZ4RwFPJsin61b2BO5uCenADqX4FQDtR/S8p0CjI4RK/ulea', NULL, 1, '', 1, '2016-05-20 18:11:39', '2016-05-20 18:11:39');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announcements_neighborhoods_id_foreign` (`neighborhoods_id`) USING BTREE,
  ADD KEY `announcements_announcement_types_id_foreign` (`announcement_types_id`);

--
-- Indices de la tabla `announcement_types`
--
ALTER TABLE `announcement_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `charges`
--
ALTER TABLE `charges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charges_charge_type_id_foreign` (`charge_type_id`),
  ADD KEY `charges_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `charge_types`
--
ALTER TABLE `charge_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_expense_type_id_foreign` (`expense_type_id`),
  ADD KEY `expenses_service_provider_id_foreign` (`service_provider_id`),
  ADD KEY `expenses_payment_type_id_foreign` (`payment_type_id`);

--
-- Indices de la tabla `expense_types`
--
ALTER TABLE `expense_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilities_neighborhood_id_foreign` (`neighborhood_id`);

--
-- Indices de la tabla `facilities_rental`
--
ALTER TABLE `facilities_rental`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilities_rental_facility_id_foreign` (`facility_id`),
  ADD KEY `facilities_rental_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD KEY `group_permission_group_id_foreign` (`group_id`),
  ADD KEY `group_permission_permission_id_foreign` (`permission_id`);

--
-- Indices de la tabla `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homes_resident_id_foreign` (`resident_id`),
  ADD KEY `homes_home_type_id_foreign` (`home_type_id`),
  ADD KEY `homes_neighborhood_id_foreign` (`neighborhood_id`);

--
-- Indices de la tabla `home_types`
--
ALTER TABLE `home_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory_expense_id_foreign` (`expense_id`);

--
-- Indices de la tabla `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_payment_type_id_foreign` (`payment_type_id`),
  ADD KEY `payments_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `residents`
--
ALTER TABLE `residents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `residents_resident_type_id_foreign` (`resident_type_id`);

--
-- Indices de la tabla `resident_types`
--
ALTER TABLE `resident_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `service_providers`
--
ALTER TABLE `service_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_user_unique` (`username`) USING BTREE,
  ADD KEY `users_residents_id_foreign` (`residents_id`),
  ADD KEY `users_group_id_foreign` (`group_id`) USING BTREE,
  ADD KEY `users_neighborhood_id_foreign` (`neighborhood_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `announcement_types`
--
ALTER TABLE `announcement_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `charges`
--
ALTER TABLE `charges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `charge_types`
--
ALTER TABLE `charge_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `expense_types`
--
ALTER TABLE `expense_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `facilities_rental`
--
ALTER TABLE `facilities_rental`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `homes`
--
ALTER TABLE `homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;
--
-- AUTO_INCREMENT de la tabla `home_types`
--
ALTER TABLE `home_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `neighborhoods`
--
ALTER TABLE `neighborhoods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `residents`
--
ALTER TABLE `residents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;
--
-- AUTO_INCREMENT de la tabla `resident_types`
--
ALTER TABLE `resident_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `service_providers`
--
ALTER TABLE `service_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `charges`
--
ALTER TABLE `charges`
  ADD CONSTRAINT `charges_charge_type_id_foreign` FOREIGN KEY (`charge_type_id`) REFERENCES `charge_types` (`id`),
  ADD CONSTRAINT `charges_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`);

--
-- Filtros para la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_expense_type_id_foreign` FOREIGN KEY (`expense_type_id`) REFERENCES `expense_types` (`id`),
  ADD CONSTRAINT `expenses_service_provider_id_foreign` FOREIGN KEY (`service_provider_id`) REFERENCES `service_providers` (`id`);

--
-- Filtros para la tabla `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_neighborhood_id_foreign` FOREIGN KEY (`neighborhood_id`) REFERENCES `neighborhoods` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
