-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-05-2016 a las 01:56:14
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;

--
-- Base de datos: `ozono_miresidencial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhoods_id` int(10) UNSIGNED NOT NULL,
  `announcement_types_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `announcement` text CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `announcement_types`
--

CREATE TABLE `announcement_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `announcement_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `class` varchar(50) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `announcement_types`
--

INSERT INTO `announcement_types` (`id`, `announcement_type`, `class`, `created`, `modified`) VALUES
(1, 'General', 'default', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(2, 'Urgente', 'danger', '2016-03-10 00:00:00', '2016-03-10 00:00:00'),
(3, 'Logro', 'success', '2016-03-10 00:00:00', '2016-03-10 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `charges`
--

CREATE TABLE `charges` (
  `id` int(10) UNSIGNED NOT NULL,
  `charge_type_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `charge_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `charge_types`
--

CREATE TABLE `charge_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `charge_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `charge_types`
--

INSERT INTO `charge_types` (`id`, `charge_type`, `created`, `modified`) VALUES
(1, 'Mensualidad', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(2, 'Recargo', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(3, 'Renta de Área Común', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(4, 'Pago Daños', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(5, 'Aportación a Mejoras', '2016-01-13 13:10:00', '2016-01-13 13:10:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(11) UNSIGNED NOT NULL,
  `expense_type_id` int(10) UNSIGNED NOT NULL,
  `payment_type_id` int(10) UNSIGNED NOT NULL,
  `service_provider_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `expense_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expense_types`
--

CREATE TABLE `expense_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `expense_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `expense_types`
--

INSERT INTO `expense_types` (`id`, `expense_type`, `created`, `modified`) VALUES
(1, 'Administrativo', '2016-01-13 13:10:00', '2016-01-13 13:10:00'),
(2, 'Jardinería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(3, 'Seguridad', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(4, 'Equimamiento', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(5, 'Pavimentación', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(6, 'Alberca', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(7, 'Plomería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(8, 'Albañilería', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(9, 'Computación', '2016-01-13 13:10:01', '2016-01-13 13:10:01'),
(10, 'Transporte', '2016-01-13 13:10:01', '2016-01-13 13:10:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `facility` varchar(255) CHARACTER SET utf8 NOT NULL,
  `area_info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rental_cost` double(19,4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `facilities`
--

INSERT INTO `facilities` (`id`, `neighborhood_id`, `facility`, `area_info`, `latitude`, `longitude`, `rental_cost`, `created`, `modified`) VALUES
(1, 1, 'Entrada', 'Área ubicada al costado derecho de la caseta de vigilancia. Cuenta con baño y kiosko.', '29.076097', '-111.026481', 2000.0000, '2016-01-13 13:10:04', '2016-01-13 13:10:04'),
(2, 1, 'Alberca', 'Área ubicada una cuadra después de la caseta. Cuenta con alberca, chapoteadero, palapa y baños.', '29.075933', '-111.027681', 3500.0000, '2016-01-13 13:10:04', '2016-01-13 13:10:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facilities_rental`
--

CREATE TABLE `facilities_rental` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Residente', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(2, 'Seguridad', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(3, 'Finanzas', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(4, 'Administrador', '2016-04-08 00:09:00', '2016-04-08 00:09:00'),
(5, 'Super Administrador', '2016-04-08 00:09:00', '2016-04-08 00:09:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `group_permissions`
--

CREATE TABLE `group_permissions` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `homes`
--

CREATE TABLE `homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resident_id` int(10) UNSIGNED NOT NULL,
  `home_type_id` int(10) UNSIGNED NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `home_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_types`
--

CREATE TABLE `home_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_types`
--

INSERT INTO `home_types` (`id`, `home_type`, `created`, `modified`) VALUES
(1, 'Propia', '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(2, 'Renta', '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(3, 'Desocupada', '2016-01-13 06:09:59', '2016-01-13 06:09:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventory`
--

CREATE TABLE `inventory` (
  `id` int(10) UNSIGNED NOT NULL,
  `expense_id` int(10) UNSIGNED NOT NULL,
  `product` varchar(150) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8 NOT NULL,
  `model` varchar(150) CHARACTER SET utf8 NOT NULL,
  `serial` varchar(150) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `log` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `neighborhoods`
--

CREATE TABLE `neighborhoods` (
  `id` int(10) UNSIGNED NOT NULL,
  `neighborhood` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `monthly_fee` float NOT NULL,
  `period_start` tinyint(3) UNSIGNED NOT NULL,
  `payments_before_default` tinyint(3) UNSIGNED NOT NULL DEFAULT '2',
  `payment_on_agreement` tinyint(3) UNSIGNED NOT NULL DEFAULT '2',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `neighborhoods`
--

INSERT INTO `neighborhoods` (`id`, `neighborhood`, `latitude`, `longitude`, `website`, `additional_info`, `image`, `monthly_fee`, `period_start`, `payments_before_default`, `payment_on_agreement`, `created`, `modified`) VALUES
(1, 'Alter Real', '29.076030', '-111.026247', 'https://www.facebook.com/groups/alterreal/', 'Cerrada Alter Real del Fraccionamiento Corceles Residencial.', 'img\\neighborhood\\1\\DSC02466.JPG', 350, 15, 2, 2, '2016-03-14 00:00:00', '2016-05-17 10:06:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type_id` int(10) UNSIGNED NOT NULL,
  `home_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bonification_approved` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_types`
--

CREATE TABLE `payment_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_cash` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payment_types`
--

INSERT INTO `payment_types` (`id`, `payment_type`, `is_cash`, `created`, `modified`) VALUES
(1, 'Efectivo', 1, '2016-01-13 06:09:59', '2016-01-13 06:09:59'),
(2, 'Transferencia', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(3, 'PayPal', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(4, 'Cheque', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(5, 'Trajeta', 0, '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(6, 'Bonificación', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residents`
--

CREATE TABLE `residents` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `resident_type_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_info` tinyint(1) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resident_types`
--

CREATE TABLE `resident_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `resident_type` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `resident_types`
--

INSERT INTO `resident_types` (`id`, `resident_type`, `created`, `modified`) VALUES
(1, 'Russell Schimmel III', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(2, 'Dock Watsica', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(3, 'Rico Parisian', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(4, 'Ron Johnston', '2016-01-13 06:10:00', '2016-01-13 06:10:00'),
(5, 'Antonina Cormier', '2016-01-13 06:10:00', '2016-01-13 06:10:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_providers`
--

CREATE TABLE `service_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_info` tinyint(1) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `additional_info` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `residents_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(55) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `neighborhood_id` int(10) UNSIGNED NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `group_id`, `residents_id`, `username`, `name`, `email`, `password`, `remember_token`, `active`, `image`, `neighborhood_id`, `created`, `modified`) VALUES
(2, 5, 19, 'rob', 'Roberto', 'robtejeda@gmail.com', '$2a$10$gWRRe8DJCTorV3AiomtFx.tiQ1tEM.rfW7zXftsBLJjN9/XCSekn.', NULL, 1, '', 1, '2016-04-07 17:18:35', '2016-04-14 16:18:30'),
(4, 5, 18, 'admin', 'Roberto Tejeda', 'rob@ozonostudios.com', '$2a$10$KC/cNgpM9tBP7iouZrr22.AbXzKDU9ElpX0bqDu9KfnSysHlpI.9C', NULL, 1, 'img\\user\\4\\DSC02591.JPG', 1, '2016-04-07 17:36:05', '2016-04-13 22:32:54');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announcements_neighborhoods_id_foreign` (`neighborhoods_id`) USING BTREE,
  ADD KEY `announcements_announcement_types_id_foreign` (`announcement_types_id`);

--
-- Indices de la tabla `announcement_types`
--
ALTER TABLE `announcement_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `charges`
--
ALTER TABLE `charges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charges_charge_type_id_foreign` (`charge_type_id`),
  ADD KEY `charges_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `charge_types`
--
ALTER TABLE `charge_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_expense_type_id_foreign` (`expense_type_id`),
  ADD KEY `expenses_service_provider_id_foreign` (`service_provider_id`),
  ADD KEY `expenses_payment_type_id_foreign` (`payment_type_id`);

--
-- Indices de la tabla `expense_types`
--
ALTER TABLE `expense_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilities_neighborhood_id_foreign` (`neighborhood_id`);

--
-- Indices de la tabla `facilities_rental`
--
ALTER TABLE `facilities_rental`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilities_rental_facility_id_foreign` (`facility_id`),
  ADD KEY `facilities_rental_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD KEY `group_permission_group_id_foreign` (`group_id`),
  ADD KEY `group_permission_permission_id_foreign` (`permission_id`);

--
-- Indices de la tabla `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homes_resident_id_foreign` (`resident_id`),
  ADD KEY `homes_home_type_id_foreign` (`home_type_id`),
  ADD KEY `homes_neighborhood_id_foreign` (`neighborhood_id`);

--
-- Indices de la tabla `home_types`
--
ALTER TABLE `home_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventory_expense_id_foreign` (`expense_id`);

--
-- Indices de la tabla `neighborhoods`
--
ALTER TABLE `neighborhoods`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_payment_type_id_foreign` (`payment_type_id`),
  ADD KEY `payments_home_id_foreign` (`home_id`);

--
-- Indices de la tabla `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `residents`
--
ALTER TABLE `residents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `residents_resident_type_id_foreign` (`resident_type_id`);

--
-- Indices de la tabla `resident_types`
--
ALTER TABLE `resident_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `service_providers`
--
ALTER TABLE `service_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_user_unique` (`username`) USING BTREE,
  ADD KEY `users_residents_id_foreign` (`residents_id`),
  ADD KEY `users_group_id_foreign` (`group_id`) USING BTREE,
  ADD KEY `users_neighborhood_id_foreign` (`neighborhood_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `announcement_types`
--
ALTER TABLE `announcement_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `charges`
--
ALTER TABLE `charges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `charge_types`
--
ALTER TABLE `charge_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `expense_types`
--
ALTER TABLE `expense_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `facilities_rental`
--
ALTER TABLE `facilities_rental`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `homes`
--
ALTER TABLE `homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `home_types`
--
ALTER TABLE `home_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `neighborhoods`
--
ALTER TABLE `neighborhoods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `residents`
--
ALTER TABLE `residents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `resident_types`
--
ALTER TABLE `resident_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `service_providers`
--
ALTER TABLE `service_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `charges`
--
ALTER TABLE `charges`
  ADD CONSTRAINT `charges_charge_type_id_foreign` FOREIGN KEY (`charge_type_id`) REFERENCES `charge_types` (`id`),
  ADD CONSTRAINT `charges_home_id_foreign` FOREIGN KEY (`home_id`) REFERENCES `homes` (`id`);

--
-- Filtros para la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_expense_type_id_foreign` FOREIGN KEY (`expense_type_id`) REFERENCES `expense_types` (`id`),
  ADD CONSTRAINT `expenses_service_provider_id_foreign` FOREIGN KEY (`service_provider_id`) REFERENCES `service_providers` (`id`);

--
-- Filtros para la tabla `facilities`
--
ALTER TABLE `facilities`
  ADD CONSTRAINT `facilities_neighborhood_id_foreign` FOREIGN KEY (`neighborhood_id`) REFERENCES `neighborhoods` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
