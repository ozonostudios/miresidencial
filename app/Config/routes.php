<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
    Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
    Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Custom Routes
 */
    Router::connect('/usuarios/ingresar', array('controller' => 'users', 'action' => 'login'));
    Router::connect('/usuarios/salir', array('controller' => 'users', 'action' => 'logout'));
    Router::connect('/mi-perfil/*', array('controller' => 'users', 'action' => 'edit'));
    Router::connect('/mi-residencial/*', array('controller' => 'neighborhoods', 'action' => 'edit'));
    Router::connect('/reportes/*', array('controller' => 'pages', 'action' => 'reports'));
    Router::connect('/file-upload/*', array('controller' => 'app', 'action' => 'upload'));
    Router::connect('/image-exists/*', array('controller' => 'app', 'action' => 'imageExists'));

/**
 * Batch connect for scaffolding routes for
 * translation
 */

    $controllers = array(
        'announcements' =>          'avisos',
        'announcements_types' =>    'tipos_aviso',
        'charges' =>                'cargos',
        'charge_types' =>           'tipos_cargo',
        'expenses' =>               'gastos',
        'expense_types' =>          'tipos_gasto',
        'facilities' =>             'areas',
        'facilities_rentals' =>     'renta_areas',
        'groups' =>                 'grupos',
        'homes' =>                  'casas',
        'home_types' =>             'tipos_casa',
        'inventories' =>            'inventario',
        'logs' =>                   'logs',
        'neighborhoods' =>          'fraccionamientos',
        'payments' =>               'pagos',
        'payment_types' =>          'tipos_pago',
        'permissions' =>            'permisos',
        'residents' =>              'residentes',
        'resident_types' =>         'tipos_residente',
        'service_providers' =>      'proveedores',
        'users' =>                  'usuarios',
        'groups' =>                 'tipos_usuario',
    );

    $routes = array(
        'index' => '',
        'add' => '/agregar/*',
        'edit' => '/editar/*',
        'view' => '/ver/*',
    );

    $repeated = array(
        'index' => '/index/*',
    );

    foreach ($controllers as $key1 => $value1) {

        foreach ($routes as $key2 => $value2) {

            Router::connect('/'.$controllers[$key1].$routes[$key2], array('controller' => $key1, 'action' => $key2));
        }

        foreach ($repeated as $key2 => $value2) {

            Router::connect('/'.$controllers[$key1].$repeated[$key2], array('controller' => $key1, 'action' => $key2));
        }
    }
    
    // Router::connect('/casas/index/*', array('controller' => 'homes', 'action' => 'index'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
    CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
    require CAKE . 'Config' . DS . 'routes.php';
