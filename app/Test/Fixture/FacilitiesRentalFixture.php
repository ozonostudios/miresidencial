<?php
/**
 * FacilitiesRental Fixture
 */
class FacilitiesRentalFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'facilities_rental';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'primary'),
		'facility_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'home_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'from' => array('type' => 'timestamp', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'to' => array('type' => 'timestamp', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'created' => array('type' => 'timestamp', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'modified' => array('type' => 'timestamp', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'facilities_rental_facility_id_foreign' => array('column' => 'facility_id', 'unique' => 0),
			'facilities_rental_home_id_foreign' => array('column' => 'home_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'facility_id' => 1,
			'home_id' => 1,
			'from' => 1453792459,
			'to' => 1453792459,
			'created' => 1453792459,
			'modified' => 1453792459
		),
	);

}
