<?php
App::uses('NeighborhoodsController', 'Controller');

/**
 * NeighborhoodsController Test Case
 */
class NeighborhoodsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.neighborhood',
		'app.facility',
		'app.home',
		'app.resident',
		'app.resident_type',
		'app.home_type',
		'app.charge',
		'app.charge_type',
		'app.facilities_rental',
		'app.log',
		'app.payment',
		'app.payment_type'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
