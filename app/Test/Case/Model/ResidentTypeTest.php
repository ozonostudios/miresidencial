<?php
App::uses('ResidentType', 'Model');

/**
 * ResidentType Test Case
 */
class ResidentTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.resident_type',
		'app.resident'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ResidentType = ClassRegistry::init('ResidentType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ResidentType);

		parent::tearDown();
	}

}
