<?php
App::uses('Payment', 'Model');

/**
 * Payment Test Case
 */
class PaymentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.payment',
		'app.payment_type',
		'app.home',
		'app.resident',
		'app.home_type',
		'app.neighborhood',
		'app.facility',
		'app.charge',
		'app.charge_type',
		'app.facilities_rental',
		'app.log'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Payment = ClassRegistry::init('Payment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Payment);

		parent::tearDown();
	}

}
