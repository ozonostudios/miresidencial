<?php
App::uses('Neighborhood', 'Model');

/**
 * Neighborhood Test Case
 */
class NeighborhoodTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.neighborhood',
		'app.facility',
		'app.home',
		'app.resident',
		'app.home_type',
		'app.charge',
		'app.charge_type',
		'app.facilities_rental',
		'app.log',
		'app.payment'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Neighborhood = ClassRegistry::init('Neighborhood');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Neighborhood);

		parent::tearDown();
	}

}
