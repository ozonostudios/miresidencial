<?php
App::uses('AnnouncementType', 'Model');

/**
 * AnnouncementType Test Case
 */
class AnnouncementTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.announcement_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AnnouncementType = ClassRegistry::init('AnnouncementType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AnnouncementType);

		parent::tearDown();
	}

}
