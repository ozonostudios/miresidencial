<?php
App::uses('FacilitiesRental', 'Model');

/**
 * FacilitiesRental Test Case
 */
class FacilitiesRentalTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.facilities_rental',
		'app.facility',
		'app.neighborhood',
		'app.home'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FacilitiesRental = ClassRegistry::init('FacilitiesRental');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FacilitiesRental);

		parent::tearDown();
	}

}
