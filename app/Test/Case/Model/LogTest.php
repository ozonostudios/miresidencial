<?php
App::uses('Log', 'Model');

/**
 * Log Test Case
 */
class LogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.log',
		'app.home',
		'app.resident',
		'app.home_type',
		'app.neighborhood',
		'app.charge',
		'app.charge_type',
		'app.facilities_rental',
		'app.facility',
		'app.payment'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Log = ClassRegistry::init('Log');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Log);

		parent::tearDown();
	}

}
