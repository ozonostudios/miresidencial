<?php
App::uses('HomeType', 'Model');

/**
 * HomeType Test Case
 */
class HomeTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.home_type',
		'app.home'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->HomeType = ClassRegistry::init('HomeType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->HomeType);

		parent::tearDown();
	}

}
