<?php
App::uses('Inventory', 'Model');

/**
 * Inventory Test Case
 */
class InventoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.inventory',
		'app.expense',
		'app.expense_type',
		'app.payment_type',
		'app.payment',
		'app.home',
		'app.resident',
		'app.resident_type',
		'app.home_type',
		'app.neighborhood',
		'app.facility',
		'app.charge',
		'app.charge_type',
		'app.facilities_rental',
		'app.log',
		'app.service_provider'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Inventory = ClassRegistry::init('Inventory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Inventory);

		parent::tearDown();
	}

}
