<?php
App::uses('ChargeType', 'Model');

/**
 * ChargeType Test Case
 */
class ChargeTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.charge_type',
		'app.charge'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ChargeType = ClassRegistry::init('ChargeType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ChargeType);

		parent::tearDown();
	}

}
