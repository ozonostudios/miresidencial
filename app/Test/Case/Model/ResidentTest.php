<?php
App::uses('Resident', 'Model');

/**
 * Resident Test Case
 */
class ResidentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.resident',
		'app.resident_type',
		'app.home',
		'app.home_type',
		'app.neighborhood',
		'app.facility',
		'app.charge',
		'app.charge_type',
		'app.facilities_rental',
		'app.log',
		'app.payment',
		'app.payment_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Resident = ClassRegistry::init('Resident');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Resident);

		parent::tearDown();
	}

}
