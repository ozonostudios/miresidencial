<?php
App::uses('AppController', 'Controller');
/**
 * Payments Controller
 *
 * @property Payment $Payment
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class PaymentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');
/**
 * add method
 *
 * @return void
 */
    public function add($id = null) {

        if ($this->request->is('post')) {

            $this->Payment->create();

            $email = '';
            $email_request = $this->request->data['Payment']['send_email'];

            unset($this->request->data['Payment']['send_email']);


            if ($this->Payment->save($this->request->data)) {

                if((int) $email_request) {
                    
                    $this->Payment->Home->recursive = 1;
                    $payment_home = $this->Payment->Home->find('first',(int)$this->request->data['Payment']['home_id']);
                    $email = $payment_home['Resident']['email'];
                    /**
                     * If there's a valid email, send an email with the amount
                     * of the payment
                     */
                    $subject = 'MiResidencial.mx - Recibo de Pago #' . $this->Payment->getLastInsertID();
                    
                    $message = 
                    '<html><body>'.
                        '<h2>Recibo de Pago #' . $this->Payment->getLastInsertID().'</h2>'.
                        '<p>Se ha registrado un pago en el sistema por <b>$'.number_format($this->request->data['Payment']['amount']).'</b> a la cuenta de su residencia en <b>'.$payment_home['Home']['address'].'</b></p>'.
                        '<p>El concepto es <b>'.$this->request->data['Payment']['payment_details'].'</b></p>'.
                        '<h3>Usted ya puede revisar sus estados de cuenta en <a href="http://www.miresidencial.mx/mi-cuenta" target="_parent">http://www.miresidencial.mx/mi-cuenta</a></h3>'.
                    '</body></html>';


                    if($email != '') {
                        mail( $email, $subject, $message, $this->Payment->mail_headers );
                    }

                    $this->Flash->success('El pago se ha aplicado, le enviamos un comprobante de pago a '.$payment_home['Resident']['email']);
                } else {
                    $this->Flash->success('El pago se ha aplicado.');
                }          

                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error('El pago no pudo ser agregado, intente de nuevo.');
            }
        }

        $paymentTypes = $this->Payment->PaymentType->find('list');
        $homes = $this->Payment->Home->find('list');
        $this->set(compact('paymentTypes', 'homes'));
    }
/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Pagos');
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Payment->id = $id;

        if (!$this->Payment->exists()) {

            throw new NotFoundException('Pago inválido.');
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Payment->delete()) {

            $this->Flash->success('El pago fue eliminado.');
        } else {

                $this->Flash->error('El pago no pudo ser borrado, intente de nuevo.');
        }

        return $this->redirect(array('action' => 'index'));
    }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->Payment->exists($id)) {

            throw new NotFoundException('Pago inválido.');
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Payment->save($this->request->data)) {

                $this->Flash->success('El pago ha sido editado.');
                // return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error('El pago no pudo ser editado, intente de nuevo.');
            }
        } else {

            $options = array('conditions' => array('Payment.' . $this->Payment->primaryKey => $id));
            $this->request->data = $this->Payment->find('first', $options);
        }
        
        $paymentTypes = $this->Payment->PaymentType->find('list');
        $homes = $this->Payment->Home->find('list');
        $this->set(compact('paymentTypes', 'homes'));
    }   

/**
 * [getChartData gets data for index view chart]
 * @return [Object] [angular chartjs formatted json]
 */
    public function getChartData() {

        $data = $this->Payment->getChartData($this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }
/**
 * [getChartData gets data for index view chart]
 * @return [Object] [angular chartjs formatted json]
 */
    public function getBonifications() {

        $data = $this->Payment->getBonifications($this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }
/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->Paginator->settings = array(
            'order' => array(
                'Payment.id' => 'DESC'
            ));     

        $this->Payment->recursive = 0;
        $this->set('payments', $this->Paginator->paginate());
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->Payment->exists($id)) {

            throw new NotFoundException('Pago inválido.');
        }
        
        $options = array('conditions' => array('Payment.' . $this->Payment->primaryKey => $id));
        $this->set('payment', $this->Payment->find('first', $options));
    }
}
