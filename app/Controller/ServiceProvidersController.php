<?php
App::uses('AppController', 'Controller');
/**
 * ServiceProviders Controller
 *
 * @property ServiceProvider $ServiceProvider
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ServiceProvidersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
	public function beforeRender() {

		$this->set('title_for_layout', 'Proveedores');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ServiceProvider->recursive = 0;
		$this->set('serviceProviders', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ServiceProvider->exists($id)) {
			throw new NotFoundException(__('Invalid service provider'));
		}
		$options = array('conditions' => array('ServiceProvider.' . $this->ServiceProvider->primaryKey => $id));
		$this->set('serviceProvider', $this->ServiceProvider->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ServiceProvider->create();
			if ($this->ServiceProvider->save($this->request->data)) {
				$this->Flash->success(__('The service provider has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The service provider could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ServiceProvider->exists($id)) {
			throw new NotFoundException(__('Invalid service provider'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ServiceProvider->save($this->request->data)) {
				$this->Flash->success(__('The service provider has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The service provider could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ServiceProvider.' . $this->ServiceProvider->primaryKey => $id));
			$this->request->data = $this->ServiceProvider->find('first', $options);
		}
	}
/**
 * [search performs the async call on the model]
 * @param  [String] $value [name of resident]
 * @return [Array]        [list of residents that matches the query string]
 */
    public function search($value) {

        $data = $this->ServiceProvider->asyncSearch($value, $this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ServiceProvider->id = $id;
		if (!$this->ServiceProvider->exists()) {
			throw new NotFoundException(__('Invalid service provider'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ServiceProvider->delete()) {
			$this->Flash->success(__('The service provider has been deleted.'));
		} else {
			$this->Flash->error(__('The service provider could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
