<?php
App::uses('AppController', 'Controller');
/**
 * Homes Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class HomesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');

/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {

            $this->Home->create();

            if ($this->Home->save($this->request->data)) {

                $this->Flash->success(__('The home has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The home could not be saved. Please, try again.'));
            }
        }

        $residents = $this->Home->Resident->find('list');
        $homeTypes = $this->Home->HomeType->find('list');
        $neighborhoods = $this->Home->Neighborhood->find('list');
        $this->set(compact('residents', 'homeTypes', 'neighborhoods'));
    }
/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Casas');
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Home->id = $id;

        if (!$this->Home->exists()) {

            throw new NotFoundException(__('Invalid home'));
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Home->delete()) {

            $this->Flash->success(__('The home has been deleted.'));
        } else {

            $this->Flash->error(__('The home could not be deleted. Please, try again.'));
        }

        return $this->redirect(array('action' => 'index'));
    }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->Home->exists($id)) {

            throw new NotFoundException(__('Invalid home'));
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Home->save($this->request->data)) {

                $this->Flash->success(__('The home has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The home could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Home.' . $this->Home->primaryKey => $id));
            $this->request->data = $this->Home->find('first', $options);
        }

        $residents = $this->Home->Resident->find('list');
        $homeTypes = $this->Home->HomeType->find('list');
        $neighborhoods = $this->Home->Neighborhood->find('list');

        $this->set(compact('residents', 'homeTypes', 'neighborhoods'));
    }    
/**
 * getChartHomes method
 *
 * @return array
 */
    public function getChartHomes() {

        $data = $this->Home->homeChartData();
        $this->autoRender = false;

        return json_encode($data);
    }
/**
 * getHomeData method
 *
 * @return array
 */
    public function getHomeData($id=null) {


        if (!$this->Home->exists($id)) {

            throw new NotFoundException(__('Invalid home'));
        }
        
        $options = array('conditions' => array('Home.' . $this->Home->primaryKey => $id));
        $data = $this->Home->find('first', $options);

        $this->autoRender = false;

        return json_encode($data);
    }      
/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->Home->recursive = 1;

        $homes = $this->Paginator->paginate();
        $return = array();

        foreach ($homes as $home) {

            $return[] = $this->Home->homeStatusCheck($home);
        }

        $this->set('homes', $return);
    }    
/**
 * [search performs the async call on the model]
 * @param  [String] $value [address of home]
 * @return [Array]        [list of homes that matches the query string]
 */
    public function search($value) {

        $data = $this->Home->asyncSearch($value, $this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->Home->exists($id)) {

            throw new NotFoundException(__('Invalid home'));
        }
        
        $this->Home->recursive = 2;

        $options = array('conditions' => array('Home.' . $this->Home->primaryKey => $id));
        $home = $this->Home->find('first', $options);

        /**
         * MAKE IT PART OF THE QUERY
         */

        $payments=0;
        $charges=0;

        foreach ($home['Payment'] as $payment) {
            $payments += $payment['amount'];
        }

        foreach ($home['Charge'] as $charge) {
            $charges += $charge['amount'];
        }

        $home['Home']['payments'] = $payments;
        $home['Home']['charges'] = $charges;
        $home['Home']['debt'] = $payments-$charges;

        $default_check = $home['Neighborhood']['payments_before_default'] * $home['Neighborhood']['monthly_fee'];

        if($home['Home']['debt'] >= 0) {

            $home['Home']['status'] = 'Al Corriente';
        } else if($home['Home']['debt'] > -($default_check)) {

            $home['Home']['status'] = 'En Convenio';
        } else {
            
            $home['Home']['status'] = 'Atrasado';
        }

        /*******************************/

        $this->set('home', $home);
    }  
}
