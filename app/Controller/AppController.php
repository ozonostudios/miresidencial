<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $paginationLimit = 20;
    public $session_data = [];
    public $site_webroot = '';
    public $components = array(
        'DebugKit.Toolbar',
        'Flash',
        'Session',
        'Cookie',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )          
        )
    );

    public $months = Array(
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
    );


    public function beforeFilter() {
        /**
         * [$this->paginate pagination limit for list views]
         * @var array
         */
        $this->paginate = array('limit' => $this->paginationLimit);
        $this->Session->write('Neighborhood.id', 1);

        $this->site_webroot = $_SERVER['DOCUMENT_ROOT'];

        if($this->Cookie->read('Neighborhood.id') && $this->Cookie->read('User.id')) {

            $this->session_data['Neighborhood.id'] = $this->Cookie->read('Neighborhood.id');
            $this->session_data['User.id'] = $this->Cookie->read('User.id');
        }
    }

    public function beforeRender() {
        /**
         * Useful for hidding html elements for 
         * non logged in users like navbar
         */
        $this->set('isLoggedIn', $this->Auth->user());
    }

/**
 * [upload uploads logo image]
 * @param  [Number] $neighborhood_id [Id of the neighborhood]
 * @return [Text]                  [Response message]
 */
    public function upload($folder, $id) {

        $this->autoRender = false;
        $directory = strstr(__DIR__, 'Controller\\', true);
        /**
         * Handle file upload
         */
        $filename = $_FILES['file']['name'];
        $dest_folder = $directory.'img/'.$folder.'/'.$id.'/';
        $destination = $dest_folder.$filename;

        if(!file_exists($dest_folder)) {
            mkdir($dest_folder);
        }

        if(move_uploaded_file($_FILES['file']['tmp_name'] , $destination)) {

            $modelName = ucwords($folder);
            $this->loadModel($modelName);
            $this->$modelName->id = $id;
            $this->$modelName->saveField('image', $destination);
        }

        return $destination;
    }

    public function imageExists($folder, $id) {
        
        $this->autoRender = false;
        $modelName = ucwords($folder);

        $this->loadModel($modelName);
        $path = $this->$modelName->find('list', array(
            'fields' => array('image'),
            'conditions' => array(
                'id' => $id
            )
        ));

        if(file_exists($path[$id])) {

            return '\\'.$path[$id];
        } else {
            
            return '\\';
        }

    }
}
