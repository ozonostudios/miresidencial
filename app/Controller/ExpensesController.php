<?php
App::uses('AppController', 'Controller');
/**
 * Expenses Controller
 *
 * @property Expense $Expense
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ExpensesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');
/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {

            $addInventory = $this->request->data['Expense']['add_inventory'];
            unset($this->request->data['Expense']['add_inventory']);

            $this->Expense->create();

            if ($this->Expense->save($this->request->data)) {

                $this->Flash->success('El gasto ha sido registrado.');

                if($addInventory) {

                    return $this->redirect('/inventario/agregar/'.$this->Expense->getLastInsertID());
                } else {
                    
                    return $this->redirect(array('action' => 'index'));
                }
            } else {

                $this->Flash->error('El gasto no pudo ser registrado.');
            }
        }

        $expenseTypes = $this->Expense->ExpenseType->find('list');
        $paymentTypes = $this->Expense->PaymentType->find('list');
        $serviceProviders = $this->Expense->ServiceProvider->find('list');
        $this->set(compact('expenseTypes', 'paymentTypes', 'serviceProviders'));
    }
/**
 * [audit return information for
 * audit expenses on dashboard]
 * @return [JSON] [description]
 */
    function audit() {

        $this->autoRender = false;
        $data = $this->Expense->auditData($this->session_data['Neighborhood.id']);

        return json_encode($data);
    }

/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Gastos');
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Expense->id = $id;

        if (!$this->Expense->exists()) {

            throw new NotFoundException('Gasto inválido.');
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Expense->delete()) {

            $this->Flash->success(__('The expense has been deleted.'));
        } else {

            $this->Flash->error(__('The expense could not be deleted. Please, try again.'));
        }

        return $this->redirect(array('action' => 'index'));
    }   
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->Expense->exists($id)) {

            throw new NotFoundException(__('Invalid expense'));
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Expense->save($this->request->data)) {

                $this->Flash->success(__('The expense has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The expense could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Expense.' . $this->Expense->primaryKey => $id));
            $this->request->data = $this->Expense->find('first', $options);
        }

        $paymentTypes = $this->Expense->PaymentType->find('list');
        $expenseTypes = $this->Expense->ExpenseType->find('list');
        $serviceProviders = $this->Expense->ServiceProvider->find('list');
        $this->set('id', $id);
        $this->set(compact('expenseTypes', 'paymentTypes', 'serviceProviders'));
    }
/**
 * [getChartData gets data for index view chart]
 * @return [Object] [angular chartjs formatted json]
 */
    public function getChartData() {

        $data = $this->Expense->getChartData($this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }    
/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->Paginator->settings = array(
            'order' => array(
                'Expense.id' => 'DESC'
            ));

        $this->Expense->recursive = 0;
        $this->set('expenses', $this->Paginator->paginate());
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->Expense->exists($id)) {

            throw new NotFoundException(__('Invalid expense'));
        }

        $options = array('conditions' => array('Expense.' . $this->Expense->primaryKey => $id));
        $this->set('expense', $this->Expense->find('first', $options));
    }
}
