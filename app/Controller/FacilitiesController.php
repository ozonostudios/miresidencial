<?php
App::uses('AppController', 'Controller');
/**
 * Facilities Controller
 *
 * @property Facility $Facility
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class FacilitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');
/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {

            $this->Facility->create();

            if ($this->Facility->save($this->request->data)) {

                $this->Flash->success(__('The facility has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The facility could not be saved. Please, try again.'));
            }
        }

        $neighborhoods = $this->Facility->Neighborhood->find('list');
        $this->set(compact('neighborhoods'));
    }
/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Áreas Comunes');
    } 
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->Facility->exists($id)) {

            throw new NotFoundException(__('Invalid facility'));
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Facility->save($this->request->data)) {

                $this->Flash->success(__('The facility has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The facility could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Facility.' . $this->Facility->primaryKey => $id));
            $this->request->data = $this->Facility->find('first', $options);
        }

        $neighborhoods = $this->Facility->Neighborhood->find('list');
        $this->set(compact('neighborhoods'));
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Facility->id = $id;

        if (!$this->Facility->exists()) {

            throw new NotFoundException(__('Invalid facility'));
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Facility->delete()) {

            $this->Flash->success(__('The facility has been deleted.'));
        } else {

            $this->Flash->error(__('The facility could not be deleted. Please, try again.'));
        }
        
        return $this->redirect(array('action' => 'index'));
    }
/**
 * [getAllFacilities returns a list of all facilities
 * from requested neighborhood]
 * @return [Array] [list of facilities]
 */
    public function getAllFacilities() {

        $data = $this->Facility->getAllFacilities();
        $this->autoRender = false;

        return json_encode($data);
    }
/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->Facility->recursive = 0;
        $this->set('facilities', $this->Paginator->paginate());
    }
/**
 * [search performs the async call on the model]
 * @param  [String] $value [name of facility]
 * @return [Array]        [list of facilities that matches the query string]
 */
    public function search($value) {

        $data = $this->Facility->asyncSearch($value, $this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }    
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

    public function view($id = null) {
        
        if (!$this->Facility->exists($id)) {

            throw new NotFoundException(__('Invalid facility'));
        }

        $options = array('conditions' => array('Facility.' . $this->Facility->primaryKey => $id));
        $this->set('facility', $this->Facility->find('first', $options));
    }
}
