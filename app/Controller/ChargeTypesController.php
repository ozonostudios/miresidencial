<?php
App::uses('AppController', 'Controller');
/**
 * ChargeTypes Controller
 *
 * @property ChargeType $ChargeType
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ChargeTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
	public function beforeRender() {

		$this->set('title_for_layout', 'Tipo de Cargo');
	}
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ChargeType->recursive = 0;
		$this->set('chargeTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ChargeType->exists($id)) {
			throw new NotFoundException(__('Invalid charge type'));
		}
		$options = array('conditions' => array('ChargeType.' . $this->ChargeType->primaryKey => $id));
		$this->set('chargeType', $this->ChargeType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ChargeType->create();
			if ($this->ChargeType->save($this->request->data)) {
				$this->Flash->success(__('The charge type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The charge type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ChargeType->exists($id)) {
			throw new NotFoundException(__('Invalid charge type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ChargeType->save($this->request->data)) {
				$this->Flash->success(__('The charge type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The charge type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ChargeType.' . $this->ChargeType->primaryKey => $id));
			$this->request->data = $this->ChargeType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ChargeType->id = $id;
		if (!$this->ChargeType->exists()) {
			throw new NotFoundException(__('Invalid charge type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ChargeType->delete()) {
			$this->Flash->success(__('The charge type has been deleted.'));
		} else {
			$this->Flash->error(__('The charge type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
