<?php
App::uses('AppController', 'Controller');
/**
 * Inventories Controller
 *
 * @property Inventory $Inventory
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class InventoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');

/**
 * add method
 *
 * @return void
 */
    public function add($expense_id = null) {

        if ($this->request->is('post')) {

            $this->Inventory->create();
            /**
             * Process expense_id if not present
             */
            if(!is_numeric($this->request->data['Inventory']['expense_id'])) {

                $this->request->data['Inventory']['expense_id'] = 0;
            }

            if ($this->Inventory->save($this->request->data)) {

                $lastId = $this->Inventory->id;

                $this->Flash->success('El producto ha sido agregado al inventario.');
                return $this->redirect('/inventario/editar/'.$lastId);

            } else {

                $this->Flash->error('El producto no pudo ser agregado al inventario, por favor intente de nuevo.');
            }
        }

        if($expense_id == null) {

            $show_expense_info = 0;
            $expenses = $this->Inventory->Expense->find('list');
        } else {
            
            $show_expense_info = 1;
            $expense = $this->Inventory->Expense->find('all', array(
                'conditions' => array(
                    'Expense.id' => $expense_id
                    )
                )
            );

            $expense = $expense[0];

            $expenses = $this->Inventory->Expense->find('list', array(
                'conditions' => array(
                    'Expense.id' => $expense_id
                    ),
                )
            );
        }
            
        $statuses = $this->Inventory->Status->find('list');

        $this->set('show_expense_info', $show_expense_info);
        $this->set(compact('expenses', 'statuses', 'expense'));
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Inventory->id = $id;

        if (!$this->Inventory->exists()) {

            throw new NotFoundException('Número de Inventario inválido.');
        }

        $this->request->allowMethod('post', 'delete', 'get');

        if ($this->Inventory->delete()) {

                $this->Flash->success('El producto ha sido eliminado del inventario.');
        } else {

                $this->Flash->error('El producto no pudo ser eliminado del inventario, por favor intente de nuevo.');
        }

        return $this->redirect(array('action' => 'index'));
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->Inventory->exists($id)) {

            throw new NotFoundException('Número de Inventario inválido.');
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Inventory->save($this->request->data)) {

                $this->Flash->success('El producto ha sido editado.');
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error('El producto no pudo ser editado, por favor intente de nuevo.');
            }
        } else {

            $options = array('conditions' => array('Inventory.' . $this->Inventory->primaryKey => $id));
            $this->request->data = $this->Inventory->find('first', $options);
        }

        $show_expense_info = 0;
        $expense = [];

        if($this->request->data['Inventory']['expense_id'] > 0) {
            
            $show_expense_info = 1;
            $expense = $this->Inventory->Expense->find('all', array(
                'conditions' => array(
                    'Expense.id' => $id
                    )
                )
            );

            $expense = $expense[0];
        }

        $this->set('show_expense_info', $show_expense_info);

        $statuses = $this->Inventory->Status->find('list');

        $this->set('id', $id);
        $this->set('expense', $expense);
        $this->set(compact('statuses'));
    }

/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->Inventory->recursive = 0;
        $this->set('inventories', $this->Paginator->paginate());
    }
/**
 * [search performs the async call on the model]
 * @param  [String] $value [name of resident]
 * @return [Array]        [list of residents that matches the query string]
 */
    public function search($value) {

        $data = $this->Inventory->asyncSearch($value, $this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->Inventory->exists($id)) {

            throw new NotFoundException('Número de Inventario inválido.');
        }

        $options = array('conditions' => array('Inventory.' . $this->Inventory->primaryKey => $id));
        $display_sidebar = false;

        $inventory = $this->Inventory->find('first', $options);
        $show_expense_info = (int)$inventory['Inventory']['expense_id'] === 0 ? 0 : 1;

        if(file_exists($this->site_webroot.'/img/inventories/'.$inventory['Inventory']['image']) && $inventory['Inventory']['image'] != "") {

            $inventory['Inventory']['image'] = '/img/inventories/'.$inventory['Inventory']['image'];
            $display_sidebar = true;
        } else {

            $inventory['Inventory']['image'] = '';
        }

        if(file_exists($this->site_webroot.'/uploads/brochures/'.$inventory['Inventory']['brochure']) && $inventory['Inventory']['brochure']) {

            $inventory['Inventory']['brochure'] = '/uploads/brochures/'.$inventory['Inventory']['brochure'];
            $display_sidebar = true;
        } else {

            $inventory['Inventory']['brochure'] = '';
        }
        
        $this->set('display_sidebar', $display_sidebar);
        $this->set('show_expense_info', $show_expense_info);
        $this->set('inventory', $inventory);
    }
}
