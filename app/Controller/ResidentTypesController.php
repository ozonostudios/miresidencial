<?php
App::uses('AppController', 'Controller');
/**
 * ResidentTypes Controller
 *
 * @property ResidentType $ResidentType
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ResidentTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
	public function beforeRender() {

		$this->set('title_for_layout', 'Tipo de Residente');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ResidentType->recursive = 0;
		$this->set('residentTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ResidentType->exists($id)) {
			throw new NotFoundException(__('Invalid resident type'));
		}
		$options = array('conditions' => array('ResidentType.' . $this->ResidentType->primaryKey => $id));
		$this->set('residentType', $this->ResidentType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ResidentType->create();
			if ($this->ResidentType->save($this->request->data)) {
				$this->Flash->success(__('The resident type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resident type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ResidentType->exists($id)) {
			throw new NotFoundException(__('Invalid resident type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ResidentType->save($this->request->data)) {
				$this->Flash->success(__('The resident type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The resident type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ResidentType.' . $this->ResidentType->primaryKey => $id));
			$this->request->data = $this->ResidentType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ResidentType->id = $id;
		if (!$this->ResidentType->exists()) {
			throw new NotFoundException(__('Invalid resident type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ResidentType->delete()) {
			$this->Flash->success(__('The resident type has been deleted.'));
		} else {
			$this->Flash->error(__('The resident type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
