<?php
App::uses('AppController', 'Controller');
/**
 * Charges Controller
 *
 * @property Charge $Charge
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ChargesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');
/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {

            $this->Charge->create();

            if ($this->Charge->save($this->request->data)) {

                $this->Flash->success(__('The charge has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The charge could not be saved. Please, try again.'));
            }
        }
    }

    public function applyCharges() {

        $this->autoRender = false;
        /**
         * [$chargesAlredyApplied make sure we don't apply charges more
         * than once]
         * @var [Boolean]
         */
        $chargesAlredyApplied = 
            $this->Charge->monthCharges($this->session_data['Neighborhood.id']) == 0 ? 
                false : true;
        /**
         * Two important conditions to check before applying charges
         */ 
        if(!$chargesAlredyApplied && isset($this->session_data['Neighborhood.id'])) {
            /**
             * Return success if charges were applied correctly
             */
            return $this->Charge->applyCharges($this->session_data['Neighborhood.id']);
        }

        return 0;
    }


/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Cargos');
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Charge->id = $id;

        if (!$this->Charge->exists()) {

            throw new NotFoundException(__('Invalid charge'));
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Charge->delete()) {

            $this->Flash->success(__('The charge has been deleted.'));
        } else {

            $this->Flash->error(__('The charge could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
/**
 * [getChartData gets data for index view chart]
 * @return [Object] [angular chartjs formatted json]
 */
    public function getChartData() {

        $data = $this->Charge->getChartData($this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }    
/**
 * index method
 *
 * @return void
 */
    public function index() {

        /**
         * [$showChargesReminder if charges haven't been applied, show
         * reminder box]
         * @var [Boolean]
         */
        $showChargesReminder = 
            $this->Charge->monthCharges($this->session_data['Neighborhood.id']) == 0 ? 
                true : false;

        $this->Charge->recursive = 0;
        $this->Paginator->settings = array(
            'order' => array(
                'Charge.id' => 'DESC'
            ));

        $this->set('month', $this->months[date('n')-1]);
        $this->set('showChargesReminder', $showChargesReminder);
        $this->set('charges', $this->Paginator->paginate());
    }
}
