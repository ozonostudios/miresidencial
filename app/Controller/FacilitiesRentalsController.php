<?php
App::uses('AppController', 'Controller');
/**
 * FacilitiesRentals Controller
 *
 * @property FacilitiesRental $FacilitiesRental
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class FacilitiesRentalsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');

/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Renta de Áreas');
    }
/**
 * [checkAvailability checks availability of a
 * facility in requested date]
 * @return [type] [description]
 */
    public function checkAvailability($id) {

        $data = $this->FacilitiesRental->checkAvailability($id);
        $this->autoRender = false;

        return json_encode($data);
    }   
/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->FacilitiesRental->recursive = 0;
        $this->set('facilitiesRentals', $this->Paginator->paginate());
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->FacilitiesRental->exists($id)) {

            throw new NotFoundException(__('Invalid facilities rental'));
        }

        $options = array('conditions' => array('FacilitiesRental.' . $this->FacilitiesRental->primaryKey => $id));
        $this->set('facilitiesRental', $this->FacilitiesRental->find('first', $options));
    }

/**
 * add method
 *
 * @return void
 */
    public function add($params = null) {

        if($params != null) {

            $params = explode('-', $params);
        }

        if ($this->request->is('post')) {

            $this->FacilitiesRental->create();

            $this->request->data['FacilitiesRental']['to'] = $this->request->data['FacilitiesRental']['from'];

            if ($this->FacilitiesRental->save($this->request->data)) {

                $this->Flash->success(__('The facilities rental has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The facilities rental could not be saved. Please, try again.'));
            }
        }

        $facilities = $this->FacilitiesRental->Facility->find('list');
        $homes = $this->FacilitiesRental->Home->find('list');
        $this->set('params', $params);
        $this->set(compact('facilities', 'homes'));
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->FacilitiesRental->exists($id)) {

            throw new NotFoundException(__('Invalid facilities rental'));
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->FacilitiesRental->save($this->request->data)) {

                $this->Flash->success(__('The facilities rental has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The facilities rental could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('FacilitiesRental.' . $this->FacilitiesRental->primaryKey => $id));
            $this->request->data = $this->FacilitiesRental->find('first', $options);
        }

        $facilities = $this->FacilitiesRental->Facility->find('list');
        $homes = $this->FacilitiesRental->Home->find('list');
        $this->set(compact('facilities', 'homes'));
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->FacilitiesRental->id = $id;

        if (!$this->FacilitiesRental->exists()) {

            throw new NotFoundException(__('Invalid facilities rental'));
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->FacilitiesRental->delete()) {

            $this->Flash->success(__('The facilities rental has been deleted.'));
        } else {

            $this->Flash->error(__('The facilities rental could not be deleted. Please, try again.'));
        }
        
        return $this->redirect(array('action' => 'index'));
    }
}
