<?php
App::uses('AppController', 'Controller');
/**
 * AnnouncementTypes Controller
 *
 * @property AnnouncementType $AnnouncementType
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class AnnouncementTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AnnouncementType->recursive = 0;
		$this->set('announcementTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AnnouncementType->exists($id)) {
			throw new NotFoundException(__('Invalid announcement type'));
		}
		$options = array('conditions' => array('AnnouncementType.' . $this->AnnouncementType->primaryKey => $id));
		$this->set('announcementType', $this->AnnouncementType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AnnouncementType->create();
			if ($this->AnnouncementType->save($this->request->data)) {
				$this->Flash->success(__('The announcement type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The announcement type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AnnouncementType->exists($id)) {
			throw new NotFoundException(__('Invalid announcement type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AnnouncementType->save($this->request->data)) {
				$this->Flash->success(__('The announcement type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The announcement type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AnnouncementType.' . $this->AnnouncementType->primaryKey => $id));
			$this->request->data = $this->AnnouncementType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AnnouncementType->id = $id;
		if (!$this->AnnouncementType->exists()) {
			throw new NotFoundException(__('Invalid announcement type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AnnouncementType->delete()) {
			$this->Flash->success(__('The announcement type has been deleted.'));
		} else {
			$this->Flash->error(__('The announcement type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
