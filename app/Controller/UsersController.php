<?php
App::uses('AppController', 'Controller');
/**
 * Password enctypter
 */
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session', 'Cookie');

/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {

            $this->User->create();

            if ($this->User->save($this->request->data)) {

                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }

        $groups = $this->User->Group->find('list');
        $neighborhoods = $this->User->Neighborhood->find('list');

        $residents = $this->User->Resident->find('list', array(
            'order' => 'name ASC',
            'joins' => array(
                array(
                    'table' => 'homes',
                    'alias' => 'Home',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Home.resident_id = Resident.id'
                    )
                )
            ),
            'conditions' =>array(
                'NOT' => array('Resident.id' => $this->assignedResidents()),
                'Home.neighborhood_id' => $this->session_data['Neighborhood.id']
            )            
        ));
        $this->set(compact('groups','residents','neighborhoods'));
    }    
/**
 * [assignedResidents gets all already assigned residents to users
 * to reduce list on the add/edit forms]
 * @return [Array] [already assigned residents]
 */
    public function assignedResidents() {
        
        $this->User->recursive = 0;
        $residents = $this->User->find('list',array(
            'fields' => array('residents_id')
        ));

        return $residents;
    }
/**
 * [beforeFilter actions to be executed before
 * a view it's rendered]
 */
    public function beforeFilter() {

        parent::beforeFilter();
        /**
         * Allow users to register and logout.
         */
        // $this->Auth->allow('logout','edit'); 
        $this->Auth->allow('*'); 
    }
/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Usuarios');
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->User->delete()) {

            $this->Flash->success(__('The user has been deleted.'));
        } else {

            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(array('action' => 'index'));
    }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        /**
         * [$title form's title]
         * @var string
         */
        $title = 'Editar Usuario';

        /**
         * If id param is null, assign the one from session
         * variable and change form's title
         */
        if(is_null($id)) {

            $id = $this->session_data['User.id'];
            $title = 'Mi Perfil';
        }

        if (!$this->User->exists($id)) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->User->save($this->request->data)) {

                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }

        if($this->request->data['Resident']['name'] != "") {

            $conditions = array('Resident.id' => $this->request->data['Resident']['id']);
        } else {

            $conditions = array(
                'NOT' => array('Resident.id' => $this->assignedResidents())
            );            
        }

        $residents = $this->User->Resident->find('list', array(
            'order' => 'name ASC',
            'conditions' => $conditions
        ));

        $groups = $this->User->Group->find('list');
        $neighborhoods = $this->User->Neighborhood->find('list');

        $this->set('title', $title);
        $this->set('id', $id);
        $this->set(compact('groups','residents','neighborhoods'));
    }
/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }
/**
 * [login user's login view]
 */
    public function login() {

        $this->layout = 'login';

        if ($this->request->is('post')) {

            if ($this->Auth->login()) {

                $options = array('conditions' => array('User.username' => $this->request->data['User']['username']));
                $this->User->recursive = 0;
                $user = $this->User->find('first', $options);

                $this->Cookie->write('User.id', $user['User']['id'], true, '2 weeks');
                $this->Cookie->write('Neighborhood.id', $user['User']['neighborhood_id'], true, '2 weeks');

                return $this->redirect('/');
            }
            $this->Flash->error(__('Usuario o Contraseña Inválida, por favor ntente de nuevo.'));
        }
    }
/**
 * [logout user's logout]
 */
    public function logout() {
        
        return $this->redirect($this->Auth->logout());
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->User->exists($id)) {

            throw new NotFoundException(__('Invalid user'));
        }

        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }
}
