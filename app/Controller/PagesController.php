<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
    public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *  or MissingViewException in debug mode.
 */
    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        $title_for_layout = 'Panel de Control';
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
/**
 * [reports sets records for reports on
 * selected dates]
 * @param  [String] $when [time frame coming from the user]
 */
    public function reports($when) {

        setlocale(LC_ALL,"es_ES");

        switch($when) {

            default:
            case 'hoy' : 
                $conditions = array('DAYOFYEAR(created)=DAYOFWEEK(CURDATE())');
                $title = date('l j').' de '.date('F, Y');
                break;
            case 'semana' : 
                $conditions = array('WEEKOFYEAR(created)=WEEKOFYEAR(CURDATE())');
                $title = 'Esta Semana';
                $when = 'esta '.$when;
                break;
            case 'mes' : 
                $conditions = array('MONTH(created)=MONTH(CURDATE())');
                $title = date('F');
                $when = 'este '.$when;
                break;
            case 'anio' : 
                $conditions = array('YEAR(created)=YEAR(CURDATE())');
                $title = 'Año '.date('Y');
                $when = 'este '.$when;
                break;
        }

        $chargesModel = ClassRegistry::init('Charges');
        $chargesModel->recursive = 2;
        $charges = $chargesModel->find('all', array('conditions' => $conditions));

        $expensesModel = ClassRegistry::init('Expenses');
        $expensesModel->recursive = 2;
        $expenses = $expensesModel->find('all', array('conditions' => $conditions));

        $paymentsModel = ClassRegistry::init('Payments');
        $paymentsModel->recursive = 2;
        $payments = $paymentsModel->find('all', array('conditions' => $conditions));

        $this->set(compact('charges','expenses','payments'));
        $this->set('timeframe', $title);
        $this->set('when',$when);
    }
}
