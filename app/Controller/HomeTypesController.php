<?php
App::uses('AppController', 'Controller');
/**
 * HomeTypes Controller
 *
 * @property HomeType $HomeType
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class HomeTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
	public function beforeRender() {

		$this->set('title_for_layout', 'Tipo de Casa');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->HomeType->recursive = 0;
		$this->set('homeTypes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->HomeType->exists($id)) {
			throw new NotFoundException(__('Invalid home type'));
		}
		$options = array('conditions' => array('HomeType.' . $this->HomeType->primaryKey => $id));
		$this->set('homeType', $this->HomeType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->HomeType->create();
			if ($this->HomeType->save($this->request->data)) {
				$this->Flash->success(__('The home type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The home type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->HomeType->exists($id)) {
			throw new NotFoundException(__('Invalid home type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->HomeType->save($this->request->data)) {
				$this->Flash->success(__('The home type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The home type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('HomeType.' . $this->HomeType->primaryKey => $id));
			$this->request->data = $this->HomeType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->HomeType->id = $id;
		if (!$this->HomeType->exists()) {
			throw new NotFoundException(__('Invalid home type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->HomeType->delete()) {
			$this->Flash->success(__('The home type has been deleted.'));
		} else {
			$this->Flash->error(__('The home type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
