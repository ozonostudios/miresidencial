<?php
App::uses('AppController', 'Controller');
/**
 * Neighborhoods Controller
 *
 * @property Neighborhood $Neighborhood
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class NeighborhoodsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');
/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {

            $this->Neighborhood->create();

            if ($this->Neighborhood->save($this->request->data)) {

                $this->Flash->success(__('El Fraccionamiento ha sido guardado.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                
                $this->Flash->error(__('The neighborhood could not be saved. Please, try again.'));
            }
        }
    }
/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Tipo de Fraccionamiento');
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Neighborhood->id = $id;

        if (!$this->Neighborhood->exists()) {

            throw new NotFoundException(__('Fraccionamiento Inválido'));
        }
        $this->request->allowMethod('post', 'delete');

        if ($this->Neighborhood->delete()) {

            $this->Flash->success(__('El Fraccionamiento ha sido eliminado.'));
        } else {

            $this->Flash->error(__('El Fraccionamiento no pudo ser eliminado, por favor intente de nuevo.'));
        }

        return $this->redirect(array('action' => 'index'));
    }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {


        if(is_null($id)) {
            $id = $this->session_data['Neighborhood.id'];
        }        

        if (!$this->Neighborhood->exists($id)) {

            throw new NotFoundException(__('Fraccionamiento Inválido'));
        }

        if ($this->request->is(array('post', 'put'))) {
            /**
             * Save data
             */
            if ($this->Neighborhood->save($this->request->data)) {

                $this->Flash->success(__('El Fraccionamiento ha sido editado.'));
            } else {

                $this->Flash->error(__('El Fraccionamiento no pudo ser editado, por favor intente de nuevo.'));
            }
        } else {

            $options = array('conditions' => array('Neighborhood.' . $this->Neighborhood->primaryKey => $id));
            $this->request->data = $this->Neighborhood->find('first', $options);
        }

        $this->set('neighborhood_id', $id);
    }    
/**
 * getChartHomes method
 *
 * @return array
 */
    public function getNeighborhoodData($id) {

        if (!$this->Neighborhood->exists($id)) {

            throw new NotFoundException(__('Fraccionamiento Inválido'));
        }
        
        $options = array('conditions' => array('Neighborhood.' . $this->Neighborhood->primaryKey => $id));
        $this->autoRender = false;

        return json_encode($this->Neighborhood->find('first', $options));
    }
/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->Neighborhood->recursive = 0;
        $this->set('neighborhoods', $this->Paginator->paginate());
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->Neighborhood->exists($id)) {

            throw new NotFoundException(__('Fraccionamiento Inválido'));
        }

        $options = array('conditions' => array('Neighborhood.' . $this->Neighborhood->primaryKey => $id));
        $this->set('neighborhood', $this->Neighborhood->find('first', $options));
    }
}
