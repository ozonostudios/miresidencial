<?php
App::uses('AppController', 'Controller');
/**
 * Charges Controller
 *
 * @property Charge $Charge
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class BalanceController extends AppController {

/**
 * getChartBalance method
 *
 * @return array
 */
    public function getChartBalance() {

        $this->autoRender = false;
        $data = $this->Balance->balanceChartData($this->session_data['Neighborhood.id']);

        return json_encode($data);
    }

/**
 * getMonthBalance method
 *
 * @return array
 */
    public function getMonthBalance() {

        $this->autoRender = false;
        $data = $this->Balance->monthBalanceData($this->session_data['Neighborhood.id']);

        return json_encode($data);
    }
/**
 * getChartMonthBalance method
 *
 * @return array
 */
    public function getChartMonthBalance() {

        $this->autoRender = false;
        $data = $this->Balance->monthBalanceChartData($this->session_data['Neighborhood.id']);

        return json_encode($data);
    }
/**
 * getChartMonthBalance method
 *
 * @return array
 */
    public function getChargesPaymentsChartData() {

        $this->autoRender = false;
        $data = $this->Balance->chargesPaymentsChartData($this->session_data['Neighborhood.id']);

        return json_encode($data);
    }    
/**
 * getTotalBalance method
 *
 * @return array
 */
    public function getTotalBalance() {

        $this->autoRender = false;
        $data = $this->Balance->totalBalanceData($this->session_data['Neighborhood.id']);

        return json_encode($data);
    }    
}
