<?php
App::uses('AppController', 'Controller');
/**
 * Residents Controller
 *
 * @property Resident $Resident
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ResidentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');

/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {

            $this->Resident->create();

            if ($this->Resident->save($this->request->data)) {

                $this->Flash->success(__('The resident has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The resident could not be saved. Please, try again.'));
            }
        }

        $residentTypes = $this->Resident->ResidentType->find('list');
        $this->set(compact('residentTypes'));
    }

/**
 * [beforeRender callback executed before the
 * page its rendered]
 * @return [Void] [No value returned]
 */
    public function beforeRender() {

        $this->set('title_for_layout', 'Residentes');
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Resident->id = $id;

        if (!$this->Resident->exists()) {

            throw new NotFoundException(__('Invalid resident'));
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Resident->delete()) {

            $this->Flash->success(__('The resident has been deleted.'));
        } else {

            $this->Flash->error(__('The resident could not be deleted. Please, try again.'));
        }

        return $this->redirect(array('action' => 'index'));
    }
    
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->Resident->exists($id)) {

            throw new NotFoundException(__('Invalid resident'));
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Resident->save($this->request->data)) {

                $this->Flash->success(__('The resident has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The resident could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Resident.' . $this->Resident->primaryKey => $id));
            $this->request->data = $this->Resident->find('first', $options);
        }

        $residentTypes = $this->Resident->ResidentType->find('list');
        $this->set(compact('residentTypes'));
    }

/**
 * index method
 *
 * @return void
 */
    public function index() {

        $this->Resident->recursive = 2;
        $this->set('residents', $this->Paginator->paginate());
    }

/**
 * [search performs the async call on the model]
 * @param  [String] $value [name of resident]
 * @return [Array]        [list of residents that matches the query string]
 */
    public function search($value) {

        $data = $this->Resident->asyncSearch($value, $this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->Resident->exists($id)) {

            throw new NotFoundException(__('Invalid resident'));
        }

        $options = array('conditions' => array('Resident.' . $this->Resident->primaryKey => $id));
        $this->set('resident', $this->Resident->find('first', $options));
    }   
}
