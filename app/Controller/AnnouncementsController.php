<?php
App::uses('AppController', 'Controller');
/**
 * Announcements Controller
 *
 * @property Announcement $Announcement
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class AnnouncementsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Flash', 'Session');

/**
 * add method
 *
 * @return void
 */
    public function add() {

        $this->autoRender = false;

        if ($this->request->is('post')) {

            $this->Announcement->create();

            if ($this->Announcement->save($this->request->data)) {

                $this->Flash->success(__('The announcement has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The announcement could not be saved. Please, try again.'));
            }
        }
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {

        $this->Announcement->id = $id;

        if (!$this->Announcement->exists()) {

            throw new NotFoundException(__('Invalid announcement'));
        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Announcement->delete()) {

            $this->Flash->success(__('The announcement has been deleted.'));
        } else {

            $this->Flash->error(__('The announcement could not be deleted. Please, try again.'));
        }

        return $this->redirect(array('action' => 'index'));
    }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {

        if (!$this->Announcement->exists($id)) {

            throw new NotFoundException(__('Invalid announcement'));
        }

        if ($this->request->is(array('post', 'put'))) {

            if ($this->Announcement->save($this->request->data)) {

                $this->Flash->success(__('The announcement has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Flash->error(__('The announcement could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Announcement.' . $this->Announcement->primaryKey => $id));
            $this->request->data = $this->Announcement->find('first', $options);
        }

        $neighborhoods = $this->Announcement->Neighborhood->find('list');
        $this->set(compact('neighborhoods'));
    }
/**
 * [getAnnouncements returns a list of all announcements
 * from requested neighborhood]
 * @return [Array] [list of announcements]
 */
    public function getAllAnnouncements() {

        $data = $this->Announcement->getAllAnnouncements($this->session_data['Neighborhood.id']);
        $this->autoRender = false;

        return json_encode($data);
    }

/**
 * index method
 *
 * @return void
 */
    public function index() {

        $announcementTypes = $this->Announcement->AnnouncementType->find('list');
        $this->Announcement->recursive = 1;
        $neighborhoods_id = $this->session_data['Neighborhood.id'];

        $this->Paginator->settings = array(
            'order' => array(
                'Announcement.id' => 'DESC'
            ));
        
        $this->set('neighborhoods_id', $neighborhoods_id);
        $this->set(compact('announcementTypes'));
        $this->set('announcements', $this->Paginator->paginate());
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {

        if (!$this->Announcement->exists($id)) {

            throw new NotFoundException(__('Invalid announcement'));
        }

        $options = array('conditions' => array('Announcement.' . $this->Announcement->primaryKey => $id));
        $this->set('announcement', $this->Announcement->find('first', $options));
    }   
}
