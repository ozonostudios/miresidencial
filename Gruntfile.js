'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({

        jshint: {
            files: [
                'app/webroot/js/app/**/*.js',
                'app/webroot/js/components/**/*.js',
                'app/webroot/js/shared/**/*.js',
            ],
        },

        clean: {
            js: ["app/webroot/js/libs/*.js", ],
            css: ["app/webroot/css/**/*.css", ],
        },
        /**
         * [copy copies from bower component folder to
         * webroot]
         * @type {Object}
         */
        copy: {
            fontawesome: {
                files: [
                    {
                        cwd: 'bower_components/fontawesome/fonts',
                        expand: true,
                        flatten: false,
                        src: ['*.*'],
                        dest: 'app/webroot/css/fonts/',
                    },
                ],
            },
            bootswatch: {
                files: [
                    {
                        cwd: 'bower_components/bootswatch-dist/fonts',
                        expand: true,
                        flatten: false,
                        src: ['*.*'],
                        dest: 'app/webroot/css/fonts/',
                    },
                ],
            },        
        },
        /**
         * [less compile less files to CSS]
         * @type {Object}
         */
        less: {
            src: {
                files: {
                    'app/webroot/css/custom/general.css': 'app/webroot/less/general/*.less',
                    'app/webroot/css/custom/modules.css': 'app/webroot/less/modules/*.less',
                    'app/webroot/css/custom/views.css': 'app/webroot/less/views/*.less',
                    'app/webroot/css/custom/media-queries.css': 'app/webroot/less/media-queries/*.less',
                },
            },        
        },
        /**
         * [cssmin minifies css libraries]
         * @type {Object}
         */
        cssmin: {
            minify: {
                files:{
                    'app/webroot/css/libs/libraries.compiled.min.css': [
                        'app/webroot/css/libs/libraries.vendors.css',
                        'app/webroot/css/libs/libraries.custom.css',
                    ],
                },
            },        
        },
        /**
         * [concat concatenates multiple files into one]
         * @type {Object}
         */
        concat: {
            /**
             * [vendors_js third party libraries]
             * @type {Object}
             */
            vendors_js: {
                options: {
                    separator: ';',
                    stripBanners:  {
                        block: true,
                        line: true,
                    },
                },                
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/bootstrap/dist/js/bootstrap.js',
                    'bower_components/Chart.js/Chart.js',
                    'bower_components/angular/angular.js',
                    'bower_components/angular-bootstrap/ui-bootstrap.js',
                    'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'bower_components/angular-animate/angular-animate.js',
                    'bower_components/angular-chart.js/dist/angular-chart.js',
                    'bower_components/angular-cookies/angular-cookies.js',
                    'bower_components/angular-messages/angular-messages.js',
                    'bower_components/angular-moment/angular-moment.js',
                    'bower_components/angular-resource/angular-resource.js',
                    'bower_components/angular-sanitize/angular-sanitize.js',
                    'bower_components/angular-touch/angular-touch.js',
                    'bower_components/angular-parallax/scripts/angular-parallax.js',
                    'bower_components/ng-responsive-calendar/dist/js/calendar-tpls.js',
                    'bower_components/underscore/underscore.js',
                    'bower_components/ng-file-upload/ng-file-upload.js',
                    'bower_components/ng-file-upload/ng-file-upload-shim.js',
                    'bower_components/ng-img-crop/compile/unminified/ng-img-crop.js',
                ],
                dest: 'app/webroot/js/libs/libraries.vendors.js',
            },
            /**
             * [application_js angular application related files]
             * @type {Object}
             */
            application_js: {
                options: {
                    separator: ';',
                    stripBanners:  {
                        block: true,
                        line: true,
                    },
                },                
                src: [
                    'app/webroot/js/app/**/*.js',
                    'app/webroot/js/components/**/*.js',
                    'app/webroot/js/shared/**/*.js',
                ],
                dest: 'app/webroot/js/libs/libraries.application.js',
            },                     
            /**
             * [vendors_css third party style sheets]
             * @type {Object}
             */
            vendors_css: {
                src: [
                    'bower_components/bootswatch-dist/css/bootstrap.css',
                    'bower_components/angular-chart.js/dist/angular-chart.css',
                    'bower_components/fontawesome/css/font-awesome.css',
                    'bower_components/ng-responsive-calendar/dist/css/calendar.css',
                ],
                dest: 'app/webroot/css/libs/libraries.vendors.css',
            },
            /**
             * [custom_css custom style sheets]
             * @type {Object}
             */
            application_css: {
                src: [
                    'app/webroot/css/custom/general.css',
                    'app/webroot/css/custom/modules.css',
                    'app/webroot/css/custom/views.css',
                    'app/webroot/css/custom/media-queries.css',
                ],
                dest: 'app/webroot/css/libs/libraries.custom.css',
            },         
        },
        /**
         * [uglify uglifies concatenated JS files]
         * @type {Object}
         */
        uglify: {
            options: {
                mangle: false,
            },
            application: {
                files: {'app/webroot/js/libs/libraries.application.min.js': ['app/webroot/js/libs/libraries.application.js',],},
            },
            vendors: {
                files: {'app/webroot/js/libs/libraries.vendors.min.js': ['app/webroot/js/libs/libraries.vendors.js',],},
            },
        },
    });

    grunt.registerTask('js', [
        'jshint',
        'clean:js', 
        'concat:application_js', 
        'concat:vendors_js',
        'uglify',
    ]);

    grunt.registerTask('css', [
        'clean:css',
        'copy:fontawesome',
        'copy:bootswatch',
        'less',
        'concat:vendors_css',
        'concat:application_css',
        'cssmin',
    ]);

    grunt.registerTask('default', [
        'css',
        'js',
    ]);
};